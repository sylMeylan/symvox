#include <QApplication>
#include <QResource>

#include "SymVox/Run/Management/include/RunManager.hh"
#include "SymVox/App/Tests/Basic/include/TestApp.hh"
#include "SymVox/App/Tests/CustomRenderer/include/BasicRendererApp.hh"
#include "SymVox/App/Tests/MemPool/include/MemPoolApp.hh"
#include "SymVox/App/Tests/Thread/include/ThreadApp.hh"
#include "SymVox/App/Tests/Billboard/include/BillboardApp.hh"
#include "SymVox/App/Tests/ParticleEffect/include/ParticleEffectApp.hh"

/** @file */

/**
 * @brief Main function of the SymVox toolkit
 * @param argc
 * @param argv
 * @return Exit status
 *
 * This is the main function of Symvox.
 * It is used only when SymVox is compiled and executed as an application and not as a library.
 * In the case of a "library style" use, the content of this function can usually be found in the main function of
 * the application built against the symVox library.
 * The different parts of the SymVox library are created and initialized here.
 * There should be:
 *  - An AppManager with the name of the application.
 *  - A RunManager that should be given a pointer to the AppManager and then be initialized and started.
 */
int main(int argc, char** argv)
{
    using namespace SV;

    // To use qt we need to create a qt app
    QApplication app(argc, argv);

    //QResource::registerResource("resource.rcc");

    // App manager
    //TestApp appManager;
    //BasicRendererApp appManager;
    //MemPoolApp appManager;
    //ThreadApp appManager;
    //BillboardApp appManager;
    ParticleEffectApp appManager;

    // Create the run manager
    // This is where the main loop is defined
    Run::RunManager run;

    // Attach the application
    run.SetAppManager(&appManager);

    // Initialize
    // This will create a Qt MainWindow.
    // If the program is console-only you should remove this call.
    run.Initialize();

    // Start
    run.StartMainLoop();

    // End of the program
    return 0;
}
