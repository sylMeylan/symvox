# To define :
#  LIBSYMVOX_FOUND - System has SymVox
#  LIBSYMVOX_INCLUDE_DIRS - The SymVox include directories
#  LIBSYMVOX_LIBRARIES - The libraries needed to use SymVox

# config_prefix
set(config_prefix "${CMAKE_CURRENT_LIST_DIR}/../../..")

set(LIBSYMVOX_LIBRARY "${config_prefix}/lib/libSymVox.so.1")
set(LIBSYMVOX_INCLUDE_DIR "${config_prefix}/include")

set(LIBSYMVOX_LIBRARIES ${LIBSYMVOX_LIBRARY} )
set(LIBSYMVOX_INCLUDE_DIRS "${LIBSYMVOX_INCLUDE_DIR}/SymVox-1.0.0" )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set SYMVOX_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LibSymVox  DEFAULT_MSG
                                  LIBSYMVOX_LIBRARY LIBSYMVOX_INCLUDE_DIR)

mark_as_advanced(LIBSYMVOX_INCLUDE_DIR LIBSYMVOX_LIBRARY )
