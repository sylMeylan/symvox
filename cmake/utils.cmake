#----------------------------------------------------------------------------
# Locate sources and headers for this project
#

# Macro to add sources
#
macro (add_sources)
    file(RELATIVE_PATH _relPath "${CMAKE_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")
    foreach(_src ${ARGN})
        if(_relPath)
            list (APPEND SRCS "${_relPath}/src/${_src}")
        else()
            list (APPEND SRCS "src/${_src}")
        endif()
    endforeach()
    if (_relPath)
        # propagate SRCS to parent directory
        set(sources ${sources} ${SRCS} PARENT_SCOPE)
    else()
        set(sources ${sources} ${SRCS})
    endif()
endmacro()

# Macro to add headers
#
macro (add_headers)

    file(RELATIVE_PATH _relPath "${CMAKE_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")

    # variable used to detect if the next header is a QOBJECT
    # that need to be processed by the Qt meta-object compiler
    set(isQObj OFF)

    # Loop on all the headers
    foreach(_hdrs ${ARGN})

        # If a header is QOBBJECT then save the next one twice: in HDRS and in HDRS_QOBJ
        # The content of HDRS_QOBJ will be processed by Qt meta-object compiler
        if(_hdrs MATCHES "QOBJECT")
            set(isQObj ON)
        else()

            if(_relPath)
                list(APPEND HDRS "${_relPath}/include/${_hdrs}")

                if(isQObj)
                    list(APPEND HDRS_QOBJ "${_relPath}/include/${_hdrs}")
                    set(isQObj OFF)
                endif()

            else()

                list(APPEND HDRS "include/${_hdrs}")
                if(isQObj)
                    list(APPEND HDRS_QOBJ "include/${_hdrs}")
                    set(isQObj OFF)
                endif()

            endif()

        endif()

    endforeach()

    if (_relPath)

        # propagate headers to parent directory
        set(headers ${headers} ${HDRS} PARENT_SCOPE)

        # propagate headers_QOBJ to parent directory
        set(headers_QOBJ ${headers_QOBJ} ${HDRS_QOBJ} PARENT_SCOPE)

    else()

        # propagate headers to parent directory
        set(headers ${headers} ${HDRS})

        # propagate headers_QOBJ to parent directory
        set(headers_QOBJ ${headers_QOBJ} ${HDRS_QOBJ})

    endif()

endmacro()

# Macro to add UI files
#
macro (add_uis)
    file(RELATIVE_PATH _relPath "${CMAKE_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")
    foreach(_ui ${ARGN})
        if(_relPath)
            list (APPEND UI "${_relPath}/ui/${_ui}")
        else()
            list (APPEND UI "ui/${_ui}")
        endif()
    endforeach()
    if (_relPath)
        # propagate UI to parent directory
        set(uis ${uis} ${UI} PARENT_SCOPE)
    else()
        set(uis ${uis} ${UI})
    endif()
endmacro()

# Macro to add qml files
#
macro (add_qml)
    file(RELATIVE_PATH _relPath "${CMAKE_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")
    foreach(_qml ${ARGN})
        if(_relPath)
            list (APPEND QML "${_relPath}/${_qml}")
        else()
            list (APPEND QML "${_qml}")
        endif()
    endforeach()
    if (_relPath)
        # propagate UI to parent directory
        set(qml ${qml} ${QML} PARENT_SCOPE)
    else()
        set(qml ${qml} ${QML})
    endif()
endmacro()

# Macro to add resource files
#
macro (add_res)
    file(RELATIVE_PATH _relPath "${CMAKE_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")
    foreach(_res ${ARGN})
        if(_relPath)
            list (APPEND RES "${_relPath}/${_res}")
        else()
            list (APPEND RES "${_res}")
        endif()
    endforeach()
    if (_relPath)
        # propagate UI to parent directory
        set(res ${res} ${RES} PARENT_SCOPE)
    else()
        set(res ${res} ${RES})
    endif()
endmacro()

# Macro to add a file to the list of "file to be copied"
#
macro (add_copy)
    file(RELATIVE_PATH _relPath "${CMAKE_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}")
    foreach(_copy ${ARGN})
        if(_relPath)
            list (APPEND COPY "${_relPath}/${_copy}")
        else()
            list (APPEND COPY "${_copy}")
        endif()
    endforeach()
    if (_relPath)
        # propagate UI to parent directory
        set(copy ${copy} ${COPY} PARENT_SCOPE)
    else()
        set(copy ${copy} ${COPY})
    endif()
endmacro()

# Function to copy the files from the source dir to the build dir
function (CopyFiles files)
    # ${files} is the name of the list
    # ${{files}} is the list itself
    # Copy the files
    foreach(_script ${${files}})
      configure_file(
        ${PROJECT_SOURCE_DIR}/${_script}
        ${PROJECT_BINARY_DIR}/${_script}
        COPYONLY
        )
    endforeach()
endfunction()

# Check if c++11 can be enabled and do it (if possible)
#
macro (TestIfCpp11)
    include(CheckCXXCompilerFlag)
    CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
    if(COMPILER_SUPPORTS_CXX11)
            set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
    else()
            message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
    endif()
endmacro()

function (writeQrcFile name ${res})

    file(WRITE ${name}.qrc
"<!DOCTYPE RCC>
<RCC>
    <qresource prefix=\"/\">\n"
        )

    foreach(_res ${res})
        file(APPEND ${name}.qrc
"        <file>${_res}</file>\n"
            )
    endforeach()

    file(APPEND ${name}.qrc
"    </qresource>
</RCC>"
        )

endfunction()

# Generate a binary resource.rcc file from the resource.qrc
#
function (GenerateResourceBinary name)
add_custom_target(
    ProcessResources
    COMMAND rcc -binary ${PROJECT_SOURCE_DIR}/${name}.qrc -o ${name}.rcc
)
add_dependencies(${PROJECT_NAME} ProcessResources)
endfunction()

# Add the resource files labelled "outfiles" in a cpp file that can be compiled and added to an executable
#
function(CustomAddResources outfiles )

    set(options)
    set(oneValueArgs)
    set(multiValueArgs OPTIONS)

    cmake_parse_arguments(_RCC "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    set(rcc_files ${_RCC_UNPARSED_ARGUMENTS})
    set(rcc_options ${_RCC_OPTIONS})

    foreach(it ${rcc_files})
        get_filename_component(outfilename ${it} NAME_WE)
        get_filename_component(infile ${it} ABSOLUTE)
        get_filename_component(rc_path ${infile} PATH)
        set(outfile ${CMAKE_CURRENT_BINARY_DIR}/qrc_${outfilename}.cpp)

        set(_RC_DEPENDS)
        if(EXISTS "${infile}")
            #  parse file for dependencies
            #  all files are absolute paths or relative to the location of the qrc file
            file(READ "${infile}" _RC_FILE_CONTENTS)
            string(REGEX MATCHALL "<file[^<]+" _RC_FILES "${_RC_FILE_CONTENTS}")
            foreach(_RC_FILE ${_RC_FILES})
                string(REGEX REPLACE "^<file[^>]*>" "" _RC_FILE "${_RC_FILE}")
                if(NOT IS_ABSOLUTE "${_RC_FILE}")
                    set(_RC_FILE "${rc_path}/${_RC_FILE}")
                endif()
                set(_RC_DEPENDS ${_RC_DEPENDS} "${_RC_FILE}")
            endforeach()
            # Since this cmake macro is doing the dependency scanning for these files,
            # let's make a configured file and add it as a dependency so cmake is run
            # again when dependencies need to be recomputed.
            qt5_make_output_file("${infile}" "" "qrc.depends" out_depends)
            configure_file("${infile}" "${out_depends}" COPYONLY)
        else()
            # The .qrc file does not exist (yet). Let's add a dependency and hope
            # that it will be generated later
            set(out_depends)
        endif()

        add_custom_command(OUTPUT ${outfile}
                           COMMAND ${Qt5Core_RCC_EXECUTABLE}
                           ARGS ${rcc_options} -name ${outfilename} -o ${outfile} ${infile}
                           MAIN_DEPENDENCY ${infile}
                           DEPENDS ${_RC_DEPENDS} "${out_depends}" VERBATIM)
        list(APPEND ${outfiles} ${outfile})
    endforeach()
    set(${outfiles} ${${outfiles}} PARENT_SCOPE)
endfunction()

