SymVox Presentation {#mainpage}
=========

# Introduction {#section0}

SymVox is a library dedicated to the creation, modification and visualization of complex simulations. 

It is built with Qt, Assimp and z-lib. 

New user wishing to look at the code should start with the main.cc. 

# Setup {#section1}

## SymVox compilation and setup {#section1-0}

Create a folder to store the source code of the project:
```sh
mkdir symvox
```
Paste the code in the folder. 

Create a folder named symvox-build-res to compile the project in "RELEASE" mod. 

```sh
mkdir symvox-build-rel
cd symvox-build-rel
```

Configure the projet with CMAKE:

```sh
cmake -DSYMVOX_USE_AS_LIB=ON ../symvox # Relative path to the source code folder
```

Then, start the compile process:

```sh
make
```

And trigger the installation of the library

```sh
sudo make install
```

This program can be run with OpenGL 3 using following command: 
```sh
MESA_GL_VERSION_OVERRIDE=4.5 MESA_GLSL_VERSION_OVERRIDE=450 <program-name> 
# (see: https://askubuntu.com/questions/882628/updating-opengl-support-ubuntu-16-04-lts-mesa-12-0-6) 
```

