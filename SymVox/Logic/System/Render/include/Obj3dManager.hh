/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef LOGIC_SYSTEM_OBJ3DMANAGER_HH
#define LOGIC_SYSTEM_OBJ3DMANAGER_HH

#include "SymVox/Utils/include/StrToIdContainer.hh"
#include "SymVox/3D/Object/Standard/include/Object.hh"

namespace SV { namespace Logic {

/*!
 * \brief The Obj3dManager class
 * This class takes care of the obj3d classes registered into it. They are registered by the render system and, sometimes, retrieved by it.
 */
class Obj3dManager
{
public:
    Obj3dManager();
    ~Obj3dManager();

    int TryToAddObject(const std::string& name, SV::TriD::VObject* obj);

    void SetInstanceBufferFlag(int index, bool b);
    void SetAllInstanceBufferFlags(bool b);

    int GetIntegerId(const std::string& strId);

    void LoadAllMeshes();
    void ClearAllMeshes();
    void RemoveAllObj();

    int GetNumOfObj();

    SV::TriD::VObject *GetObj(int id);
    SV::TriD::VObject *GetObj(const std::string& id);

    bool IsObjInside(int id);
    bool IsObjInside(const std::string& id);

    void RemoveObj(int id);

    Utils::StrToIdContainer<SV::TriD::VObject*>& GetObjContainer() {return fObjsToDisplay;}

    std::string DumpContent();

private:
    Utils::StrToIdContainer<SV::TriD::VObject*> fObjsToDisplay;
};

}}

#endif // LOGIC_SYSTEM_OBJ3DMANAGER_HH
