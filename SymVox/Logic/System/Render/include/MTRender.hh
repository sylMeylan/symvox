/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef LOGIC_SYSTEM_MTRENDER_HH
#define LOGIC_SYSTEM_MTRENDER_HH

#include <QMutex>
#include <QMutexLocker>
#include <QQueue>
#include <QTime>
#include "SymVox/Logic/System/Management/include/VSystem.hh"
#include "SymVox/Run/Thread/ThreadPool/include/ThreadPoolManager.hh"
#include "Obj3dManager.hh"
#include "SymVox/3D/Object/Standard/include/AssetsManager.hh"
#include "SymVox/Run/Scene/Graph/include/VSceneNode.hh"
#include "SymVox/Logic/Component/Renderable/include/Renderable.hh"
#include "SymVox/GUI/Camera/include/Camera.hh"
#include "SymVox/GUI/Windows/include/ViewOpenGL.hh"
#include "SymVox/Utils/include/Utils.hh"
#include "SymVox/3D/Memory/include/CPUBuffer.hh"
#include "SymVox/3D/Object/Skybox/include/SkyBox.hh"
#include "TaskRecursiveWorldAnalyseToFillBuffers.hh"
#include "TaskWorldAnalyseToFillBuffers.hh"
#include "SymVox/Run/Memory/Pool/include/StandardMemoryPool.hh"

namespace SV { namespace Logic {

/*!
 * \brief The Render class
 * This is the system in charge of the 3D render process
 */
class MTRender : public VSystem
{
public:
    MTRender();

    virtual ~MTRender();

    virtual void Initialize() override;

    virtual void Process() override;

    void RenderWorld(QMatrix4x4& projection, QMatrix4x4& view, QVector3D& lightPos, float lightPower);

    // World
    void SetWorld(Run::VSceneNode* world) {fpWorld=world;}
    Run::VSceneNode* GetWorld() {return fpWorld;}
    const Run::VSceneNode* GetWorld() const {return fpWorld;}

    // Camera
    void SetCamera(GUI::Camera* camera) {fpCamera=camera;}
    GUI::Camera* GetCamera() {return fpCamera;}
    const GUI::Camera* GetCamera() const {return fpCamera;}

    // View OpenGl
    void SetView(GUI::ViewOpenGL* view) {fpViewOpenGL=view;}
    GUI::ViewOpenGL* GetView() {return fpViewOpenGL;}
    const GUI::ViewOpenGL* GetView() const {return fpViewOpenGL;}

    // LightPos
    void SetLightPos(const QVector3D& pos) {fLightPos=pos;}
    const QVector3D& GetLightPos() const {return fLightPos;}

    // Light power
    void SetLightPower(float p) {fLightPower=p;}
    float GetLightPower() const {return fLightPower;}

    // LightManager
    SV::TriD::LightManager* GetLightManager() {return &fLightManager;}

    // SkyBox
    void SetSkyBox(SV::TriD::SkyBox* sb) {fpSkyBox = sb;}
    SV::TriD::SkyBox* GetSkyBox() {return fpSkyBox;}

protected:
    Run::VSceneNode* fpWorld;
    GUI::Camera* fpCamera;
    GUI::ViewOpenGL* fpViewOpenGL;
    Run::ThreadPool* fpThreadPool;
    SV::TriD::SkyBox* fpSkyBox;
    Run::MemoryPool* fpMemoryPool;

    QMutex fGlobalWriteBufferMutex;
    QMutex fGlobalQueueBufferMutex;
    Obj3dManager fObj3dManager;
    SV::TriD::AssetsManager fAssetsManager;
    std::vector<Run::VRunnable*> fTasks;
    QQueue<DataForTask> fDataToProcess;
    std::map<int, SV::TriD::CPUBuffer<float> > fInstanceBuffers;

    SV::TriD::LightManager fLightManager;
    QVector3D fLightPos;
    float fLightPower;

    void TransferDataFromLodToObj3d(Logic::LodModel& lod, SV::TriD::VObject &obj3d);

    void RecursiveWorldAnalyseToCreateMeshObj(Run::VSceneNode* currentObj,
                                              std::map<std::string, bool>& isProcessed,
                                              QMatrix4x4 modelPos = QMatrix4x4() );

    void CreateObj3dAndMesh(Run::VSceneNode* currentObj, Logic::LodModel& lod, const std::string& type3d);

    void RecursiveWorldAnalyseToCreateMultiMeshObj(Run::VSceneNode* currentObj, QMatrix4x4 modelPos = QMatrix4x4() );

    void RecursiveObjAnalyseToCreateMultiMesh(Run::VSceneNode* currentObj,
                                              SV::TriD::VObject *multiMeshObj,
                                              int forcedLodIndex=-1,
                                              QMatrix4x4 modelPos = QMatrix4x4() ,
                                              bool rootMultiMeshObj=false);

    void RecursiveWorldAnalyseToFillBuffers(Run::VSceneNode* currentObj,
                                            GUI::Camera* camera,
                                            int forcedLodIndex = -1,
                                            QMatrix4x4 modelMat = QMatrix4x4() );


};

}}

#endif // LOGIC_SYSTEM_RENDER_HH
