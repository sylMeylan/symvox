/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TASKWORLDANALYSETOFILLBUFFERS_HH
#define TASKWORLDANALYSETOFILLBUFFERS_HH

#include <QMutex>
#include <QMutexLocker>
#include <QReadWriteLock>

#include "SymVox/Run/Scene/Graph/include/VSceneNode.hh"
#include "SymVox/Logic/Component/Renderable/include/Renderable.hh"
#include "SymVox/GUI/Camera/include/Camera.hh"
#include "SymVox/Run/Thread/ThreadPool/include/ThreadPool.hh"
#include "Obj3dManager.hh"
#include "SymVox/3D/Memory/include/CPUBuffer.hh"
#include "SymVox/Utils/include/Utils.hh"
#include "SymVox/Run/Memory/Pool/include/StandardMemoryPool.hh"
#include "SymVox/Run/Memory/Management/include/Allocation.hh"
#include "SymVox/Run/Thread/Runnable/include/VRunnable.hh"
#include <QQueue>
namespace SV { namespace Logic {

struct DataForTask {
    DataForTask()
    {

    }

    ~DataForTask()
    {

    }

    Run::VSceneNode* currentObj = nullptr;
    GUI::Camera* camera = nullptr;
    Run::ThreadPool* threadPool = nullptr;
    Logic::Obj3dManager* obj3dManager = nullptr;
    std::map<int, SV::TriD::CPUBuffer<float> >* instanceBuffers = nullptr;
    Run::MemoryPool* memoryPool = nullptr;
    QQueue<DataForTask>* fpDataToProcess = nullptr;
    QMutex* globalMutex = nullptr;
    QMutex* globalQueueBufferMutex = nullptr;
    int forcedLodIndex = -1;
    QMatrix4x4 modelMat = QMatrix4x4();
};

class TaskWorldAnalyseToFillBuffers : public Run::VRunnable
{
public:
    TaskWorldAnalyseToFillBuffers();
    ~TaskWorldAnalyseToFillBuffers();

    void SetParameters(const DataForTask& data);

    void Run();

    bool GetIsAvailable();

private:
    DataForTask fData;

    bool fFinished;
    bool fFinishedCopy;
    QReadWriteLock fDataLock;
    QReadWriteLock fReadWriteLock;

    bool IsInFrontOfCamera(const QVector3D& objPos, float sizeIndicator, GUI::Camera* camera);

    void InitialiseCPUInstanceBuffer(SV::TriD::CPUBuffer<float>& bufferCPU);

    void PutDataInInstanceBuffer(SV::TriD::CPUBuffer<float>& bufferCPU,
                                 const QMatrix4x4& mat,
                                 Run::VSceneNode* obj,
                                 LodModel& lod);

};

}}

#endif // TASKWORLDANALYSETOFILLBUFFERS_HH
