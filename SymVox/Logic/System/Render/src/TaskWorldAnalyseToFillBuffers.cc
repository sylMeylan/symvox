/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/TaskWorldAnalyseToFillBuffers.hh"

namespace SV { namespace Logic {

TaskWorldAnalyseToFillBuffers::TaskWorldAnalyseToFillBuffers()
    : Run::VRunnable()
{
    fData = DataForTask();

    fFinished = true;
}

TaskWorldAnalyseToFillBuffers::~TaskWorldAnalyseToFillBuffers()
{

}

void TaskWorldAnalyseToFillBuffers::SetParameters(const DataForTask& data)
{
    fDataLock.lockForWrite();
    fData = data;
    fDataLock.unlock();

    fReadWriteLock.lockForWrite();
    fFinished = false;
    fReadWriteLock.unlock();
}

void TaskWorldAnalyseToFillBuffers::Run()
{
    // - Get the current object 3dType
    // - Get the Lod object corresponding to the 3dType
    // - Check if a forced lod level is set
    // - Retrieve lod values (3d type to be displayed...)
    // - Fill the matrix buffers
    // - Do it again for all the childs

    fReadWriteLock.lockForWrite();
    fFinished = false;
    fReadWriteLock.unlock();

    fDataLock.lockForRead();

    bool isChildDisplay (true);

    int forcedChildIndex = fData.forcedLodIndex;

    // Check we do have a renderable component for the current obj
    if(fData.currentObj->HasComponent(List::Renderable) )
    {
        // Get the renderable component
        Renderable* renderable = static_cast<Renderable*> (fData.currentObj->GetComponent(List::Renderable) );

        int  numOfLods = renderable->GetNumOfLods();

        // Check if we do have a lod for the current object
        if(numOfLods > 0)
        {
            // ************************************
            // Check if a forced lod level is set
            // ************************************

            int lodIndex = -1;

            // Default force lod value is -1.
            // It means no force lod is set.
            if(fData.forcedLodIndex == -1)
            {
                float distance = (Utils::ExtractPosFromMatrix(fData.modelMat*fData.currentObj->GetModelMat() ) - fData.camera->GetPosition() ).length();

                lodIndex =  renderable->GetLodManager().GetIndexToDisplay(distance);
            }

            // A forced lod index is set
            else
            {
                if(fData.forcedLodIndex < renderable->GetNumOfLods() )
                    lodIndex = fData.forcedLodIndex;
                else
                    lodIndex = renderable->GetNumOfLods()-1;
            }

            LodModel& lod = renderable->GetLod(lodIndex);

            // ************************************
            // Check the object position and the one of the camera
            // ************************************

            // Get the position matrix
            QMatrix4x4 model = fData.modelMat * fData.currentObj->GetModelMat();

            bool isDisplayable = IsInFrontOfCamera(Utils::ExtractPosFromMatrix(model), lod.GetSizeIndicator(), fData.camera);

            // ************************************
            // Fill the buffers
            // ************************************

            isChildDisplay = lod.IsShowChildren();

            if(isDisplayable)
            {
                if(lod.GetPrimitive() != nullptr || lod.IsMultiMesh() )
                {
                    // Look for the id of the lod and if is -1 (defaut value) then initialise it
                    int id = lod.Get3dTypeId();

                    if(id == -1)
                    {
                        if(!fData.obj3dManager->IsObjInside(lod.Get3dType() ) )
                        {
                            std::stringstream ss;
                            ss << "The lod model "<<lod.Get3dType()<<" was not loaded as a 3d object and, thus, was not registered in the 3d object manager\n";
                            ss << "The object is named "<<fData.currentObj->GetName();
                            Utils::Exception(Utils::ExceptionType::Fatal, "Render::RecursiveWorldAnalyseToFillBuffers", ss.str() );
                        }

                        id = fData.obj3dManager->GetIntegerId(lod.Get3dType() );
                        lod.Set3dTypeId(id);
                    }

                    // Create the corresponding instance buffer (or retrieve it, if it exists)
                    SV::TriD::CPUBuffer<float>& bufferCPU = (*(fData.instanceBuffers))[id];

                    fData.globalMutex->lock();

                    // Initialise the buffer if needed
                    if(!bufferCPU.GetIsInitialised() )
                        InitialiseCPUInstanceBuffer(bufferCPU);

                    // Add the data to the buffer
                    PutDataInInstanceBuffer(bufferCPU, model, fData.currentObj, lod);

                    fData.globalMutex->unlock();
                }
            }
        }
    }

    // ********************************************
    // Process the childs (recursion happens here)
    // ********************************************

    // Do we want to process the child ?
    if(isChildDisplay)
    {
        // Process normal childs

        int childNum = fData.currentObj->GetNumOfChildren();

        for(int i=0; i<childNum; ++i)
        {
            Run::VSceneNode* child = fData.currentObj->GetChild(i);

            DataForTask data;
            data.camera = fData.camera;
            data.currentObj = child;
            data.forcedLodIndex = forcedChildIndex;
            data.fpDataToProcess = fData.fpDataToProcess;
            data.instanceBuffers = fData.instanceBuffers;
            data.memoryPool = fData.memoryPool;
            data.modelMat = fData.modelMat*fData.currentObj->GetModelMat();
            data.globalMutex = fData.globalMutex;
            data.globalQueueBufferMutex = fData.globalQueueBufferMutex;
            data.obj3dManager = fData.obj3dManager;
            data.threadPool = fData.threadPool;

            fData.globalQueueBufferMutex->lock();

            // Submit a new data set for processing
            fData.fpDataToProcess->enqueue(data);

            fData.globalQueueBufferMutex->unlock();
        }
    }

    fDataLock.unlock();

    fReadWriteLock.lockForWrite();
    fFinished = true;
    fReadWriteLock.unlock();
}

bool TaskWorldAnalyseToFillBuffers::GetIsAvailable()
{
    fReadWriteLock.lockForRead();
    fFinishedCopy = fFinished;
    fReadWriteLock.unlock();

    return fFinishedCopy;
}

bool TaskWorldAnalyseToFillBuffers::IsInFrontOfCamera(const QVector3D& objPos, float sizeIndicator, GUI::Camera* camera)
{
    QVector3D posObjToCamera = objPos - camera->GetPosition();

    // Projection of the object pos vector in the camera frame of coord on the
    // camera orientation vector.
    float projection = QVector3D::dotProduct(posObjToCamera, camera->GetOrientation() );

    return (projection > -sizeIndicator/2);
}

void TaskWorldAnalyseToFillBuffers::InitialiseCPUInstanceBuffer(SV::TriD::CPUBuffer<float>& bufferCPU)
{
    // Model matrix data: 16 floats per matrix
    bufferCPU.AddData("ModelMatrix", 16);

    // Color 4 floats per matrix
    bufferCPU.AddData("Color", 4);

    // Size 1 float per size value
    bufferCPU.AddData("Size", 1);

    // Set the initialisation flag to true
    bufferCPU.SetIsInitialised(true);
}

void TaskWorldAnalyseToFillBuffers::PutDataInInstanceBuffer(SV::TriD::CPUBuffer<float>& bufferCPU, const QMatrix4x4& mat, Run::VSceneNode* obj, LodModel& lod)
{
    // *****************************************
    // Put the model matrix in a float vector
    // *****************************************

    // Pointer a the start of the QMatrix4x4 data
    const float* dataPointer = mat.constData();

    // Model matrix data
    SV::TriD::Data<float>& matData = bufferCPU.GetData(0);

    // Loop on all the data within QMatrix4x4
    for(int j=0; j<16; j++)
    {
        matData.AddValue(*(dataPointer+j) );
    }

    if(lod.IsColorInstancing() )
    {
//        // *****************************************
//        // Color data
//        // *****************************************

//        // The color retrieval is strange and should be improved

//        // Color of the current instance
//        const QColor& color = obj->GetColor();

//        // Get the alpha value from the model
//        float alphaVal = lod.GetColor().alpha();

//        // Color data
//        SV::TriD::Data<float>& colorData = bufferCPU.GetData(1);

//        colorData.AddValue(float(color.redF() ) );
//        colorData.AddValue(float(color.greenF() ) );
//        colorData.AddValue(float(color.blueF() ) );
//        colorData.AddValue(alphaVal);

//        // *****************************************
//        // Size data
//        // *****************************************

//        // Size data
//        SV::TriD::Data<float>& sizeData = bufferCPU.GetData(2);

//        // Always one here
//        sizeData.AddValue(1);
    }
}

}}

