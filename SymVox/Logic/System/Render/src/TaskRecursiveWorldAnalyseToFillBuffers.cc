/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/TaskRecursiveWorldAnalyseToFillBuffers.hh"

namespace SV { namespace Logic {

TaskRecursiveWorldAnalyseToFillBuffers::TaskRecursiveWorldAnalyseToFillBuffers(Run::VSceneNode* currentObj,
                                                                               GUI::Camera* camera,
                                                                               Run::ThreadPool* threadPool,
                                                                               Logic::Obj3dManager* obj3dManager,
                                                                               std::map<int, SV::TriD::CPUBuffer<float> >* instanceBuffers,
                                                                               Run::MemoryPool* memoryPool,
                                                                               QMutex* mutex,
                                                                               int forcedLodIndex,
                                                                               QMatrix4x4 modelMat )
    : Run::VRunnable()
{
    fpCurrentObj = currentObj;
    fpCamera = camera;
    fpThreadPool = threadPool;
    fpObj3dManager = obj3dManager;
    fpInstanceBuffers = instanceBuffers;
    fpMutex = mutex;
    fForcedLodIndex = forcedLodIndex;
    fModelMat = modelMat;
    fpMemoryPool = memoryPool;

    SetAutoDelete(true);
}

TaskRecursiveWorldAnalyseToFillBuffers::~TaskRecursiveWorldAnalyseToFillBuffers()
{

}

void TaskRecursiveWorldAnalyseToFillBuffers::Run()
{
    // - Get the current object 3dType
    // - Get the Lod object corresponding to the 3dType
    // - Check if a forced lod level is set
    // - Retrieve lod values (3d type to be displayed...)
    // - Fill the matrix buffers
    // - Do it again for all the childs

    bool isChildDisplay (true);

    int forcedChildIndex = fForcedLodIndex;

    // Check we do have a renderable component for the current obj
    if(fpCurrentObj->HasComponent(List::Renderable) )
    {
        // Get the renderable component
        Renderable* renderable = static_cast<Renderable*> (fpCurrentObj->GetComponent(List::Renderable) );

        int  numOfLods = renderable->GetNumOfLods();

        // Check if we do have a lod for the current object
        if(numOfLods > 0)
        {
            // ************************************
            // Check if a forced lod level is set
            // ************************************

            int lodIndex = -1;

            // Default force lod value is -1.
            // It means no force lod is set.
            if(fForcedLodIndex == -1)
            {
                float distance = (Utils::ExtractPosFromMatrix(fModelMat*fpCurrentObj->GetModelMat() ) - fpCamera->GetPosition() ).length();

                lodIndex =  renderable->GetLodManager().GetIndexToDisplay(distance);
            }

            // A forced lod index is set
            else
            {
                if(fForcedLodIndex < renderable->GetNumOfLods() )
                    lodIndex = fForcedLodIndex;
                else
                    lodIndex = renderable->GetNumOfLods()-1;
            }

            LodModel& lod = renderable->GetLod(lodIndex);

            // ************************************
            // Check the object position and the one of the camera
            // ************************************

            // Get the position matrix
            QMatrix4x4 model = fModelMat * fpCurrentObj->GetModelMat();

            bool isDisplayable = IsInFrontOfCamera(Utils::ExtractPosFromMatrix(model), lod.GetSizeIndicator(), fpCamera);

            // ************************************
            // Fill the buffers
            // ************************************

            isChildDisplay = lod.IsShowChildren();

            if(isDisplayable)
            {
                if(lod.GetPrimitive() != nullptr || lod.IsMultiMesh() )
                {
                    // Look for the id of the lod and if is -1 (defaut value) then initialise it
                    int id = lod.Get3dTypeId();

                    if(id == -1)
                    {
                        if(!fpObj3dManager->IsObjInside(lod.Get3dType() ) )
                        {
                            std::stringstream ss;
                            ss << "The lod model "<<lod.Get3dType()<<" was not loaded as a 3d object and, thus, was not registered in the 3d object manager\n";
                            ss << "The object is named "<<fpCurrentObj->GetName();
                            Utils::Exception(Utils::ExceptionType::Fatal, "Render::RecursiveWorldAnalyseToFillBuffers", ss.str() );
                        }

                        id = fpObj3dManager->GetIntegerId(lod.Get3dType() );
                        lod.Set3dTypeId(id);
                    }

                    // Create the corresponding instance buffer (or retrieve it, if it exists)
                    SV::TriD::CPUBuffer<float>& bufferCPU = (*fpInstanceBuffers)[id];

                    fpMutex->lock();

                    // Initialise the buffer if needed
                    if(!bufferCPU.GetIsInitialised() )
                        InitialiseCPUInstanceBuffer(bufferCPU);

                    // Add the data to the buffer
                    PutDataInInstanceBuffer(bufferCPU, model, fpCurrentObj, lod);

                    fpMutex->unlock();
                }
            }
        }
    }

    // ********************************************
    // Process the childs (recursion happens here)
    // ********************************************

    // Do we want to process the child ?
    if(isChildDisplay)
    {
        // Process normal childs

        int childNum = fpCurrentObj->GetNumOfChildren();

        for(int i=0; i<childNum; ++i)
        {
            Run::VSceneNode* child = fpCurrentObj->GetChild(i);

            TaskRecursiveWorldAnalyseToFillBuffers* task =
                    new TaskRecursiveWorldAnalyseToFillBuffers(
                        child,
                        fpCamera,
                        fpThreadPool,
                        fpObj3dManager,
                        fpInstanceBuffers,
                        fpMemoryPool,
                        fpMutex,
                        forcedChildIndex,
                        fModelMat*fpCurrentObj->GetModelMat()
                        );

            //fpMutex->lock();
            fpThreadPool->Start(task);
            //fpMutex->unlock();
        }
    }
}

bool TaskRecursiveWorldAnalyseToFillBuffers::IsInFrontOfCamera(const QVector3D& objPos, float sizeIndicator, GUI::Camera* camera)
{
    QVector3D posObjToCamera = objPos - camera->GetPosition();

    // Projection of the object pos vector in the camera frame of coord on the
    // camera orientation vector.
    float projection = QVector3D::dotProduct(posObjToCamera, camera->GetOrientation() );

    return (projection > -sizeIndicator/2);
}

void TaskRecursiveWorldAnalyseToFillBuffers::InitialiseCPUInstanceBuffer(SV::TriD::CPUBuffer<float>& bufferCPU)
{
    // Model matrix data: 16 floats per matrix
    bufferCPU.AddData("ModelMatrix", 16);

    // Color 4 floats per matrix
    bufferCPU.AddData("Color", 4);

    // Size 1 float per size value
    bufferCPU.AddData("Size", 1);

    // Set the initialisation flag to true
    bufferCPU.SetIsInitialised(true);
}

void TaskRecursiveWorldAnalyseToFillBuffers::PutDataInInstanceBuffer(SV::TriD::CPUBuffer<float>& bufferCPU, const QMatrix4x4& mat, Run::VSceneNode* obj, LodModel& lod)
{
    // *****************************************
    // Put the model matrix in a float vector
    // *****************************************

    // Pointer a the start of the QMatrix4x4 data
    const float* dataPointer = mat.constData();

    // Model matrix data
    SV::TriD::Data<float>& matData = bufferCPU.GetData(0);

    // Loop on all the data within QMatrix4x4
    for(int j=0; j<16; j++)
    {
        matData.AddValue(*(dataPointer+j) );
    }

    if(lod.IsColorInstancing() )
    {
//        // *****************************************
//        // Color data
//        // *****************************************

//        // The color retrieval is strange and should be improved

//        // Color of the current instance
//        const QColor& color = obj->GetColor();

//        // Get the alpha value from the model
//        float alphaVal = lod.GetColor().alpha();

//        // Color data
//        SV::TriD::Data<float>& colorData = bufferCPU.GetData(1);

//        colorData.AddValue(float(color.redF() ) );
//        colorData.AddValue(float(color.greenF() ) );
//        colorData.AddValue(float(color.blueF() ) );
//        colorData.AddValue(alphaVal);

//        // *****************************************
//        // Size data
//        // *****************************************

//        // Size data
//        SV::TriD::Data<float>& sizeData = bufferCPU.GetData(2);

//        // Always one here
//        sizeData.AddValue(1);
    }
}

}}
