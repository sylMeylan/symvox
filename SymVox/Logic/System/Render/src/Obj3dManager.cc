/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Obj3dManager.hh"

namespace SV { namespace Logic {

Obj3dManager::Obj3dManager()
{

}

Obj3dManager::~Obj3dManager()
{

}

int Obj3dManager::TryToAddObject(const std::string& name, SV::TriD::VObject* obj)
{
    if(!fObjsToDisplay.IsInside(name) )
    {
        int id = fObjsToDisplay.Add(obj, name);

        obj->SetAutoId(id);
        obj->SetName(name);

        return id;
    }
    else
    {
        std::ostringstream oss;
        oss << name <<" was found inside the obj to display. ";
        oss << "This should not happen.";
        Utils::Exception(Utils::ExceptionType::Fatal, "Obj3dManager::TryToAddObject", oss.str() );
        delete obj;
        return fObjsToDisplay.GetIndex(name);
    }
}

void Obj3dManager::SetInstanceBufferFlag(int index, bool b)
{
    fObjsToDisplay.Get(index)->SetUpdateInstanceBuffer(b);
}

void Obj3dManager::SetAllInstanceBufferFlags(bool b)
{
    if(b)
        fObjsToDisplay.ApplyOnEveryObj([](SV::TriD::VObject* obj)->void{ obj->SetUpdateInstanceBuffer(true); } );
    else
        fObjsToDisplay.ApplyOnEveryObj([](SV::TriD::VObject* obj)->void{ obj->SetUpdateInstanceBuffer(false); } );
}

int Obj3dManager::GetIntegerId(const std::string& strId)
{
    return fObjsToDisplay.GetIndex(strId);
}

void Obj3dManager::LoadAllMeshes()
{
    fObjsToDisplay.ApplyOnEveryObj([](SV::TriD::VObject* obj)->void{ obj->Load();} );
}

void Obj3dManager::ClearAllMeshes()
{
    fObjsToDisplay.ApplyOnEveryObj([](SV::TriD::VObject* obj)->void{ obj->GetMesh().RemoveData(); } );
}

void Obj3dManager::RemoveAllObj()
{
    fObjsToDisplay.RemoveAll();
}

int Obj3dManager::GetNumOfObj()
{
    return fObjsToDisplay.GetNumOfObj();
}

SV::TriD::VObject* Obj3dManager::GetObj(int id)
{
    return fObjsToDisplay.Get(id);
}

SV::TriD::VObject* Obj3dManager::GetObj(const std::string& id)
{
    return fObjsToDisplay.Get(fObjsToDisplay.GetIndex(id) );
}

bool Obj3dManager::IsObjInside(int id)
{
    return fObjsToDisplay.IsInside(id);
}

bool Obj3dManager::IsObjInside(const std::string& id)
{
    return fObjsToDisplay.IsInside(id);
}

void Obj3dManager::RemoveObj(int id)
{
    fObjsToDisplay.Remove(id);
}

std::string Obj3dManager::DumpContent()
{
    std::ostringstream oss;

    for(int i=0, ie=fObjsToDisplay.GetNumOfObj(); i<ie; i++)
    {
        oss << i << " " <<  fObjsToDisplay.GetName(i) << "\n";
    }

    return oss.str();
}

}}
