/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/MTRender.hh"

namespace SV { namespace Logic {

static QTime time;
static int duration = 0;
static unsigned long long int frameCount = 0;

MTRender::MTRender() : VSystem()
{
    SetName("RenderSystem");

    fpWorld = nullptr;
    fpCamera = nullptr;
    fpViewOpenGL = nullptr;
    fLightPower = 100;
    fLightPos = QVector3D(0,0,100);
    fpSkyBox = nullptr;

    // No need to delete this one
    fpThreadPool = Run::ThreadPoolManager::Instance()->GetThreadPool();

    fpMemoryPool = new Run::StandardMemoryPool(sizeof(TaskRecursiveWorldAnalyseToFillBuffers)*1000000, false);

    for(int i=0; i<fpThreadPool->GetMaxThreadCount(); i++)
    {
        Run::VRunnable* task = new TaskWorldAnalyseToFillBuffers();

        fTasks.push_back(task);
    }
}

MTRender::~MTRender()
{std::cout<<"Total duration: "<<duration << " msecs for "<<frameCount<<" frames"<<std::endl;
    delete fpSkyBox;
    delete fpMemoryPool;

    for(auto& t : fTasks)
    {
        if(t != nullptr)
            delete t;
    }
}

void MTRender::Initialize()
{
    // ***********************
    // Checks
    // ***********************

    // Check that all the pointers are set

    if(fpWorld==nullptr || fpCamera==nullptr || fpViewOpenGL==nullptr)
    {
        std::stringstream ss;
        ss << "At least one of the following pointer was not initialised properly. You should call the appropriate Set() method.\n";
        ss <<"fpWorld="<<fpWorld<<", fpCamera="<<fpCamera<<", fpViewOpenGL="<<fpViewOpenGL;
        Utils::Exception(Utils::ExceptionType::Fatal, "Render::Initialise()", ss.str() );
    }

    // ***********************
    // Load the 3d objects
    // ***********************

    // Meshes

    if(fVerbose>0)
        std::cout<<"Start RecursiveWorldAnalyseToCreateMeshObj..."<<std::endl;

    std::map<std::string, bool> isProcessed;
    RecursiveWorldAnalyseToCreateMeshObj(fpWorld, isProcessed);

    if(fVerbose>0)
        std::cout<<"End RecursiveWorldAnalyseToCreateMeshObj"<<std::endl;

    // Multi meshes

    if(fVerbose>0)
        std::cout<<"Start RecursiveWorldAnalyseToCreateMultiMeshObj..."<<std::endl;

    RecursiveWorldAnalyseToCreateMultiMeshObj(fpWorld);

    if(fVerbose>0)
        std::cout<<"End RecursiveWorldAnalyseToCreateMultiMeshObj"<<std::endl;

    if(fVerbose>0)
        std::cout<<"Load all meshes..."<<std::endl;

    fObj3dManager.LoadAllMeshes();

    if(fVerbose>0)
        std::cout<<"End the mesh loading process"<<std::endl;
}

void MTRender::Process()
{
    // Clean the buffer container

    // Loop on all the buffers within the container
    for(auto& it : fInstanceBuffers)
    {
        // Get the current buffer
        SV::TriD::CPUBuffer<float>& buffer = it.second;

        // Reset all its data but no "shrinking"
        buffer.ResetAllData();
    }
time.start();
    // Analyse recursively the world to generate the matrix buffers
    RecursiveWorldAnalyseToFillBuffers(fpWorld, fpCamera);
duration += time.elapsed();frameCount++;
    // Enable the refresh action of the instance buffers
    fObj3dManager.SetAllInstanceBufferFlags(true);

    // **********************************
    // Display the world on the screen
    // **********************************

    fpViewOpenGL->Clean();
    fpViewOpenGL->Resize();
    fpViewOpenGL->SaveViewMat();

    fpCamera->LookAt(fpViewOpenGL->GetView() );

    // Render the skybox
    if(fpSkyBox != nullptr)
        fpSkyBox->Display(QMatrix4x4(), fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition(), fLightManager);

    // Render all the world objects
    RenderWorld(fpViewOpenGL->GetProjection(), fpViewOpenGL->GetView(), fLightPos, fLightPower);

    fpViewOpenGL->LoadSavedViewMat();
    fpViewOpenGL->Refresh();
}

void MTRender::RenderWorld(QMatrix4x4& projection, QMatrix4x4& view, QVector3D& lightPos, float lightPower)
{
    Utils::StrToIdContainer<SV::TriD::VObject*>& objContainer = fObj3dManager.GetObjContainer();
    std::map<int, SV::TriD::VObject*>& obj3dMap = objContainer.GetObjMap();

    // Loop on all the 3D objects registered into the objContainer
    // and try to render them if possible.
    for(auto& it : obj3dMap )
    {
        SV::TriD::VObject* obj = it.second;

        // Get the buffer
        SV::TriD::CPUBuffer<float>& bufferCPU = fInstanceBuffers[it.first];

        if(bufferCPU.GetNumOfData() > 0)
        {
            int instanceNum = bufferCPU.GetData(0).GetNumOfValues() / bufferCPU.GetData(0).GetSetSize();

            if(instanceNum > 0 && obj != nullptr)
            {
                obj->SetInstanceBuffer(&bufferCPU);
                obj->Display(QMatrix4x4(), view, projection, fpCamera->GetPosition(), fLightManager);
            }
        }
    }
}

void MTRender::TransferDataFromLodToObj3d(LodModel& lod, SV::TriD::VObject& obj3d)
{
    // Shader

    // Check we do not have a shader program that is already defined
    if(obj3d.GetShaderID() == -1)
    {
        const std::string& shaderVert = lod.GetVertexShader();
        const std::string& shaderFrag = lod.GetFragShader();

        std::stringstream ss;
        ss << shaderVert << " " << shaderFrag;

        if(!fAssetsManager.IsShaderInside(shaderVert, shaderFrag) )
        {
            SV::TriD::Shader* shader = new SV::TriD::Shader(shaderVert, shaderFrag);
            shader->AddAttribLocation("in_Vertex");
            shader->AddAttribLocation("in_Color");
            shader->AddAttribLocation("in_TextureCoord");
            shader->AddAttribLocation("in_NormalCoord");
            shader->AddAttribLocation("in_Model");

            shader->Load();

            int shaderId = fAssetsManager.AddShader(shader);
            obj3d.SetShaderId(shaderId);
        }
        else
        {
            int shaderId = fAssetsManager.GetShaderId(shaderVert, shaderFrag);
            obj3d.SetShaderId(shaderId);
        }
    }
}

void MTRender::RecursiveWorldAnalyseToCreateMeshObj(Run::VSceneNode* currentObj,
                                                  std::map<std::string, bool>& isProcessed,
                                                  QMatrix4x4 modelPos)
{
    // Loop on all the objects/nodes of the tree.
    // Check if they need a mesh object.
    // If yes, generate and save that mesh object.

    // Test the object components to check we have a renderable component
    if(currentObj->HasComponent(List::Renderable) )
    {
        // Get the lodModels of the current node
        Renderable* renderable = static_cast<Renderable*>( currentObj->GetComponent(List::Renderable) );

        // Get the number of compo models contained within it
        int numberOfModels = renderable->GetNumOfLods();

        // if it has more than 0 compoModels it means there may be models to be loaded
        if(numberOfModels > 0)
        {
            // Loop on all the lod levels
            for(int index=0; index<numberOfModels; index++)
            {
                LodModel& lodModel = renderable->GetLod(index);

                // Get the string type3d that is an id used to do the link 3dObj-Node
                const std::string& type3d = lodModel.Get3dType();

                // Check if the type3d was already added into the obj3d container
                // And that the primitive is not a nullptr
                if(!fObj3dManager.IsObjInside(lodModel.Get3dTypeId() ) && lodModel.GetPrimitive() != nullptr )
                {
                    CreateObj3dAndMesh(currentObj, lodModel, type3d);
                }
            }
        }
    }

    // ********************************************
    // Process the children (recursion happens here)
    // ********************************************

    isProcessed[currentObj->GetName()] = true;

    int childNum = currentObj->GetNumOfChildren();

    for(int i=0; i<childNum; i++)
    {
        Run::VSceneNode* child = currentObj->GetChild(i);

        // If the object was not processed then do it, otherwise skip it
        if(!isProcessed[child->GetName()])
            RecursiveWorldAnalyseToCreateMeshObj(child, isProcessed, modelPos * child->GetModelMat() );
    }
}

void MTRender::CreateObj3dAndMesh(Run::VSceneNode* currentObj, LodModel& lod, const std::string& type3d)
{
    if(fVerbose>0)
        std::cout<<"Start mesh creation for "<<type3d<<"..."<<std::endl;

    SV::TriD::VPrimitive* primitive = lod.GetPrimitive();

    if(primitive != nullptr)
    {
        // Create the 3d object
        SV::TriD::VObject* obj3d = primitive->CreateObject(&fAssetsManager);

        if(obj3d != nullptr)
        {
            obj3d->SetName(lod.Get3dType());

            // Check a shader was not set during the "CreateObject" process
            if(obj3d->GetShaderID() == -1)
                TransferDataFromLodToObj3d(lod, *obj3d);

//            // Set the color

//            if(lod.IsColorInstancing() && !lod.IsLoader() )
//            {
//                //obj3d->GetMesh().FillColor(lod.GetColor() );
//                //obj3d->GetMesh().FillColor(QColor(currentObj->GetColor().red(), currentObj->GetColor().green(), currentObj->GetColor().blue(), lod.GetColor().alpha() ) );
//            }
//            else if(!lod.IsLoader() )
//            {
//                //obj3d->GetMesh().FillColor(lod.GetColor() );
//            }

            // Register the 3d object
            int id = fObj3dManager.TryToAddObject(lod.Get3dType(), obj3d);
            lod.Set3dTypeId(id); // And set its attributed id
        }
        else
        {
            std::stringstream ss;
            ss << "The obj3d was returned as a nullptr for the lod "<< lod.Get3dType() <<" of the node named "<<currentObj->GetName();
            Utils::Exception(Utils::ExceptionType::Warning, "Render::CreateObj3dAndMesh", ss.str() );
        }
    }

    if(fVerbose>0)
        std::cout<<"End of the mesh creation for "<<lod.Get3dType()<<"."<<std::endl;
}

void MTRender::RecursiveWorldAnalyseToCreateMultiMeshObj(Run::VSceneNode* currentObj, QMatrix4x4 modelPos)
{
    // Loop on all the geoObject of the tree.
    // Check if they need a multi mesh object.
    // If yes, generate and save that multi mesh object.

    // We look into the lod object to see the different representations that will be used.
    // We generate them is they are of the multi mesh type.

    bool oneWasMultiMesh (false);

    if(currentObj->HasComponent(List::Renderable) )
    {
        // Get the current lodModels
        Renderable& renderable = * static_cast<Renderable*> (currentObj->GetComponent(List::Renderable) );

        int numberOfLods = renderable.GetNumOfLods();

        // Check we have at least one lod that is defined
        if(numberOfLods > 0)
        {
            // Loop on all the compo models
            for(int index=0, eindex=numberOfLods; index<eindex; ++index)
            {
                // Get the current compo model
                LodModel& lod = renderable.GetLod(index);

                // Get the index childForcedLodIndex
                int childForcedIndex = lod.GetChildForcedIndex();
                // -1 is the default value

                // Check if it is a multi mesh object
                // and if it was processed before (if it was then skip it)
                // and that the primitive is not a nullptr
                if(lod.IsMultiMesh() && !fObj3dManager.IsObjInside(lod.Get3dTypeId() ) && lod.GetPrimitive() != nullptr )
                {
                    // Create an OGLObject
                    SV::TriD::VObject* multiMeshObj = lod.GetPrimitive()->CreateObject(&fAssetsManager);

                    if(multiMeshObj != nullptr)
                    {
                        TransferDataFromLodToObj3d(lod, *multiMeshObj);

                        // Add the object to the container
                        int id = fObj3dManager.TryToAddObject(lod.Get3dType(), multiMeshObj);
                        lod.Set3dTypeId(id);

                        if(fVerbose>0)
                            std::cout<<"Multi mesh generation: "<< lod.Get3dType() <<" for obj named " << currentObj->GetName() <<"..."<<std::endl;

                        RecursiveObjAnalyseToCreateMultiMesh(currentObj, multiMeshObj, childForcedIndex, QMatrix4x4(), true);

                        if(fVerbose>0)
                            std::cout<<"End of the multi mesh generation"<<std::endl;

                        oneWasMultiMesh = true;
                    }
                    else
                    {
                        std::stringstream ss;
                        ss << "The obj3d was returned as a nullptr for the lod "<< lod.Get3dType() <<" of the node named "<<currentObj->GetName();
                        Utils::Exception(Utils::ExceptionType::Warning, "Render::RecursiveWorldAnalyseToCreateMultiMeshObj", ss.str() );
                    }
                }
                else if(lod.IsMultiMesh() && fObj3dManager.IsObjInside(lod.Get3dTypeId() ) )
                    oneWasMultiMesh = true;
            }
        }
    }

    // If one object was multi mesh we stop here the loop since all the sub multi meshes were generated
    // during the first scan.
    // This will cause issues if we want to have a multi mesh lod and a detailed composite mesh at the same time
    if(!oneWasMultiMesh)
    {
        // Process normal childs

        int childNum = currentObj->GetNumOfChildren();

        for(int i=0; i<childNum; ++i)
        {
            Run::VSceneNode* child = currentObj->GetChild(i);

            RecursiveWorldAnalyseToCreateMultiMeshObj(child, modelPos*currentObj->GetModelMat() );
        }
    }
}

void MTRender::RecursiveObjAnalyseToCreateMultiMesh(Run::VSceneNode* currentObj,
                                                     SV::TriD::VObject* multiMeshObj,
                                                     int forcedLodIndex,
                                                     QMatrix4x4 modelPos,
                                                     bool rootMultiMeshObj)
{
    bool isMultiMesh = false;
    bool isChildDisplay = true;

    int localForcedIndex = forcedLodIndex;

    if(currentObj->HasComponent(List::Renderable) )
    {
        Renderable& renderable = * static_cast<Renderable*> (currentObj->GetComponent(List::Renderable) );

        int numberOfLods = renderable.GetNumOfLods();

        if(numberOfLods > 0 && numberOfLods > forcedLodIndex)
        {
            if(numberOfLods < forcedLodIndex)
                localForcedIndex = numberOfLods-1;

            LodModel& lod = renderable.GetLod(localForcedIndex);

            isChildDisplay = lod.IsShowChildren();

            isMultiMesh = lod.IsMultiMesh();

            const std::string& type3d = lod.Get3dType();

            // If object is not already built then we need to do it
            if(!fObj3dManager.IsObjInside(lod.Get3dTypeId() ) && isMultiMesh && !rootMultiMeshObj)
            {
                SV::TriD::Object* obj = new SV::TriD::Object();

                if(multiMeshObj != nullptr)
                {
                    TransferDataFromLodToObj3d(lod, *obj);

                    int id = fObj3dManager.TryToAddObject(type3d, obj);
                    lod.Set3dTypeId(id);

                    RecursiveObjAnalyseToCreateMultiMesh(currentObj, obj, forcedLodIndex, QMatrix4x4(), true);
                }
                else
                {
                    std::stringstream ss;
                    ss << "The obj3d was returned as a nullptr for the lod "<< lod.Get3dType() <<" of the node named "<<currentObj->GetName();
                    Utils::Exception(Utils::ExceptionType::Warning, "Render::RecursiveObjAnalyseToCreateMultiMesh", ss.str() );
                }
            }

            // Check if the current object has a mesh and, if not, skip it
            if(fObj3dManager.IsObjInside(lod.Get3dTypeId() ) )
            {
                //std::cout<<"Add a mesh to multi mesh, mesh: "<<type3d<<std::endl;

                // Get current obj mesh
                // We need to create a new mesh because we will translate all the variables
                SV::TriD::Mesh currentMesh = fObj3dManager.GetObj(lod.Get3dTypeId() )->GetMesh();

                if(currentMesh.VerticesCount()>0)
                {
                    //std::cout<<"vertex in mesh: "<<currentMesh.VerticesCount()<<std::endl;

                    // Transform the mesh to place it at its position in its mother volume
                    currentMesh.ApplyTransform(modelPos);

                    // Add the mesh to the multiMeshObj
                    multiMeshObj->GetMesh().add_mesh(currentMesh);
                }
            }
        }
    }

    // Check the object is a not a multi mesh that has been loaded before
    // If it is, we do not want to load it two times
    if((!isMultiMesh || rootMultiMeshObj ) && (isChildDisplay || rootMultiMeshObj) )
    {
        // ********************************************
        // Process the childs (recursion happens here)
        // ********************************************

        int childNum = currentObj->GetNumOfChildren();

        for(int i=0; i<childNum; ++i)
        {
            Run::VSceneNode* child = currentObj->GetChild(i);

            RecursiveObjAnalyseToCreateMultiMesh(child, multiMeshObj,
                                                 forcedLodIndex, modelPos* child->GetModelMat(), false);
        }
    }
}

void MTRender::RecursiveWorldAnalyseToFillBuffers(Run::VSceneNode *currentObj,
                                                  GUI::Camera *camera,
                                                  int forcedLodIndex,
                                                  QMatrix4x4 modelMat)
{
    int initActiveThreadCount = fpThreadPool->GetActiveThreadCount();
    int maxThreadCount = fpThreadPool->GetMaxThreadCount();
    int availThreadCount = maxThreadCount-initActiveThreadCount;

    // Start a first task

    TaskWorldAnalyseToFillBuffers* task = static_cast<TaskWorldAnalyseToFillBuffers*>(fTasks[0]);

    DataForTask data;
    data.camera = camera;
    data.currentObj = currentObj;
    data.forcedLodIndex = forcedLodIndex;
    data.fpDataToProcess = &fDataToProcess;
    data.instanceBuffers = &fInstanceBuffers;
    data.memoryPool = fpMemoryPool;
    data.modelMat = modelMat;
    data.globalMutex = &fGlobalWriteBufferMutex;
    data.globalQueueBufferMutex = &fGlobalQueueBufferMutex;
    data.obj3dManager = &fObj3dManager;
    data.threadPool = fpThreadPool;

    // Start the task in a thread
    task->SetParameters(data);
    fpThreadPool->Start(task);

    bool allFinished = false;

    while( !( allFinished && fDataToProcess.size() == 0 ) )
    {
//        std::cout<< (fpThreadPool->GetActiveThreadCount() > initActiveThreadCount)
//                 <<" fDataToProcess.size "<< fDataToProcess.size()
//                   <<" allFinished "<< allFinished
//                     <<" activeThreadCount() "<< fpThreadPool->GetActiveThreadCount()
//                    << " cond " << ( allFinished && fDataToProcess.size() == 0 )
//                <<std::endl;

        if(fpThreadPool->GetActiveThreadCount() > initActiveThreadCount)
            allFinished = false;
        else
        {
            allFinished = true;

            for(Run::VRunnable* t : fTasks)
            {
                TaskWorldAnalyseToFillBuffers* task = static_cast<TaskWorldAnalyseToFillBuffers*>(t);

                if(!task->GetIsAvailable())
                {
                    allFinished = false;
                }
                else if(fDataToProcess.size() != 0 )
                {
                    task->SetParameters(fDataToProcess.dequeue() );
                    bool success = fpThreadPool->TryStart(task);

                    if(!success)
                    {
                        Utils::Exception(Utils::ExceptionType::Warning,
                                         "MTRender::RecursiveWorldAnalyseToFillBuffers",
                                         "Error in thread management");
                    }
                }
            }
        }
    }
}

}}
