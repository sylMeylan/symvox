/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Render.hh"

namespace SV { namespace Logic {

Render::Render() : VSystem()
{
    SetName("RenderSystem");

    fpWorld = nullptr;
    fpCamera = nullptr;
    fpViewOpenGL = nullptr;
    fLightPower = 100;
    fLightPos = QVector3D(0,0,100);
    fpSkyBox = nullptr;
}

Render::~Render()
{
    delete fpSkyBox;
}

void Render::Initialize()
{
    // ***********************
    // Checks
    // ***********************

    // Check that all the pointers are set

    if(fpWorld==nullptr || fpCamera==nullptr || fpViewOpenGL==nullptr)
    {
        std::stringstream ss;
        ss << "At least one of the following pointer was not initialised properly. You should call the appropriate Set() method.\n";
        ss <<"fpWorld="<<fpWorld<<", fpCamera="<<fpCamera<<", fpViewOpenGL="<<fpViewOpenGL;
        Utils::Exception(Utils::ExceptionType::Fatal, "Render::Initialise()", ss.str() );
    }

    // ***********************
    // Load the 3d objects
    // ***********************

    fpViewOpenGL->MakeContextCurrent();

    // Meshes

    if(fVerbose>0)
        std::cout<<"Start RecursiveWorldAnalyseToCreateMeshObj..."<<std::endl;

    std::map<std::string, bool> isProcessed;
    RecursiveWorldAnalyseToCreateMeshObj(fpWorld, isProcessed);

    if(fVerbose>0)
        std::cout<<"End RecursiveWorldAnalyseToCreateMeshObj"<<std::endl;

    // Multi meshess

    if(fVerbose>0)
        std::cout<<"Start RecursiveWorldAnalyseToCreateMultiMeshObj..."<<std::endl;

    RecursiveWorldAnalyseToCreateMultiMeshObj(fpWorld);

    if(fVerbose>0)
        std::cout<<"End RecursiveWorldAnalyseToCreateMultiMeshObj"<<std::endl;

    if(fVerbose>0)
        std::cout<<"Load all meshes..."<<std::endl;

    fObj3dManager.LoadAllMeshes();

    if(fVerbose>0)
        std::cout<<"End the mesh loading process"<<std::endl;
}

void Render::Process()
{
    fpViewOpenGL->MakeContextCurrent();

    // Clean the buffer container

    // Loop on all the buffers within the container
    for(auto& it : fInstanceBuffers)
    {
        // Get the current buffer
        SV::TriD::CPUBuffer<float>& buffer = it.second;

        // Reset all its data but no "shrinking"
        buffer.ResetAllData();
    }

    // Analyse recursively the world to generate the matrix buffers
    RecursiveWorldAnalyseToFillBuffers(fpWorld, fpCamera);

    // Enable the refresh action of the instance buffers
    fObj3dManager.SetAllInstanceBufferFlags(true);

    // **********************************
    // Display the world on the screen
    // **********************************

    fpViewOpenGL->Clean();
    fpViewOpenGL->Resize();
    fpViewOpenGL->SaveViewMat();

    fpCamera->LookAt(fpViewOpenGL->GetView() );

    // Render the skybox
    if(fpSkyBox != nullptr)
        fpSkyBox->Display(QMatrix4x4(), fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition(), fLightManager);

    // Render all the world objects
    RenderWorld(fpViewOpenGL->GetProjection(), fpViewOpenGL->GetView(), fLightPos, fLightPower);

    fpViewOpenGL->LoadSavedViewMat();
    fpViewOpenGL->Refresh();
}

void Render::RenderWorld(QMatrix4x4& projection, QMatrix4x4& view, QVector3D& lightPos, float lightPower)
{
    Utils::StrToIdContainer<SV::TriD::VObject*>& objContainer = fObj3dManager.GetObjContainer();

    // If we use this map to display the 3D-objects then their order will be the one of their "int" id stored in the map.
    // Those are generated during the first loading pass on the graphe, thus this first loading pass will
    // set the order in which the object are display.
    // We want to avoid this because of the transparency: an object inside another transparent one should be display "before" its container.
    // In the end, we are going to use the index map that has key-string to do so. Indeed, the string are the lod names and can be tuned by the user.
    std::map<int, SV::TriD::VObject*>& obj3dMap = objContainer.GetObjMap();
    // Below is the index map with the string-keys.
    const std::map<std::string, int>& indexMap = objContainer.GetIndexMap();

    // Loop on all the 3D objects registered into the objContainer
    // and try to render them if possible.
    for(auto& it : indexMap) //obj3dMap )
    {
        SV::TriD::VObject* obj = obj3dMap[it.second];//it.second;

        // Get the buffer
        SV::TriD::CPUBuffer<float>& bufferCPU = fInstanceBuffers[it.second];//[it.first];

        if(bufferCPU.GetNumOfData() > 0)
        {
            int instanceNum = bufferCPU.GetData(0).GetNumOfValues() / bufferCPU.GetData(0).GetSetSize();

            if(instanceNum > 0 && obj != nullptr)
            {
                obj->SetInstanceBuffer(&bufferCPU);
                obj->Display(QMatrix4x4(), view, projection, fpCamera->GetPosition(), fLightManager);
            }
        }
    }
}

void Render::Reload3DObjects()
{
    // Remove all the objets from the manager
    fObj3dManager.RemoveAllObj();

    fInstanceBuffers.clear();

    // Reload the objets
    Initialize();

}

void Render::TransferDataFromLodToObj3d(LodModel& lod, SV::TriD::VObject& obj3d, bool fromMultiMesh)
{
    // Shader

    // Check no shader program that is already defined for this 3D object
    // or that the lod was specifically configured to overload pre-defined shaders.
    if(obj3d.GetShaderID() == -1
            || lod.GetForceShader()
            )
    {
        const std::string& shaderVert = lod.GetVertexShader();
        const std::string& shaderFrag = lod.GetFragShader();
        const std::string& shaderGeo = lod.GetGeoShader();

        // Skip it if no shader were defined.
        if( !(shaderVert == "" && shaderFrag == "" && shaderGeo == "") )
        {
            if(!fAssetsManager.IsShaderInside(shaderVert, shaderFrag, shaderGeo) )
            {
                SV::TriD::Shader* shader = new SV::TriD::Shader(shaderVert, shaderFrag, shaderGeo);
                shader->AddAttribLocation("in_Vertex");
                shader->AddAttribLocation("in_Color");
                shader->AddAttribLocation("in_TextureCoord");
                shader->AddAttribLocation("in_NormalCoord");
                shader->AddAttribLocation("in_Model");

                shader->Load();

                int shaderId = fAssetsManager.AddShader(shader);
                obj3d.SetShaderId(shaderId);
            }
            else
            {
                int shaderId = fAssetsManager.GetShaderId(shaderVert, shaderFrag, shaderGeo);

                obj3d.SetShaderId(shaderId);
            }
        }
    }

    if( ( !lod.IsColorInstancing() )
            && !lod.IsMultiMesh()
            && !lod.IsLoader()
            )
    {
        for(unsigned int i=0, ie=obj3d.GetNumOfMeshEntries();i<ie; i++)
            obj3d.GetMeshEntry(i).fMesh.FillColor(lod.GetColor() );
    }
}

void Render::RecursiveWorldAnalyseToCreateMeshObj(Run::VSceneNode* currentObj,
                                                  std::map<std::string, bool>& isProcessed,
                                                  QMatrix4x4 modelPos)
{
    // Attention:
    // In this method, only lod.Get3dType() should be used since
    // lod.Get3dTypeId() is not defined yet.
    // Indeed, it is defined at the first render pass.

    // Loop on all the objects/nodes of the tree.
    // Check if they need a mesh object.
    // If yes, generate and save that mesh object.

    // Test the object components to check we have a renderable component
    if(currentObj->HasComponent(List::Renderable) )
    {
        // Get the lodModels of the current node
        Renderable* renderable = static_cast<Renderable*>( currentObj->GetComponent(List::Renderable) );

        // Get the number of compo models contained within it
        int numberOfModels = renderable->GetNumOfLods();

        // if it has more than 0 compoModels it means there may be models to be loaded
        if(numberOfModels > 0)
        {
            // Loop on all the lod levels
            for(int index=0; index<numberOfModels; index++)
            {
                LodModel& lodModel = renderable->GetLod(index);

                // Get the string type3d that is an id used to do the link 3dObj-Node
                const std::string& type3d = lodModel.Get3dType();

                // Check if the type3d was already added into the obj3d container
                // And that the primitive is not a nullptr
                // And that the object is not "multi mesh"
                if(!fObj3dManager.IsObjInside(type3d )
                        && lodModel.GetPrimitive() != nullptr
                        && !lodModel.IsMultiMesh() )
                {
                    CreateObj3dAndMesh(currentObj, lodModel, type3d);
                }
            }
        }
    }

    // ********************************************
    // Process the children (recursion happens here)
    // ********************************************

    isProcessed[currentObj->GetName()] = true;

    int childNum = currentObj->GetNumOfChildren();

    for(int i=0; i<childNum; i++)
    {
        Run::VSceneNode* child = currentObj->GetChild(i);

        // If the object was not processed then do it, otherwise skip it
        if(!isProcessed[child->GetName()])
            RecursiveWorldAnalyseToCreateMeshObj(child, isProcessed, modelPos * child->GetModelMat() );
    }
}

void Render::CreateObj3dAndMesh(Run::VSceneNode* currentObj, LodModel& lod, const std::string& type3d)
{
    // Attention:
    // In this method, only lod.Get3dType() should be used since
    // lod.Get3dTypeId() is not defined yet.
    // Indeed, it is defined at the first render pass.

    if(fVerbose>0)
        std::cout<<"Start mesh creation for "<<type3d<<"..."<<std::endl;

    SV::TriD::VPrimitive* primitive = lod.GetPrimitive();

    if(primitive != nullptr)
    {
        // Create the 3d object
        SV::TriD::VObject* obj3d = primitive->CreateObject(&fAssetsManager);

        if(obj3d != nullptr)
        {
            obj3d->SetName(lod.Get3dType());

            // Check a shader was not set during the "CreateObject" process
            if(obj3d->GetShaderID() == -1)
                TransferDataFromLodToObj3d(lod, *obj3d);

//            // Set the color

//            if(lod.IsColorInstancing() && !lod.IsLoader() )
//            {
//                //obj3d->GetMesh().FillColor(lod.GetColor() );
//                //obj3d->GetMesh().FillColor(QColor(currentObj->GetColor().red(), currentObj->GetColor().green(), currentObj->GetColor().blue(), lod.GetColor().alpha() ) );
//            }
//            else if(!lod.IsLoader() )
//            {
//                //obj3d->GetMesh().FillColor(lod.GetColor() );
//            }

            // Register the 3d object
            int id = fObj3dManager.TryToAddObject(lod.Get3dType(), obj3d);
            lod.Set3dTypeId(id); // And set its attributed id
        }
        else
        {
            std::stringstream ss;
            ss << "The obj3d was returned as a nullptr for the lod "<< lod.Get3dType() <<" of the node named "<<currentObj->GetName();
            Utils::Exception(Utils::ExceptionType::Warning, "Render::CreateObj3dAndMesh", ss.str() );
        }
    }

    if(fVerbose>0)
        std::cout<<"End of the mesh creation for "<<lod.Get3dType()<<"."<<std::endl;
}

void Render::RecursiveWorldAnalyseToCreateMultiMeshObj(Run::VSceneNode* currentObj, QMatrix4x4 modelPos)
{
    // Attention:
    // In this method, only lod.Get3dType() should be used since
    // lod.Get3dTypeId() is not defined yet.
    // Indeed, it is defined at the first render pass.

    // Loop on all the geoObject of the tree.
    // Check if they need a multi mesh object.
    // If yes, generate and save that multi mesh object.

    // We look into the lod object to see the different representations that will be used.
    // We generate them is they are of the multi mesh type.

    bool oneWasMultiMesh (false);

    if(currentObj->HasComponent(List::Renderable) )
    {
        // Get the current lodModels
        Renderable& renderable = *static_cast<Renderable*> (currentObj->GetComponent(List::Renderable) );

        int numberOfLods = renderable.GetNumOfLods();

        // Check we have at least one lod that is defined
        if(numberOfLods > 0)
        {
            // Loop on all the compo models
            for(int index=0, eindex=numberOfLods; index<eindex; ++index)
            {
                // Get the current compo model
                LodModel& lod = renderable.GetLod(index);

                // Get the index childForcedLodIndex
                int childForcedIndex = lod.GetChildForcedIndex();
                // -1 is the default value

                // Check if it is a multi mesh object
                // and if it was processed before (if it was then skip it)
                // and that the primitive is not a nullptr
                if(lod.IsMultiMesh() && !fObj3dManager.IsObjInside(lod.Get3dType() ) && lod.GetPrimitive() != nullptr )
                {
                    // Create an 3DObject that will store all the mesh of the multi-mesh object
                    SV::TriD::VObject* multiMeshObj = lod.GetPrimitive()->CreateObject(&fAssetsManager);

                    if(multiMeshObj != nullptr)
                    {
                        TransferDataFromLodToObj3d(lod, *multiMeshObj, true);

                        // Add the object to the container
                        int id = fObj3dManager.TryToAddObject(lod.Get3dType(), multiMeshObj);
                        lod.Set3dTypeId(id);

                        if(fVerbose>0)
                            std::cout<<"Multi mesh generation: "<< lod.Get3dType() <<" for obj named " << currentObj->GetName() <<" ..."<<std::endl;

                        RecursiveObjAnalyseToCreateMultiMesh(currentObj, multiMeshObj, childForcedIndex, QMatrix4x4(), true);

                        if(fVerbose>0)
                            std::cout<<"End of the multi mesh generation"<<std::endl;

                        oneWasMultiMesh = true;
                    }
                    else
                    {
                        std::stringstream ss;
                        ss << "The obj3d was returned as a nullptr for the lod "<< lod.Get3dType() <<" of the node named "<<currentObj->GetName();
                        Utils::Exception(Utils::ExceptionType::Warning, "Render::RecursiveWorldAnalyseToCreateMultiMeshObj", ss.str() );
                    }
                }
                else if(lod.IsMultiMesh() && fObj3dManager.IsObjInside(lod.Get3dTypeId() ) )
                    oneWasMultiMesh = true;
            }
        }
    }

    // If one object was multi mesh we stop here the loop since all the sub multi meshes were generated
    // during the first scan.
    // This will cause issues if we want to have a multi mesh lod and a detailed composite mesh at the same time
    if(!oneWasMultiMesh)
    {
        // Process normal childs

        int childNum = currentObj->GetNumOfChildren();

        for(int i=0; i<childNum; ++i)
        {
            Run::VSceneNode* child = currentObj->GetChild(i);

            RecursiveWorldAnalyseToCreateMultiMeshObj(child, modelPos*currentObj->GetModelMat() );
        }
    }
}

void Render::RecursiveObjAnalyseToCreateMultiMesh(Run::VSceneNode* currentObj,
                                                     SV::TriD::VObject* multiMeshObj,
                                                     int forcedLodIndex,
                                                     QMatrix4x4 modelPos,
                                                     bool rootMultiMeshObj)
{
    // Attention:
    // In this method, only lod.Get3dType() should be used since
    // lod.Get3dTypeId() is not defined yet.
    // Indeed, it is defined at the first render pass.

    // In this recursive method, first we check if the object can be displayed.
    // If not, we go children (loop in the method) or to the next one (because of the loop outside the method).
    // If it can be shown, then try to:
    // 1) Built the 3D object if it does not already exist
    // 2) If it has a mesh, we add it to the multimesh 3D object
    // Finally, we end up at the last bloc of code where we go to children or next object.

    bool isMultiMesh = false;
    bool isChildDisplay = true;

    int localForcedIndex = forcedLodIndex;

    if(currentObj->HasComponent(List::Renderable) )
    {
        Renderable& renderable = * static_cast<Renderable*> (currentObj->GetComponent(List::Renderable) );

        int numberOfLods = renderable.GetNumOfLods();

        if(numberOfLods > 0 && numberOfLods > forcedLodIndex)
        {
            if(numberOfLods < forcedLodIndex)
                localForcedIndex = numberOfLods-1;

            LodModel& lod = renderable.GetLod(localForcedIndex);

            isChildDisplay = lod.IsShowChildren();

            isMultiMesh = lod.IsMultiMesh();

            const std::string& type3d = lod.Get3dType();

            // If object is not already built then we need to do it
            if(!fObj3dManager.IsObjInside(lod.Get3dType() )
                    && !rootMultiMeshObj
                    )
            {
                SV::TriD::VPrimitive* primitive = lod.GetPrimitive();

                if(primitive != nullptr)
                {
                    SV::TriD::VObject* obj = primitive->CreateObject(&fAssetsManager);

                    if(obj != nullptr)
                    {
                        TransferDataFromLodToObj3d(lod, *obj, true);

                        int id = fObj3dManager.TryToAddObject(type3d, obj);

                        lod.Set3dTypeId(id);

                        //#ifndef NDEBUG
                        std::cout<< "Object "<<lod.Get3dType()<<" was detected as a new multimesh"<<std::endl;
                        //#endif

                        // Build a multimesh if it is
                        if(isMultiMesh)
                            RecursiveObjAnalyseToCreateMultiMesh(currentObj, obj, forcedLodIndex, QMatrix4x4(), true);
                    }
                    else
                    {
                        std::stringstream ss;
                        ss << "The obj3d was returned as a nullptr for the lod "<< lod.Get3dType() <<" of the node named "<<currentObj->GetName();
                        Utils::Exception(Utils::ExceptionType::Warning, "Render::RecursiveObjAnalyseToCreateMultiMesh", ss.str() );
                    }
                }
            }

            // Check if the current object has a mesh and, if not, skip it.
            // We also avoid to an already created object its own mesh.
            if(fObj3dManager.IsObjInside(lod.Get3dType() )
                    && lod.Get3dType() !=  multiMeshObj->GetName()
                    )
            {
                #ifndef NDEBUG
                std::cout<<"Add a mesh ("<<lod.Get3dType()<<") to multi-mesh object ("
                        << multiMeshObj->GetName() <<")"
                        <<std::endl;
                #endif

                // Get current obj mesh
                // We need to create a new mesh because we will translate all the variables
                SV::TriD::Mesh currentMesh = fObj3dManager.GetObj(lod.Get3dType() )->GetMesh();

                if(currentMesh.VerticesCount()>0)
                {

                    // Set the color of the mesh for the multi-mesh final object.
                    // Usually, color is managed through instance buffer but it cannot be
                    // when dealing with multi-mesh
                    if(!lod.IsMultiMesh() )
                        currentMesh.FillColor(lod.GetColor() );

                    //std::cout<<"vertex in mesh: "<<currentMesh.VerticesCount()<<std::endl;

                    // Transform the mesh to place it at its position in its mother volume
                    currentMesh.ApplyTransform(modelPos);

                    // Add the mesh to the multiMeshObj
                    multiMeshObj->GetMesh().add_mesh(currentMesh);
                }
            }
        }
    }

    // Check the object is a not a multi mesh that has been loaded before
    // If it is, we do not want to load it two times
    if((!isMultiMesh || rootMultiMeshObj ) && (isChildDisplay || rootMultiMeshObj) )
    {
        // ********************************************
        // Process the childs (recursion happens here)
        // ********************************************

        int childNum = currentObj->GetNumOfChildren();

        for(int i=0; i<childNum; ++i)
        {
            Run::VSceneNode* child = currentObj->GetChild(i);

            RecursiveObjAnalyseToCreateMultiMesh(child, multiMeshObj,
                                                 forcedLodIndex, modelPos* child->GetModelMat(), false);
        }
    }
}

void Render::RecursiveWorldAnalyseToFillBuffers(Run::VSceneNode* currentObj, GUI::Camera* camera, int forcedLodIndex, QMatrix4x4 modelMat)
{
    // - Get the current object 3dType
    // - Get the Lod object corresponding to the 3dType
    // - Check if a forced lod level is set
    // - Retrieve lod values (3d type to be displayed...)
    // - Fill the matrix buffers
    // - Do it again for all the childs

    bool isChildDisplay (true);

    int forcedChildIndex = forcedLodIndex;

    // Check we do have a renderable component for the current obj
    if(currentObj->HasComponent(List::Renderable) )
    {
        // Get the renderable component
        Renderable* renderable = static_cast<Renderable*> (currentObj->GetComponent(List::Renderable) );

        int  numOfLods = renderable->GetNumOfLods();

        // Check if we do have a lod for the current object
        if(numOfLods > 0)
        {
            // ************************************
            // Check if a forced lod level is set
            // ************************************

            int lodIndex = -1;

            // Default force lod value is -1.
            // It means no force lod is set.
            if(forcedLodIndex == -1)
            {
                float distance = (Utils::ExtractPosFromMatrix(modelMat*currentObj->GetModelMat() ) - camera->GetPosition() ).length();

                lodIndex =  renderable->GetLodManager().GetIndexToDisplay(distance);
            }

            // A forced lod index is set
            else
            {
                if(forcedLodIndex < renderable->GetNumOfLods() )
                    lodIndex = forcedLodIndex;
                else
                    lodIndex = renderable->GetNumOfLods()-1;
            }

            LodModel& lod = renderable->GetLod(lodIndex);

            // ************************************
            // Check the object position and the one of the camera
            // ************************************

            // Get the position matrix
            QMatrix4x4 model = modelMat * currentObj->GetModelMat();

            bool isDisplayable = IsInFrontOfCamera(Utils::ExtractPosFromMatrix(model), lod.GetSizeIndicator(), camera);

            // ************************************
            // Fill the buffers
            // ************************************

            isChildDisplay = lod.IsShowChildren();

            // Set the forced lod index if any
            if(forcedLodIndex == -1) forcedChildIndex = lod.GetChildForcedIndex();

            if(isDisplayable)
            {
                if(lod.GetPrimitive() != nullptr || lod.IsMultiMesh() )
                {
                    // Look for the id of the lod and if is -1 (defaut value) then initialise it
                    int id = lod.Get3dTypeId();

                    if(id == -1)
                    {
                        if(!fObj3dManager.IsObjInside(lod.Get3dType() ) )
                        {
                            std::stringstream ss;
                            ss << "The lod model "<<lod.Get3dType()<<" was not loaded as a 3d object and, thus, was not registered in the 3d object manager\n";
                            ss << "The object is named "<<currentObj->GetName();
                            Utils::Exception(Utils::ExceptionType::Warning, "Render::RecursiveWorldAnalyseToFillBuffers", ss.str() );
                        }
                        else
                        {
                            id = fObj3dManager.GetIntegerId(lod.Get3dType() );
                            lod.Set3dTypeId(id);
                        }
                    }

                    // Create the corresponding instance buffer (or retrieve it, if it exists)
                    SV::TriD::CPUBuffer<float>& bufferCPU = fInstanceBuffers[id];

                    // Initialise the buffer if needed
                    if(!bufferCPU.GetIsInitialised() )
                        InitialiseCPUInstanceBuffer(bufferCPU, lod);

                    // Add the data to the buffer
                    PutDataInInstanceBuffer(bufferCPU, model, currentObj, lod);
                }
            }
        }
    }

    // ********************************************
    // Process the childs (recursion happens here)
    // ********************************************

    // Do we want to process the child ?
    if(isChildDisplay)
    {
        // Process normal childs

        int childNum = currentObj->GetNumOfChildren();

        for(int i=0; i<childNum; ++i)
        {
            Run::VSceneNode* child = currentObj->GetChild(i);

            RecursiveWorldAnalyseToFillBuffers(child, camera, forcedChildIndex, modelMat*currentObj->GetModelMat() );
        }
    }
}

bool Render::IsInFrontOfCamera(const QVector3D& objPos, float sizeIndicator, GUI::Camera* camera)
{
    QVector3D posObjToCamera = objPos - camera->GetPosition();

    // Projection of the object pos vector in the camera frame of coord on the
    // camera orientation vector.
    float projection = QVector3D::dotProduct(posObjToCamera, camera->GetOrientation() );

    return (projection > -sizeIndicator/2);
}

void Render::InitialiseCPUInstanceBuffer(SV::TriD::CPUBuffer<float>& bufferCPU, LodModel& lod)
{
    // If no buffer is set then we go to the default one
    if(lod.GetCPUBuffer().GetNumOfData() == 0)
    {
        // Model matrix data: 16 floats per matrix
        bufferCPU.AddData("ModelMatrix", 16);

        // Color 4 floats per matrix
        bufferCPU.AddData("Color", 4);

        // Size 1 float per size value
        bufferCPU.AddData("Size", 1);

        // Set the initialisation flag to true
        bufferCPU.SetIsInitialised(true);
    }
    // Case where a buffer is included in the lod.
    // This is done when a specific object is used.
    else
    {
        // Get the buffer stored in the lod
        const SV::TriD::CPUBuffer<float>& lodBufferCPU = lod.GetCPUBuffer();

        // Check if we need to add the model matrix in the buffer
        if(lod.GetIncludeModelInCPUObjBuffer() )
        {
            // The model matrix is a 8x8 matrix
            // But we cannot store it as such in an obj buffer
            bufferCPU.AddData("in_Model_0", 4);
            bufferCPU.AddData("in_Model_1", 4);
            bufferCPU.AddData("in_Model_2", 4);
            bufferCPU.AddData("in_Model_3", 4);
        }

        // Loop on all the data stored in the buffer to construct the cpu buffer as a copy of the lod buffer
        for(int i=0, ie=lodBufferCPU.GetNumOfData(); i<ie; i++)
        {
            bufferCPU.AddData(lodBufferCPU.GetDataName(i), lodBufferCPU.GetData(i).GetSetSize() );
        }

        // Set the initialisation flag to true
        bufferCPU.SetIsInitialised(true);
    }
}

void Render::PutDataInInstanceBuffer(SV::TriD::CPUBuffer<float>& bufferCPU,
                                     const QMatrix4x4& mat,
                                     Run::VSceneNode* /*obj*/,
                                     LodModel& lod)
{
    // Check if there is a lod buffer defined in the lod.
    // If yes, then we use a specific object and a specific buffer transfer to the GPU
    // If no, then we use the classic way with preset buffer format (color instancing, model...)

    // "No" case
    if(lod.GetCPUBuffer().GetNumOfData() == 0)
    {
        // *****************************************
        // Put the model matrix in a float vector
        // *****************************************

        // Pointer a the start of the QMatrix4x4 data
        const float* dataPointer = mat.constData();

        // Model matrix data
        SV::TriD::Data<float>& matData = bufferCPU.GetData(0);

        // Loop on all the data within QMatrix4x4
        for(int j=0; j<16; j++)
        {
            matData.AddValue(*(dataPointer+j) );
        }

        if(lod.IsColorInstancing() )
        {
            // *****************************************
            // Color data
            // *****************************************
            const QColor& color = lod.GetColor();

            // Color data
            SV::TriD::Data<float>& colorData = bufferCPU.GetData(1);

            colorData.AddValue(float(color.redF() ) );
            colorData.AddValue(float(color.greenF() ) );
            colorData.AddValue(float(color.blueF() ) );
            colorData.AddValue(float(color.alphaF() ) );

            // *****************************************
            // Size data
            // *****************************************

            // Size data
            SV::TriD::Data<float>& sizeData = bufferCPU.GetData(2);

            // Always one here
            sizeData.AddValue(1);
        }

        if(lod.IsColorInstancing() )
        {
            // *****************************************
            // Color data
            // *****************************************

            const QColor& color = lod.GetColor();

            // Color data
            SV::TriD::Data<float>& colorData = bufferCPU.GetData(1);

            colorData.AddValue(float(color.redF() ) );
            colorData.AddValue(float(color.greenF() ) );
            colorData.AddValue(float(color.blueF() ) );
            colorData.AddValue(float(color.alphaF() ) );

            // *****************************************
            // Size data
            // *****************************************

            // Size data
            SV::TriD::Data<float>& sizeData = bufferCPU.GetData(2);

            // Always one here
            sizeData.AddValue(1);
        }
    }

    // "Yes" case
    else
    {
        // The CPU buffer structure was defined previously in the buffer initialisation function.
        // We now need to fill the buffer with actual data for each concerned object.
        // Here, we have access to everything (obj, lod, model matrix etc.) to fill it.

        // Loop on all the data stored in the CPU buffer
        for(int i=0, ie=bufferCPU.GetNumOfData(); i<ie; i++)
        {
            // Check if we need to add model matrix in the cpubuffer
            if(i==0 && lod.GetIncludeModelInCPUObjBuffer()==true)
            {
                // Pointer a the start of the QMatrix4x4 data
                const float* dataPointer = mat.constData();

                // Model matrix data
                SV::TriD::Data<float>& matData_0 = bufferCPU.GetData(0);
                SV::TriD::Data<float>& matData_1 = bufferCPU.GetData(1);
                SV::TriD::Data<float>& matData_2 = bufferCPU.GetData(2);
                SV::TriD::Data<float>& matData_3 = bufferCPU.GetData(3);

                // Loop on all the data within QMatrix4x4
                for(int j=0; j<4; j++)
                {
                    matData_0.AddValue(*(dataPointer+j) );
                }

                // Loop on all the data within QMatrix4x4
                for(int j=4; j<8; j++)
                {
                    matData_1.AddValue(*(dataPointer+j) );
                }

                // Loop on all the data within QMatrix4x4
                for(int j=8; j<12; j++)
                {
                    matData_2.AddValue(*(dataPointer+j) );
                }

                // Loop on all the data within QMatrix4x4
                for(int j=12; j<16; j++)
                {
                    matData_3.AddValue(*(dataPointer+j) );
                }

                i=3;
            }

            // Normal case automatically configured
            else
            {
                int index = lod.GetIncludeModelInCPUObjBuffer() ? i-4 : i;

                // Get the data to fill
                TriD::Data<float>& data = bufferCPU.GetData(i);

                // Get the lod data to be injected in the CPU buffer
                TriD::Data<float>& lodData = lod.GetCPUBuffer().GetData(index);

                // Fill the data by looping on all the float to be added
                for(int s=0, se=data.GetSetSize(); s<se; s++)
                {
                    data.AddValue(lodData.GetValue(s) );
                }
            }
        }
    }
}

}}
