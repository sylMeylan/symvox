add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Physics")
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Render")
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Sound")
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Management")
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Utils")

add_sources()

add_headers()

add_uis()
