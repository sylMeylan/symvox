/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef LOGIC_COMPONENT_LODMODEL_HH
#define LOGIC_COMPONENT_LODMODEL_HH

#include <string>
#include <QColor>

#include "SymVox/3D/Geometry/include/VPrimitive.hh"

namespace SV { namespace  Logic {

class LodModel
{
public:
    LodModel();
    ~LodModel();

    LodModel(const LodModel& toBeCopied);
    LodModel& operator=(const LodModel& toBeCopied);

    // Distance
    void SetDistance(float d) {fDistance=d;}
    float GetDistance() const {return fDistance;}

    // SizeIndicator
    void SetSizeIndicator(float s) {fSizeIndicator=s;}
    float GetSizeIndicator() const {return fSizeIndicator;}

    // Shaders
    void SetShaders(const std::string& vertFile, const std::string& fragFile, const std::string& geoFile="");
    bool GetForceShader() const {return fForceShader;}
    const std::string& GetVertexShader() const {return fVertexShaderFile;}
    const std::string& GetFragShader() const {return fFragShaderFile;}
    const std::string& GetGeoShader() const {return fGeoShaderFile;}

    // Multi mesh
    void SetIsMultiMesh(bool b) {fIsMultiMesh=b;}
    bool IsMultiMesh() {return fIsMultiMesh;}

    // Loader
    void SetIsLoader(bool b) {fIsLoader=b;}
    bool IsLoader() {return fIsLoader;}

    // Show children
    void SetShowChildren(bool b) {fShowChildren=b;}
    bool IsShowChildren() {return fShowChildren;}

    // 3dType
    void Set3dType(const std::string& type3d) {f3dType=type3d;}
    const std::string& Get3dType() const {return f3dType;}
    void Set3dTypeId(int id) {f3dTypeId=id;}
    int Get3dTypeId() const {return f3dTypeId;}

    // Child forced index
    void SetChildForcedIndex(int i) {fChildForcedIndex=i;}
    int GetChildForcedIndex() const {return fChildForcedIndex;}

    // Color
    void SetColor(const QColor& color) {fColor=color;}
    const QColor& GetColor() const {return fColor;}

    // Color instancing
    void SetColorInstancing(bool b) {fIsColorInstancing=b;}
    bool IsColorInstancing() const {return fIsColorInstancing;}

    // Primitive
    void SetPrimitive(SV::TriD::VPrimitive* primitive) {fpPrimitive=primitive;}
    const SV::TriD::VPrimitive* GetPrimitive() const {return fpPrimitive;}
    SV::TriD::VPrimitive* GetPrimitive() {return fpPrimitive;}

    // CPU buffer
    void SetCPUBuffer(const SV::TriD::CPUBuffer<float>& buffer);
    const SV::TriD::CPUBuffer<float>& GetCPUBuffer() const {return fCPUBuffer;}
    SV::TriD::CPUBuffer<float>& GetCPUBuffer() {return fCPUBuffer;}

    // Automatically include model matrix data in CPU buffer
    void SetIncludeModelInCPUObjBuffer(bool b) {fIncludeModelInCPUObjBuffer=b;}
    bool GetIncludeModelInCPUObjBuffer() const {return fIncludeModelInCPUObjBuffer;}

private:

    bool fIsMultiMesh;
    bool fIsColorInstancing;
    bool fIsLoader;
    bool fShowChildren;
    bool fForceShader;
    float fDistance;
    float fSizeIndicator;

    bool fIncludeModelInCPUObjBuffer;
    SV::TriD::CPUBuffer<float> fCPUBuffer;

    int f3dTypeId;
    int fChildForcedIndex;

    std::string f3dType;
    std::string fVertexShaderFile;
    std::string fFragShaderFile;
    std::string fGeoShaderFile;

    QColor fColor;

    // Contains the geometrical informations of the object (shape...)
    SV::TriD::VPrimitive* fpPrimitive;
};

}}

#endif // LOGIC_COMPONENT_LODMODEL_HH
