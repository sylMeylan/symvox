/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef LOGIC_COMPONENT_VRENDERABLE_HH
#define LOGIC_COMPONENT_VRENDERABLE_HH

#include "SymVox/Logic/Component/Management/include/VComponent.hh"
#include "SymVox/Logic/Component/Management/include/ComponentCRTP.hh"

#include "LodManager.hh"

namespace SV { namespace  Logic {

class Renderable : public ComponentCRTP<VComponent, Renderable>
{
public:
    Renderable();
    ~Renderable() override;

    Renderable(const Renderable& toBeCopied);
    Renderable& operator=(const Renderable& toBeCopied);

    // LodManager
    void SetLodManager(const LodManager& lodManager) {fLodManager=lodManager;}
    const LodManager& GetLodManager() const {return fLodManager;}
    LodManager& GetLodManager() {return fLodManager;}

    // Methods to add new lod models
    void AddLod(float distance,
                const std::string& name,
                SV::TriD::VPrimitive* primitive=nullptr,
                bool isMultiMesh=false,
                bool isColorInstancing=false,
                const std::string& vertexShader="",
                const std::string& fragShader="");

    void AddLod(const Logic::LodModel& lodModel);
    void InsertLod(unsigned int i, const Logic::LodModel& lodModel);

    // Remove
    void RemoveLod(int index);
    void RemoveAllLods();

    // Gets
    LodModel& GetLod(int index);
    const LodModel& GetLod(int index) const;
    int GetNumOfLods() const;

    // Lod retrieval as a function of distance
    LodModel& LodForDistance(float distance);
    const LodModel& LodForDistance(float distance) const;

    // Lod color
    void SetLodColorForAll(const QColor& color, bool keepAlpha=false);

    void SetPrimitive(SV::TriD::VPrimitive* primitive) {fpPrimitive=primitive;}
    SV::TriD::VPrimitive* GetPrimitive() {return fpPrimitive;}
    const SV::TriD::VPrimitive* GetPrimitive() const {return fpPrimitive;}

private:
    SV::TriD::VPrimitive* fpPrimitive;
    LodManager fLodManager;
};

}}

#endif // LOGIC_COMPONENT_VRENDERABLE_HH
