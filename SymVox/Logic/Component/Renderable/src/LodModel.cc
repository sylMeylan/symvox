/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/LodModel.hh"

namespace SV { namespace  Logic {

LodModel::LodModel()
{
    f3dTypeId  = -1;
    fChildForcedIndex = -1;

    fIsMultiMesh = false;
    fIsColorInstancing = false;
    fIsLoader = false;
    fShowChildren = true;
    fForceShader = false;
    fDistance = 0;
    fSizeIndicator = 1;
    fIncludeModelInCPUObjBuffer = false;

    fpPrimitive = nullptr;

    f3dType = "";
    fVertexShaderFile = "";
    fFragShaderFile = "";
    fGeoShaderFile = "";
}

LodModel::LodModel(const LodModel& toBeCopied)
{
    if(&toBeCopied != this)
    {
        f3dTypeId = toBeCopied.f3dTypeId;
        fChildForcedIndex = toBeCopied.fChildForcedIndex;
        fForceShader = toBeCopied.fForceShader;
        fIsMultiMesh = toBeCopied.fIsMultiMesh;
        fIsColorInstancing = toBeCopied.fIsColorInstancing;
        fIsLoader = toBeCopied.fIsLoader;
        fShowChildren = toBeCopied.fShowChildren;
        fDistance = toBeCopied.fDistance;
        fSizeIndicator = toBeCopied.fSizeIndicator;
        fColor = toBeCopied.fColor;
        fCPUBuffer = toBeCopied.fCPUBuffer;
        fIncludeModelInCPUObjBuffer = toBeCopied.fIncludeModelInCPUObjBuffer;

        if(toBeCopied.fpPrimitive != nullptr)
            fpPrimitive = toBeCopied.fpPrimitive->Clone();
        else
            fpPrimitive = nullptr;

        f3dType = toBeCopied.f3dType;
        fVertexShaderFile = toBeCopied.fVertexShaderFile;
        fFragShaderFile = toBeCopied.fFragShaderFile;
        fGeoShaderFile = toBeCopied.fGeoShaderFile;
    }
}

LodModel::~LodModel()
{
    if(fpPrimitive != nullptr)
    {
        delete fpPrimitive;
        fpPrimitive = nullptr;
    }
}

LodModel &LodModel::operator=(const LodModel &toBeCopied)
{
    if(&toBeCopied != this)
    {
        f3dTypeId = toBeCopied.f3dTypeId;
        fChildForcedIndex = toBeCopied.fChildForcedIndex;
        fForceShader = toBeCopied.fForceShader;
        fIsMultiMesh = toBeCopied.fIsMultiMesh;
        fIsColorInstancing = toBeCopied.fIsColorInstancing;
        fIsLoader = toBeCopied.fIsLoader;
        fShowChildren = toBeCopied.fShowChildren;
        fDistance = toBeCopied.fDistance;
        fSizeIndicator = toBeCopied.fSizeIndicator;
        fColor = toBeCopied.fColor;
        fCPUBuffer = toBeCopied.fCPUBuffer;
        fIncludeModelInCPUObjBuffer = toBeCopied.fIncludeModelInCPUObjBuffer;

        if(toBeCopied.fpPrimitive != nullptr)
            fpPrimitive = toBeCopied.fpPrimitive->Clone();
        else
            fpPrimitive = nullptr;

        f3dType = toBeCopied.f3dType;
        fVertexShaderFile = toBeCopied.fVertexShaderFile;
        fFragShaderFile = toBeCopied.fFragShaderFile;
        fGeoShaderFile = toBeCopied.fGeoShaderFile;
    }

    return *this;
}

void LodModel::SetShaders(const std::string &vertFile, const std::string &fragFile, const std::string &geoFile)
{
    if(vertFile == ""
            && fragFile == ""
            && geoFile == ""
            )
    {
        fForceShader = false;
    }
    else
    {
        fVertexShaderFile = vertFile;
        fFragShaderFile = fragFile;
        fGeoShaderFile = geoFile;
        fForceShader = true;
    }
}

void LodModel::SetCPUBuffer(const SV::TriD::CPUBuffer<float> &buffer)
{
    fCPUBuffer = buffer;
}

}}
