/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/LodManager.hh"

namespace SV { namespace  Logic {

LodManager::LodManager()
{

}

LodManager::~LodManager()
{

}

void LodManager::AddLod(const LodModel& lod)
{
    fLods.push_back(lod);
}

void LodManager::InsertLod(unsigned int i, const LodModel &lod)
{
    fLods.insert(fLods.begin()+i, lod);
}

void LodManager::RemoveLod(int index)
{
    fLods.erase(fLods.begin()+index);
}

void LodManager::RemoveAllLods()
{
    fLods.clear();
    fLods.shrink_to_fit();
}

int LodManager::GetNumOfLods() const
{
    return fLods.size();
}

int LodManager::GetIndexToDisplay(float distance) const
{
    // Compare distance camera-object to the ones stored in the ranges container.
    // Select the interval in which the distance is and return the corresponding lod index.

    float upperIntervalLimit (0.0f);

    int intervalIndex (-1);

    int size = GetNumOfLods();

    while(distance >= upperIntervalLimit && size > intervalIndex+1)
    {
        ++intervalIndex;

        upperIntervalLimit = GetLod(intervalIndex).GetDistance();
    }

    return intervalIndex;
}

}}
