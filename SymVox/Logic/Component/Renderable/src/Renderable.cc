/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Renderable.hh"

namespace SV { namespace  Logic {

Renderable::Renderable() : ComponentCRTP<VComponent, Renderable>()
{
    SetName("Renderable");
    SetTypeId(List::Renderable);

    fpPrimitive = nullptr;
}

Renderable::~Renderable()
{
    if(fpPrimitive != nullptr)
    {
        delete fpPrimitive;
        fpPrimitive = nullptr;
    }
}

Renderable::Renderable(const Renderable &toBeCopied)
{
    fLodManager = toBeCopied.fLodManager;

    if(toBeCopied.fpPrimitive != nullptr)
        fpPrimitive = toBeCopied.fpPrimitive->Clone();
    else
        fpPrimitive = nullptr;
}

Renderable &Renderable::operator=(const Renderable &toBeCopied)
{
    if(this != &toBeCopied)
    {
        fLodManager = toBeCopied.fLodManager;

        if(toBeCopied.fpPrimitive != nullptr)
            fpPrimitive = toBeCopied.fpPrimitive->Clone();
        else
            fpPrimitive = nullptr;
    }

    return *this;
}

void Renderable::AddLod(float distance,
                        const std::string& name,
                        SV::TriD::VPrimitive* primitive,
                        bool isMultiMesh,
                        bool isColorInstancing,
                        const std::string& vertexShader,
                        const std::string& fragShader)
{
    LodModel lod;
    lod.SetDistance(distance);
    lod.Set3dType(name);
    lod.SetPrimitive(primitive);
    lod.SetIsMultiMesh(isMultiMesh);
    lod.SetColorInstancing(isColorInstancing);
    lod.SetShaders(vertexShader, fragShader);

    fLodManager.AddLod(lod);
}

void Renderable::AddLod(const LodModel &lodModel)
{
    fLodManager.AddLod(lodModel);
}

void Renderable::InsertLod(unsigned int i, const LodModel &lodModel)
{
    fLodManager.InsertLod(i, lodModel);
}

void Renderable::RemoveLod(int index)
{
    fLodManager.RemoveLod(index);
}

void Renderable::RemoveAllLods()
{
    fLodManager.RemoveAllLods();
}

LodModel& Renderable::GetLod(int index)
{
    return fLodManager.GetLod(index);
}

const LodModel& Renderable::GetLod(int index) const
{
    if(index > GetNumOfLods() )
    {
        std::ostringstream oss;
        oss << "The renderable has " << GetNumOfLods() << " lods stored and was asked a lod with an index of: "
            << index;
        Utils::Exception(Utils::ExceptionType::Fatal, "Renderable::GetLod",
                         oss.str()
                         );
    }

    return fLodManager.GetLod(index);
}

int Renderable::GetNumOfLods() const
{
    return fLodManager.GetNumOfLods();
}

LodModel& Renderable::LodForDistance(float distance)
{
    return fLodManager.GetLod(fLodManager.GetIndexToDisplay(distance) );
}

const LodModel&Renderable::LodForDistance(float distance) const
{
    return fLodManager.GetLod(fLodManager.GetIndexToDisplay(distance) );
}

void Renderable::SetLodColorForAll(const QColor &color, bool keepAlpha)
{
    // Loop on all the lod of the renderable and set their color.
    for(unsigned int i=0, ie=GetNumOfLods(); i<ie; i++)
    {
        Logic::LodModel& lod = GetLod(i);

        if(keepAlpha)
        {
            int alpha = lod.GetColor().alpha();
            lod.SetColor(QColor(color.red(), color.green(), color.blue(), alpha) );
        }
        else
            lod.SetColor(color);
    }
}

}}
