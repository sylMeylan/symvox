/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/VComponent.hh"

#include "SymVox/Run/Scene/Graph/include/VSceneNode.hh"

namespace SV { namespace  Logic {

VComponent::VComponent() :
    fTypeId(List::Nothing),
    fName("noName"),
    fpOwnerNode(nullptr)
{

}

VComponent::VComponent(const std::string& name) :
    fTypeId(List::Nothing),
    fName(name),
    fpOwnerNode(nullptr)
{

}

VComponent::VComponent(const VComponent &toBeCopied)
{
    fTypeId = toBeCopied.fTypeId;
    fName = toBeCopied.fName;
    fpOwnerNode = nullptr;
}

VComponent::~VComponent()
{

}

void VComponent::SetOwnerNode(Run::VSceneNode* node)
{
    if(fpOwnerNode==nullptr)
        fpOwnerNode = node;
    else
    {
        std::ostringstream oss;
        oss << "An owner node was already set . We cannot set a second one because of memory management. ";
        oss << "The node "<<node->GetName()<<" cannot be set as owner.";
        Utils::Exception(Utils::ExceptionType::Fatal, "VSetOwnerNode",
                         oss.str() );
    }
}

}}

