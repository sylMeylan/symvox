#add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Management")

add_sources(
    ThreadApp.cc
    Task.cc
)

add_headers(
    ThreadApp.hh
    Task.hh
)

add_uis()
