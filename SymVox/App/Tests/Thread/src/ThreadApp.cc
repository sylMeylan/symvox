/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/ThreadApp.hh"

namespace SV {

ThreadApp::ThreadApp() : VAppManager()
{
    fAppType = AppType::Console;
}

ThreadApp::~ThreadApp()
{

}

void ThreadApp::Load()
{

}

void ThreadApp::Unload()
{

}

void ThreadApp::Process()
{
    Task* t = new Task();

    Run::ThreadPool* threadPool = Run::ThreadPoolManager::Instance()->GetThreadPool();

    int initialActiveThreadNum = threadPool->GetActiveThreadCount();

    std::cout<< "init: "<<initialActiveThreadNum << " for "
             << threadPool->GetMaxThreadCount()
             << " max threads. "
             << std::endl;
    std::cout<< "Main thread has is: " << QThread::currentThread() << std::endl;

    threadPool->Start(t);
    threadPool->Start(t);

    std::cout<< "thread count after: "<<threadPool->GetActiveThreadCount() << "\n";

    // Wait for all the threads to end before going on.
    // This is where we ensure the synchronization of the system.
    while(initialActiveThreadNum < threadPool->GetActiveThreadCount() )
    {
        // Wait for 1 ms to avoid surchargin the main loop with useless work.
        QThread::msleep(1);
    }

    delete t;

    //delete t;
}

}
