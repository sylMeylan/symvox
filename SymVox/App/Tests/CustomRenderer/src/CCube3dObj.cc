/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/CCube3dObj.hh"
#include <cmath>

using namespace SV;
using namespace TriD;
using namespace TriD;

CCube3dObj::CCube3dObj(AssetsManager* assetsManager) : Object(assetsManager)
{

}

CCube3dObj::~CCube3dObj()
{

}

void CCube3dObj::Load()
{
    this->initializeOpenGLFunctions();

    // Set the references for the current mesh entry

    for(int i=0; i<GetNumOfMeshEntries(); i++)
    {
        MeshEntry& meshEntry = GetMeshEntry(i);

        VAO& vao = meshEntry.fVao;
        Mesh& mesh = meshEntry.fMesh;
        GPUObj3DBuffer& objBuffGPU = meshEntry.fObjBuffGPU;
        GPUIndexBuffer& indexBuffGPU = meshEntry.fIndexBuffGPU;
        GPUInstanceBuffer& instanceBuffGPU = meshEntry.fInstanceBuffGPU;

        meshEntry.fConnectivitySize = meshEntry.fMesh.Connectivity().size();

        if(vao.IsExisting() )
            vao.Delete();

        vao.Init();

        // Create a buffer on the CPU with the mesh data
        CPUBuffer<float> object3DBuffer("ObjectBuffer", GL_ARRAY_BUFFER, GL_STATIC_DRAW, GL_FLOAT);
        object3DBuffer.AddData("Positions", mesh.Positions(), 3); // 0
        object3DBuffer.AddData("Colors", mesh.Colors(), 4); // 1
        object3DBuffer.AddData("TextureCoord", mesh.TexCoords(), 2); //  2
        object3DBuffer.AddData("Normals", mesh.Normals(), 3); // 3

        // Send the CPU buffer on the GPU thanks to the bufferGPU class.
        objBuffGPU.SetVAO(&vao);
        objBuffGPU.SendToGPU(object3DBuffer);

        // Index buffer

        CPUBuffer<unsigned int> indexBuffCPU("IndexBuffer", GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW, GL_UNSIGNED_INT);
        indexBuffCPU.AddData("Index", mesh.Connectivity(), 0);

        indexBuffGPU.SetVAO(&vao);
        indexBuffGPU.SendToGPU(indexBuffCPU);

        // Instance buffer

        instanceBuffGPU.SetVAO(&vao);
        meshEntry.fUpdateInstanceBuffer = true;
    }

    DirectionalLight* dirLight = new DirectionalLight;
    // Ambient, diffuse, specular
    dirLight->SetLight(QVector3D(0.8f, 0.8f, 0.8f), QVector3D(0.2f, 0.2f, 0.2f), QVector3D(1.0f, 1.0f, 1.0f) );
    fLightManager.AddDirLight("SunLight", dirLight);

    PointLight* pL = new PointLight;
    pL->SetLight(QVector3D(0.5f, 0.5f, 0.5f), QVector3D(1.0f, 1.0f, 1.0f), QVector3D(1.0f, 1.0f, 1.0f) );
    pL->SetAttenuation(1.0f, 0.1f, 0.01f);
    pL->SetSourcePoint(QVector3D(10,0,10) );
    fLightManager.AddPointLight("PointLight", pL);

    SpotLight* spotLight1 = new SpotLight;
    // Ambient, diffuse, specular
    spotLight1->SetLight(QVector3D(0.0f, 0.0f, 0.0f), QVector3D(1.0f, 1.0f, 1.0f), QVector3D(1.0f, 1.0f, 1.0f) );
    // Constant, linear, quadratic
    spotLight1->SetAttenuation(1.0f, 0.09f, 0.032f);
    // Position
    spotLight1->SetSourcePoint(QVector3D(10.0f, 10.0f, 0.0f) );
    // Direction, cutOff, outerCutOff
    spotLight1->SetSpot(QVector3D(0.0f,-10.0f,0.0f), std::cos(12.5f/180*M_PI), std::cos(15.0f/180*M_PI) );
    fLightManager.AddSpotLight("spot1", spotLight1);

    SpotLight* spotLight2 = new SpotLight;
    // Ambient, diffuse, specular
    spotLight2->SetLight(QVector3D(0.0f, 0.0f, 0.0f), QVector3D(0.5f, 0.5f, 0.5f), QVector3D(1.0f, 1.0f, 1.0f) );
    // Constant, linear, quadratic
    spotLight2->SetAttenuation(1.0f, 0.045f, 0.0075f);
    // Position
    spotLight2->SetSourcePoint(QVector3D(0.0f, -10.0f, 0.0f) );
    // Direction, cutOff, outerCutOff
    spotLight2->SetSpot(QVector3D(0.0f,1.0f,0.0f), std::cos(12.5f/180*M_PI), std::cos(15.0f/180*M_PI) );
    fLightManager.AddSpotLight("spot2", spotLight2);


}

void CCube3dObj::Display(const QMatrix4x4& model, const QMatrix4x4& view, const QMatrix4x4& proj, const QVector3D& cameraPos, LightManager& lightManager)
{
    for(int i=0; i<GetNumOfMeshEntries(); i++)
    {
        //glDisable(GL_CULL_FACE);

        MeshEntry& meshEntry = GetMeshEntry(i);

        VAO& vao = meshEntry.fVao;
        GPUIndexBuffer& indexBuffGPU = meshEntry.fIndexBuffGPU;
        int& textureDiffuseId = meshEntry.fTextureDiffuseId;
        int& textureSpecularId = meshEntry.fTextureSpecularId;
        int& textureEmissionId = meshEntry.fTextureEmissionId;
        int& shaderId = GetMeshEntry(0).fShaderId;
        unsigned int& connectivitySize = meshEntry.fConnectivitySize;

        // Shader activation

        if(shaderId==-1)
        {
            Utils::Exception(Utils::ExceptionType::Fatal, "Object::Display",
                             "fShaderId=-1 and, thus, was not defined for"+fName);
        }

        // Retrieve the shader
        Shader* shader = fpAssetsManager->GetShader(shaderId);

        shader->Use();

        // Lock the VAO
        vao.Bind();

        // Send the uniform variables to the shader
        shader->Send4fv("model", model);
        shader->Send4fv("view", view);
        shader->Send4fv("projection", proj);

        shader->Send3f("viewPos", cameraPos.x(), cameraPos.y(), cameraPos.z());

        // Light management

        lightManager.SendLightsToShader(shader);

        // Set material properties
        //shader->Send3f("material.ambient", 1.0f, 0.5f, 0.31f);
        shader->Send1i("material_diffuse", 0); // Texture
        shader->Send1i("material_specular", 1); // Texture
        shader->Send1i("material_emission", 2); // Texture
        shader->Send1f("material.shininess", 32.0f); // Default

        // Lock the VBO
        // We bind the index buffer to allow indexing.
        indexBuffGPU.GetVbo()->Bind();

        // Bind texture if any

        Texture* textureDiffuse = nullptr;

        if(textureDiffuseId != -1)
        {
            textureDiffuse = fpAssetsManager->GetTexture(textureDiffuseId);
            textureDiffuse->Bind(GL_TEXTURE0);
        }

        Texture* textureSpecular = nullptr;

        if(textureSpecularId != -1)
        {
            textureSpecular = fpAssetsManager->GetTexture(textureSpecularId);
            textureSpecular->Bind(GL_TEXTURE1);
        }

        Texture* textureEmission = nullptr;

        if(textureEmissionId != -1)
        {
            textureEmission = fpAssetsManager->GetTexture(textureEmissionId);
            textureEmission->Bind(GL_TEXTURE2);
        }

        glDrawElements(
                    GL_TRIANGLES,           // mode
                    connectivitySize,   // count
                    GL_UNSIGNED_INT,        // type
                    (void*)0              // offset
                    );

        // Unbind texture if a texture was binded before
        if(textureDiffuse != nullptr)
            textureDiffuse->Unbind();
        if(textureSpecular != nullptr)
            textureSpecular->Unbind();
        if(textureEmission != nullptr)
            textureEmission->Unbind();

        // Unlock the VBO
        indexBuffGPU.GetVbo()->Unbind();

        // Unlock the VAO
        vao.Unbind();

        // Deactivate shader
        shader->Unuse();

        //glEnable(GL_CULL_FACE);
    }


}

void CCube3dObj::DisplayAsOutlined(const QMatrix4x4& model, const QMatrix4x4& view, const QMatrix4x4& proj, const QVector3D& cameraPos)
{
    // Stencil buffer use

    glEnable(GL_STENCIL_TEST);

    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

    glClear(GL_STENCIL_BUFFER_BIT);

    glStencilFunc(GL_ALWAYS, 1, 0xFF); // All fragments should update the stencil buffer
    glStencilMask(0xFF); // Enable writing to the stencil buffer

    // Render the cube
    Display(model, view, proj, cameraPos, fLightManager);

    glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
    glStencilMask(0x00); // Disable writing to the stencil buffer
    //glDisable(GL_DEPTH_TEST);

    int oldShaderID = this->GetShaderID();
    int newShaderID = fpAssetsManager->AutoAddShader(":/SymVox/3D/Shaders/multi_lights.vert", ":/SymVox/3D/Shaders/singleColor.frag", "", {"in_Vertex", "in_Color", "in_TextureCoord", "in_NormalCoord"} );
    this->SetShaderId(newShaderID);

    QMatrix4x4 newModel = model;
    newModel.scale(1.2);

    // Render the ouline
    Display(newModel, view, proj, cameraPos, fLightManager);

    this->SetShaderId(oldShaderID);

    glStencilMask(0xFF);

    //glEnable(GL_DEPTH_TEST);

    glDisable(GL_STENCIL_TEST);

    //glStencilMask(0x00);
}
