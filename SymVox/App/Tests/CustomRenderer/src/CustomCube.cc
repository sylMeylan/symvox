/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/CustomCube.hh"

#include "SymVox/3D/Geometry/include/Loader.hh"

using namespace SV;
using namespace TriD;

CustomCube::~CustomCube()
{

}

VObject* CustomCube::CreateObject(AssetsManager* assetsManager)
{
    // Create the new obj
    CCube3dObj* obj = new CCube3dObj(assetsManager);

    // Add a mesh entry
    obj->AddMeshEntry();

    // *********************
    // Mesh construction
    // *********************

    // Get its mesh object to modify it
    Mesh* mesh = &(obj->GetMesh() );

    QVector3D diff;

    diff =  - fXAxis - fYAxis - fZAxis;
    VertID v000 = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,0) );//
    VertID v000bis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,1) );//
    VertID v000bisbis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,0) );//

    diff =    fXAxis - fYAxis - fZAxis;
    VertID v001 = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,0) );//
    VertID v001bis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,0) );//
    VertID v001bisbis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,1) );//

    diff =  - fXAxis + fYAxis - fZAxis;
    VertID v010 = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,1) );//
    VertID v010bis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,1) );//
    VertID v010bisbis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,1) );//

    diff =    fXAxis + fYAxis - fZAxis;
    VertID v011 = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,1) );//
    VertID v011bis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,1) );//
    VertID v011bisbis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,1) );//

    diff =  - fXAxis - fYAxis + fZAxis;
    VertID v100 = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,0) );//
    VertID v100bis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,0) );//
    VertID v100bisbis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,0) );//

    diff =    fXAxis - fYAxis + fZAxis;
    VertID v101 = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,0) );//
    VertID v101bis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,0) );//
    VertID v101bisbis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,0) );//

    diff =  - fXAxis + fYAxis + fZAxis;
    VertID v110 = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,1) );//
    VertID v110bis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,1) );//
    VertID v110bisbis = mesh->AddVertex2(fOrigin + diff, diff.normalized(),  QVector2D(0,0) );//

    diff =    fXAxis + fYAxis + fZAxis;
    VertID v111 = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(0,1) );//
    VertID v111bis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,1) );//
    VertID v111bisbis = mesh->AddVertex2(fOrigin + diff, diff.normalized(), QVector2D(1,0) );//

    AddFaceInv(mesh, v000, v001, v011);
    mesh->AddFace(v000, v010, v011);//

    mesh->AddFace(v101, v001bis, v011bis);
    AddFaceInv(mesh, v101, v111, v011bis);//

    AddFaceInv(mesh, v100, v101bis, v001bisbis);
    mesh->AddFace(v100, v000bis, v001bisbis);//

    mesh->AddFace(v100bis, v101bis, v111bis);
    AddFaceInv(mesh, v100bis, v110, v111bis);//

    mesh->AddFace(v000bisbis, v100bisbis, v110bis);
    AddFaceInv(mesh, v000bisbis, v010bis, v110bis);//

    mesh->AddFace(v110bisbis, v111bisbis, v011bisbis);
    AddFaceInv(mesh, v110bisbis, v010bisbis, v011bisbis); //

    mesh->FillColor(QColor(255,0,0,255));

    mesh->ComputeNormals();

    // *********************
    // Shader inclusion
    // *********************

    //std::string vertShader ("3D/Shaders/basic_dir_light.vert");
    //std::string fragShader ("3D/Shaders/basic_dir_light.frag");
    std::string vertShader (":/SymVox/3D/Shaders/multi_lights.vert");
    std::string fragShader (":/SymVox/3D/Shaders/multi_lights.frag");

    int shaderId = assetsManager->AutoAddShader(vertShader, fragShader, "", {"in_Vertex", "in_Color", "in_TextureCoord", "in_NormalCoord"} );
    obj->SetShaderId(shaderId);

    // ********************
    // Texture
    // ********************

    int texId = assetsManager->AutoAddTexture("Data/Assets/container2");
    obj->SetTextureDiffuseId(texId);
    assetsManager->GetTexture(texId)->Load();

    texId = assetsManager->AutoAddTexture("Data/Assets/container2_specular");
    obj->SetTextureSpecularId(texId);
    assetsManager->GetTexture(texId)->Load();

//    texId = assetsManager->AutoAddTexture("Data/Assets/matrix");
//    obj->SetTextureEmissionId(texId);
//    assetsManager->GetTexture(texId)->SetIsMirrored(true);
//    assetsManager->GetTexture(texId)->Load();

    // Return the object 3d
    return obj;
}

VObject* CustomCube::CreateObjectLoader(AssetsManager* assetsManager)
{
    Loader loader("Data/Assets/Nanosuit/nanosuit.obj");

    // Force the creation of a CCube3dObj and not of an Object (classic type)
    VObject* obj = loader.ManuallyCreateObject<CCube3dObj>(assetsManager);//  CreateObject<CCube3dObj>(assetsManager);

    // *********************
    // Shader inclusion
    // *********************

    //std::string vertShader ("3D/Shaders/basic_dir_light.vert");
    //std::string fragShader ("3D/Shaders/basic_dir_light.frag");
    std::string vertShader (":/SymVox/3D/Shaders/multi_lights.vert");
    std::string fragShader (":/SymVox/3D/Shaders/multi_lights.frag");
    //std::string vertShader (":/SymVox/3D/Shaders/multi_lights_instancing_tex.vert");
    //std::string fragShader (":/SymVox/3D/Shaders/multi_lights_instancing_tex.frag");

    int shaderId = assetsManager->AutoAddShader(vertShader, fragShader, "", {"in_Vertex", "in_Color", "in_TextureCoord", "in_NormalCoord"} );
    obj->SetShaderId(shaderId);

    return obj;
}

int CustomCube::AddFaceInv(Mesh* mesh, VertID a, VertID b, VertID c)
{
    return mesh->AddFace(c, b, a);
}
