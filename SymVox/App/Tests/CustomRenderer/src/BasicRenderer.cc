/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/BasicRenderer.hh"

namespace SV {

BasicRenderer::BasicRenderer() : Logic::Render(), Utils::OpenGL()
{

}

BasicRenderer::~BasicRenderer()
{
    delete fpCube3d;
    delete fpNanosuit;
}

void BasicRenderer::Initialize()
{
    // Check that all the pointers are set

    if(fpWorld==nullptr || fpCamera==nullptr || fpViewOpenGL==nullptr)
    {
        std::stringstream ss;
        ss << "At least one of the following pointer was not initialised properly. You should call the appropriate Set() method.\n";
        ss <<"fpWorld="<<fpWorld<<", fpCamera="<<fpCamera<<", fpViewOpenGL="<<fpViewOpenGL;
        Utils::Exception(Utils::ExceptionType::Fatal, "Render::Initialise()", ss.str() );
    }

    // ...

    CustomCube cubePrimitive(1);
    fpCube3d = cubePrimitive.CreateObject(&fAssetsManager);

    fpCube3d->Load();

    fpNanosuit = cubePrimitive.CreateObjectLoader(&fAssetsManager);
    fpNanosuit->Load();

    // ----------------------------

    this->initializeOpenGLFunctions();

    fpShader = new TriD::Shader(":/SymVox/3D/Shaders/fbo_quad.vert",
                              ":/SymVox/3D/Shaders/fbo_quad.frag"
                                            );
    fpShader->Load();



    float quadVertices[] = { // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
            // positions   // texCoords
            -1.0f,  1.0f,  0.0f, 1.0f,
            -1.0f, -1.0f,  0.0f, 0.0f,
             1.0f, -1.0f,  1.0f, 0.0f,

            -1.0f,  1.0f,  0.0f, 1.0f,
             1.0f, -1.0f,  1.0f, 0.0f,
             1.0f,  1.0f,  1.0f, 1.0f
    };

    // screen quad VAO
    unsigned int quadVBO;
    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &quadVBO);
    glBindVertexArray(quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

    // // //

    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

    // generate texture
    glGenTextures(1, &texColorBuffer2);
    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_2D, texColorBuffer2);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2000, 2000, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    // attach it to currently bound framebuffer object
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texColorBuffer2, 0);

    unsigned int rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 2000, 2000);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void BasicRenderer::Process()
{
    // **********************************
    // Display the world on the screen
    // **********************************

    fpViewOpenGL->Clean();
    fpViewOpenGL->Resize();
    fpViewOpenGL->SaveViewMat();

    // ------------------------------------
    // first pass
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // we're not using the stencil buffer now
    glEnable(GL_DEPTH_TEST);

    // ------------------------------------

    fpCamera->LookAt(fpViewOpenGL->GetView() );

    // Render the skybox
    if(fpSkyBox != nullptr)
        fpSkyBox->Display(QMatrix4x4(),fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition(), fLightManager);

    // Render all the world objects

    QMatrix4x4 model;
    model.setToIdentity();
    model.translate(-10,0,0);
    static_cast<CCube3dObj*> (fpCube3d) ->Display(model, fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition(), fLightManager);

    for(int i=0; i<20; i++)
    {
        model.translate(1.2,0,0);
        static_cast<CCube3dObj*> (fpCube3d) ->Display(model, fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition(), fLightManager);
    }

    model.setToIdentity();

    model.translate(0,3,0);
    static_cast<CCube3dObj*> (fpCube3d) ->Display(model, fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition(), fLightManager);

    model.translate(0,3,0);
    static_cast<CCube3dObj*> (fpCube3d) ->DisplayAsOutlined(model, fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition() );
    //->Display(model, fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition() );

    model.setToIdentity();
    model.translate(0, 0, -10);
    dynamic_cast<CCube3dObj*> (fpNanosuit) ->Display(model, fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition(), fLightManager);
    //fpNanosuit ->Display(model, fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition(), fLightManager);

    // ----------------

    // second pass
    glBindFramebuffer(GL_FRAMEBUFFER, 0); // back to default
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    fpShader->Use();
    fpShader->Send1i("screenTexture", 10);
    glBindVertexArray(quadVAO);
    glDisable(GL_DEPTH_TEST);
    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_2D, texColorBuffer2);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    fpShader->Unuse();

    // ----------------

    fpViewOpenGL->LoadSavedViewMat();
    fpViewOpenGL->Refresh();

}

}
