/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/BasicRendererApp.hh"

namespace SV {

BasicRendererApp::BasicRendererApp() : VAppManager()
{

}

BasicRendererApp::~BasicRendererApp()
{

}

void BasicRendererApp::Load()
{
    fpCamera = new GUI::Camera(QVector3D(0,0,-30), QVector3D(0,10,0), QVector3D(0,1,0), 1., 5);

    fpWorldNode = new Run::SceneNode();
    fpWorldNode->SetName("World");
    fpWorldNode->AddComponent(new Logic::Renderable(), Logic::List::Renderable);

    // ...

    //fpCameraMouseStyle = new GUI::MouseStyle("mouseStyle", fpViewOpenGL, fpCamera);
    fpCameraMouseStyle = new GUI::GrabStyle("grabStyle", fpViewOpenGL, fpCamera);

    fpSysRender = new BasicRenderer();

    SV::TriD::LightManager* lightManager = fpSysRender->GetLightManager();

    SV::TriD::DirectionalLight* dirLight = new SV::TriD::DirectionalLight();
    // Ambient, diffuse, specular
    dirLight->SetDirection(QVector3D(0,0,-1) );
    dirLight->SetLight(QVector3D(0.8f, 0.8f, 0.8f), QVector3D(0.5f, 0.5f, 0.5f), QVector3D(1.0f, 1.0f, 1.0f) );
    lightManager->AddDirLight("SunLight", dirLight);

    SV::TriD::PointLight* pL = new SV::TriD::PointLight();
    pL->SetLight(QVector3D(0.5f, 0.5f, 0.5f), QVector3D(1.0f, 1.0f, 1.0f), QVector3D(1.0f, 1.0f, 1.0f) );
    pL->SetAttenuation(1.0f, 0.1f, 0.01f);
    pL->SetSourcePoint(QVector3D(0,0,1000) );
    lightManager->AddPointLight("PointLight", pL);

    SV::TriD::SpotLight* spotLight1 = new SV::TriD::SpotLight();
    // Ambient, diffuse, specular
    spotLight1->SetLight(QVector3D(0.0f, 0.0f, 0.0f), QVector3D(1.0f, 1.0f, 1.0f), QVector3D(1.0f, 1.0f, 1.0f) );
    // Constant, linear, quadratic
    spotLight1->SetAttenuation(1.0f, 0.09f, 0.032f);
    // Position
    spotLight1->SetSourcePoint(QVector3D(0.0f, 0.0f, -10.0f) );
    // Direction, cutOff, outerCutOff
    spotLight1->SetSpot(QVector3D(0.0f,0.0f,10.0f), std::cos(12.5f/180*M_PI), std::cos(15.0f/180*M_PI) );
    lightManager->AddSpotLight("spot1", spotLight1);

    SV::TriD::SpotLight* spotLight2 = new SV::TriD::SpotLight();
    // Ambient, diffuse, specular
    spotLight2->SetLight(QVector3D(0.0f, 0.0f, 0.0f), QVector3D(0.5f, 0.5f, 0.5f), QVector3D(1.0f, 1.0f, 1.0f) );
    // Constant, linear, quadratic
    spotLight2->SetAttenuation(1.0f, 0.045f, 0.0075f);
    // Position
    spotLight2->SetSourcePoint(QVector3D(0.0f, -10.0f, 0.0f) );
    // Direction, cutOff, outerCutOff
    spotLight2->SetSpot(QVector3D(0.0f,1.0f,0.0f), std::cos(12.5f/180*M_PI), std::cos(15.0f/180*M_PI) );
    lightManager->AddSpotLight("spot2", spotLight2);

    fpSysRender->SetVerbose(1);
    fpSysRender->SetView(fpViewOpenGL);
    fpSysRender->SetLightPos(QVector3D(0,0,100));
    fpSysRender->SetLightPower(1000);
    fpSysRender->SetCamera(fpCamera);
    fpSysRender->SetWorld(fpWorldNode);

    fpSysRender->Initialize();

    //fpSysSound = new Logic::Sound();
    //fpSysSound->Initialize();
}

void BasicRendererApp::Unload()
{
    delete fpCamera;
    delete fpWorldNode;
    delete fpCameraMouseStyle;
    delete fpSysRender;
    //delete fpSysSound;
}

void BasicRendererApp::Process()
{
    fpViewOpenGL->UpdateInput();

    fpCameraMouseStyle->Try(fpViewOpenGL->GetInput() );

    fpSysRender->Process();
}

}
