/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef CUSTOMCUBE_HH
#define CUSTOMCUBE_HH

#include "SymVox/3D/Object/Standard/include/Object.hh"
#include "SymVox/3D/Geometry/include/PrimitiveCRTP.hh"
#include "SymVox/3D/Geometry/include/Cube.hh"
#include "CCube3dObj.hh"

class CustomCube : public SV::TriD::PrimitiveCRTP<SV::TriD::VPrimitive, CustomCube>
{

public:
    CustomCube(float size) :
        PrimitiveCRTP(),
        fOrigin(QVector3D(0,0,0) ),
        fXAxis(QVector3D(size/2,0,0) ),
        fYAxis(QVector3D(0,size/2,0) ),
        fZAxis(QVector3D(0,0,size/2) )
    {}

    ~CustomCube();

    SV::TriD::VObject* CreateObject(SV::TriD::AssetsManager* assetsManager) override final;

    SV::TriD::VObject* CreateObjectLoader(SV::TriD::AssetsManager* assetsManager);

private:
    QVector3D fOrigin;
    QVector3D fXAxis;
    QVector3D fYAxis;
    QVector3D fZAxis;

    int AddFaceInv(SV::TriD::Mesh* mesh, SV::TriD::VertID a, SV::TriD::VertID b, SV::TriD::VertID c);
};

#endif // CUSTOMCUBE_HH
