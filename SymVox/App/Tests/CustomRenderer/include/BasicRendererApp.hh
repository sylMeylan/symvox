/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef BASICRENDERERAPP_HH
#define BASICRENDERERAPP_HH

#include "SymVox/App/Management/include/VAppManager.hh"

#include "SymVox/Logic/System/Sound/include/Sound.hh"

#include "BasicRenderer.hh"
#include "SymVox/Logic/System/Render/include/Render.hh"
#include "SymVox/Logic/Component/Renderable/include/Renderable.hh"
#include "SymVox/3D/Geometry/include/Cube.hh"
#include "SymVox/3D/Geometry/include/Loader.hh"

#include "SymVox/GUI/Camera/include/Camera.hh"
#include "SymVox/Run/Scene/Graph/include/SceneNode.hh"
#include "SymVox/GUI/UserAction/Management/include/UserActionManager.hh"
#include "SymVox/GUI/UserAction/CameraStyle/include/MouseStyle.hh"
#include "SymVox/GUI/UserAction/CameraStyle/include/GrabStyle.hh"

namespace SV {

class BasicRendererApp : public VAppManager
{
public:
    BasicRendererApp();
    ~BasicRendererApp();

    void Load() override final;
    void Unload() override final;
    void Process() override final;

private:
    BasicRenderer* fpSysRender;
    Logic::Sound* fpSysSound;

    GUI::Camera* fpCamera;
    Run::SceneNode* fpWorldNode;
    GUI::VMoveStyle* fpCameraMouseStyle;
};

}

#endif // BASICRENDERERAPP_HH
