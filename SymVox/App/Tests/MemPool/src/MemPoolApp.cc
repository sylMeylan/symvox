/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/MemPoolApp.hh"

namespace SV {

MemPoolApp::MemPoolApp() : VAppManager()
{
    fAppType = AppType::Console;
}

MemPoolApp::~MemPoolApp()
{

}

#include <vector>
void MemPoolApp::Load()
{
    // http://www.codinglabs.net/tutorial_memory_pool.aspx

    StandardMemoryPool smp (512, true);


    void* b1 = smp.allocate(32);
    void* b2 = smp.allocate(32);

smp.dumpToFile("memlog.txt", 4);
MemoryPoolManager::Instance()->GetPool()->dumpToFile("dd.txt", 4);
    int* b3 = NEW(&smp) int(2455);
    std::vector<int, Allocator<int, MemPoolPol<int> > >* c3 = NEW(&smp) std::vector<int, Allocator<int, MemPoolPol<int> > >(10);

    void* b4 = smp.allocate(32);

    MemoryPoolManager::Instance()->GetPool()->dumpToFile("dd2.txt", 4);

    smp.dumpToFile("memlog2.txt", 4);

    c3->reserve(100);
    c3->push_back(4);
    smp.dumpToFile("memlog2bis.txt", 4);
MemoryPoolManager::Instance()->GetPool()->dumpToFile("dd3.txt", 4);
    //smp.free(b2);
    DELETE(&smp, b3);
    smp.dumpToFile("memlog3.txt", 4);
    void* b5 = smp.allocate(10);
    smp.dumpToFile("memlog4.txt", 4);
}

void MemPoolApp::Unload()
{

}

void MemPoolApp::Process()
{
    //this->fpViewOpenGL->SetEndMainLoop(true);
}

}
