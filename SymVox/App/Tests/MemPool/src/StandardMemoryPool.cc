/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/StandardMemoryPool.hh"

using namespace SV;

StandardMemoryPool::StandardMemoryPool(uint32_t sizeInBytes, bool boundCheck) : MemoryPool()
{
    // Set the bound check
    if(boundCheck) fBoundsCheck = 1;

    // Allocate the total memory
    fpPoolMemory = ::new unsigned char[sizeInBytes];

    fFreePoolSize = sizeInBytes - sizeof(Chunk);
    fTotalPoolSize = sizeInBytes;

    // Trash it if required
    if(fTrashOnCreation)
        memset(fpPoolMemory, s_trashOnCreation, sizeInBytes);

    // Allocate the first free block
    if(fBoundsCheck)
    {
        fFreePoolSize -= s_boundsCheckSize * 2;

        Chunk freeChunk(sizeInBytes - sizeof(Chunk) - 2*s_boundsCheckSize);
        freeChunk.write(fpPoolMemory + s_boundsCheckSize);
        memcpy(fpPoolMemory, s_startBound, s_boundsCheckSize);
        memcpy(fpPoolMemory + sizeInBytes - s_boundsCheckSize, s_endBound, s_boundsCheckSize);
    }
    else
    {
        Chunk freeChunk(sizeInBytes - sizeof(Chunk) );
        freeChunk.write(fpPoolMemory);
    }
}

StandardMemoryPool::~StandardMemoryPool()
{
    // Deallocation
    ::delete[] fpPoolMemory;
}

void* StandardMemoryPool::allocate(uint32_t _size)
{
    uint32_t requiredSize = _size + sizeof(Chunk);

    // If guards are required, add their size
    if(fBoundsCheck)
        requiredSize += s_boundsCheckSize * 2;

    // Now search for a block big enough
    Chunk* block = (Chunk*)( fBoundsCheck == 1 ? fpPoolMemory + s_boundsCheckSize : fpPoolMemory);
    while(block)
    {
        if(block->fFree && block->fUserdataSize >= requiredSize ) break;
        block = block->fNext;
    }

    unsigned char* blockData = (unsigned char*)block;

    // If block is found, return NULL
    if(!block) return NULL;

    // If the block is valid, create a new free block with what remains of the block memory
    uint32_t freeUserDataSize = block->fUserdataSize - requiredSize;
    if( freeUserDataSize > s_minFreeBlockSize)
    {
        Chunk freeBlock(freeUserDataSize);
        freeBlock.fNext = block->fNext;
        freeBlock.fPrev = block;
        freeBlock.write( blockData + requiredSize );
        if(freeBlock.fNext)
            freeBlock.fNext->fPrev = (Chunk*)(blockData + requiredSize);
        if(fBoundsCheck)
            memcpy( blockData + requiredSize - s_boundsCheckSize, s_startBound, s_boundsCheckSize );
        block->fNext = (Chunk*)(blockData + requiredSize);
        block->fUserdataSize = _size;
    }

    // If a block is found, update the pool size
    fFreePoolSize -= block->fUserdataSize;

    // Set the memory block
    block->fFree = false;

    // Move the memory around if guards are needed
    if(fBoundsCheck)
    {
        memcpy( blockData - s_boundsCheckSize, s_startBound, s_boundsCheckSize );
        memcpy( blockData + sizeof(Chunk) + block->fUserdataSize, s_endBound, s_boundsCheckSize );
    }

    //Trash on alloc if required
    if(fTrashOnAlloc)
        memset(blockData + sizeof(Chunk), s_trashOnAllocSignature, block->fUserdataSize);

    return (blockData + sizeof(Chunk));
}

void StandardMemoryPool::free(void *ptr)
{
    // is a valid node?
    if(!ptr) return;
    Chunk* block = (Chunk*)( (unsigned char*)ptr - sizeof(Chunk) );

    if(block->fFree == true)
    {
        Utils::Exception(Utils::ExceptionType::Warning, "StandardMemoryPool::free", "This block is already free");
        return;
    }

    uint32_t fullBlockSize = block->fUserdataSize + sizeof(Chunk) + (fBoundsCheck == 1 ? s_boundsCheckSize * 2 : 0);
    fFreePoolSize += block->fUserdataSize;

    Chunk* headBlock = block;
    Chunk* prev = block->fPrev;
    Chunk* next = block->fNext;

    // If the node before is free I merge it with this one
    if(block->fPrev && block->fPrev->fFree)
    {
        headBlock = block->fPrev;
        prev = block->fPrev->fPrev;
        next = block->fNext;

        // Include the prev node in the block size so we trash it as well
        fullBlockSize += fBoundsCheck == 1 ? block->fPrev->fUserdataSize + sizeof(Chunk) + s_boundsCheckSize * 2 : block->fPrev->fUserdataSize + sizeof(Chunk);

        // If there is a next one, we need to update its pointer
        if(block->fNext)
        {
            // We will re point the next
            block->fNext->fPrev = headBlock;

            // Include the next node in the block size if it is free so we trash it as well
            if( block->fNext->fFree )
            {
                // We will point to next's next
                next = block->fNext->fNext;
                if(block->fNext->fNext)
                    block->fNext->fNext->fPrev = headBlock;

                fullBlockSize += fBoundsCheck == 1 ? block->fNext->fUserdataSize + sizeof(Chunk) + s_boundsCheckSize * 2 : block->fNext->fUserdataSize + sizeof(Chunk);
            }
        }
    }
    else
        // If next node is free lets merge it to the current one
        if(block->fNext && block->fNext->fFree)
        {
            headBlock = block;
            prev = block->fPrev;
            next = block->fNext->fNext;

            // Include the next node in the block size so we trash it as well
            fullBlockSize += fBoundsCheck == 1 ? block->fNext->fUserdataSize + sizeof(Chunk) + s_boundsCheckSize * 2 : block->fNext->fUserdataSize + sizeof(Chunk);
        }

    // Create the free block
    unsigned char* freeBlockStart = (unsigned char*)headBlock;
    if(fTrashOnFree)
        memset( fBoundsCheck == 1 ? freeBlockStart - s_boundsCheckSize : freeBlockStart, s_trashOnFreeSignature, fullBlockSize );

    uint32_t freeUserDataSize = fullBlockSize - sizeof(Chunk);
    freeUserDataSize = (fBoundsCheck == 1) ? freeUserDataSize - s_boundsCheckSize * 2 : freeUserDataSize;

    Chunk freeBlock(freeUserDataSize);
    freeBlock.fPrev = prev;
    freeBlock.fNext = next;
    freeBlock.write(freeBlockStart);

    // Move the memory around if guards are needed
    if(fBoundsCheck)
    {
        memcpy( freeBlockStart - s_boundsCheckSize, s_startBound, s_boundsCheckSize );
        memcpy( freeBlockStart + sizeof(Chunk) + freeUserDataSize, s_endBound, s_boundsCheckSize );
    }
}

bool StandardMemoryPool::integrityCheck() const
{
    if( fBoundsCheck == 1 )
        {
            Chunk* temp = (Chunk*)(fpPoolMemory + s_boundsCheckSize);

            while(temp != NULL)
            {
                if(memcmp(((uint32_t*)temp) - s_boundsCheckSize, s_startBound, s_boundsCheckSize) != 0) return false;
                if(memcmp(((uint32_t*)temp) + sizeof(Chunk) + temp->fUserdataSize, s_endBound, s_boundsCheckSize) != 0) return false;

                temp = temp->fNext;
            }
        }
        return true;
}

void StandardMemoryPool::dumpToFile(const std::string& fileName, const uint32_t itemsPerLine) const
{
    std::FILE* f = std::fopen(fileName.c_str(), "w+");
    if(f)
    {
        fprintf(f, "Memory pool ----------------------------------\n");
        fprintf(f, "Type: Standard Memory\n");
        fprintf(f, "Total Size: %d\n", fTotalPoolSize);
        fprintf(f, "Free Size: %d\n", fFreePoolSize);

        // Now search for a block big enough
        Chunk* block = (Chunk*)( fBoundsCheck == 1 ? fpPoolMemory + s_boundsCheckSize : fpPoolMemory);

        while(block)
        {
            if(block->fFree)
                fprintf(f, "Free:\t%p [Bytes:%d]\n", block, block->fUserdataSize);
            else
                fprintf(f, "Used:\t%p [Bytes:%d]\n", block, block->fUserdataSize);
            block = block->fNext;
        }

        fprintf(f, "\n\nMemory Dump:\n");
        unsigned char* ptr = fpPoolMemory;
        unsigned char* charPtr = fpPoolMemory;

        fprintf(f, "Start: %p\n", ptr);
        unsigned char i = 0;

        // Write the hex memory data
        uint32_t bytesPerLine = itemsPerLine * 4;

        fprintf(f, "\n%p: ", ptr);
        fprintf(f, "%02x", *(ptr) );
        ++ptr;
        for(i = 1; ((uint32_t)(ptr - fpPoolMemory) < fTotalPoolSize); ++i, ++ptr)
        {
            if(i == bytesPerLine)
            {
                // Write all the chars for this line now
                fprintf(f, "  %p", charPtr);
                for(uint32_t charI = 0; charI<bytesPerLine; ++charI, ++charPtr)
                    fprintf(f, "%c", *charPtr);
                charPtr = ptr;

                // Write the new line memory data
                fprintf(f, "\n%p: ", ptr);
                fprintf(f, "%02x", *(ptr) );
                i = 0;
            }
            else
                fprintf(f, ":%02x", *(ptr) );
        }

        // Fill any gaps in the tab
        if( (uint32_t)(ptr - fpPoolMemory) >= fTotalPoolSize)
        {
            uint32_t lastLineBytes = i;
            for(i; i< bytesPerLine; i++)
                fprintf(f," --");

            // Write all the chars for this line now
            fprintf(f, "  %p", charPtr);
            for(uint32_t charI = 0; charI<lastLineBytes; ++charI, ++charPtr)
                fprintf(f, "%c", *charPtr);
            charPtr = ptr;
        }
    }

    fclose(f);

}


