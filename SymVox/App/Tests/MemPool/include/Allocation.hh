/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef ALLOCATION_HH
#define ALLOCATION_HH

#include <new>

#include "MemoryPool.hh"
#include "SymVox/Utils/include/Exception.hh"

/*
*	Overload of operator new for memory pool allocation
*/
inline void* operator new(size_t size, MemoryPool& pool)
{
    void* ptr = pool.allocate(size);

    if(ptr == NULL)
        SV::Utils::Exception(SV::Utils::ExceptionType::Warning, "Allocation.h:operatorNew", "No free memory in pool");

    return ptr;
}

/*
*	Overload of operator delete
*/
inline void operator delete(void* ptr, MemoryPool& pool)
{
    pool.free(ptr);
}

/*
*	Overload of operator delete for memory pool allocation
*/
template<class T>
void __delete__(T* ptr, MemoryPool& pool)
{
    if (ptr) {
        ptr->~T();
        pool.free(ptr);
    }
}

#undef      DELETE

#define		NEW(memoryPool)						new(*memoryPool)
#define		DELETE(memoryPool, ptr)				__delete__(ptr, *memoryPool)
#define		POOL(name)							(MemoryPoolManager::it().getPool(name))

#endif // ALLOCATION_HH
