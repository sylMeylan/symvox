/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef BILLBOARDLIST_HH
#define BILLBOARDLIST_HH

#include "SymVox/3D/Object/Standard/include/Texture.hh"
#include "SymVox/3D/Memory/include/GPUObj3DBuffer.hh"
#include "SymVox/3D/Memory/include/CPUBuffer.hh"
#include "SymVox/3D/Object/Standard/include/VObject.hh"

class BillboardList : public SV::TriD::VObject
{
public:
    BillboardList(const std::string& textureFile, SV::TriD::AssetsManager* assetsManager=nullptr);
    ~BillboardList() override;

    void Load() override final;

    void Display(const QMatrix4x4& model,
                 const QMatrix4x4& view,
                 const QMatrix4x4& proj,
                 const QVector3D& cameraPos,
                 SV::TriD::LightManager& lightManager) override final;


private:
    SV::TriD::CPUBuffer<float> fPositions;
    unsigned int fVb;
    int fTextureId;
    int fShaderId;
    std::string fTextureFile;

    std::vector<float> CreatePositionBuffer();
};

#endif // BILLBOARDLIST_HH
