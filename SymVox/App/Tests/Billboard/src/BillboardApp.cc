/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/BillboardApp.hh"

namespace SV {

BillboardApp::BillboardApp() : VAppManager()
{
    fpBillboardList = nullptr;
    fpCameraMouseStyle = nullptr;
}

BillboardApp::~BillboardApp()
{

}

void BillboardApp::Load()
{
    fpCamera = new GUI::Camera(QVector3D(0,0,-30), QVector3D(0,10,0), QVector3D(0,1,0), 1., 5);
    fpBillboardList = new BillboardList("Data/Assets/monster_hellknight.png", &fAssetsManager);
    fpCameraMouseStyle = new GUI::GrabStyle("grabStyle", fpViewOpenGL, fpCamera);

    fpBillboardList->Load();

    fpCamera->SetPosition(QVector3D(0,0.5,0));
    fpCamera->SetViewDirection(QVector3D(0,0.1,0.9));
    fpCamera->SetSensitivity(0.1);
    fpCamera->SetSpeed(0.1);
}

void BillboardApp::Unload()
{
    delete fpBillboardList;
    delete fpCamera;
    delete fpCameraMouseStyle;
}

void BillboardApp::Process()
{
    fpViewOpenGL->UpdateInput();

    fpCameraMouseStyle->Try(fpViewOpenGL->GetInput() );

    // **********************************
    // Display the world on the screen
    // **********************************

    fpViewOpenGL->Clean();
    fpViewOpenGL->Resize();
    fpViewOpenGL->SaveViewMat();

    fpCamera->LookAt(fpViewOpenGL->GetView() );

    fpBillboardList->Display(QMatrix4x4(), fpViewOpenGL->GetView(), fpViewOpenGL->GetProjection(), fpCamera->GetPosition(), fLightManager);

    fpViewOpenGL->LoadSavedViewMat();
    fpViewOpenGL->Refresh();
}

}
