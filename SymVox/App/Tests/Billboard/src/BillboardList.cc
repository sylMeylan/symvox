/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/BillboardList.hh"

using namespace SV;

BillboardList::BillboardList(const std::string& textureFile, SV::TriD::AssetsManager* assetsManager)
    : SV::TriD::VObject(assetsManager)
{
    fTextureFile = textureFile;
    fTextureId = assetsManager->AutoAddTexture(textureFile);
    fShaderId = -1;
    fVb = 0;
}

BillboardList::~BillboardList()
{

}

void BillboardList::Load()
{
    this->initializeOpenGLFunctions();

    fpAssetsManager->GetTexture(fTextureId)->Load();

    fMeshEntryVect.push_back(new SV::TriD::MeshEntry() );
    SV::TriD::MeshEntry& meshEntry = *fMeshEntryVect.at(0);

    SV::TriD::VAO& vao = meshEntry.fVao;
    vao.Init();

    std::vector<float> positions = CreatePositionBuffer();

    SV::TriD::CPUBuffer<float> positionsBuffCPU("CPU_billboard_positions");
    positionsBuffCPU.AddData("positions", positions, 3);
    meshEntry.fObjBuffGPU.SetVAO(&vao);
    meshEntry.fObjBuffGPU.SendToGPU(positionsBuffCPU);

    SV::TriD::Shader* shader = new SV::TriD::Shader(":SymVox/3D/Shaders/billboard.vert",
                                                      ":SymVox/3D/Shaders/billboard.frag",
                                                      ":SymVox/3D/Shaders/billboard.geom");

    shader->AddAttribLocation("gP");
    shader->AddAttribLocation("gV");
    shader->AddAttribLocation("gCameraPos");
    shader->AddAttribLocation("gColorMap");
    shader->AddAttribLocation("gBillboardSize");

    shader->Load();

    fShaderId = fpAssetsManager->AddShader(shader);
}

void BillboardList::Display(const QMatrix4x4 &model,
                            const QMatrix4x4 &view,
                            const QMatrix4x4 &proj,
                            const QVector3D &cameraPos,
                            SV::TriD::LightManager &lightManager)
{
    SV::TriD::Shader* shader = fpAssetsManager->GetShader(fShaderId);
    SV::TriD::Texture* texture = fpAssetsManager->GetTexture(fTextureId);
    SV::TriD::MeshEntry& meshEntry = *fMeshEntryVect.at(0);
    SV::TriD::VAO& vao = meshEntry.fVao;
    SV::TriD::GPUObj3DBuffer& objBuffer = meshEntry.fObjBuffGPU;

    shader->Use();

    vao.Bind();

    texture->Bind(GL_TEXTURE0);

    shader->Send4fv("gP", proj);
    shader->Send4fv("gV", view);
    shader->Send3f("gCameraPos", cameraPos.x(), cameraPos.y(), cameraPos.z() );
    shader->Send1i("gColorMap", 0 );
    shader->Send1f("gBillboardSize", 1 );
    objBuffer.GetVbo()->Bind();

    glDrawArrays(GL_POINTS, 0, 10*10);

    objBuffer.GetVbo()->Unbind();
    texture->Unbind();

    vao.Unbind();

    shader->Unuse();
}

std::vector<float> BillboardList::CreatePositionBuffer()
{
    std::vector<float> positions;

    for (unsigned int j = 0 ; j < 10 ; j++)
    {
        for (unsigned int i = 0 ; i < 10 ; i++)
        {
            QVector3D pos((float)i, 0.0f, (float)j);

            positions.push_back(pos.x() );
            positions.push_back(pos.y() );
            positions.push_back(pos.z() );
        }
    }

    return positions;
}

