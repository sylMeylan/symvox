/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/ParticleEffect.hh"

#define MAX_PARTICLES 10000
#define PARTICLE_LIFETIME 10.0f

#define PARTICLE_TYPE_LAUNCHER 0.0f
#define PARTICLE_TYPE_SHELL 1.0f
#define PARTICLE_TYPE_SECONDARY_SHELL 2.0f

#define ZERO_MEM(a) memset(a, 0, sizeof(a))

namespace SV {

ParticleEffect::ParticleEffect(SV::TriD::AssetsManager* assetsManager) : SV::TriD::VObject(assetsManager)
{
    m_currVB = 0;
    m_currTFB = 1;
    m_isFirst = true;
    fTime = 0;
    fFireWorkTextureId = 0;
    fBillboardShaderId = 0;
    fUpdateShaderId = 0;
    fQueryId = 0;

    fLaunchDelay = 30.0f;
    fShellLifetime = 5000.0f;
    fSecondaryShellLifetime = 2500.0f;

    fBillboardSize = 0.03f;

    ZERO_MEM(m_transformFeedback);
    ZERO_MEM(m_particleBuffer);
}

ParticleEffect::~ParticleEffect()
{
    if(m_transformFeedback[0] != 0)
    {
        //glDeleteTransformFeedbacks(2, m_transformFeedback);
    }

    if(m_particleBuffer[0] != 0)
    {
        glDeleteBuffers(2, m_particleBuffer);
    }
}

void ParticleEffect::Load()
{
    this->initializeOpenGLFunctions();

    fVao.Init();

    Particle particles[MAX_PARTICLES];
    ZERO_MEM(particles);

    // Define the first particle that is the launcher
    QVector3D launcherPos(0,0,10);
    QVector3D launchVelocity(0, 0, 200);
    particles[0].Type = PARTICLE_TYPE_LAUNCHER;
    particles[0].PosX = launcherPos.x();
    particles[0].PosY = launcherPos.y();
    particles[0].PosZ = launcherPos.z();
    particles[0].VelX = launchVelocity.x();
    particles[0].VelY = launchVelocity.y();
    particles[0].VelZ = launchVelocity.z();
    particles[0].LifetimeMillis = 0.0f; // Infinite life time...

    // Create the 2 transform feedback associated with two buffers
    glGenTransformFeedbacks(2, m_transformFeedback); // transform feedback
    glGenBuffers(2, m_particleBuffer); // standard

    for(unsigned int i=0; i<2; i++)
    {
        glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, m_transformFeedback[i]);
        glBindBuffer(GL_ARRAY_BUFFER, m_particleBuffer[i]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(particles), particles, GL_DYNAMIC_DRAW);
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_particleBuffer[i]);
    }

    // *****************
    // Update shader
    // *****************

    SV::TriD::Shader* updateShader = new SV::TriD::Shader(":/SymVox/3D/Shaders/ps_update.vert",
                                                            ":/SymVox/3D/Shaders/ps_update.frag",
                                                            ":/SymVox/3D/Shaders/ps_update.geom");

    updateShader->AddAttribLocation("gDeltaTimeMillis");
    updateShader->AddAttribLocation("gTime");
    updateShader->AddAttribLocation("gRandomTexture");
    updateShader->AddAttribLocation("gLauncherLifetime");
    updateShader->AddAttribLocation("gShellLifetime");
    updateShader->AddAttribLocation("gSecondaryShellLifetime");

    updateShader->AddVaryings("Type1");
    updateShader->AddVaryings("Position1");
    updateShader->AddVaryings("Velocity1");
    updateShader->AddVaryings("Age1");

    updateShader->Load();

    fUpdateShaderId = fpAssetsManager->AddShader(updateShader);


    // *****************
    // Random texture
    // *****************

    m_randomTexture.SetSizes(100, 100);
    m_randomTexture.SetSize(1000);
    // Texture unit 3 is associated to random texture here
    m_randomTexture.Load(GL_TEXTURE3);

    // *****************
    // Billboard shader
    // *****************

    SV::TriD::Shader* billboardShader = new SV::TriD::Shader(":/SymVox/3D/Shaders/ps_billboard.vert",
                                                               ":/SymVox/3D/Shaders/ps_billboard.frag",
                                                               ":/SymVox/3D/Shaders/ps_billboard.geom");

    billboardShader->AddAttribLocation("gP");
    billboardShader->AddAttribLocation("gV");
    billboardShader->AddAttribLocation("gCameraPos");
    billboardShader->AddAttribLocation("gColorMap");
    billboardShader->AddAttribLocation("gBillboardSize");

    billboardShader->Load();

    fBillboardShaderId = fpAssetsManager->AddShader(billboardShader);

    // ****************
    // Launcher texture
    // ****************

    SV::TriD::Texture* launcherTexture = new SV::TriD::Texture("Data/Assets/fireworks_red.jpg");

    launcherTexture->Load();

    fFireWorkTextureId = fpAssetsManager->AddTexture(launcherTexture, "launcherTexture");

    glGenQueries(1, &fQueryId);

    fTime = 0;
    m_isFirst = true;
}

void ParticleEffect::Display(const QMatrix4x4 &model,
                             const QMatrix4x4 &view,
                             const QMatrix4x4 &proj,
                             const QVector3D &cameraPos,
                             SV::TriD::LightManager &lightManager)
{
    int deltaTimeMillis = Utils::TimeUtilities::Instance()->GetDeltaTime();

    fTime += deltaTimeMillis;

    UpdateParticles(deltaTimeMillis);

    RenderParticles(view, proj, cameraPos);

    m_currVB = m_currTFB;
    m_currTFB =(m_currTFB+1) & 0x1;
}

void ParticleEffect::UpdateParticles(int DeltaTimeMillis)
{
    // Here, we update the positions of the particle thanks to the feedback buffer

    SV::TriD::Shader* updateShader = fpAssetsManager->GetShader(fUpdateShaderId);

    updateShader->Use();

    m_randomTexture.Bind(GL_TEXTURE3);

    updateShader->Send1f("gDeltaTimeMillis", DeltaTimeMillis);
    updateShader->Send1f("gTime", fTime);
    updateShader->Send1i("gRandomTexture", 0 );
    updateShader->Send1f("gLauncherLifetime", fLaunchDelay);
    updateShader->Send1f("gShellLifetime", fShellLifetime);
    updateShader->Send1f("gSecondaryShellLifetime", fSecondaryShellLifetime);

    glEnable(GL_RASTERIZER_DISCARD);

    fVao.Bind();

    glBindBuffer(GL_ARRAY_BUFFER, m_particleBuffer[m_currVB]);
    glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, m_transformFeedback[m_currTFB]);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)0);    // type
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)4);         // position
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)16);        // velocity
    glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)28);          // lifetime

    glBeginTransformFeedback(GL_POINTS);

    glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, fQueryId);

    if(m_isFirst)
    {
        glDrawArrays(GL_POINTS, 0, 1);

        m_isFirst = false;
    }
    else
    {
        glDrawTransformFeedback(GL_POINTS, m_transformFeedback[m_currVB]);
        //glDrawArrays(GL_POINTS, 0, fCurrentNumParticles[m_currTFB]);
    }

    glEndTransformFeedback();

    glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
    glGetQueryObjectuiv(fQueryId, GL_QUERY_RESULT, &fCurrentNumParticles[m_currTFB]);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);

    fVao.Unbind();

    m_randomTexture.Unbind();

    updateShader->Unuse();
}

void ParticleEffect::RenderParticles(const QMatrix4x4 &V, const QMatrix4x4 &P, const QVector3D &cameraPos)
{
    SV::TriD::Shader* billboardShader = fpAssetsManager->GetShader(fBillboardShaderId);
    SV::TriD::Texture* texture = fpAssetsManager->GetTexture(fFireWorkTextureId);

    billboardShader->Use();

    texture->Bind();

    billboardShader->Send4fv("gP", P);
    billboardShader->Send4fv("gV", V);
    billboardShader->Send3f("gCameraPos", cameraPos.x(), cameraPos.y(), cameraPos.z() );
    billboardShader->Send1i("gColorMap", 0 );
    billboardShader->Send1f("gBillboardSize",  fBillboardSize);

    glDisable(GL_RASTERIZER_DISCARD);
 //std::cout<<"m_currTFB "<<m_currTFB << " val "<<fCurrentNumParticles[m_currTFB]<<" - "<<fCurrentNumParticles[0]<< " - "<< fCurrentNumParticles[1]<< std::endl;
    fVao.Bind();

    glBindBuffer(GL_ARRAY_BUFFER, m_particleBuffer[m_currTFB]);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)4);  // position
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)0);

    glDrawTransformFeedback(GL_POINTS, m_transformFeedback[m_currTFB]);
    //glDrawArrays(GL_POINTS, 0, fCurrentNumParticles[m_currTFB]);

    glDisableVertexAttribArray(0);

    fVao.Unbind();

    texture->Unbind();

    billboardShader->Unuse();
}

}
