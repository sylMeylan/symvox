/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef APP_VAPPMANAGER_HH
#define APP_VAPPMANAGER_HH

#include <string>

#include "SymVox/GUI/Windows/include/VMainWindow.hh"
#include "SymVox/Logic/System/Management/include/VSystem.hh"

#include "SymVox/Logic/System/Render/include/Render.hh"

#define M_PI 3.14159265358979323846

namespace SV {

class VAppManager
{
public:
    enum AppType {
        GUI,
        ConsoleWithMainLoop,
        Console,
    };

    VAppManager();
    virtual ~VAppManager();

    virtual void Load()=0;
    virtual void Unload()=0;

    virtual void Process()=0;

    // Frame rate
    int GetFrameRate() {return fFrameRate;}
    void SetFrameRate(int f) {fFrameRate=f;}

    // Name
    void SetName(const std::string& name) {fName=name;}
    const std::string& GetName() const {return fName;}

    // Windows
    void SetMainWindow(GUI::VMainWindow* mainWin) {fpMainWindow=mainWin;}
    void SetViewOpenGL(GUI::ViewOpenGL* viewWin) {fpViewOpenGL=viewWin;}

    // Console only
    bool GetAppType() const {return fAppType;}
    void SetAppType(AppType t) {fAppType=t;}

    bool GetConsoleEndMainLoop() const {return fConsoleEndMainLoop;}

protected:
    int fFrameRate;
    bool fConsoleEndMainLoop;
    AppType fAppType;
    std::string fName;

    GUI::VMainWindow* fpMainWindow;
    GUI::ViewOpenGL* fpViewOpenGL;

    Utils::StrToIdContainer<Logic::VSystem*> fSystems;

private:
    VAppManager(const VAppManager& toBeCopied);
    VAppManager& operator=(const VAppManager& toBeCopied);
};

}

#endif // APP_VAPPMANAGER_HH
