/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/RunManager.hh"

namespace SV { namespace Run {

RunManager::RunManager() :
    fEndMainLoop(false),
    fInitAMainLoop(true),
    fpAppManager(nullptr)
{
    fpMainWindow = nullptr;

    // Create the threadpool mananger singleton
    ThreadPoolManager::Instance();
}

RunManager::~RunManager()
{
    // Delete the singleton
    ThreadPoolManager::DeleteInstance();

    if(fpMainWindow != nullptr)
        delete fpMainWindow;
}

void RunManager::Initialize()
{
    // Check if the app is console only
    if(fpAppManager->GetAppType() == VAppManager::AppType::Console)
    {
        // Console only initialization
        fInitAMainLoop = false;
    }
    else if(fpAppManager->GetAppType() == VAppManager::AppType::ConsoleWithMainLoop)
    {
        fInitAMainLoop = true;
    }
    else
    {
        // GUI initialization

        // Do we have a main window defined in the user app ?
        // If no, then create a defaut one.
        if(fpMainWindow == nullptr)
        {
            // This main window will be detroyed at the end of the run
            fpMainWindow = new GUI::MainWindow();
        }

        fpMainWindow->Initialise();

        // Set the main window title
        fpMainWindow->SetTitle(fpAppManager->GetName() );

        // Retrieve the screen x and y size
        //QRect screenGeometry = QApplication::desktop()->availableGeometry();

        // Apply the retrieved sizes to the main window to get full screen
        //fMainWindow.resize(screenGeometry.width(), screenGeometry.height() );

        // Display the main window to the screen
        fpMainWindow->show();

        // Retrieve the openGL view window
        // The pointer belongs to the mainwindow.
        GUI::ViewOpenGL* viewGL = fpMainWindow->GetViewOpenGL();

        // Initialise context and view
        viewGL->InitialiseContext();
        viewGL->InitialiseView();

        // Pass the window addresses to the app manager
        fpAppManager->SetMainWindow(fpMainWindow);
        fpAppManager->SetViewOpenGL(viewGL);
    }
}

void RunManager::StartMainLoop()
{
    fpAppManager->Load();

    int frameRate = fpAppManager->GetFrameRate(); // 1000/60 : 60 FPS
    int timeSpend (0);

    // ViewGL
    GUI::ViewOpenGL* viewGL = fpMainWindow->GetViewOpenGL();

    // Time object
    QTime time;

    // Main loop
    while(!fEndMainLoop)
    {
        // Start the time
        time.start();

        // Mandatory to get the Qt system working
        // The GUI events are processed here
        qApp->processEvents(QEventLoop::AllEvents);

        // Test if we should immediatly exit the main loop
        if(viewGL == nullptr && !fInitAMainLoop)
            fEndMainLoop = true;

        // Render
        //viewGL->Render();
        fpAppManager->Process();

        // Test if an "exit" buttons was pressed somewhere
        if(viewGL != nullptr)
            fEndMainLoop = viewGL->GetEndMainLoop();
        else if(!fInitAMainLoop)
            fEndMainLoop = false;
        else
            fEndMainLoop = fpAppManager->GetConsoleEndMainLoop();

        // Get the time spend within the loop
        timeSpend = time.elapsed();

        // If that time is inferior to the frame rate then sleep until it is not
        // Once it is not, do the loop again
        if(timeSpend < frameRate)
            Sleeper::msleep(frameRate - timeSpend);

        // Set the time elasped value in the TimeUtilities singleton
        Utils::TimeUtilities::Instance()->SetDeltaTime(time.elapsed() );
    }

    fpAppManager->Unload();

    // Delete the singleton
    Utils::TimeUtilities::DeleteInstance();
}

void RunManager::SetAppManager(VAppManager* appManager)
{
    if(fpAppManager == nullptr)
    {
        fpAppManager = appManager;
    }
    else
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "RunManager::SetAppManager",
                         "An AppManager was already set");
    }
}

void RunManager::SetMainWin(GUI::VMainWindow *mainWin)
{
    fpMainWindow = mainWin;
}

}}

