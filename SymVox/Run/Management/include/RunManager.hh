/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef RUN_RUNMANAGER_HH
#define RUN_RUNMANAGER_HH

#include <QThread>
#include <QApplication>
#include <QDesktopWidget>
#include <QTime>

#include "SymVox/Utils/include/TimeUtilities.hh"
#include "SymVox/GUI/Windows/include/VMainWindow.hh"
#include "SymVox/GUI/Windows/include/MainWindow.hh"
#include "SymVox/App/Management/include/VAppManager.hh"
#include "SymVox/Utils/include/Exception.hh"
#include "SymVox/Run/Thread/ThreadPool/include/ThreadPoolManager.hh"

namespace SV { namespace Run {

// **********************************
// A hack to get a Qt sleep function
// **********************************

/*!
 * \brief The Sleeper class
 * This class is a hack to introduce a Qt sleep function.
 */
class Sleeper : public QThread
{
public:
    static void usleep(unsigned long usecs){QThread::sleep(usecs);}
    static void msleep(unsigned long msecs){QThread::msleep(msecs);}
    static void sleep(unsigned long secs){QThread::sleep(secs);}
};

// **********************************
// Normal class
// **********************************

/*!
 * \brief The RunManager class
 * This is the hearth class of Flair.
 * This class initialise Flair, start it and stop it. As such, it contains the main loop.
 */
class RunManager
{
public:
    /// A constructor
    RunManager();

    /// A destructor
    ~RunManager();

    /*!
     * \brief Initialise
     * This is where the GUI is setup with the OpenGL context. The UserApp to be used is also initialised here.
     */
    void Initialize();

    /*!
     * \brief StartMainLoop
     * The main loop is the place where everything is updated in the program.
     * When the main loop ends, the program stop refreshing itself and runs its finalisation processes (cleanup for example).
     */
    void StartMainLoop();

    /*!
     * \brief SetAppManager
     * \param appManager
     * This method is used to associate an user application manager to the RunManager.
     * The manager can handle several user applications at the same time to allow the user to switch between them at runtime.
     * However, having only one user application is perfectly fine.
     */
    void SetAppManager(VAppManager* appManager);


    void SetMainWin(GUI::VMainWindow* mainWin); ///< Define a custom main window
    const GUI::VMainWindow& GetMainWin() const {return *fpMainWindow;} ///< A getter.
    GUI::VMainWindow& GetMainWin() {return *fpMainWindow;} ///< A getter.

private:
    bool fEndMainLoop; ///< When this flag turn to false then the program ends its main loop and quit.
    bool fInitAMainLoop; ///< A boolean flag to determine if we need to start a main loop in SymVox.
    VAppManager* fpAppManager; ///< The user application manager than can handle several user applications.
    GUI::VMainWindow* fpMainWindow; ///< The main window in which the 3D is rendered.
};

}}

#endif // RUN_RUNMANAGER_HH
