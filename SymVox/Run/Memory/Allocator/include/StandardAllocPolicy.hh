/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef RUN_MEMORY_StandardAllocPolicy_HH
#define RUN_MEMORY_StandardAllocPolicy_HH

#define ALLOCATOR_TRAITS(T)                \
typedef T                 type;            \
typedef type              value_type;      \
typedef value_type*       pointer;         \
typedef value_type const* const_pointer;   \
typedef value_type&       reference;       \
typedef value_type const& const_reference; \
typedef std::size_t       size_type;       \
typedef std::ptrdiff_t    difference_type; \

namespace SV { namespace Run {

template<typename T>
struct max_allocations
{
  enum{value = static_cast<std::size_t>(-1) / sizeof(T)};
};

template<typename T>
class heap
{
public:

  ALLOCATOR_TRAITS(T)

  template<typename U>
  struct rebind
  {
      typedef heap<U> other;
  };

  // Default Constructor
  heap(void){}

  // Copy Constructor
  template<typename U>
  heap(heap<U> const& other){}

  // Allocate memory
  pointer allocate(size_type count, const_pointer /* hint */ = 0)
  {
      if(count > max_size()){throw std::bad_alloc();}
      return static_cast<pointer>(::operator new(count * sizeof(type), ::std::nothrow));
  }

  // Delete memory
  void deallocate(pointer ptr, size_type /* count */)
  {
      ::operator delete(ptr);
  }

  // Max number of objects that can be allocated in one call
  size_type max_size(void) const {return max_allocations<T>::value;}
};

}}

#endif
