#add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Management")

add_sources(
    Chunk.cc
    StandardMemoryPool.cc
)

add_headers(
    Chunk.hh
    StandardMemoryPool.hh
)

add_uis()
