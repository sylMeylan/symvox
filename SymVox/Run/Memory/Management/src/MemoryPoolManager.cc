/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/MemoryPoolManager.hh"

namespace SV { namespace Run {

MemoryPool* MemoryPoolManager::CreatePool(const std::string &name,
                                          uint32_t sizeInBytes,
                                          MemoryPoolManager::PoolType pt,
                                          bool check)
{
    MemoryPool* mp = nullptr;

    // Check if the chosen name is not already used in the list of pools
    if(fPools.end() != fPools.find(name) )
    {
        std::ostringstream oss;
        oss << "The name "<<name<<" is already used by a pool";
        Utils::Exception(Utils::ExceptionType::Fatal,
                         "MemoryPoolManager::CreatePool",
                         oss.str() );

        return nullptr;
    }

    switch(pt)
    {
    case PoolType::Standard:
        mp = new StandardMemoryPool(sizeInBytes, check);
        break;
    default: // By default we throw an fatal exception
        Utils::Exception(Utils::ExceptionType::Fatal,
                         "MemoryPoolManager::CreatePool",
                         "Asked pool type is not unknown. You should probably add it to the PoolManager enum.");
        return nullptr;
    }

    // If we are there then the pool was correctly created.
    // We still have to add its pointer to the pool map.
    fPools[name] = mp;

    // To finish, return a the pointer of the pool.
    return mp;
}

MemoryPoolManager::MemoryPoolManager() : Utils::Singleton<MemoryPoolManager>()
{

}

MemoryPoolManager::~MemoryPoolManager()
{
    for(auto& it : fPools)
    {
        MemoryPool* mp = it.second;

        if(mp != nullptr) delete mp;
    }
}

}}
