/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef RUN_MEMORY_MEMORYPOOLMANAGER_HH
#define RUN_MEMORY_MEMORYPOOLMANAGER_HH

#include <map>
#include <string>
#include <sstream>

#include "SymVox/Run/Memory/Pool/include/StandardMemoryPool.hh"

#include "SymVox/Utils/include/Exception.hh"
#include "SymVox/Utils/include/Singleton.hh"

namespace SV { namespace Run {

// This memory pool manager can create pool and retrieve them with their names.
// What there is still to do:
// - find a way to connect this "management" with the allocator and real-life use
// For example, we don't want to use NEW(memPool) and get NULL because there is no more room in the pool.
// How to deal with it ?
class MemoryPoolManager : public Utils::Singleton<MemoryPoolManager>
{
public:
    friend MemoryPoolManager* Utils::Singleton<MemoryPoolManager>::Instance();
    friend void Utils::Singleton<MemoryPoolManager>::DeleteInstance();

    enum PoolType {
        Standard
    };

    MemoryPool* CreatePool(const std::string& name,
                           uint32_t sizeInBytes,
                           PoolType pt = PoolType::Standard, bool check=true);

    template<class T> MemoryPool* CreatePoolFor(const std::string& name,
                                                unsigned int numOfObj,
                                                PoolType pt = PoolType::Standard)
    {
        uint32_t objByteSize = sizeof(T);

        uint32_t finalSize = objByteSize * numOfObj + sizeof(Run::Chunk);

        return CreatePool(name, finalSize, pt, false);
    }

    MemoryPool* GetPool(const std::string& name) {return fPools.at(name);}

private:
    MemoryPoolManager();
    ~MemoryPoolManager();

    typedef std::map<std::string, MemoryPool*> mapMemoryPool;
    mapMemoryPool fPools;
};

}}

#endif // MEMORYPOOLMANAGER_HH
