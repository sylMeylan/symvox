/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef RUN_MEMORY_MEMORYPOOL_HH
#define RUN_MEMORY_MEMORYPOOL_HH

#ifndef NDEBUG
#define TRASH_POOLS 1
#else
#define TRASH_POOLS 0
#endif

#include <cstdint>
#include <string>

namespace SV { namespace Run {

class MemoryPool
{
public:
    MemoryPool();
    virtual ~MemoryPool();

    virtual void* allocate(uint32_t size) = 0;
    virtual void  free(void* ptr) = 0;
    virtual bool  integrityCheck() const = 0;
    virtual void  dumpToFile(const std::string& fileName, const uint32_t itemsPerLine) const = 0;

    uint32_t getFreePoolSize() const { return fFreePoolSize; }
    uint32_t getTotalPoolSize() const { return fTotalPoolSize; }
    bool  hasBoundsCheckOn() const { return fBoundsCheck; }

    static const unsigned char s_trashOnCreation = 0xCC;
    static const unsigned char s_trashOnAllocSignature = 0xAB;
    static const unsigned char s_trashOnFreeSignature  = 0xFE;
    static const unsigned char s_boundsCheckSize = 16;
    static const unsigned char s_startBound[s_boundsCheckSize];
    static const unsigned char s_endBound[s_boundsCheckSize];

protected:

    uint32_t fTotalPoolSize;
    uint32_t fFreePoolSize;

    // Bitfield
    unsigned fTrashOnCreation : 1;
    unsigned fTrashOnAlloc : 1;
    unsigned fTrashOnFree : 1;
    unsigned fBoundsCheck : 1;

private:
    friend class MemoryPoolManager;
};

}}

#endif // MEMORYPOOL_HH
