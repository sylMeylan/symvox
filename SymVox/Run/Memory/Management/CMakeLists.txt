#add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Management")

add_sources(
    MemoryPool.cc
    MemoryPoolManager.cc
)

add_headers(
    MemoryPool.hh
    Allocation.hh
    MemoryPoolManager.hh
)

add_uis()
