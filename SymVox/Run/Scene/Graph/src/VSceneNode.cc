/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/VSceneNode.hh"

namespace SV { namespace Run {

VSceneNode::VSceneNode()
{
    fId = -1;
    fIsWrapperNode = false;
    fName = "noName";
    fType = "";
}

VSceneNode::VSceneNode(const std::string& name)
{
    fId = -1;
    fIsWrapperNode = false;
    fName = name;
    fType = "";
}

VSceneNode::~VSceneNode()
{
    RemoveAllChildren();
    RemoveAllComponents();
}

VSceneNode::VSceneNode(const VSceneNode& toBeCopied)
{
    RemoveAllParentOnly();
    RemoveAllChildren();

    Copy(toBeCopied);
}

VSceneNode& VSceneNode::operator=(const VSceneNode& toBeCopied)
{
    RemoveAllParentOnly();
    RemoveAllChildren();

    Copy(toBeCopied);

    return *this;
}

void VSceneNode::Copy(const VSceneNode& toBeCopied)
{
    fId = toBeCopied.fId;
    fName = toBeCopied.fName;
    fType = toBeCopied.fType;
    fModelMat = toBeCopied.fModelMat;
    fIsWrapperNode = toBeCopied.fIsWrapperNode;

    // If the current node is not a wrapper then we clone all the childs
    if(!toBeCopied.fIsWrapperNode)
        CloneTheChildrenOf(toBeCopied);

    // Components copy

    //fComponents = toBeCopied.fComponents;

    // Set the owner node
    for(auto& comp : toBeCopied.fComponents)
    {
        this->AddComponent(comp.second->Clone(), comp.second->GetTypeId() );
    }
}

void VSceneNode::ApplyTransform(const QMatrix4x4& trans)
{
    fModelMat = trans * fModelMat;
}

void VSceneNode::SetPosition(const QVector3D &pos)
{
    // Fourth matrix column
    //
    fModelMat(0,3) = pos.x();
    fModelMat(1,3) = pos.y();
    fModelMat(2,3) = pos.z();
}

void VSceneNode::TranslateInLocalFrame(const QVector3D& vect)
{
    fModelMat.translate(vect);
}

void VSceneNode::TranslateInLocalFrame(float x, float y, float z)
{
    fModelMat.translate(x,y,z);
}

void VSceneNode::TranslateInParentFrame(const QVector3D& vect)
{
    TranslateInLocalFrame(ConvertToLocalFromParentFrame(vect) );
}

void VSceneNode::TranslateInFrame(const QVector3D &transVect, const QMatrix4x4& frame)
{
    TranslateInLocalFrame(ConvertToLocalFromFrame(transVect, frame) );
}

void VSceneNode::RotateInLocalFrame(float angle, const QVector3D& axis)
{
    fModelMat.rotate(angle, axis);
}

void VSceneNode::RotateInLocalFrame(float angle, float x, float y, float z)
{
    fModelMat.rotate(angle, x, y, z);
}

void VSceneNode::RotateInLocalFrame(const QQuaternion& quaternion)
{
    fModelMat.rotate(quaternion);
}

void VSceneNode::RotateInParentFrame(float angle, const QVector3D& axis)
{
    RotateInLocalFrame(angle, ConvertToLocalFromParentFrame(axis) );
}

void VSceneNode::RotateInFrame(float angle, const QVector3D& axis, const QMatrix4x4& frame)
{
    RotateInLocalFrame(angle, ConvertToLocalFromFrame(axis, frame) );
}

QMatrix3x3 VSceneNode::ExtractRotMatFromPosAndRot(const QMatrix4x4 &mat4x4) const
{
    QMatrix3x3 mat3x3;

    for(int line=0; line<3; ++line)
    {
        for(int column=0; column<3; ++column)
        {
            mat3x3(line, column) = mat4x4(line, column);
        }
    }

    return mat3x3;
}

QVector3D VSceneNode::ExtractPosFromMatrix(const QMatrix4x4 &model) const
{
    return QVector3D(model(0,3), model(1,3), model(2,3) );
}

QVector3D VSceneNode::GetPosVectorInMotherFrame() const
{
    return QVector3D(fModelMat(0,3), fModelMat(1,3), fModelMat(2,3) );
}

QMatrix4x4 VSceneNode::GetModelMatrixInParentFrame(const VSceneNode *parent) const
{
    QMatrix4x4 modelTransititionMatrix = QMatrix4x4();

    // First, look for the parent passed as argument

    // Current parent node in which the model matrix is already defined
    const VSceneNode* node = this;

    bool isFound = RecursivelyAnalyseParentsToLookFor(node, parent, QMatrix4x4(), modelTransititionMatrix);

    if(!isFound)
    {
        std::ostringstream oss;
        oss << "SceneNode::GetModelMatrixInParentFrame: "<<parent->GetName();
        oss << " is not a parent node of "<<node->GetName();
        oss << " itself a child of "<<node->GetParent(0)->GetName() << std::endl;
        Utils::Exception(Utils::ExceptionType::Fatal, "VSceneNode::GetModelMatrixInParentFrame", oss.str() );
    }

    return modelTransititionMatrix;
}

QVector3D VSceneNode::GetPosVectorInParentFrame(const VSceneNode *parent) const
{
    return ExtractPosFromMatrix(GetModelMatrixInParentFrame(parent) );
}

QVector3D VSceneNode::GetPosVectorInDirectParentFrame(const QMatrix4x4 &frame) const
{
    // Extract the frame rotation matrix
    QMatrix3x3 frameRotMat = ExtractRotMatFromPosAndRot(frame);

    // Get the current object position relative to the frame origin but in the world frame.
    QVector3D posInWorld = GetPosVectorInMotherFrame();

    QGenericMatrix<3, 1, float> posInFrame;
    posInFrame(0,0) = posInWorld.x();
    posInFrame(0,1) = posInWorld.y();
    posInFrame(0,2) = posInWorld.z();

    posInFrame = posInFrame * frameRotMat;

    return QVector3D(posInFrame(0,0), posInFrame(0,1), posInFrame(0,2) );
}

QVector3D VSceneNode::ConvertToLocalFromParentFrame(const QVector3D &vect)
{
    // Extract the current frame rotation matrix
    QMatrix3x3 rotationMatrix = ExtractRotMatFromPosAndRot(fModelMat);

    // Create a position vection in a suitable format to use Qt matrix features
    QGenericMatrix<3,1,float> transVect;
    transVect(0,0) = vect.x();
    transVect(0,1) = vect.y();
    transVect(0,2) = vect.z();

    // Multiply to get the world equivalent vector in the local frame
    transVect = transVect * rotationMatrix;

    // Return the vector in the local frame
    return QVector3D(transVect(0,0), transVect(0,1),transVect(0,2) );
}

QVector3D VSceneNode::ConvertToLocalFromFrame(const QVector3D &vect, const QMatrix4x4 &frame)
{
    QVector3D vectInWorldFrame = ConvertToParentFromFrame(vect, frame);

    return ConvertToLocalFromParentFrame(vectInWorldFrame);
}

QVector3D VSceneNode::ConvertToParentFromLocalFrame(const QVector3D &vect)
{
    // Extract the current frame rotation matrix
    QMatrix3x3 rotationMatrix = ExtractRotMatFromPosAndRot(fModelMat);
    rotationMatrix = rotationMatrix.transposed();

    // Create a position vection in a suitable format to use Qt matrix features
    QGenericMatrix<3,1,float> transVect;
    transVect(0,0) = vect.x();
    transVect(0,1) = vect.y();
    transVect(0,2) = vect.z();

    // Multiply to get the world equivalent vector in the local frame
    transVect = transVect * rotationMatrix;

    // Return the vector in the local frame
    return QVector3D(transVect(0,0), transVect(0,1),transVect(0,2) );
}

QVector3D VSceneNode::ConvertToParentFromFrame(const QVector3D &vect, const QMatrix4x4 &frame)
{
    // Extract the current frame rotation matrix
    QMatrix3x3 rotationMatrix = ExtractRotMatFromPosAndRot(frame);
    rotationMatrix = rotationMatrix.transposed();

    // Create a position vection in a suitable format to use Qt matrix features
    QGenericMatrix<3,1,float> transVect;
    transVect(0,0) = vect.x();
    transVect(0,1) = vect.y();
    transVect(0,2) = vect.z();

    // Multiply to get the world equivalent vector in the local frame
    transVect = transVect * rotationMatrix;

    // Return the vector in the local frame
    return QVector3D(transVect(0,0), transVect(0,1),transVect(0,2) );
}

void VSceneNode::AddParent(VSceneNode* parent)
{
    fParents.push_back(parent);
    parent->AddChild(this);
}

void VSceneNode::AddParentOnly(VSceneNode* parent)
{
    fParents.push_back(parent);
}

void VSceneNode::RemoveParentOnly(int index)
{
    VSceneNode* parent = fParents.at(static_cast<size_t>(index) );

    RemoveParentOnly(parent);
}

void VSceneNode::RemoveParentOnly(VSceneNode* parent)
{
    int index = FindParentIndex(parent);

    if(index < GetNumOfParents() && index >= 0 )
        fParents.erase(fParents.begin()+index);
    else
    {
//        std::stringstream ss;
//        ss << "Parent was not found"<<std::endl;
//        ss << "Index was found to be "<<index<<" and parent container size is "<<GetNumOfParents()<<std::endl;
//        ss << "We were looking for the parent named: "<<parent->GetName()<<std::endl;
//        ss << "The current node is named: "<<this->GetName()<<std::endl;

//        Utils::Exception(Utils::ExceptionType::Warning, "VSceneNode::RemoveParentOnly",
//                         ss.str() );

    }
}

void VSceneNode::RemoveAllParentOnly()
{
    fParents.clear();
}

int VSceneNode::FindParentIndex(VSceneNode* parent)
{
    // Could be faster with std::binary_search(fParents.begin(), fParents.end(), parent);
    return static_cast<int>(std::distance(fParents.begin(), std::find(fParents.begin(), fParents.end(), parent) ) );
}

void VSceneNode::AddChild(VSceneNode *child)
{
    child->AddParentOnly(this);
    fChildren.push_back(child);
}

void VSceneNode::AddCopyChild(VSceneNode *childToBeCopied)
{
    // Create a deep copy of the object
    VSceneNode* copyChild = childToBeCopied->Clone();

    copyChild->RemoveAllParentOnly();

    AddChild(copyChild);
}

void VSceneNode::AddChild(VSceneNode* child, int pos)
{
    child->AddParentOnly(this);

    auto it = fChildren.begin();

    std::advance(it, pos);

    fChildren.insert(it, child);
}

void VSceneNode::AddCopyChild(VSceneNode *childToBeCopied, int pos)
{
    // Create a deep copy of the object
    VSceneNode* copyChild = childToBeCopied->Clone();

    copyChild->RemoveAllParentOnly();

    AddChild(copyChild, pos);
}

void VSceneNode::RemoveChild(int index)
{
    // Delete the child if it has no parents

    GetChild(index)->RemoveParentOnly(this);

    if(GetChild(index)->GetNumOfParents() == 0) delete fChildren.at(index );

    fChildren.erase(fChildren.begin()+index);

    // Release unused memory
    fChildren.shrink_to_fit();
}

void VSceneNode::RemoveChild(VSceneNode *child)
{
    RemoveChild(FindChildIndex(child) );
}

int VSceneNode::FindChildIndex(VSceneNode *child)
{
    auto it = std::find(fChildren.begin(), fChildren.end(), child);

    int index = std::distance(fChildren.begin(), it);

    if(index >= fChildren.size() )
    {
        std::stringstream ss;
        ss << "Index was not found \n";
        ss << "Child name: "<<child->GetName()<<"\n";
        ss << "Current node name: " <<this->GetName()<<"\n";

        Utils::Exception(Utils::ExceptionType::Warning, "VSceneNode::FindChildIndex",
                         ss.str() );
    }

    return index;
}

VSceneNode *VSceneNode::GetLastAddedChild() const
{
    if(GetNumOfChildren() > 0)
        return fChildren.at(GetNumOfChildren()-1);
    else
        return nullptr;
}

void VSceneNode::AddComponent(Logic::VComponent* component, Logic::List l)
{
    if(HasComponent(l))
        RemoveComponent(l);

    component->SetOwnerNode(this);
    component->SetTypeId(l);

    fComponents[l] = component;
}

void VSceneNode::RemoveComponent(Logic::List index)
{
    if(HasComponent(index) )
    {
        Logic::VComponent* compo = GetComponent(index);
        delete compo;
        fComponents.erase(index);
    }
}

void VSceneNode::RemoveAllComponents()
{
    for(auto& i : fComponents)
    {
        if(i.second != nullptr)
            delete i.second;
    }

    fComponents.clear();
}

bool VSceneNode::HasComponent(Logic::List index) const
{
    return (fComponents.find(index) != fComponents.end() ) ? true : false;
}

void VSceneNode::SetRenderable(Logic::VComponent* component)
{
    using namespace Logic;

    AddComponent(component, List::Renderable);
}

Logic::Renderable *VSceneNode::GetRenderable()
{
    using namespace Logic;

    if(HasComponent(List::Renderable) )
        return static_cast<Renderable*>(GetComponent(List::Renderable) );
    else
        return nullptr;
}

const Logic::Renderable *VSceneNode::GetRenderable() const
{
    using namespace Logic;

    if(HasComponent(List::Renderable) )
        return static_cast<const Renderable*>(GetComponent(List::Renderable) );
    else
        return nullptr;
}

bool VSceneNode::HasRenderable() const
{
    return HasComponent(Logic::List::Renderable);
}

std::string VSceneNode::DescribeYourself()
{
    std::ostringstream oss;

    int versionIO = 2;
    oss << "_versionIO_SceneNode "<<versionIO<<"\n";

    oss << "_Name " << fName << "\n";
    oss << "_Type " << fType << "\n";
    oss << "_CopyNumber " << fId << "\n";

    oss << "_ModelMatrix ";
    for(int i=0, ie=16; i<ie; ++i)
    {
        oss << *(fModelMat.data()+i) << " ";
    }
    oss << "\n";

    return oss.str();
}

void VSceneNode::LoadYourself(std::ifstream &is)
{
    std::string dump;

    int versionIO;
    is >> dump >> versionIO;

    if(versionIO == 2)
    {
        is >> dump >> fName;
        is >> dump >> fType;
        is >> dump >> fId;

        is >> dump;
        std::vector<float> data(16);
        for(int i=0, ie=16; i<ie; ++i)
        {
            is >> data[i];
        }
        SetModelMat( (QMatrix4x4(&data[0])).transposed() );
    }
    else if(versionIO == 1)
    {
        bool isDisplayable;
        QColor color;

        is >> dump >> fName;
        is >> dump >> fType;
        is >> dump >> fId;
        is >> dump >> isDisplayable;

        int r,g,b,a;
        is >> dump >> r >> g >> b >> a;
        color = QColor(r,g,b,a);

        is >> dump;
        std::vector<float> data(16);
        for(int i=0, ie=16; i<ie; ++i)
        {
            is >> data[i];
        }
        SetModelMat( (QMatrix4x4(&data[0])).transposed() );
    }
    else
    {
        std::ostringstream oss;
        oss << "The input version: "<<versionIO<<" is not registered.\n";
        oss << "Nothing is done.";
        Utils::Exception(Utils::ExceptionType::Warning, "SceneNode::LoadYourself", oss.str() );
    }
}

bool VSceneNode::RecursivelyAnalyseParentsToLookFor(const VSceneNode *currentNode, const VSceneNode *searchedParentNode, QMatrix4x4 model, QMatrix4x4 &res) const
{
    bool isFound = false;

    // The order of the multiplication does matter
    model = currentNode->GetModelMat() * model;

    if(currentNode == searchedParentNode)
    {
        isFound=true;
        res = model;
    }

    int parentNum = currentNode->GetNumOfParents();
    int i = 0;

    while( i< parentNum && !isFound)
    {
        const VSceneNode* parent = currentNode->GetParent(i);

        isFound = RecursivelyAnalyseParentsToLookFor(parent, searchedParentNode, model, res);

        ++i;
    }

    return isFound;
}

void VSceneNode::CloneTheChildrenOf(const VSceneNode& toBeCopied)
{
   RemoveAllParentOnly();
   fChildren.clear();

   // Loop on all the childs to be cloned
   for(int i=0, ie=toBeCopied.GetNumOfChildren(); i<ie; i++)
   {
       AddChild(toBeCopied.GetChild(i)->Clone() );
   }
}

void VSceneNode::RemoveAllChildren()
{
    for(int i=GetNumOfChildren()-1; i>=0; i--)
    {
        RemoveChild(i);
    }

    // Clear the container with all the pointers.
    fChildren.clear();

    // Release the memory
    fChildren.shrink_to_fit();
}

void VSceneNode::RemoveAllChildrenOnly()
{
    // Clear the container with all the pointers.
    fChildren.clear();

    // Release the memory
    fChildren.shrink_to_fit();
}

}}
