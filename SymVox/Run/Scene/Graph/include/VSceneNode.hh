/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef SCENE_VSCENENODE_HH
#define SCENE_VSCENENODE_HH

#include <sstream>
#include <fstream>
#include <QMatrix4x4>
#include <QColor>

#include "SymVox/Utils/include/Exception.hh"
#include "SymVox/Logic/Component/Management/include/VComponent.hh"
#include "SymVox/Logic/Component/Renderable/include/Renderable.hh"

namespace SV { namespace Run {

class VSceneNode
{
public:
    VSceneNode();
    VSceneNode(const std::string& name);
    virtual ~VSceneNode();

    VSceneNode(const VSceneNode& toBeCopied);
    virtual VSceneNode* Clone() const = 0;
    void Copy(const VSceneNode& toBeCopied);

    // Name
    void SetName(const std::string& name) {fName=name;}
    const std::string& GetName() const {return fName;}

    // Id
    void SetId(int id) {fId=id;}
    int GetId() const {return fId;}

    void SetType(const std::string& type) {fType=type;}
    const std::string& GetType() const {return fType;}

    // Wrapper status
    bool GetIsWrapperNode() const {return fIsWrapperNode;}

    // Model matrix
    void SetModelMat(const QMatrix4x4& modelMat) {fModelMat=modelMat;}
    const QMatrix4x4& GetModelMat() const {return fModelMat;}
    QMatrix4x4& GetModelMat() {return fModelMat;}
    void ApplyTransform(const QMatrix4x4& trans);

    // Set 3D position
    void SetPosition(const QVector3D& pos);

    // Translations
    void TranslateInLocalFrame(const QVector3D& vect);
    void TranslateInLocalFrame(float x, float y, float z);
    void TranslateInParentFrame(const QVector3D& vect);
    void TranslateInFrame(const QVector3D& transVect, const QMatrix4x4& frame);

    // Rotations
    void RotateInLocalFrame(float angle, const QVector3D& axis);
    void RotateInLocalFrame(float angle, float x, float y, float z);
    void RotateInLocalFrame(const QQuaternion& quaternion);
    void RotateInParentFrame(float angle, const QVector3D& axis);
    void RotateInFrame(float angle, const QVector3D& axis, const QMatrix4x4& frame);

    // Extract and Get
    QMatrix3x3 ExtractRotMatFromPosAndRot(const QMatrix4x4 &mat4x4) const;
    QVector3D ExtractPosFromMatrix(const QMatrix4x4& model) const;
    QMatrix4x4 GetModelMatrixInParentFrame(const VSceneNode *parent) const;
    QVector3D GetPosVectorInParentFrame(const VSceneNode *parent) const;
    QVector3D GetPosVectorInDirectParentFrame(const QMatrix4x4& frame) const;
    QVector3D GetPosVectorInMotherFrame() const;

    // Convert
    QVector3D ConvertToLocalFromParentFrame(const QVector3D& vect);
    QVector3D ConvertToLocalFromFrame(const QVector3D& vect, const QMatrix4x4& frame);
    QVector3D ConvertToParentFromLocalFrame(const QVector3D& vect);
    QVector3D ConvertToParentFromFrame(const QVector3D& vect, const QMatrix4x4& frame);

    // Parents
    void AddParent(VSceneNode* parent);
    void AddParentOnly(VSceneNode* parent);
    void RemoveParentOnly(int index);
    void RemoveParentOnly(VSceneNode* parent);
    void RemoveAllParentOnly();
    int GetNumOfParents() const {return fParents.size();}
    int FindParentIndex(VSceneNode* parent);
    VSceneNode* GetParent(int index=0) {return fParents.at(index);}
    const VSceneNode* GetParent(int index=0) const {return fParents.at(index);}

    // Children
    void AddChild(VSceneNode* child); // Addition at the end of the vector
    void AddChild(VSceneNode* child, int pos); // Insertion in the vector at "pos"
    void AddCopyChild(VSceneNode* childToBeCopied);
    void AddCopyChild(VSceneNode* childToBeCopied, int pos);
    void RemoveChild(int index); // Remove and delete child if it has no parent anymore
    void RemoveChild(VSceneNode* child); // Remove and delete child if it has no parent anymore
    //void RemoveChildOnly(int index); // Only remove the child address of the vector
    //void RemoveChildOnly(VSceneNode* child); // Only remove the child address of the vector
    void RemoveAllChildren(); // Clear the child vector and delete the children if they have zero parent
    void RemoveAllChildrenOnly(); // Clear the child vector
    int GetNumOfChildren() const {return fChildren.size();}
    int FindChildIndex(VSceneNode* child);
    VSceneNode* GetChild(int index) {return fChildren.at(index);}
    const VSceneNode* GetChild(int index) const {return fChildren.at(index);}
    VSceneNode* GetLastAddedChild() const;

    // Components
    void AddComponent(Logic::VComponent* component, Logic::List l);
    void RemoveComponent(Logic::List index);
    void RemoveAllComponents();
    int GetNumOfComponents() {return fComponents.size();}
    Logic::VComponent* GetComponent(Logic::List index) {return fComponents.at(index);}
    const Logic::VComponent* GetComponent(Logic::List index) const {return fComponents.at(index);}
    bool HasComponent(Logic::List index) const;
    // Renderable
    void SetRenderable(Logic::VComponent* component);
    Logic::Renderable* GetRenderable();
    const Logic::Renderable* GetRenderable() const;
    bool HasRenderable() const;

    virtual std::string DescribeYourself();
    virtual void LoadYourself(std::ifstream &is);

protected:
    bool fIsWrapperNode;
    int fId;
    std::string fName;
    std::string fType;
    QMatrix4x4 fModelMat;

    std::vector<VSceneNode*> fParents;
    std::vector<VSceneNode*> fChildren;

    std::map<Logic::List, Logic::VComponent*>  fComponents;

    bool RecursivelyAnalyseParentsToLookFor(const VSceneNode* currentNode, const VSceneNode* searchedParentNode, QMatrix4x4 model, QMatrix4x4& res) const;

    void CloneTheChildrenOf(const VSceneNode& toBeCopied);

    VSceneNode& operator=(const VSceneNode& toBeCopied);
};

}}

#endif // SCENE_VSCENENODE_HH
