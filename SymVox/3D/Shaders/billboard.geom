#version 330

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = 4) out;

uniform mat4 gP;
uniform mat4 gV;
uniform vec3 gCameraPos;
uniform float gBillboardSize;

out vec2 TexCoord;

void main()
{
    // Generate ViewProjection matrix
    mat4 gVP = gP * gV;

    vec3 Pos = gl_in[0].gl_Position.xyz;

    // The line above is unique to the GS. Since it is executed on a complete primitive we actually
    // have access to each of the vertices that comprise it. This is done using the built-in variable 'gl_in'.
    // This variable is an array of structures that contains, among other things, the position that was written
    // into gl_Position in the VS. To access it we go to the slot we are interested in using the index of the vertex.
    // In this specific example the input topology is point list so there is only a single vertex.
    // We access it using 'gl_in[0]'. If the input topology was a triangle we could also
    // have written 'gl_in[1]' and 'gl_in[2]'. We only need the first three components of the position
    // vector and we extract them to a local variable using '.xyz'.

    // In initial tuto, billboard were not adjusted in Z direction.

    vec3 toCamera = normalize(gCameraPos - Pos);
    vec3 up = vec3(0.0, 1.0, 0.0);
    // vec3 right = cross(toCamera, up);
    vec3 right = normalize(cross(toCamera, up));

    // This is to make to billboard axe to follow the camera axe
    vec3 up2 = normalize(cross(right, toCamera));

    // Here we make the billboard face the camera per the explanation at the end of the background section.
    // We do a cross product between the vector from the point to the camera and a vector that points straight up.
    // This provides the vector that points right when looking at the point from the camera point of view.
    // We will now use it to 'grow' a quad around the point.

    Pos -= (right * 0.5) * gBillboardSize;
    Pos -= up2 * 0.5 * gBillboardSize;
    gl_Position = gVP * vec4(Pos, 1.0);
    TexCoord = vec2(0.0, 0.0);
    EmitVertex();

    //Pos.y += 1.0;
    Pos += up2 * gBillboardSize;
    gl_Position = gVP * vec4(Pos, 1.0);
    TexCoord = vec2(0.0, 1.0);
    EmitVertex();

    //Pos.y -= 1.0;
    Pos -= up2 * gBillboardSize;
    Pos += right * gBillboardSize;
    gl_Position = gVP * vec4(Pos, 1.0);
    TexCoord = vec2(1.0, 0.0);
    EmitVertex();

    //Pos.y += 1.0;
    Pos += up2 * gBillboardSize;
    gl_Position = gVP * vec4(Pos, 1.0);
    TexCoord = vec2(1.0, 1.0);
    EmitVertex();

    EndPrimitive();
}
