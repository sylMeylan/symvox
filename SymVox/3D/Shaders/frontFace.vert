// GLSL version
#version 150 core

// Entries
in vec3 in_Vertex;
in vec4 in_Color;
in vec2 in_TextureCoord;
in vec3 in_NormalCoord;

// Uniform
uniform mat4 modelviewProjection;
uniform mat4 view;
uniform mat4 model;

uniform vec3 lightPosition_worldspace;
uniform float lightIntensity;

// Ouptut
out vec4 color;
out vec3 position_worldspace;
out vec3 eyeDirection_cameraspace;
out vec3 lightDirection_cameraspace;
out vec3 normal_cameraspace;

out vec2 mapping;

// Fonction main
void main()
{
        // Final vertex position
//        gl_Position = modelviewProjection * vec4(in_Vertex, 1.0);


        gl_Position = vec4(in_Vertex, 1.0) + modelviewProjection * vec4(0,0,0,1);

        mapping = in_Vertex.xy;


    // Color send to the fragment shader
    color = in_Color;

    // Vertex position in world space
    position_worldspace = ( model * vec4(in_Vertex, 1.0) ).xyz;

    // Vector from the vertex to the camera in camera space.
    // Camera is at the origin within camera space.
    vec3 vertexPosition_cameraspace = ( view * model * vec4(in_Vertex, 1.0) ).xyz;
    eyeDirection_cameraspace = vec3(0.,0.,0.) - vertexPosition_cameraspace;

    // Vector from the vertex to the light within camera space.
    // Model is ommited because it is identity.
    vec3 lightPosition_cameraspace = ( view * vec4(lightPosition_worldspace, 1.0) ).xyz;
    lightDirection_cameraspace = lightPosition_cameraspace + eyeDirection_cameraspace;

    // Vertex normal within camera space
    // This is only correct if the model matrix does not scale the object,
    // if it does then use the model inverse transpose matrix.
    normal_cameraspace = (inverse(view) * transpose(model) * vec4(in_NormalCoord, 0) ).xyz;

    // UV (texture coord)
    // UV = vertexUV;
}
