#version 330 core

uniform sampler2D material_diffuse;
uniform sampler2D material_specular;
uniform sampler2D material_emission;
struct Material {
    //vec3 ambient;
    //vec3 diffuse;
    //vec3 specular;
    float shininess;
};

struct Light {
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

out vec4 color;

uniform vec3 viewPos;
uniform Material material;
uniform Light light;

void main()
{
    // Ambient
    vec3 ambient = light.ambient * vec3(texture(material_diffuse, TexCoords));

    // Diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(light.position - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * vec3(texture(material_diffuse, TexCoords)); //light.diffuse * (diff * material.diffuse);

    // Specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * spec * vec3(texture(material_specular, TexCoords));

    // Emission
    vec3 emission = vec3(texture(material_emission, TexCoords));

    // Spotlight (soft edges)
    float theta = dot(lightDir, normalize(-light.direction));
    float epsilon = (light.cutOff - light.outerCutOff);
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    diffuse  *= intensity;
    specular *= intensity;

    // Attenuation
    float distance    = length(light.position - FragPos);
    float attenuation = 1.0f / (light.constant + light.linear * distance +
                        light.quadratic * (distance * distance));
    ambient  *= attenuation;
    diffuse  *= attenuation;
    specular *= attenuation;

    // Result
    vec3 result = ambient + diffuse + specular + emission;
    color = vec4(result, 1.0f);
}


// θ, θ in degrees, ϕ (inner cutoff), ϕ in degrees, γ (outer cutoff), γ (outer cutoff), ϵ, I
//
// 0.87 	30 	0.91 	25 	0.82 	35 	0.91 - 0.82 = 0.09 	0.87 - 0.82 / 0.09 = 0.56
// 0.9 	26 	0.91 	25 	0.82 	35 	0.91 - 0.82 = 0.09 	0.9 - 0.82 / 0.09 = 0.89
// 0.97 	14 	0.91 	25 	0.82 	35 	0.91 - 0.82 = 0.09 	0.97 - 0.82 / 0.09 = 1.67
// 0.83 	34 	0.91 	25 	0.82 	35 	0.91 - 0.82 = 0.09 	0.83 - 0.82 / 0.09 = 0.11
// 0.64 	50 	0.91 	25 	0.82 	35 	0.91 - 0.82 = 0.09 	0.64 - 0.82 / 0.09 = -2.0
// 0.966 	15 	0.9978 	12.5 	0.953 	17.5 	0.966 - 0.953 = 0.0448 	0.966 - 0.953 / 0.0448 = 0.29
