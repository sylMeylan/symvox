// GLSL version
#version 330 core

// Entries
layout (location = 0) in vec3 in_Vertex;
layout (location = 1) in vec4 in_Color;
layout (location = 2) in vec2 in_TextureCoord;
layout (location = 3) in vec3 in_NormalCoord;
in mat4 in_Model; // location 4, 5, 6 and 7
// After this line it is mandatory to add location information because of the previous 4x4 matrix
layout (location = 8) in float in_Radius;
layout (location = 9) in vec4 in_InstanceColor;

// Uniform
uniform vec3 lightPosition_worldspace;
uniform float lightIntensity;
uniform mat4 view;
uniform mat4 projection;

// Ouptut
out vec4 color;
out vec3 position_worldspace;
out vec3 eyeDirection_cameraspace;
out vec3 lightDirection_cameraspace;
out vec3 normal_cameraspace;

// Fonction main
void main()
{
        mat4 modelViewProjection = projection * view * in_Model;
        mat4 modelView = view * in_Model;

        // Final vertex position
        gl_Position = modelViewProjection * vec4(in_Vertex, 1.0/in_Radius);

        // Color send to the fragment shader
        color = in_InstanceColor;

        // Vertex position in world space
        position_worldspace = ( in_Model * vec4(in_Vertex, 1.0) ).xyz;

        // Vector from the vertex to the camera in camera space.
        // Camera is at the origin within camera space.
        vec3 vertexPosition_cameraspace = ( modelView * vec4(in_Vertex, 1.0) ).xyz;
        eyeDirection_cameraspace = vec3(0.,0.,0.) - vertexPosition_cameraspace;

        // Vector from the vertex to the light within camera space.
        // in_Model is ommited because it is identity.
        vec3 lightPosition_cameraspace = ( view * vec4(lightPosition_worldspace, 1.0) ).xyz;
        lightDirection_cameraspace = lightPosition_cameraspace + eyeDirection_cameraspace;

        // Vertex normal within camera space
        // This is only correct if the in_Model matrix does not scale the object,
        // if it does then use the in_Model inverse transpose matrix.
        normal_cameraspace = (modelView * vec4(in_NormalCoord, 0) ).xyz;

        // UV (texture coord)
        // UV = vertexUV;

        int InstanceID = gl_InstanceID;
}
