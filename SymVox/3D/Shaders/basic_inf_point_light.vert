#version 330 core

in vec3 in_Vertex;
in vec4 in_Color;
in vec2 in_TextureCoord;
in vec3 in_NormalCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 FragPos;
out vec3 Normal;
out vec2 TexCoords;

void main()
{
    gl_Position = projection * view * model * vec4(in_Vertex, 1.0f);
    FragPos = vec3(model * vec4(in_Vertex, 1.0f) );
    TexCoords = in_TextureCoord;
    Normal = mat3(transpose(inverse(model))) * in_NormalCoord;
}
