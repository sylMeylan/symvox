#version 330 core

layout (location = 0) in vec3 in_Vertex;
layout (location = 1) in vec4 in_Color;
layout (location = 2) in vec2 in_TextureCoord;
layout (location = 3) in vec3 in_NormalCoord;
in mat4 in_Model; // location 4, 5, 6 and 7
// After this line it is mandatory to add location information because of the previous 4x4 matrix
// For example: layout (location = 8) in float in_Radius;

//uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 FragPos;
out vec3 Normal;
out vec4 colorForFrag;

void main()
{
    gl_Position = projection * view * in_Model * vec4(in_Vertex, 1.0f);
    FragPos = vec3(in_Model * vec4(in_Vertex, 1.0f) );
    colorForFrag = in_Color;
    Normal = mat3(transpose(inverse( in_Model ))) * in_NormalCoord;
}
