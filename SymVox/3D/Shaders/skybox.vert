#version 330 core

//layout (location = 0) in vec3 in_position;

in vec3 in_Vertex;
in vec4 in_Color;
in vec2 in_TextureCoord0;
in vec3 in_NormalCoord;
in float  in_Model;

uniform mat4 cam_mat;
uniform mat4 view;
uniform mat4 projection;

out vec3 texCoord0;

void main()
{
    vec4 WVP_pos = projection * view * cam_mat * vec4(in_Vertex, 1.0);
    gl_Position = WVP_pos.xyww;
    texCoord0 = in_Vertex;
}
