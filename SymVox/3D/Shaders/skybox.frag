#version 330

in vec3 texCoord0;

uniform samplerCube gCubeMaptexture;

out vec4 fragColor;

void main()
{
    fragColor = texture(gCubeMaptexture, texCoord0);
}
