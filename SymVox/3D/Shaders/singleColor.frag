#version 330 core

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

out vec4 color;

void main()
{
    color = vec4(0.04, 0.28, 0.26, 1.0);
}
