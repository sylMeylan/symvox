#version 330

uniform sampler2D gColorMap;

in vec2 TexCoord;
in float Type2;
out vec4 FragColor;

#define PARTICLE_TYPE_LAUNCHER 0.0f
#define PARTICLE_TYPE_SHELL 1.0f
#define PARTICLE_TYPE_SECONDARY_SHELL 2.0f

void main()
{
    FragColor = texture2D(gColorMap, TexCoord);

    if(Type2 == PARTICLE_TYPE_SECONDARY_SHELL
            && FragColor.r >= 0.7
            && FragColor.g < 0.6
            && FragColor.b < 0.6
            )
    {
        FragColor = vec4(1,0.9,0,1); // Orange/Yellow
    }

    if (FragColor.r >= 0.9 && FragColor.g >= 0.9 && FragColor.b >= 0.9) {
        discard;
    }
}
