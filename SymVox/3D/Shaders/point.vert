// GLSL version
#version 330 core

// Entries
in vec3 in_Vertex;
in vec4 in_Color;
in vec2 in_TextureCoord;
in vec3 in_NormalCoord;
in mat4 in_Model;

// Uniform
uniform vec3 lightPosition_worldspace;
uniform float lightIntensity;
uniform mat4 view;
uniform mat4 projection;

// Ouptut
out vec4 color;
out vec3 position_worldspace;
out vec3 eyeDirection_cameraspace;
out vec3 lightDirection_cameraspace;
out vec3 normal_cameraspace;

// Fonction main
void main()
{
        mat4 modelViewProjection = projection * view * in_Model;
        mat4 modelView = view * in_Model;

        // Final vertex position
        gl_Position = modelViewProjection * vec4(in_Vertex, 1.0);

        gl_PointSize = 6.0;

        // Color send to the fragment shader
        color = in_Color;

        //int InstanceID = gl_InstanceID;
}
