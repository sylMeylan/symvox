#version 330

layout (location = 0) in vec3 Position;
layout (location = 1) in float Type;

uniform mat4 view;
uniform mat4 projection;

out float Type1;

void main()
{
    gl_Position = vec4(Position, 1.0);
    Type1 = Type;
}
