#version 330 core

// Interpolated values from the vertex shader
in vec4 color;
in vec3 position_worldspace;
in vec3 normal_cameraspace;
in vec3 eyeDirection_cameraspace;
in vec3 lightDirection_cameraspace;
in vec2 coordTexture;

// constant values
uniform vec3 lightPosition_worldspace;
uniform float lightIntensity;
uniform mat4 view;
uniform mat4 projection;
// Uniform texture
uniform sampler2D tex;

out vec4 out_Color;


void main()
{
    // Light emission properties
    // Usually they are uniforms
    vec3 lightColor = vec3(1.0); // white light
    float lightPower = lightIntensity;

    vec4 colorC = color;

    //color = texture(tex, coordTexture);
    vec4 texColor = texture(tex, coordTexture);

    // Material property
     //out_Color = vec4(color, 1.0); // material diffuse color
     vec3 materialDiffuseColor = texColor.rgb;
     vec3 materialAmbientColor = vec3(0.6) * materialDiffuseColor;
     vec3 materialSpecularColor = vec3(0.3);

     // Distance to the light
     float distance = length(lightPosition_worldspace - position_worldspace);

     // Normal of the computed fragment within camera space
     vec3 n = normalize(normal_cameraspace);

     // Direction of the light (from the fragment to the light)
     vec3 l = normalize(lightDirection_cameraspace);

     // Cosinus of the angle between the normal and the light direction,
     // clamped above 0
     // - light is at the vertical of the triangle: 1
     // - light is perpendiculat to the triangle: 0
     // - light is behind the triangle: 0
     float cosTheta = clamp( dot(n,l), 0, 1);

     // Eye vector (towards the camera)
     vec3 E = normalize(eyeDirection_cameraspace);

     // Direction in which the triangle reflects the light
     vec3 R = reflect(-l, n);

     // Cosinus of the angle between the eye vector and the reflect vector,
     // clamped to 0
     // - looking into the reflection: 1
     // - looking elswhere : <1
     float cosAlpha = clamp( dot(E,R), 0, 1 );

     // Ambient: indirect lighting
     // Diffuse: color of the object
     // Specular: reflective highlight like a mirror
     vec3 color_beforeOut =
             materialAmbientColor
             + materialDiffuseColor * lightColor * lightPower * cosTheta/(distance*distance)
             + lightColor * lightPower * pow(cosAlpha,10)/(distance*distance);

     //out_Color = vec4(color_beforeOut, texColor.a);

     //out_Color.rgb = pow(out_Color.rgb, vec3(1.0 / 2.2));

    out_Color = vec4(color_beforeOut, texColor.a);
    //out_Color.rgb = pow(out_Color.rgb, vec3(1.0 / 2.2));

    //out_Color =  texColor;

//     out_Color = vec4(normal_cameraspace, 1.0);
}
