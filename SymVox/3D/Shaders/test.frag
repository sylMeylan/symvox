//precision highp float;
//varying vec3 position_worldspace;
//varying vec3 normal_cameraspace;

#version 150 core

in vec4 color;
in vec3 position_worldspace;
in vec3 normal_cameraspace;
in vec3 eyeDirection_cameraspace;
in vec3 lightDirection_cameraspace;

in vec2 mapping;

uniform float lightIntensity;

out vec4 out_Color;

const float pi = 3.141597;

void main()
{
  out_Color = vec4(1.0, 0.0, 0.0,1.0) ;

  float radius = sqrt(dot(mapping, mapping));

  if(radius > 1.0)
  {
      out_Color = vec4(0.0) ;
      return;
  }

  float angle = atan((mapping.y / mapping.x)) ;
  if(angle < 0) angle += pi;
  if(mapping.y < 0) angle += pi;

  float angle2 = asin(radius);

//  vec3 color = vec3(angle / 2 / pi);
//  vec3 color = vec3(angle2 / 2 / pi);

  vec3 frag_normal = vec3(cos(angle) * sin(angle2), sin(angle) * sin(angle2), cos(angle2));

//  vec3 color = 0.5*(vec3(1.0) + frag_normal);
//  vec3 color = 0.5*(vec3(1.0) + normalize(eyeDirection_cameraspace));

//  float t = dot(-frag_normal, vec3(0, 0, -1));
  float t = dot(frag_normal, normalize(lightDirection_cameraspace));
  t = max(t, 0.0);


  float t2 = dot(reflect(eyeDirection_cameraspace, frag_normal), normalize(lightDirection_cameraspace));
  t2 = max(t, 0.0);
  t2 = pow(t, 128);

  vec3 color = vec3(0.0, 0.0, 1.0) * 0.1
             + vec3(1.0, 0.0, 0.0) * t
             + vec3(1.0, 1.0, 1.0) * t2;


  out_Color = vec4(color, 1.0);
}

