#version 330 core

uniform sampler2D material_diffuse;
uniform sampler2D material_specular;
uniform sampler2D material_emission;
struct Material {
    //vec3 ambient;
    //vec3 diffuse;
    //vec3 specular;
    float shininess;
};

struct Light {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

out vec4 color;

uniform vec3 viewPos;
uniform Material material;
uniform Light light;

void main()
{
    // Ambient
    vec3 ambient = light.ambient * vec3(texture(material_diffuse, TexCoords));

    // Diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(light.position - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * vec3(texture(material_diffuse, TexCoords)); //light.diffuse * (diff * material.diffuse);

    // Specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * spec * vec3(texture(material_specular, TexCoords));

    // Emission
    vec3 emission = vec3(texture(material_emission, TexCoords));

    // Attenuation
    float distance    = length(light.position - FragPos);
    float attenuation = 1.0f / (light.constant + light.linear * distance +
                        light.quadratic * (distance * distance));
    ambient  *= attenuation;
    diffuse  *= attenuation;
    specular *= attenuation;

    // Result
    vec3 result = ambient + diffuse + specular + emission;
    color = vec4(result, 1.0f);
}

// Ogre3D wiki
// Distance 	Constant 	Linear 	Quadratic
// 7 	1.0 	0.7 	1.8
// 13 	1.0 	0.35 	0.44
// 20 	1.0 	0.22 	0.20
// 32 	1.0 	0.14 	0.07
// 50 	1.0 	0.09 	0.032
// 65 	1.0 	0.07 	0.017
// 100 	1.0 	0.045 	0.0075
// 160 	1.0 	0.027 	0.0028
// 200 	1.0 	0.022 	0.0019
// 325 	1.0 	0.014 	0.0007
// 600 	1.0 	0.007 	0.0002
// 3250 	1.0 	0.0014 	0.000007
