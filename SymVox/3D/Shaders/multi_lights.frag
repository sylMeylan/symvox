#version 330 core

// Uniform sampler2D variables are defined outside of the struct to avoid
// compatibility issues.
uniform sampler2D material_diffuse;
uniform sampler2D material_specular;
uniform sampler2D material_emission;
struct Material {
    //vec3 ambient;
    //vec3 diffuse;
    //vec3 specular;
    float shininess;
};

struct DirLight {
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 position;

    float constant;
    float linear;
    float quadratic;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;

    float constant;
    float linear;
    float quadratic;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

const int MAX_NR_POINT_LIGHTS = 4;
const int MAX_NR_SPOT_LIGHTS = 4;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

out vec4 color;

uniform vec3 viewPos;
uniform DirLight dirLight;
uniform PointLight pointLights[MAX_NR_POINT_LIGHTS];
uniform SpotLight spotLights[MAX_NR_SPOT_LIGHTS];
uniform Material material;
uniform int numPointLights;
uniform int numSpotLights;

// Function prototypes
vec4 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
vec4 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec4 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{
    // Properties
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - FragPos);

//    float alpha_value = texture(material_diffuse, TexCoords).w;
//    if(alpha_value < 0.1)
//        discard;

    // == ======================================
    // Our lighting is set up in 3 phases: directional, point lights and an optional flashlight
    // For each phase, a calculate function is defined that calculates the corresponding color
    // per lamp. In the main() function we take all the calculated colors and sum them up for
    // this fragment's final color.
    // == ======================================

    // Phase 1: Directional lighting
    vec4 result = CalcDirLight(dirLight, norm, viewDir);

    // Phase 2: Point lights
    for(int i=0; i < numPointLights; i++)
    {
        result += CalcPointLight(pointLights[i], norm, FragPos, viewDir);
    }

    // Phase 3: Spot light
    for(int i=0; i < numSpotLights; i++)
    {
        result += CalcSpotLight(spotLights[i], norm, FragPos, viewDir);
    }

    vec4 emission = texture(material_emission, TexCoords);

    result += emission;

    // Final results
    color = result;
}

// Calculates the color when using a directional light.
vec4 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(-light.direction);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // Combine results
    vec4 ambient = vec4(light.ambient, 1.0f) * texture(material_diffuse, TexCoords);
    vec4 diffuse = vec4(light.diffuse, 1.0f) * diff * texture(material_diffuse, TexCoords);
    vec4 specular = vec4(light.specular, 1.0f) * spec * texture(material_specular, TexCoords);

    return (ambient + diffuse + specular);
}

// Calculates the color when using a point light.
vec4 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0f);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);
    // Attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0f / (light.constant + light.linear*distance + light.quadratic*(distance*distance) );
    // Combine results
    vec4 ambient = vec4(light.ambient, 1.0f) * texture(material_diffuse, TexCoords);
    vec4 diffuse = vec4(light.diffuse, 1.0f) * diff * texture(material_diffuse, TexCoords);
    vec4 specular = vec4(light.specular, 1.0f) * spec * texture(material_specular, TexCoords);
    vec4 emission = texture(material_emission, TexCoords);
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    return (ambient + diffuse + specular);
}

// Calculates the color when using a spot light.
vec4 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // Diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // Specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // Attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    // Spotlight intensity
    float theta = dot(lightDir, normalize(-light.direction));
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    // Combine results
    vec4 ambient = vec4(light.ambient, 1.0f) * texture(material_diffuse, TexCoords);
    vec4 diffuse = vec4(light.diffuse, 1.0f) * diff * texture(material_diffuse, TexCoords);
    vec4 specular = vec4(light.specular, 1.0f) * spec * texture(material_specular, TexCoords);
    vec4 emission = texture(material_emission, TexCoords);
    ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;

    return (ambient + diffuse + specular);
}


