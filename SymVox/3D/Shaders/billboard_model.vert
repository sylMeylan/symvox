#version 330

layout(location = 0) in vec4 in_Model_0;
layout(location = 1) in vec4 in_Model_1;
layout(location = 2) in vec4 in_Model_2;
layout(location = 3) in vec4 in_Model_3;
layout(location = 4) in vec3 Position;
layout(location = 5) in vec4 indi_color;
layout(location = 6) in float size;

out vec4 vColor;
out float vSize;

void main()
{
    vColor = indi_color;
    vSize = size;

    mat4 in_Model = mat4(
                          in_Model_0,           // first column
                          in_Model_1,           // second column
                          in_Model_2,           // third column
                          in_Model_3          // fourth column
                         );

    gl_Position = in_Model * vec4(Position, 1);
}

