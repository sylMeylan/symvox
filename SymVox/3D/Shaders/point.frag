#version 330 core

// Interpolated values from the vertex shader
in vec4 color;
in vec3 position_worldspace;
in vec3 normal_cameraspace;
in vec3 eyeDirection_cameraspace;
in vec3 lightDirection_cameraspace;

out vec4 out_Color;

// constant values
uniform vec3 lightPosition_worldspace;
uniform float lightIntensity;
uniform mat4 view;
uniform mat4 projection;

void main()
{


     out_Color = color;
}
