//precision highp float;
//varying vec3 position_worldspace;
//varying vec3 normal_cameraspace;

#version 150 core

in vec4 color;
in vec3 position_worldspace;
in vec3 normal_cameraspace;
in vec3 eyeDirection_cameraspace;
in vec3 lightDirection_cameraspace;
uniform float lightIntensity;

out vec4 out_Color;

vec3 rim(vec3 color, float start, float end, float coef)
{
  vec3 normal = normalize(normal_cameraspace);
  vec3 eye = vec3(0,0,1);
  float rim = smoothstep(start, end, 1.0 - dot(normal, eye));
  return clamp(rim, 0.0, 1.0) * coef * color;
}

float phong()
{
  return dot(normalize(normal_cameraspace), normalize(lightDirection_cameraspace));
}

vec3 desaturate(vec3 color)
{
//  return mix(color, vec3(1,1,1), 0.9);
  return color + vec3(1,2,3) * 0.5;
}

void main()
{
//  vec3 color_final = rim(vec3(1.0,2.0,3.0), 0.0, 2.0, 1.0);

  vec3 initial_color = color.xyz;
  vec3 rim_base_color = vec3(1.0,2.0,3.0);
//  vec3 rim_base_color = desaturate(initial_color);

  vec3 color_phong = phong() * initial_color;
  vec3 color_rim = rim(rim_base_color, 0.0, 2.0, 1.0);
  
//  vec3 color_final = mix(color_phong, color_rim, 0.9);
  vec3 color_final = color_phong + color_rim;
  
  color_final = vec3(1.0) + normalize(normal_cameraspace);
  color_final *= 0.5;

  out_Color = vec4(color_final,1.0) ;
}

