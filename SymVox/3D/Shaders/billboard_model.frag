#version 330

uniform sampler2D gColorMap;

in vec4 Color;
in vec2 TexCoord;
in vec4 geoColor;

out vec4 FragColor;

void main()
{
    vec4 texColor = texture2D(gColorMap, TexCoord);
    //texColor = vec4(0.5, 0.5, 0.5, 0.5);

    //if( (texColor.r == 0 && texColor.g == 0 && texColor.b == 0) )
    if( (texColor.a == 0) )
    {
        discard;
    }

    //FragColor = texColor;
    FragColor = geoColor;
}
