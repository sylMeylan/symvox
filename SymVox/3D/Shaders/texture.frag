// GLSL version
#version 150 core

// Entry
in vec2 coordTexture;

// Uniform texture
uniform sampler2D tex;

// Output
out vec4 out_Color;

void main()
{
    // Pixel color
    out_Color = texture(tex, coordTexture);
}
