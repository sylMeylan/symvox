/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_GEO_ELLIPTICCYLINDER_HH
#define TRID_GEO_ELLIPTICCYLINDER_HH

#include "PrimitiveCRTP.hh"
#include "Cylindre.hh"

namespace SV { namespace TriD {

class EllipticCylinder : public PrimitiveCRTP<VPrimitive,EllipticCylinder>
{
public:
    EllipticCylinder(float semiMajor, float semiMinor, const Line& line, unsigned int subdiv = 10);
    ~EllipticCylinder() override;

    SV::TriD::VObject* CreateObject(AssetsManager* assetsManager) override;
    bool IsWithin(const QVector3D& pos) override;
    QVector3D RandomPointWithin() override;

    float GetSemiMajor() const {return fSemiMajor;}
    float GetSemiMinor() const {return fSemiMinor;}
    float GetSemiHeight() const {return fSemiHeight;}
    Line& GetLine() {return fLine;}
    const Line& GetLine() const {return fLine;}

    void SetSemiMajor(float val) {fSemiMajor=val;}
    void SetSemiMinor(float val) {fSemiMinor=val;}
    void SetLine(const Line& line);
    void SetSubDiv(int subdiv) {fSubDiv=subdiv;}

private:
    float fSemiMajor;
    float fSemiMinor;
    float fSemiHeight;
    Line fLine;
    int fSubDiv;

    void CreateMesh(SV::TriD::Mesh* mesh);
};

}}

#endif // ELLIPTICCYLINDER_HH
