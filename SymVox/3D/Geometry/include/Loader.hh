/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_GEO_LOADER_HH
#define TRID_GEO_LOADER_HH

#include <QFileInfo>
#include <QResource>
#include <QTemporaryFile>
#include <QDir>

#include "PrimitiveCRTP.hh"

#include "SymVox/Utils/include/Exception.hh"

// Assimp library
#include "SymVox/Externals/Includes/assimp/Importer.hpp" // C++ importer interface
#include "SymVox/Externals/Includes/assimp/scene.h" // Output data structure
#include "SymVox/Externals/Includes/assimp/postprocess.h" // Post processing flags

namespace SV { namespace TriD {

/*!
 * \brief The Loader class
 * This class load a 3d model into the program. To do so, it uses the assimp library
 */
class Loader : public PrimitiveCRTP<VPrimitive, Loader>
{
public:
    Loader(const std::string& fileName,
           unsigned int flags=aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs
            ); ///< Contructor

    ~Loader() override; ///< Destructor

    /*!
     * \brief CreateObject
     * \param assetsManager
     * \return
     * This method takes the assets manager as argument and return the 3D object class to be used.
     * This is where the 3D object is built.
     */
    virtual VObject* CreateObject(AssetsManager* assetsManager) override
    {
        fpObj = new Object(assetsManager);
        //fpObj = new CCube3dObj(assetsManager);
        fpAssetsManager = assetsManager;

        ImportFromFile(fFileName);

        return fpObj;
    }

    template<typename T=Object>
    VObject* ManuallyCreateObject(AssetsManager* assetsManager)
    {
        fpObj = new T(assetsManager);
        //fpObj = new CCube3dObj(assetsManager);
        fpAssetsManager = assetsManager;

        ImportFromFile(fFileName);

        return fpObj;
    }

private:

    std::string fFileName; ///< Name of the file to be processed by Assimp.
    unsigned int fFlags; ///< Flags corresponding to Assimp options used in the import process.
    Object* fpObj; ///< Object3D to be manipulated in the class.
    SV::TriD::AssetsManager* fpAssetsManager; ///< Assets manager to be manipulated in the class.
    std::map<int, int> fMatIdxTextIt; ///< Use to make the link between the assimp material ID and the Flair texture ID.
    std::string fDefaultTexPath;

    bool ImportFromFile(const std::string& fileName); ///< Import the model (3D) contains into the input file.
    //bool InitFromScene(const aiScene* pScene, const std::string& fileName); ///< Set the values of the Flair object 3D corresponding the assimp object being loaded.
    void ProcessMesh(unsigned int index, const aiMesh* paiMesh); ///< Load the mesh(es).
    //bool InitMaterials(const aiScene* pScene, const std::string& fileName); ///< Load the texture(s).

    void ProcessNode(aiNode* node, const aiScene* scene, const std::string& file);
    void ProcessMesh(const aiMesh* paiMesh, const aiScene* scene, const std::string& file); ///< Load the mesh(es).
    int LoadTextureAssimp(aiTextureType texType, const std::string& dir, aiMaterial* material);
    std::string FromTypeToString(aiTextureType texType);
    int LoadTexture(const std::string& texturePath);
};

}}

#endif // TRID_GEO_LOADER_HH
