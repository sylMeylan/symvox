/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_GEO_CYLINDRE_HH
#define TRID_GEO_CYLINDRE_HH

#include <math.h>
#include <QVector3D>
#include <QColor>
#include "PrimitiveCRTP.hh"

namespace SV { namespace TriD {

class Line
{

public:
    Line(QVector3D origin = QVector3D(0,0,0), QVector3D direction = QVector3D(0,0,1)) :
        fOrigin(origin),
        fDirection(direction)
    {}

    inline const QVector3D& GetOrigin() const {return fOrigin;}
    inline const QVector3D& GetDirection() const {return fDirection;}

private:
    QVector3D fOrigin;
    QVector3D fDirection;
};

class Cylindre : public PrimitiveCRTP<VPrimitive, Cylindre>
{
public:
    Cylindre(float radius = 1.0, const Line& line = Line(), unsigned int subdiv = 10) :
        PrimitiveCRTP(),
        fSubDiv(subdiv),
        fRadius(radius),
        fLine(line)
    {}

    ~Cylindre() override {}

    void SetSubDiv(unsigned int subdiv) {fSubDiv = subdiv;}
    unsigned int GetSubDiv() const {return fSubDiv;}

    inline const float GetRadius() const {return fRadius;}
    inline const Line& GetLine() const {return fLine;}

    virtual VObject* CreateObject(AssetsManager* assetsManager) override final;

private:
    unsigned int fSubDiv;
    float fRadius;
    Line fLine;

    void CreateMesh(SV::TriD::Mesh* mesh);
};



}}

#endif // CYLINDRE_HH
