/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_GEO_SPHERE_HH
#define TRID_GEO_SPHERE_HH

#include "SymVox/3D/Object/Standard/include/Object.hh"
#include "SymVox/3D/Geometry/include/PrimitiveCRTP.hh"

namespace SV { namespace TriD {

class Sphere : public PrimitiveCRTP<VPrimitive, Sphere>
{
public:
    Sphere(QVector3D centre = QVector3D(0, 0, 0), float radius = 1.0, int u=20, int v=10);
    ~Sphere() override;

    float GetRadius() const {return fRadius;}
    const QVector3D& GetCentre() const {return fCentre;}

    SV::TriD::VObject* CreateObject(AssetsManager* assetsManager) override;

    void CreateUVMesh(SV::TriD::Mesh *mesh) const;
    void CreateIcoMesh(int recursionLevel, SV::TriD::Mesh* mesh);

    void SetSubDiv(unsigned int u, unsigned int v) {fSubDiv_u=u; fSubDiv_v=v;}
    void SetSubDiv(unsigned int s) {fSubDiv=s;}

    void SetRadius(float radius) {fRadius=radius;}

    bool IsWithin(const QVector3D& pos) override;

    QVector3D RandomPointWithin() override;

    QVector3D RandomPointOnSurface() override;

private:
    float fRadius;
    QVector3D fCentre;

    unsigned int fSubDiv_u;
    unsigned int fSubDiv_v;
    unsigned int fSubDiv;

    std::vector<SV::TriD::FaceID> fFaceId;
    std::map<int64_t, int> fMiddlePointIndexCache;

    int AddVertex(QVector3D p, SV::TriD::Mesh* mesh);
    int GetMiddlePoint(int p1, int p2, SV::TriD::Mesh* mesh);

    void CreateMesh(SV::TriD::Mesh* mesh);
};

}}

#endif // TRID_GEO_SPHERE_HH
