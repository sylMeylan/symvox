/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_GEO_CUBE_HH
#define TRID_GEO_CUBE_HH

#include "SymVox/3D/Object/Standard/include/Object.hh"
#include "SymVox/3D/Geometry/include/PrimitiveCRTP.hh"

namespace SV { namespace TriD {

class Cube : public PrimitiveCRTP<VPrimitive, Cube>
{

public:

    Cube(QVector3D origin = QVector3D(0,0,0),
         QVector3D xAxis  = QVector3D(1,0,0),
         QVector3D yAxis  = QVector3D(0,1,0),
         QVector3D zAxis  = QVector3D(0,0,1)) :
        PrimitiveCRTP(),
        fOrigin(origin),
        fXAxis(xAxis),
        fYAxis(yAxis),
        fZAxis(zAxis)
    {}

    Cube(float size) :
        PrimitiveCRTP(),
        fOrigin(QVector3D(0,0,0) ),
        fXAxis(QVector3D(size/2,0,0) ),
        fYAxis(QVector3D(0,size/2,0) ),
        fZAxis(QVector3D(0,0,size/2) )
    {}


    Cube(QVector3D origin,
         float scale) :
        Cube(origin, QVector3D(scale,0,0), QVector3D(0, scale,0), QVector3D(0,0, scale))
    {}

    ~Cube() override {;}

    inline const QVector3D& GetOrigin() const {return fOrigin;}
    inline const QVector3D& GetXAxis()  const {return fXAxis; }
    inline const QVector3D& GetYAxis()  const {return fYAxis; }
    inline const QVector3D& GetZAxis()  const {return fZAxis; }


    virtual VObject* CreateObject(AssetsManager* assetsManager) override final;

    bool IsWithin(const QVector3D& pos) override final;

private:

    QVector3D fOrigin;
    QVector3D fXAxis;
    QVector3D fYAxis;
    QVector3D fZAxis;
};

}}

#endif // TRID_GEO_CUBE_HH
