/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_GEO_OBJECTPRIMITIVE_HH
#define TRID_GEO_OBJECTPRIMITIVE_HH

#include "SymVox/3D/Object/Standard/include/VObject.hh"
#include "SymVox/3D/Geometry/include/PrimitiveCRTP.hh"
#include <tuple>

// https://stackoverflow.com/questions/29220248/is-it-possible-to-store-variadic-arguments-into-a-member-variable?rq=1
//namespace SV { namespace TriD {

//template<class T>
//class ObjectPrimitive : public PrimitiveCRTP<VPrimitive, ObjectPrimitive>
//{
//public:
//    template<typename ...Args>
//    ObjectPrimitive(T* obj, Args&& ...args)
//    {
//        auto tup = std::make_tuple<Args&& ...>(args);
//    }
//    ~ObjectPrimitive() {}

//    virtual T* CreateObject(AssetsManager* assetsManager) override final {

//    }

//private:
//    T* fpObj; ///< Object3D to be manipulated in the class.
//    bool fWasCreated;
//    Args&& ...args;
//};

//}}

//namespace SV { namespace TriD {

//class ObjectPrimitive : public PrimitiveCRTP<VPrimitive, ObjectPrimitive>
//{
//public:
//    ObjectPrimitive(VObject* obj);
//    ~ObjectPrimitive();

//    virtual VObject* CreateObject(AssetsManager* assetsManager) override final;

//private:
//    VObject* fpObj; ///< Object3D to be manipulated in the class.
//    bool fWasCreated;
//};

//}}

#endif // TRID_GEO_OBJECTPRIMITIVE_HH
