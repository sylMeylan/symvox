/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Loader.hh"

#include "SymVox/App/Tests/CustomRenderer/include/CCube3dObj.hh"

namespace SV { namespace TriD {

Loader::Loader(const std::string& fileName,
               unsigned int flags)
    : PrimitiveCRTP()
{
    fFileName = fileName;
    fFlags = flags;
    fpObj = nullptr;
    fpAssetsManager = nullptr;

    fDefaultTexPath = "Data/Assets/Default/black.png";
    //fDefaultTexPath = "Data/Assets/Default/white.png";
}

Loader::~Loader()
{

}

bool Loader::ImportFromFile(const std::string& fileName)
{
    bool isDone (false);

    Assimp::Importer importer;

    // - aiProcess_GenNormals : actually creates normals for each vertex if the model didn't contain normal vectors.
    // - aiProcess_SplitLargeMeshes : splits large meshes into smaller sub-meshes which is useful if your rendering
    // has a maximum number of vertices allowed and can only process smaller meshes.
    // - aiProcess_OptimizeMeshes : actually does the reverse by trying to join several meshes into one larger mesh,
    // reducing drawing calls for optimization.

    const aiScene* pScene = importer.ReadFile(fileName.c_str(), fFlags);

    // A tentative to use assimp with the qt resource system.
    // This should work if the texture paths are also modified to be relative
    // and to have the ":/" prefix.
    //
    //    const aiScene* pScene = nullptr;

    //    QFile qrcObj(fileName.c_str() );
    //    QTemporaryFile file;

    //    if(qrcObj.open(QFile::ReadOnly) && file.open())
    //    {
    //      file.write(qrcObj.readAll());
    //      QString path = QDir::toNativeSeparators(file.fileName()).toLatin1();
    //      pScene = importer.ReadFile(path.toStdString().data(), fFlags);
    //      // the rest of your code
    //    }

    // How Qt uses assimp
    //
    //importer.SetIOHandler(new AssimpHelper::AssimpIOSystem());
    //QResource resource (fileName.c_str() );
    //const aiScene* pScene = importer.ReadFileFromMemory(resource.data(), resource.size(), fFlags );

    if(pScene != nullptr)
    {
        // If the scene is not empty then initialize it:
        // Load all the information you can from it
        //isDone = InitFromScene(pScene, fileName);

        // Remove all the mesh entries of the object3D (there should be only one).
        fpObj->RemoveAllMeshEntries();

        // Load process 
        ProcessNode(pScene->mRootNode, pScene, fileName);
        isDone = true;
    }
    else
    {
        std::string msg = "The file ";
        msg += fileName;
        msg += " could not be parsed.\n The error was: ";
        msg += importer.GetErrorString();
        Utils::Exception(Utils::ExceptionType::Warning, "Loader::ImportFromFile", msg);
    }

    return isDone;
}

void Loader::ProcessNode(aiNode* node, const aiScene* scene, const std::string& file)
{
    // Process all the node's meshes (if any)
    for(GLuint i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];

        // Add a mesh entry in object3D
        fpObj->AddMeshEntry();

        // Initialise the mesh
        ProcessMesh(mesh, scene, file);
    }

    // Then do the same for each of its children
    for(GLuint i = 0; i < node->mNumChildren; i++)
    {
        ProcessNode(node->mChildren[i], scene, file);
    }
}

void Loader::ProcessMesh(const aiMesh* paiMesh, const aiScene* scene, const std::string& file)
{
    int index = fpObj->GetNumOfMeshEntries() - 1;

    // Get the mesh entry
    SV::TriD::MeshEntry& meshEntry = fpObj->GetMeshEntry(index);

    // Get the mesh
    SV::TriD::Mesh& mesh = meshEntry.fMesh;

    // zero vector
    const aiVector3D zero3D(0.0f, 0.0f, 0.0f);

    // *************************
    // Vertices
    // *************************

    // Loop on all the vertices
    for(int i=0, ie=paiMesh->mNumVertices; i<ie; i++)
    {
        // Retrieve the pos, normal and text coord values
        const aiVector3D* pPos = &(paiMesh->mVertices[i]);
        const aiVector3D* pNormal = paiMesh->HasNormals() ? &(paiMesh->mNormals[i]) : &zero3D;
        const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &zero3D;

        // Add the vertex to the "real" mesh of the program
        mesh.AddVertex(QVector3D(pPos->x, pPos->y, pPos->z),
                       QVector3D(pNormal->x, pNormal->y, pNormal->z),
                       QColor(0.5,0.5,0.5),
                       QVector2D(pTexCoord->x, pTexCoord->y)
                       );
    }

    // *************************
    // Faces
    // *************************

    // Loop on all the faces
    for(int i=0, ie=paiMesh->mNumFaces; i<ie; i++)
    {
        // Get the current face
        const aiFace& Face = paiMesh->mFaces[i];

        // Add the face
        mesh.AddFace(Face.mIndices[0], Face.mIndices[1], Face.mIndices[2]);
    }

    // *************************
    // Material
    // *************************

    // Process material
    if(paiMesh->mMaterialIndex >= 0)
    {
        aiMaterial* material = scene->mMaterials[paiMesh->mMaterialIndex];

        QFileInfo fInfo;
        fInfo.setFile(QString(file.c_str() ) );
        std::string dir = fInfo.absolutePath().toStdString();

        // **************************
        // Diffuse Texture
        // **************************

        unsigned int diffTexCount = material->GetTextureCount(aiTextureType_DIFFUSE);

        if(diffTexCount > 0)
        {
            if(diffTexCount > 1)
            {
                std::stringstream ss;
                ss << "There are "<<diffTexCount<<" diffuse textures but the current implementation allows only 1";
                Utils::Exception(Utils::ExceptionType::Warning, "Loader::ProcessMesh", ss.str() );
            }

            meshEntry.fTextureDiffuseId = LoadTextureAssimp(aiTextureType_DIFFUSE, dir, material);
        }
        else
            meshEntry.fTextureDiffuseId = LoadTexture(fDefaultTexPath);

        // **************************
        // Specular Texture
        // **************************

        unsigned int specTexCount = material->GetTextureCount(aiTextureType_SPECULAR);

        if(specTexCount > 0)
        {
            if(specTexCount > 1)
            {
                std::stringstream ss;
                ss << "There are "<<specTexCount<<" specular textures but the current implementation allows only 1";
                Utils::Exception(Utils::ExceptionType::Warning, "Loader::ProcessMesh", ss.str() );
            }

            meshEntry.fTextureSpecularId = LoadTextureAssimp(aiTextureType_SPECULAR, dir, material);
        }
        else
            meshEntry.fTextureSpecularId = LoadTexture(fDefaultTexPath);

        // **************************
        // EmissionTexture
        // **************************

        unsigned int emissionTexCount = material->GetTextureCount(aiTextureType_EMISSIVE);

        if(emissionTexCount > 0)
        {
            if(emissionTexCount > 1)
            {
                std::stringstream ss;
                ss << "There are "<<emissionTexCount<<" emission textures but the current implementation allows only 1";
                Utils::Exception(Utils::ExceptionType::Warning, "Loader::ProcessMesh", ss.str() );
            }

            meshEntry.fTextureEmissionId = LoadTextureAssimp(aiTextureType_EMISSIVE, dir, material);
        }
        else
            meshEntry.fTextureEmissionId = LoadTexture(fDefaultTexPath);
    }
}

std::string Loader::FromTypeToString(aiTextureType texType)
{
    std::string str ("");

    if(texType ==  aiTextureType_DIFFUSE)
        str = "aiTextureType_DIFFUSE";
    else if(texType ==  aiTextureType_AMBIENT)
        str = "aiTextureType_AMBIENT";
    else if(texType ==  aiTextureType_SPECULAR)
        str = "aiTextureType_SPECULAR";
    else if(texType ==  aiTextureType_EMISSIVE)
        str = "aiTextureType_EMISSIVE";
    else if(texType ==  aiTextureType_SHININESS)
        str = "aiTextureType_SHININESS";
    else if(texType ==  aiTextureType_NORMALS)
        str = " aiTextureType_NORMALS";
    else if(texType ==  aiTextureType_HEIGHT)
        str = "aiTextureType_HEIGHT";
    else if(texType ==  aiTextureType_OPACITY)
        str = "aiTextureType_OPACITY";
    else if(texType ==  aiTextureType_DISPLACEMENT)
        str = "aiTextureType_DISPLACEMENT";
    else if(texType ==  aiTextureType_LIGHTMAP)
        str = "aiTextureType_LIGHTMAP";
    else if(texType ==  aiTextureType_REFLECTION)
        str = "aiTextureType_REFLECTION";
    else if(texType ==  aiTextureType_UNKNOWN)
        str = "aiTextureType_UNKNOWN";
    else if(texType ==  aiTextureType_NONE)
        str = "aiTextureType_NONE";

    else
    {
        std::stringstream ss;
        ss << "The aiTextureType is not recognized : "<<texType;
        Utils::Exception(Utils::ExceptionType::Warning, "Loader::FromTypeToString", ss.str() );
    }

    return str;
}

int Loader::LoadTextureAssimp(aiTextureType texType, const std::string& dir, aiMaterial* material)
{
    // Variable definitions
    aiReturn out;
    aiString path;
    aiTextureMapping texMapping;
    unsigned int uvIndex;
    float blend;
    aiTextureOp texOp;
    aiTextureMapMode texMapMode;

    // Set the variables for the i_th texture
    out = material->GetTexture(texType, 0, &path, &texMapping, &uvIndex, &blend, &texOp, &texMapMode);

//    aiTextureFlags texFlag;
//    material->Get(_AI_MATKEY_TEXFLAGS_BASE,aiTextureType_DIFFUSE, 0, texFlag);
//    bool t = true;
//    if(texFlag != aiTextureFlags_IgnoreAlpha)
//        t=false;
//    std::cout<<t<<std::endl;

    std::string fullPath = dir + "/" + path.data;
    QFileInfo texFile;
    texFile.setFile(QString(fullPath.c_str() ) );
    fullPath = texFile.absoluteFilePath().toStdString();

    if(out == aiReturn_FAILURE)
    {
        std::stringstream ss;
        ss << "The texture "<<fullPath<<" could not be loaded.";
        Utils::Exception(Utils::ExceptionType::Warning, "Loader::ProcessMesh", ss.str() );
    }
    else if(out == aiReturn_OUTOFMEMORY)
    {
        std::stringstream ss;
        ss << "The texture "<<fullPath<<" could not be loaded because there is no free memory available";
        Utils::Exception(Utils::ExceptionType::Warning, "Loader::ProcessMesh", ss.str() );
    }

    int textureId = LoadTexture(fullPath);

    return textureId;
}

int Loader::LoadTexture(const std::string& texturePath)
{
    int textureId = fpAssetsManager->AutoAddTexture(texturePath);

    if(textureId == -1)
    {
        std::string path = fDefaultTexPath;

        bool isDone = false;

        if(!fpAssetsManager->IsTextureInside(path) )
        {
            textureId = fpAssetsManager->AddTexture(new SV::TriD::Texture(path), path);

            isDone = fpAssetsManager->GetTexture(textureId)->Load();
        }

        if(!isDone)
        {
            std::string msg = "Error loading the default texture: ";
            msg += path;
            Utils::Exception(Utils::ExceptionType::Fatal, "Loader::InitMaterials", msg);
        }
    }

    return textureId;
}

}}
