/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/PolyCylinder.hh"

namespace SV { namespace TriD {



PolyCylinder::PolyCylinder(const Line &line) : PrimitiveCRTP<VPrimitive, PolyCylinder>()
{
    fLine = line;

    fSemiHeight = fLine.GetDirection().length()/2;

    fDistancePointCentreMax = 0.0f;
}

PolyCylinder::~PolyCylinder()
{

}

VObject* PolyCylinder::CreateObject(AssetsManager* assetsManager)
{
    // Create the new obj
    Object* obj = new Object(assetsManager);

    // Add a mesh entry
    obj->AddMeshEntry();

    // Get its mesh object to modify it
    Mesh* mesh = &(obj->GetMesh() );

    CreateMesh(mesh);

    return obj;
}

void PolyCylinder::CreateMesh(SV::TriD::Mesh* mesh)
{
    QVector3D p0 = fLine.GetOrigin();

    QVector3D pz = fLine.GetDirection();
    QVector3D px(1.f,0.f,0.01f);
    QVector3D py = QVector3D::normal(pz, px);
    px = QVector3D::normal(py, pz);

    // *******************
    // Generate vertices
    // *******************

    // Central vertices

    // Down vertex
    VertID downVID = mesh->AddVertex(p0, -pz.normalized() );

    // Up vertex
    VertID upVID = mesh->AddVertex(p0 + pz, pz.normalized() );

    // Contour vertices

    // Vertices for each summit
    std::vector<VertID> v_id(2*fPoints.size() );

    // Duplicates to ensure each face has a non-interpolated normal
    std::vector<VertID> v_idUpAndDown(2*fPoints.size() );

    // Loop on all the contour points
    for(size_t i=0, ie=fPoints.size(); i<ie; ++i)
    {
        QVector3D& pointPos = fPoints.at(i);

        // Classical ones

        // Add the vertex for the lower face
        v_id[2*i] = mesh->AddVertex(p0+pointPos, pointPos.normalized() );

        // Add the vertex for the upper face
        v_id[2*i+1] = mesh->AddVertex(p0+pointPos + pz, pointPos.normalized() );

        // Up and down

        // Add the vertex for the lower face
        v_idUpAndDown[2*i] = mesh->AddVertex(p0+pointPos, -pz.normalized() );

        // Add the vertex for the upper face
        v_idUpAndDown[2*i+1] = mesh->AddVertex(p0+pointPos + pz, pz.normalized() );
    }

    // *******************
    // Generate faces
    // *******************

    size_t pointSize = fPoints.size();

    // Loop on  all the faces /4
    for(size_t i=0, ie=pointSize; i<ie; ++i)
    {
        // Get the vertices used for the 4 faces to be generated here

        // Low contour 1
        VertID i00 = v_id[2*i];
        // Low contour 2
        VertID i01 = v_id[2*i+1];
        //  Up contour 1
        VertID i10 = v_id[2*((i+1)%pointSize)];
        //  Up contour 2
        VertID i11 = v_id[2*((i+1)%pointSize) + 1];

        // Generate the faces on the side (contour)
        mesh->AddFace(i00, i10, i11);
        mesh->AddFace(i00, i11, i01);

        // Get the vertices for the up and down faces
        i00 = v_idUpAndDown[2*i];
        i01 = v_idUpAndDown[2*i+1];
        i10 = v_idUpAndDown[2*((i+1)%pointSize)];
        i11 = v_idUpAndDown[2*((i+1)%pointSize) + 1];

        // Down triangle face
        mesh->AddFace(i10, i00, downVID);

        // Up triangle face
        mesh->AddFace(upVID, i01, i11);
    }
}

void PolyCylinder::AddPoint(const QVector3D& point)
{
    fPoints.push_back(point);
}

void PolyCylinder::ResetPoints()
{
    fPoints.clear();
}

void PolyCylinder::GeneratePolyCylinder(int numOfPoints, std::vector<float> distancePointCentre)
{
    ResetPoints();

    fDistancePointCentreMax = 0.0f;

    QVector3D px(1,0,0);
    QVector3D py(0,1,0);

    // Loop on all the points
    for(int i=0, ie=numOfPoints; i<ie; ++i)
    {
        float angle = i * float(2*M_PI) / numOfPoints;

        QVector3D pos = px * std::cos(angle) + py * std::sin(angle);

        pos *= distancePointCentre.at(i);

        if(distancePointCentre.at(i)>fDistancePointCentreMax)
            fDistancePointCentreMax = distancePointCentre.at(i);

        AddPoint(pos);
    }
}

void PolyCylinder::GeneratePolyCylinder(int numOfPoints, float distancePointCentre)
{
    ResetPoints();

    fDistancePointCentreMax = distancePointCentre;

    QVector3D px(1,0,0);
    QVector3D py(0,1,0);

    // Loop on all the points
    for(int i=0, ie=numOfPoints; i<ie; ++i)
    {
        float angle = i * float(2*M_PI) / numOfPoints;

        QVector3D pos = px * std::cos(angle) + py * std::sin(angle);

        pos *= distancePointCentre;

        AddPoint(pos);
    }
}

void PolyCylinder::GeneratePolyCylinderRandom(int numOfPoints, float distancePointCentre, float radiusRandFactor, float tanRandFactor)
{
    // Check that the random generator has been initialized
    if(fpGenerator == nullptr)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "PolyCylinder::GeneratePolyCylinderRandom",
                         "PolyCylinder::GeneratePolyCylinderRandom: fpGenerator was not set."
                         "You should call Primitive::SetRandomGenerator(...) somewhere"
                         );
    }

    this->ResetPoints();

    fDistancePointCentreMax = distancePointCentre;

    std::uniform_real_distribution<> uniDist(-1.0, 1.0);

    QVector3D px(1,0,0);
    QVector3D py(0,1,0);
    QVector3D pz(0,0,1);

    // Loop on all the points
    for(int i=0, ie=numOfPoints; i<ie; ++i)
    {
        float angle = i * float(2*M_PI) / numOfPoints;

        QVector3D pos = px * std::cos(angle) + py * std::sin(angle);

        pos *= distancePointCentre;

        // Radius randomisation
        pos += radiusRandFactor * distancePointCentre * float(uniDist(*fpGenerator)) * pos.normalized();

        // Tan randomisation
        QVector3D tan = QVector3D::normal(pos.normalized(), pz);
        pos += tanRandFactor * distancePointCentre * float(uniDist(*fpGenerator)) * tan.normalized();

        if(pos.length( )> fDistancePointCentreMax) fDistancePointCentreMax = pos.length();

        AddPoint(pos);
    }
}

bool PolyCylinder::IsWithin(const QVector3D& pos)
{
    bool isWithin = false;

    // Check that the random generator has been initialized
    if(fpGenerator == nullptr)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "PolyCylinder::RandomPointWithin",
                         "PolyCylinder::IsWithin: fpGenerator was not set."
                         "You should call Primitive::SetRandomGenerator(...) somewhere"
                         );
    }

    // First, check if the point is within the z max and min

    float min = fLine.GetOrigin().z();
    float max = fLine.GetDirection().z();

    float z = pos.z();

    if(z < max && z > min)
    {
        // If yes, then check the polygon projected in a 2D plane with a ray tracing algorithm in 2D

        QVector2D px(1,0);
        QVector2D py(0,1);
        QVector2D pos2D(pos.x(), pos.y() );

        std::uniform_real_distribution<> uniDis(0,1);

        // Random 2D direction
        float angle = float(uniDis(*fpGenerator) * (2*M_PI) );
        QVector2D dir = px * std::cos(angle) + py * std::sin(angle);

        // Look for the number of intersection of the random dir

        int countIntersection (0);

        // Loop on all the polygon segments
        for(size_t i=0, ie=fPoints.size(); i<ie; ++i)
        {
            QVector3D temp = fPoints.at(i);
            QVector2D C = QVector2D( temp.x(),  temp.y() );

            temp = fPoints.at( (i+1) % fPoints.size() );
            QVector2D D =  QVector2D(temp.x(), temp.y() );

            QVector2D J = D - C;

            bool isIntersection = Is2dIntersection(pos2D, dir, C, J);

            if(isIntersection) ++countIntersection;
        }

        // If the number of intersections is even then pos is not in the polygon
        if(countIntersection%2 == 0)
            isWithin = false;

        // If the number of intersections is odd then pos is in the polygon
        else
            isWithin = true;
    }

    return isWithin;
}

bool PolyCylinder::Is2dIntersection(const QVector2D& a, const QVector2D& I, const QVector2D& c, const QVector2D& J)
{
    // https://openclassrooms.com/forum/sujet/calcul-du-point-d-intersection-de-deux-segments-21661

    //    Soit le segment [AB], et le segment [CD].
    //    Je note I le vecteur AB, et J le vecteur CD

    //    Soit k le parametre du point d'intersection du segment CD sur la droite AB. on sera sur le segment si 0<k<1
    //    Soit m le parametre du point d'intersection du segment AB sur la droite CD, on sera sur le segment si 0<m<1

    //    Soit P le point d'intersction
    //    P = A + k*I; // equation (1)
    //    P = C + m*J;

    //    D'ou :
    //    A + k*I = C + m*J

    //    On décompose les points et vecteurs, on a :
    //    Ax + k*Ix = Cx + m*Jx
    //    Ay + k*Iy = Cy + m*Jy

    //    2 équations, 2 inconnues, en résolvant, on trouve :

    //    m = -(-Ix*Ay+Ix*Cy+Iy*Ax-Iy*Cx)/(Ix*Jy-Iy*Jx)
    //    k = -(Ax*Jy-Cx*Jy-Jx*Ay+Jx*Cy)/(Ix*Jy-Iy*Jx)

    //    Attention, si Ix*Jy-Iy*Jx = 0 , alors les droites sont paralleles, pas d'intersection.

    bool isIntersection (false);

    float denomi = I.x()*J.y() - I.y()*J.x();
    bool isParallel = ( std::abs( denomi  ) < 0.0001f );

    if(!isParallel)
    {
        float m = -(-I.x()*a.y() + I.x()*c.y() + I.y()*a.x() - I.y()*c.x() ) / denomi;
        float k = -( a.x()*J.y() - c.x()*J.y() - J.x()*a.y() + J.x()*c.y() ) / denomi;

        // Intersection point
        //QVector2D pIntersect = a + k*I;

        // We want to be in the "a" direction, then we want m>0
        // We want to be on the segment CD, then we want 0<k<1

        if(m > 0 && k > 0 && k <= 1)
            isIntersection = true;
    }

    return isIntersection;
}

QVector3D PolyCylinder::RandomPointWithin()
{
    // Check that the random generator has been initialized
    if(fpGenerator == nullptr)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "PolyCylinder::RandomPointWithin",
                         "fpGenerator was not set."
                         "You should call Primitive::SetRandomGenerator(...) somewhere."
                         );
    }

    std::uniform_real_distribution<> uniDis(double(-fDistancePointCentreMax), double(fDistancePointCentreMax) );

    QVector3D randomPos;

    bool isWithin = false;

    while(!isWithin)
    {
        float x = float(uniDis(*fpGenerator));
        float y = float(uniDis(*fpGenerator));
        float z = float(uniDis(*fpGenerator));

        randomPos = QVector3D(x,y,z);

        isWithin = this->IsWithin(randomPos);
    }

    return randomPos;
}

}}
