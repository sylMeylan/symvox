/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Ellipsoid.hh"

#include "SymVox/3D/Geometry/include/Sphere.hh"

namespace SV { namespace TriD {



Ellipsoid::Ellipsoid(float a, float b, float c, int subDivAlpha, int subDivPhi)
    : PrimitiveCRTP<VPrimitive, Ellipsoid>(),
      fA(a),
      fB(b),
      fC(c),
      fSubDivAlpha(subDivAlpha),
      fSubDivPhi(subDivPhi)
{

}

Ellipsoid::~Ellipsoid()
{

}

VObject* Ellipsoid::CreateObject(AssetsManager *assetsManager)
{
    // Create the new obj
    Object* obj = new Object(assetsManager);

    // Add a mesh entry
    obj->AddMeshEntry();

    // Get its mesh object to modify it
    Mesh* mesh = &(obj->GetMesh() );

    CreateMesh(mesh);

    return obj;
}

void Ellipsoid::CreateMesh(SV::TriD::Mesh* mesh)
{
    Sphere s(QVector3D(0,0,0), 1.0, fSubDivAlpha, fSubDivPhi);
    s.CreateUVMesh(mesh);

    QMatrix4x4 mat;
    mat.setColumn(0, QVector4D(fA, 0, 0, 0));
    mat.setColumn(1, QVector4D(0, fB, 0, 0));
    mat.setColumn(2, QVector4D(0, 0, fC, 0));
    mat.setColumn(3, QVector4D(0, 0, 0, 1));

    mesh->ApplyTransform(mat);

    return;

    // ***********************
    // Vertex generation
    // ***********************

    VertID up = mesh->AddVertex(QVector3D(0,0,fC), QVector3D(0,0,fC) );
    VertID down = mesh->AddVertex(QVector3D(0,0,-fC), QVector3D(0,0,-fC) );

    std::vector<VertID> vertIDs (fSubDivAlpha*fSubDivPhi);

    float deltaAlpha = 2 * M_PI / fSubDivAlpha;
    float deltaPhi = M_PI / fSubDivPhi;

    int count (0);

    // Loop on all the rings
    for(int p=1; p<fSubDivPhi; ++p)
    {
        float phi = deltaPhi * p;

        // Loop on all the alpha values
        for(int a=0; a<fSubDivAlpha; ++a)
        {
            float alpha = deltaAlpha * a;

            QVector3D vertPos(
                        fA*std::cos(alpha)*std::sin(phi),
                        fB*std::sin(alpha)*std::sin(phi),
                        fC*std::cos(phi)
                        );

            vertIDs[count] = mesh->AddVertex(vertPos, vertPos);

            ++count;
        }
    }

    // ***********************
    // Face generation
    // ***********************

    // Upper ring

    // Loop on all the vertex of the first ring
    for(int a=0; a<fSubDivAlpha; ++a)
    {
        VertID currentVertexID = 2 + a;
        VertID nextVertexID = 2 + (a+1) % (fSubDivAlpha);

        mesh->AddFace(currentVertexID, nextVertexID, up);
    }

    // Loop on all the rings minus the first
    for(int r=0; r<fSubDivPhi-2; ++r)
    {
        VertID firstVertIDOfCurrentRing = fSubDivAlpha*(r+1)+2;
        VertID firstVertIDOfPreviousRing = fSubDivAlpha*r+2;

        // Loop on all the vertex of the current ring
        for(int a=0; a<fSubDivAlpha; ++a)
        {
            VertID currentVertIDOfCurrentRing = firstVertIDOfCurrentRing + a;
            VertID currentVertIDOfPreviousRing = firstVertIDOfPreviousRing  +a;

            VertID nextVertIDOfCurrentRing = firstVertIDOfCurrentRing + (a+1)%fSubDivAlpha;
            VertID nextVertIDOfPreviousRing = firstVertIDOfPreviousRing  + (a+1)%fSubDivAlpha;

            // Square face

            // First triangle face
            mesh->AddFace(nextVertIDOfCurrentRing, currentVertIDOfPreviousRing, currentVertIDOfCurrentRing);

            // Second triangle face
            mesh->AddFace(nextVertIDOfPreviousRing, currentVertIDOfPreviousRing, nextVertIDOfCurrentRing);
        }
    }

    // Lower ring

    // Loop on all the vertex of the last ring
    for(int a=0; a<fSubDivAlpha; ++a)
    {
        VertID currentVertexID = fSubDivAlpha*(fSubDivPhi-2)+2 + a;
        VertID nextVertexID =  fSubDivAlpha*(fSubDivPhi-2)+2 + (a+1)%(fSubDivAlpha);

        mesh->AddFace(down, nextVertexID, currentVertexID);
    }
}

bool Ellipsoid::IsWithin(const QVector3D& pos)
{
    bool isWithin (false);

    float v = std::pow(pos.x(), 2) / std::pow(fA, 2) + std::pow(pos.y(), 2) / std::pow(fB, 2) + std::pow(pos.z(), 2) / std::pow(fC, 2);

    if(v < 1)
        isWithin = true;

    return isWithin;
}

QVector3D Ellipsoid::RandomPointWithin()
{
    // Check that the random generator has been initialized
    if(fpGenerator == nullptr)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "Ellipsoid::RandomPointWithin",
                         "Ellipsoid::RandomPointWithin: fpGenerator was not set.\n "
                         "You should call Primitive::SetRandomGenerator(...) somewhere"
                         );
    }

    float t (0);
    if(fA > t) t = fA;
    if(fB > t) t = fB;
    if(fC > t) t = fC;

    std::uniform_real_distribution<> uniDis(-t, t);

    QVector3D randomPos;

    bool isWithin = false;

    while(!isWithin)
    {
        float x = uniDis(*fpGenerator);
        float y = uniDis(*fpGenerator);
        float z = uniDis(*fpGenerator);

        randomPos = QVector3D(x,y,z);

        isWithin = this->IsWithin(randomPos);
    }

    return randomPos;
}

}}
