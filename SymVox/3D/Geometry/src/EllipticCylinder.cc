/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/EllipticCylinder.hh"
#include "SymVox/3D/Object/Standard/include/Mesh.hh"

namespace SV { namespace TriD {



EllipticCylinder::EllipticCylinder(float semiMajor, float semiMinor, const Line &line, unsigned int subdiv)
    : PrimitiveCRTP()
{
    fSemiMajor = semiMajor;
    fSemiMinor = semiMinor;
    fLine = line;
    fSemiHeight = fLine.GetDirection().length()/2;

    fSubDiv = subdiv;
}

EllipticCylinder::~EllipticCylinder()
{

}

VObject *EllipticCylinder::CreateObject(AssetsManager *assetsManager)
{
    // Create the new obj
    Object* obj = new Object(assetsManager);

    // Add a mesh entry
    obj->AddMeshEntry();

    // Get its mesh object to modify it
    Mesh* mesh = &(obj->GetMesh() );

    CreateMesh(mesh);

    return obj;
}

void EllipticCylinder::CreateMesh(SV::TriD::Mesh* mesh)
{
    // http://mathworld.wolfram.com/EllipticCylinder.html

    QVector3D p0 = fLine.GetOrigin();

    QVector3D pz = fLine.GetDirection();
    QVector3D px(1,0,0.01);
    QVector3D py = QVector3D::normal(pz, px);
    px = QVector3D::normal(py, pz);

    // Central vertices

    // Down vertex
    VertID downVID = mesh->AddVertex(p0, -pz.normalized() );

    // Up vertex
    VertID upVID = mesh->AddVertex(p0 + pz, pz.normalized() );

    std::vector<VertID> v_id(2 * fSubDiv);
    std::vector<VertID> v_idUpAndDown(2 * fSubDiv);

    for(int i = 0; i < fSubDiv; i++)
    {
        float t = float(i) / fSubDiv;

        float alpha = 2 * M_PI * t;

        QVector3D diff = fSemiMajor * px * std::cos(alpha) + fSemiMinor * py * std::sin(alpha);

        v_id[2*i] = mesh->AddVertex(p0 + diff, diff.normalized() );
        v_id[2*i+1] = mesh->AddVertex(p0 + diff + pz, diff.normalized() );

        v_idUpAndDown[2*i] = mesh->AddVertex(p0 + diff, -pz.normalized() );
        v_idUpAndDown[2*i+1] = mesh->AddVertex(p0 + diff + pz, pz.normalized() );
    }

    for(int i = 0; i < fSubDiv; i++)
    {
        VertID i00 = v_id[2*i];
        VertID i01 = v_id[2*i+1];
        VertID i10 = v_id[2*((i+1)%fSubDiv)];
        VertID i11 = v_id[2*((i+1)%fSubDiv) + 1];

        // Square face on the side
        mesh->AddFace(i00, i10, i11);
        mesh->AddFace(i00, i11, i01);

        i00 = v_idUpAndDown[2*i];
        i01 = v_idUpAndDown[2*i+1];
        i10 = v_idUpAndDown[2*((i+1)%fSubDiv)];
        i11 = v_idUpAndDown[2*((i+1)%fSubDiv) + 1];

        // Down triangle face
        mesh->AddFace(i10, i00, downVID);

        // Up triangle face
        mesh->AddFace(upVID, i01, i11);
    }
}

bool EllipticCylinder::IsWithin(const QVector3D& pos)
{
    bool output (false);

    float x = pos.x();
    float y = pos.y();
    float z = pos.z();

    // Ellipse
    if( std::pow( (x)/(fSemiMajor), 2) + std::pow( (y)/(fSemiMinor), 2) < 1.
            && std::abs(z) < fSemiHeight)
    {
        output = true;
    }

    return output;
}

QVector3D EllipticCylinder::RandomPointWithin()
{
    // Check that the random generator has been initialized
    if(fpGenerator == nullptr)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "EllipticCylinder::RandomPointWithin",
                         "EllipticCylinder::RandomPointWithin: fpGenerator was not set.\n"
                         "You should call Primitive::SetRandomGenerator(...) somewhere"
                         );
    }

    QVector3D posCandidate(10*fSemiMajor,10*fSemiMajor,10*fSemiMajor);

    while(!IsWithin(posCandidate) )
    {
        std::uniform_real_distribution<> xDis(-fSemiMajor,fSemiMajor);
        float x = xDis(*fpGenerator);
        std::uniform_real_distribution<> yDis(-fSemiMajor,fSemiMajor);
        float y = yDis(*fpGenerator);
        std::uniform_real_distribution<> zDis(-fSemiHeight,fSemiHeight);
        float z = zDis(*fpGenerator);

        posCandidate = QVector3D(x,y,z);
    }

    return posCandidate;
}

void EllipticCylinder::SetLine(const Line &line)
{
    fLine=line;
    fSemiHeight = fLine.GetDirection().length()/2;
}

}}
