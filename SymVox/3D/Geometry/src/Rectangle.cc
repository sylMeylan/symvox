/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Rectangle.hh"

namespace SV { namespace TriD {



Rectangle::Rectangle(float halfX, float halfY) : PrimitiveCRTP()
{
    fHalfX = halfX;
    fHalfY = halfY;
}

Rectangle::~Rectangle()
{
}

void Rectangle::CreateMesh(Mesh* mesh)
{
    VertID id1 = mesh->AddVertex(QVector3D(fHalfX, -fHalfY, 0), QVector3D(0,0,1) );
    VertID id2 = mesh->AddVertex(QVector3D(fHalfX, fHalfY, 0), QVector3D(0,0,1) );
    VertID id3 = mesh->AddVertex(QVector3D(-fHalfX, fHalfY, 0), QVector3D(0,0,1) );
    VertID id4 = mesh->AddVertex(QVector3D(-fHalfX, -fHalfY, 0), QVector3D(0,0,1) );

    mesh->AddFace(id4, id1, id2);
    mesh->AddFace(id2, id3, id4);
}

bool Rectangle::IsWithin(const QVector3D& pos)
{
    // If the pos is not in the same plane as the square it cannot be within it.
    // 0.0001 is here as tolerance limit.
    if(std::abs(pos.z() ) < 0.0001f)
        return false;

    // Check the square
    if(std::abs(pos.x() ) < fHalfX && std::abs(pos.y() ) < fHalfY )
        return true;
    else
        return false;
}

}}
