/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Sphere.hh"

namespace SV { namespace TriD {

Sphere::Sphere(QVector3D centre, float radius, int u, int v) : PrimitiveCRTP(),
    fRadius(radius),
    fCentre(centre),
    fSubDiv_u(u),
    fSubDiv_v(v),
    fSubDiv(10)
{

}

Sphere::~Sphere()
{

}

SV::TriD::VObject* Sphere::CreateObject(AssetsManager* assetsManager)
{
    // Create the new obj
    SV::TriD::Object* obj = new SV::TriD::Object(assetsManager);

    // Add a mesh entry
    obj->AddMeshEntry();

    // Get its mesh object to modify it
    SV::TriD::Mesh* mesh = &(obj->GetMesh() );

    CreateMesh(mesh);
    mesh->ComputeNormals();
    mesh->FillColor(QColor(255,0,0,255));

    return obj;
}

void Sphere::CreateMesh(SV::TriD::Mesh* mesh)
{
    mesh->RemoveData();

    CreateUVMesh(mesh);

    //CreateIcoMesh(int(fSubDiv_u/3), mesh);
    //mesh->ComputeHardNormals();
}

void Sphere::CreateUVMesh(SV::TriD::Mesh* mesh) const
{
//    mesh->Clear();
    QVector3D dx(1, 0, 0);
    QVector3D dy(0, 1, 0);
    QVector3D dz(0, 0, 1);

    mesh->AddVertex(fCentre + dz * fRadius, dz);
    for(uint j = 1; j < fSubDiv_v - 1; j++)
    {
        double phi = M_PI * j / (fSubDiv_v-1);
        for(uint i = 0; i < fSubDiv_u; i++)
        {
            double theta = -2 * M_PI * i / fSubDiv_u;

            QVector3D normal = (dx * cos(theta) + dy * sin(theta)) * sin(phi) + dz * cos(phi);
            QVector3D pos = fCentre + normal * fRadius;
            mesh->AddVertex(pos, normal);
        }
    }
    mesh->AddVertex(fCentre - dz * fRadius, -dz);


    for(uint i = 0; i < fSubDiv_u; i++)
    {
        mesh->AddFace( i+1,
                       0,
                       (i+1)%fSubDiv_u+1);
    }


    for(uint j = 1; j < fSubDiv_v - 2; j++)
    {
        for(uint i = 0; i < fSubDiv_u; i++)
        {
            mesh->AddFace( i             + 1 + (j-1) * fSubDiv_u,
                           (i+1)%fSubDiv_u + 1 + (j-1) * fSubDiv_u,
                           i             + 1 +  j    * fSubDiv_u);

            mesh->AddFace( i             + 1 +  j    * fSubDiv_u,
                           (i+1)%fSubDiv_u + 1 + (j-1) * fSubDiv_u,
                           (i+1)%fSubDiv_u + 1 +  j    * fSubDiv_u);
        }
    }

    for(uint i = 0; i < fSubDiv_u; i++)
    {
        mesh->AddFace((fSubDiv_v-3) * fSubDiv_u + i + 1,
                      (fSubDiv_v-3) * fSubDiv_u + (i+1)%fSubDiv_u + 1,
                      (fSubDiv_v-2) * fSubDiv_u + 1);
    }
}

void Sphere::CreateIcoMesh(int recursionLevel, SV::TriD::Mesh* mesh)
{
    fMiddlePointIndexCache.clear();
    fFaceId.clear();

    // Create the 12 icosahedron vertices
    //
    float t = (1.0 + sqrt(5.0))/2.0;

    AddVertex(QVector3D(-1., t, 0.), mesh);
    AddVertex(QVector3D( 1., t, 0.), mesh);
    AddVertex(QVector3D(-1.,-t, 0.), mesh);
    AddVertex(QVector3D( 1.,-t, 0.), mesh);

    AddVertex(QVector3D(0.,-1., t), mesh);
    AddVertex(QVector3D(0., 1., t), mesh);
    AddVertex(QVector3D(0.,-1.,-t), mesh);
    AddVertex(QVector3D(0., 1.,-t), mesh);

    AddVertex(QVector3D( t,0.,-1.), mesh);
    AddVertex(QVector3D( t,0., 1.), mesh);
    AddVertex(QVector3D(-t,0.,-1.), mesh);
    AddVertex(QVector3D(-t,0., 1.), mesh);

    // Create the 20 triangles of the icosahedron
    // by associating point indices to each face.
    // One face being 3 points.
    //
    std::vector<QVector3D> facesTmp;

    // 5 faces around point 0
    facesTmp.push_back(QVector3D(0,11,5));
    facesTmp.push_back(QVector3D(0,5,1));
    facesTmp.push_back(QVector3D(0,1,7));
    facesTmp.push_back(QVector3D(0,7,10));
    facesTmp.push_back(QVector3D(0,10,11));

    // 5 adjacent faces
    facesTmp.push_back(QVector3D(1,5,9));
    facesTmp.push_back(QVector3D(5,11,4));
    facesTmp.push_back(QVector3D(11,10,2));
    facesTmp.push_back(QVector3D(10,7,6));
    facesTmp.push_back(QVector3D(7,1,8));

    // 5 faces around point 3
    facesTmp.push_back(QVector3D(3,9,4));
    facesTmp.push_back(QVector3D(3,4,2));
    facesTmp.push_back(QVector3D(3,2,6));
    facesTmp.push_back(QVector3D(3,6,8));
    facesTmp.push_back(QVector3D(3,8,9));

    // 5 adjacent faces
    facesTmp.push_back(QVector3D(4,9,5));
    facesTmp.push_back(QVector3D(2,4,11));
    facesTmp.push_back(QVector3D(6,2,10));
    facesTmp.push_back(QVector3D(8,6,7));
    facesTmp.push_back(QVector3D(9,8,1));

    // Refine the triangles
    //
    for(int i=0; i<recursionLevel; ++i)
    {
        std::vector<QVector3D> facesTemp_2;

        // Loop on each face to replace it by 4 triangles
        for(size_t r=0; r<facesTmp.size(); ++r)
        {
            QVector3D face = facesTmp[r];

            // Get the three middle points of each triangular face
            int a = GetMiddlePoint(face.x(), face.y(), mesh);
            int b = GetMiddlePoint(face.y(), face.z(), mesh);
            int c = GetMiddlePoint(face.z(), face.x(), mesh);

            // Get the new 4 triangles (equivalent to one old face)
            facesTemp_2.push_back(QVector3D(face.x(), a, c));
            facesTemp_2.push_back(QVector3D(face.y(), b, a));
            facesTemp_2.push_back(QVector3D(face.z(), c, b));
            facesTemp_2.push_back(QVector3D(a, b, c));
        }

        // Erase the old faces because we calculated new ones
        facesTmp.clear();

        // Fill faces vector with the newly calculated faces
        //
        for(size_t r=0; r<facesTemp_2.size(); ++r)
        {
            facesTmp.push_back(facesTemp_2[r]);
        }
    }

    // We now have all the triangle faces and their vertice indexes.
    // We need to add them to the mesh.
    //
    // Loop on all the faces
    for(size_t r=0; r<facesTmp.size(); ++r)
    {
        QVector3D tri = facesTmp[r];

        // Add the face
        int faceId = mesh->AddFace(tri.x(), tri.y(), tri.z());

        fFaceId.push_back(faceId);
    }
}

bool Sphere::IsWithin(const QVector3D& pos)
{
    QVector3D dist = pos - fCentre;
    float radius = dist.length();

    if(radius < fRadius)
        return true;

    return false;
}

QVector3D Sphere::RandomPointWithin()
{
    // http://math.stackexchange.com/questions/87230/picking-random-points-in-the-volume-of-sphere-with-uniform-probability

    // Check that the random generator has been initialized
    if(fpGenerator == nullptr)
    {
        std::cerr<<"*********** Fatal Error **************"<<std::endl;
        std::cerr<<"Sphere::RandomPointWithin: fpGenerator was not set."<<std::endl;
        std::cerr<<"You should call Primitive::SetRandomGenerator(...) somewhere/"<<std::endl;
        std::cerr<<"**************************************"<<std::endl;
        exit(EXIT_FAILURE);
    }

    // Normal distribution with mean=0 and std=1
    std::normal_distribution<float> distribution(0.0f,1.0f);

    // Values from the normal distribution
    float x = distribution(*fpGenerator);
    float y = distribution(*fpGenerator);
    float z = distribution(*fpGenerator);

    QVector3D pos(x,y,z);

    // Random pos in unit sphere
    pos = 1.0f/(std::sqrt(x*x + y*y + z*z) ) * pos;

    // Random U uniformly distributed between 0 and 1
    std::uniform_real_distribution<> uniformDistri(0.0f,1.0f);
    float U = uniformDistri(*fpGenerator);

    // Scale the pos vector to the radius of the sphere
    pos = fRadius * std::pow(U, 1./3) * pos;

    // Return the vector (ie the point) randomly sampled within the sphere
    return pos;
}

int Sphere::AddVertex(QVector3D p, SV::TriD::Mesh* mesh)
{
    // Compute position
    //
    double length = sqrt(p.x()*p.x() + p.y()*p.y() + p.z()*p.z()) * 1./fRadius;
    QVector3D vertexPosition(p.x()/length, p.y()/length, p.z()/length);

    // Add the vertex to the mesh
    int index = mesh->AddVertex(vertexPosition);

    return index;
}

int Sphere::GetMiddlePoint(int p1, int p2, SV::TriD::Mesh* mesh)
{
    // Return the index of the point in the middle of p1 and p2

    // Check if we have already calculated it
    //
    bool firstIsSmaller = p1 < p2;

    int64_t smallerIndex = firstIsSmaller ? p1 : p2;
    int64_t greaterIndex = firstIsSmaller ? p2 : p1;

    int64_t key = (smallerIndex << 32) + greaterIndex;

    if(fMiddlePointIndexCache.find(key) != fMiddlePointIndexCache.end())
    {
        return fMiddlePointIndexCache[key];
    }

    // If the point is not in cache then we calculate it
    //
    QVector3D point1 =  mesh->GetVertex(p1).Position();
    QVector3D point2 =  mesh->GetVertex(p2).Position();

    QVector3D middle((point1.x()+point2.x())/2.0,
                     (point1.y()+point2.y())/2.0,
                     (point1.z()+point2.z())/2.0);

    // Get the index and enforce the fact that the point is on the unit sphere
    // Also save the position
    int i = AddVertex(middle, mesh);

    // Store the point
    fMiddlePointIndexCache[key] = i;

    // return the index
    return i;
}

QVector3D Sphere::RandomPointOnSurface()
{
    // Check that the random generator has been initialized
    if(fpGenerator == nullptr)
    {
        std::cerr<<"*********** Fatal Error **************"<<std::endl;
        std::cerr<<"Sphere::RandomPosOnSphereSurface: fpGenerator was not set."<<std::endl;
        std::cerr<<"You should call Primitive::SetRandomGenerator(...) somewhere."<<std::endl;
        std::cerr<<"**************************************"<<std::endl;
        exit(EXIT_FAILURE);
    }

    QVector3D dir;

    float radius = fRadius;

    double testVal = 2;

    std::uniform_real_distribution<> uniDis(-1,1);

    while(testVal >= 1)
    {
        double x1 = uniDis(*fpGenerator);
        double x2 = uniDis(*fpGenerator);

        testVal = std::pow(x1, 2) + std::pow(x2, 2);

        double x = 2*x1*std::sqrt(1 - x1*x1 - x2*x2 );
        double y = 2*x2*std::sqrt(1 - x1*x1 - x2*x2);
        double z = 1 - 2 * (x1*x1 + x2*x2);

        dir.setX(x);
        dir.setY(y);
        dir.setZ(z);

        dir *= radius;
    }

    return dir;
}

}}
