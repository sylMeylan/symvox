/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "SymVox/3D/Geometry/include/Cube.hh"

namespace SV { namespace TriD {

VObject* Cube::CreateObject(AssetsManager* assetsManager)
{
    // Create the new obj
    Object* obj = new Object(assetsManager);

    // Add a mesh entry
    obj->AddMeshEntry();

    // Get its mesh object to modify it
    Mesh* mesh = &(obj->GetMesh() );

    // ******************
    // Mesh
    // ******************

    QVector3D diff;

    diff =  - fXAxis - fYAxis - fZAxis;
    VertID v000 = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v000bis = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v000bisbis = mesh->AddVertex(fOrigin + diff, diff.normalized());

    diff =    fXAxis - fYAxis - fZAxis;
    VertID v001 = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v001bis = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v001bisbis = mesh->AddVertex(fOrigin + diff, diff.normalized());

    diff =  - fXAxis + fYAxis - fZAxis;
    VertID v010 = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v010bis = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v010bisbis = mesh->AddVertex(fOrigin + diff, diff.normalized());

    diff =    fXAxis + fYAxis - fZAxis;
    VertID v011 = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v011bis = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v011bisbis = mesh->AddVertex(fOrigin + diff, diff.normalized());

    diff =  - fXAxis - fYAxis + fZAxis;
    VertID v100 = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v100bis = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v100bisbis = mesh->AddVertex(fOrigin + diff, diff.normalized());

    diff =    fXAxis - fYAxis + fZAxis;
    VertID v101 = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v101bis = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v101bisbis = mesh->AddVertex(fOrigin + diff, diff.normalized());

    diff =  - fXAxis + fYAxis + fZAxis;
    VertID v110 = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v110bis = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v110bisbis = mesh->AddVertex(fOrigin + diff, diff.normalized());

    diff =    fXAxis + fYAxis + fZAxis;
    VertID v111 = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v111bis = mesh->AddVertex(fOrigin + diff, diff.normalized());
    VertID v111bisbis = mesh->AddVertex(fOrigin + diff, diff.normalized());

    mesh->AddFace(v000, v011, v001);
    mesh->AddFace(v000, v010, v011);

    mesh->AddFace(v000bis, v101, v100);
    mesh->AddFace(v000bis, v001bis, v101);

    mesh->AddFace(v000bisbis, v110, v010bis);
    mesh->AddFace(v000bisbis, v100bis, v110);

    mesh->AddFace(v111, v110bis, v100bisbis);
    mesh->AddFace(v111, v100bisbis, v101bis);

    mesh->AddFace(v111bis, v011bis, v010bisbis);
    mesh->AddFace(v111bis, v010bisbis, v110bisbis);

    mesh->AddFace(v111bisbis, v101bisbis, v001bisbis);
    mesh->AddFace(v111bisbis, v001bisbis, v011bisbis);

    mesh->FillColor(QColor(255,0,0,255));

    mesh->ComputeNormals();

    // Return the object 3d
    return obj;
}

bool Cube::IsWithin(const QVector3D &pos)
{
    if(pos.x() <= fXAxis.x() && pos.x() >= -fXAxis.x()
            && pos.y() <= fYAxis.y() && pos.y() >= -fYAxis.y()
            && pos.z() <= fXAxis.z() && pos.z() >= -fZAxis.z()
            )
        return true;
    else
        return false;
}


}}
