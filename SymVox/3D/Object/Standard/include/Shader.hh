/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef SHADER_HH
#define SHADER_HH

#include "SymVox/Utils/include/OpenGL.hh"

#include <string>
#include <sstream>

#include <QFile>

#include <QMatrix4x4>

#include "SymVox/Utils/include/Exception.hh"

namespace SV{ namespace TriD {

class Shader : public Utils::OpenGL
{
public:
    Shader();
    Shader(const std::string& vertexSource, const std::string& fragmentSource, const std::string &geoSource="");
    ~Shader();

    Shader(Shader const &shaderToBeCopied);
    Shader& operator=(Shader const &shaderToBeCopied);

    // Load the shader and pass information to the GLSL
    bool Load();

    void Use();
    void Unuse();

    // Used to send a matrix to the GLSL
    void Send4fv(const std::string& name, const QMatrix4x4& mat);
    void Send3vf(const std::string& name, const QVector3D& vec);
    void Send3f(const std::string& name, float a, float b, float c);
    void Send1f(const std::string& name, float a);
    void Send1i(const std::string& name, int i);

    // Program Id
    unsigned int GetProgramID() const {return fProgramID;}

    // IsLoaded
    void SetIsLoaded(bool b) {fIsShaderLoaded=b;}
    bool GetIsLoaded() const {return fIsShaderLoaded;}

    void AddAttribLocation(const std::string& location);
    void ResetLocations();

    void AddVaryings(const std::string& v);
    void ResetVaryings();

    const std::string& GetVertSource() {return fVertexSource;}
    const std::string& GetFragSource() {return fFragmentSource;}
    const std::string& GetGeoSource() {return fGeoSource;}

    int GetUniformLocation(const std::string& locName);

private:
    // Some IDs
    unsigned int fVertexID;
    unsigned int fFragmentID;
    unsigned int fProgramID;
    unsigned int fGeoID;

    std::vector<std::string> fAttribLocations;
    std::vector<std::string> fVaryings;

    // Sources of the vertex and fragment shaders
    std::string fVertexSource;
    std::string fFragmentSource;
    std::string fGeoSource;

    // Flag to check is the shader was loaded.
    // Used within the copy operator to decide if the load method needs to be called after
    // the copy process
    bool fIsShaderLoaded;

    // Compile the shader, called within the Load method
    bool CompileShader(unsigned int &shader, unsigned int type, const std::string& sourceFile);
};

}}

#endif // SHADER_HH
