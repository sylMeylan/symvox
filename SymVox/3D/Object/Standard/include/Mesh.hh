/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_OBJ_MESH_HH
#define TRID_OBJ_MESH_HH

#include <QMatrix4x4>
#include <QVector4D>
#include <QVector3D>
#include <QVector2D>
#include <QColor>
#include <iostream>
#include <sstream>

#include "SymVox/Utils/include/Exception.hh"

namespace SV{ namespace TriD {

using VertID = unsigned int;
using FaceID = unsigned int;

class Vertex
{
public:
    Vertex(const QVector3D& position,
           const QVector3D& normal = QVector3D(0.0,0.0,0.0),
           const QColor &color = QColor(0.5, 0.5, 0.5),
           const QVector2D& text_coord = QVector2D(0.0,0.0));

    Vertex(const Vertex& to_copy);

    ~Vertex();

    // Read only
    inline const QVector3D& Position()  const { return fPosition; }
    inline const QVector3D& Normal()    const { return fNormal; }
    inline const QColor& Color()     const { return fColor; }
    inline const QVector2D& TextCoord() const { return fTextCoord; }

    // Read and write
    inline QVector3D& Position()  { return fPosition; }
    inline QVector3D& Normal()    { return fNormal; }
    inline QColor&    Color()     { return fColor; }
    inline QVector2D& TextCoord() { return fTextCoord; }

private:
    QVector3D fPosition;        // position
    QVector3D fNormal;          // normal
    QColor fColor;           // color
    QVector2D fTextCoord;       // texture coordinate (only 1 texture at a time)
};

class Face
{
public:
    Face(const VertID ID0, const VertID ID1, const VertID ID2) :
        fID0(ID0),  fID1(ID1), fID2(ID2)
    {}

    Face(const Face& to_copy) :
        fID0(to_copy.ID0()),  fID1(to_copy.ID1()), fID2(to_copy.ID2())
    {}

    ~Face() {}

    const VertID& operator[] (uint i) const
    {
        if(i==0)
            return fID0;
        else if(i==1)
            return fID1;
        else // if(i==2)
            return fID2;
    }

    VertID& ID0() { return fID0; }
    VertID& ID1() { return fID1; }
    VertID& ID2() { return fID2; }

    const VertID& ID0() const { return fID0; }
    const VertID& ID1() const { return fID1; }
    const VertID& ID2() const { return fID2; }

    const uint size() const {return 3;}

    void reverse() { VertID temp = fID1; fID1 = fID2; fID2 = temp; }

private:
    VertID fID0;
    VertID fID1;
    VertID fID2;
};

class BoundingBox
{
public:
    QVector3D fMin;
    QVector3D fMax;
};

class Mesh
{
public:
    Mesh();

    ~Mesh();

    void RemoveData();

    VertID AddVertex(const QVector3D& position,
                     const QVector3D& normal = QVector3D(0.0,0.0,0.0),
                     const QColor& color = QColor(0.5,0.5,0.5) ,
                     const QVector2D& text_coord = QVector2D(0.0,0.0));

    VertID AddVertex2(const QVector3D& position,
                     const QVector3D& normal = QVector3D(0.0,0.0,0.0),
                     const QVector2D& text_coord = QVector2D(0.0,0.0));

    FaceID AddFace(const VertID id0, const VertID id1, const VertID id2);

    // Element counting methods
    inline unsigned int VerticesCount() const { return nbVertices; }
    inline unsigned int FacesCount() const { return nbFaces; }

    // Normal computation methods
    void ComputeNormals();
    void InvertNormals();

    void InvertFace(const FaceID fid);
    void InvertFaces();

    std::vector<QVector3D> GetFacesNormals() const;
    std::vector<std::vector<FaceID> > GetVerticesFaces() const;
    std::vector<QVector3D> GetVerticesNormals() const;

    // Color management
    void FillColor(const QColor& color);
    void ColorsFromNormals();
    void ColorsFromPositions();

    void SetVertColor(const VertID vid, const QColor& color);
    void SetFaceColor(const FaceID id, const QColor& color);
    void SetVertPosition(const VertID vid, const QVector3D& pos);
    void SetVertNormal  (const VertID vid, const QVector3D& nor);
    void SetVertTexCoord(const VertID vid, const QVector2D& txc);

    // Attribute acces methods

    // Read only
    const Vertex GetVertex(const VertID id) const;
    const Face GetFace(const FaceID id) const;
    inline const std::vector<float>& Positions      () const { return fPosition; }
    inline const std::vector<float>& Normals        () const { return fNormal; }
    inline const std::vector<float>& Colors         () const { return fColor; }
    inline const std::vector<float>& TexCoords      () const { return fTexCoord; }
    inline const std::vector<uint>& Connectivity    () const { return fConnectivity; }
    const QVector3D Position  (const VertID id)   const { return QVector3D(fPosition[3*id], fPosition[3*id+1], fPosition[3*id+2]); }
    const QVector3D Normal    (const VertID id)   const { return QVector3D(fNormal[3*id], fNormal[3*id+1], fNormal[3*id+2]); }
    const QColor    Color     (const VertID id)   const { return QColor(int(fColor[4*id]*255), int(fColor[4*id+1]*255), int(fColor[4*id+2]*255), int(fColor[4*id+3]*255) ); }
    const QVector2D TexCoord  (const VertID id)   const { return QVector2D(fTexCoord[2*id], fTexCoord[2*id+1]); }

    // Read and write
    inline std::vector<float>& Positions()      { return fPosition; }
    inline std::vector<float>& Normals()        { return fNormal; }
    inline std::vector<float>& Colors()         { return fColor; }
    inline std::vector<float>& TexCoords()      { return fTexCoord; }
    inline std::vector<uint>&  Connectivity()   { return fConnectivity; }

    // Display functions
    void DisplayVertices() const;
    void DisplayConnectivity() const;

    // bounding box manip
    BoundingBox ComputeBB() const;

    // Manipulation functions
    void ApplyTransform(const QMatrix4x4 &mat);
    void Scale(const double coeff);
    void Scale(const QVector3D& vect);
    void Translate(const QVector3D& vect);
    void AutoScale();
    void add_mesh(const Mesh& m);

    uint GetNumOfVertices() const {return nbVertices;}
    uint GetNumOfFaces() const {return nbFaces;}

private:
    uint nbFaces;
    uint nbVertices;

    std::vector<float> fPosition;
    std::vector<float> fNormal;
    std::vector<float> fColor;
    std::vector<float> fTexCoord;
    std::vector<uint>  fConnectivity;
};

}}

#endif // TRID_OBJ_MESH_HH
