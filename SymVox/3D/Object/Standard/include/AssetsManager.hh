/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_OBJ_ASSETSMANAGER_HH
#define TRID_OBJ_ASSETSMANAGER_HH

#include "Shader.hh"
#include "Texture.hh"

#include "SymVox/Utils/include/StrToIdContainer.hh"

namespace SV{ namespace TriD {

class AssetsManager
{
public:
    AssetsManager(); ///< A constructor
    ~AssetsManager(); ///< A destructor

    // Textures
    int AddTexture(Texture* texture, const std::string& name);
    Texture* GetTexture(int index) {return fTextures.Get(index);}
    const Texture* GetTexture(int index) const {return fTextures.Get(index);}
    int GetNumOfTextures() {return fTextures.GetNumOfObj();}
    bool IsTextureInside(const std::string& name) {return fTextures.IsInside(name);}
    int GetTextureId(const std::string& name);
    int AutoAddTexture(const std::string& fileName, bool isMirrored=true);

    // Shaders
    int AddShader(Shader* shader);
    Shader* GetShader(int index) {return fShaders.Get(index);}
    const Shader* GetShader(int index) const {return fShaders.Get(index);}
    int GetNumOfShaders() {return fShaders.GetNumOfObj();}
    bool IsShaderInside(Shader* shader);
    bool IsShaderInside(const std::string& vert, const std::string& frag, const std::string& geom="");
    int GetShaderId(const std::string& vert, const std::string& frag);
    int GetShaderId(const std::string& vert, const std::string& frag, const std::string& geo);
    int AutoAddShader(const std::string& vert, const std::string& frag,
                      const std::string &geom, const std::vector<std::string>& locations);

private:
    Utils::StrToIdContainer<Texture*> fTextures;
    Utils::StrToIdContainer<Shader*> fShaders;

    int GetShaderId(const std::string& n);
    bool IsShaderInside(const std::string& name) {return fShaders.IsInside(name);}
};

}}

#endif // TRID_OBJ_ASSETSMANAGER_HH
