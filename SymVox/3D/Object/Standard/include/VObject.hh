/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_OBJ_VOBJECT_HH
#define TRID_OBJ_VOBJECT_HH

#include "SymVox/Utils/include/OpenGL.hh"

#include "Mesh.hh"
#include "AssetsManager.hh"
#include "SymVox/3D/Object/Light/include/LightManager.hh"
#include "SymVox/3D/Memory/include/CPUBuffer.hh"
#include "SymVox/3D/Memory/include/GPUObj3DBuffer.hh"
#include "SymVox/3D/Memory/include/GPUIndexBuffer.hh"
#include "SymVox/3D/Memory/include/GPUInstanceBuffer.hh"
#include "SymVox/3D/Memory/include/VAO.hh"

namespace SV{ namespace TriD {

/*!
 * \brief The MeshEntry struct
 * One object3D is equivalent to a 3D model and, as such, it can contain several mesh entries.
 * Each mesh entry has its own mesh and associtated data.
 */
struct MeshEntry
{
    /*!
     * \brief MeshEntry
     * \param mesh
     * Constructor
     */
    MeshEntry(const Mesh& mesh=Mesh() )
    {
        fMesh = mesh;
        fTextureDiffuseId = -1;
        fTextureSpecularId = -1;
        fTextureNormalId = -1;
        fTextureReflectionId  = -1;
        fTextureEmissionId = -1;
        fShaderId  = -1;
        fConnectivitySize = -1;
        fUpdateInstanceBuffer = true;
    }

    ~MeshEntry() {} ///< Destructor

    int fTextureDiffuseId; ///< Object color ID
    int fTextureSpecularId; ///< Object "shininess" ID
    int fTextureNormalId; ///< Normal/dump map ID
    int fTextureReflectionId; ///< Reflection map ID
    int fTextureEmissionId; ///< Light emission map ID
    int fShaderId; ///< Shader ID
    unsigned int fConnectivitySize;
    bool fUpdateInstanceBuffer;

    Mesh fMesh;
    VAO fVao;
    GPUObj3DBuffer fObjBuffGPU;
    GPUIndexBuffer fIndexBuffGPU;
    GPUInstanceBuffer fInstanceBuffGPU;
};

class VObject : protected Utils::OpenGL
{
public:
    VObject();
    VObject(AssetsManager* assetsManager);

    virtual ~VObject();

    virtual void Load() = 0;

    virtual void Display(const QMatrix4x4& model, const QMatrix4x4& view, const QMatrix4x4& proj, const QVector3D& cameraPos, LightManager& lightManager) = 0;

    // Mesh
    void SetMesh(const Mesh& mesh, int index=0);
    Mesh& GetMesh(int index=0);
    const Mesh& GetMesh(int index=0) const;

    // Assets manager
    void SetAssetsManager(AssetsManager* assetsManager) {fpAssetsManager = assetsManager;}
    AssetsManager* GetAssetsManager() {return fpAssetsManager;}
    const AssetsManager* GetAssetsManager() const {return fpAssetsManager;}

    // Name
    void SetName(const std::string& name) {fName = name;}
    const std::string& GetName() const {return fName;}

    // Id
    void SetAutoId(int id)  {fAutoId=id;}
    int GetAutoId() {return fAutoId;}

    // ShaderId
    void SetShaderId(int id, int index=0);
    int GetShaderID(int index=0) const;

    // TextureId
    void SetTextureDiffuseId(int id, int index=0);
    int GetTextureDiffuseId(int index=0) const;
    void SetTextureSpecularId(int id, int index=0);
    int GetTextureSpecularId(int index=0) const;
    void SetTextureNormalId(int id, int index=0);
    int GetTextureNormalId(int index=0) const;
    void SetTextureReflectionId(int id, int index=0);
    int GetTextureReflectionId(int index=0) const;
    void SetTextureEmissionId(int id, int index=0);
    int GetTextureEmissionId(int index=0) const;

    // IsLoaded
    bool GetIsLoaded() {return fIsLoaded;}

    // InstanceBuffer
    void SetUpdateInstanceBuffer(bool b);
    bool GetIsUpdateInstanceBuffer(int index=0) const;

    // Mesh entry
    MeshEntry& GetMeshEntry(int index=0);
    const MeshEntry& GetMeshEntry(int index=0) const;
    int GetNumOfMeshEntries() const;
    void AddMeshEntry();
    void RemoveMeshEntry(int index);
    void RemoveAllMeshEntries();

    // Instance buffer
    void SetInstanceBuffer(CPUBuffer<float>* instanceBuffer) {fpInstanceBuffer=instanceBuffer;}
    CPUBuffer<float>* GetInstanceBuffer() {return fpInstanceBuffer;}

protected:
        AssetsManager* fpAssetsManager; ///< The assets manager contains all the program "3D assets": textures, shaders etc.
        bool fIsLoaded; ///< Used by the display method to check the model was loaded.
        std::vector<MeshEntry*> fMeshEntryVect; ///< This vector is "looped on" in order to render each mesh entry.
        CPUBuffer<float>* fpInstanceBuffer; ///< A pointer to the current instance buffer
        int fAutoId; ///< This is the id used by the render engine to identify the object3d. This id should not be modified by the user.
        std::string fName; ///< Name of the object (not mandatory).

private:
        // Forbid copy and assignement
        VObject(VObject& obj); ///< Copy constructor
        VObject& operator=(const VObject& obj); ///< Assignement operator

};

}}

#endif // TRID_OBJ_VOBJECT_HH
