/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TEXTURE_HH
#define TEXTURE_HH

#include "SymVox/Utils/include/OpenGL.hh"

#include <QImage>
#include <QPixmap>
#include <sstream>

#include "SymVox/Utils/include/Exception.hh"

namespace SV{ namespace TriD {

class Texture : protected Utils::OpenGL
{
public:

    enum Filter {
        basic,
        mimap,
        trilinear
    };

    Texture();
    Texture(std::string imageFile, bool mirrored = false);
    Texture(int width, int height, GLenum format, GLenum internFormat, bool emptyTexture);
    virtual ~Texture();

    Texture(const Texture &textureToBeCopied);
    Texture& operator=(Texture const &textureToBeCopied);

    virtual bool Load(GLenum textureUnit = GL_TEXTURE0);
    void LoadEmptyTexture();

    virtual void Bind(GLenum textureUnit = GL_TEXTURE0);
    virtual void Unbind();

    // OpenGL id
    GLuint GetId() const;

    // Image file
    bool SetImageFile(const std::string &imageFile);
    const std::string& GetImageFile() const {return fImageFile;}

    // IsLoaded
    void SetIsLoaded(bool b) {fIsLoaded=b;}
    bool GetIsLoaded() const {return fIsLoaded;}

    void SetIsMirrored(bool b) {fIsMirrored=b;}
    bool GetIsMirrored() {return fIsMirrored;}

    void SetFilter(Filter filter) {fFilter=filter;}
    Filter GetFilter() {return fFilter;}

    void SetInternalFormat(GLint f) {fInternalformat=f;}
    GLint GetInternalFormat() {return fInternalformat;}

    void SetSizes(unsigned int width, unsigned int height) {fWidth=width; fHeight=height;}

protected:

    GLuint fId;
    std::string fImageFile;

    // Frame Buffer Object
    int fWidth;
    int fHeight;
    GLenum fFormat;
    GLenum fInternFormat;
    bool fEmptyTexture;
    bool fIsLoaded;
    bool fIsMirrored;
    Filter fFilter;
    GLint fInternalformat;
};

}}

#endif // TEXTURE_HH
