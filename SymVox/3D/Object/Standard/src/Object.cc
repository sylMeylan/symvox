/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Object.hh"

namespace SV{ namespace TriD {

using namespace TriD;

Object::Object() : VObject()
{

}

Object::Object(AssetsManager* assetsManager)  : VObject(assetsManager)
{

}

Object::~Object()
{

}

void Object::Load()
{
    this->initializeOpenGLFunctions();

    // Set the references for the current mesh entry

    for(int i=0; i<GetNumOfMeshEntries(); i++)
    {
        MeshEntry& meshEntry = GetMeshEntry(i);

        VAO& vao = meshEntry.fVao;
        Mesh& mesh = meshEntry.fMesh;
        GPUObj3DBuffer& objBuffGPU = meshEntry.fObjBuffGPU;
        GPUIndexBuffer& indexBuffGPU = meshEntry.fIndexBuffGPU;
        GPUInstanceBuffer& instanceBuffGPU = meshEntry.fInstanceBuffGPU;

        meshEntry.fConnectivitySize = meshEntry.fMesh.Connectivity().size();

        if(vao.IsExisting() )
            vao.Delete();

        vao.Init();

        // Create a buffer on the CPU with the mesh data
        CPUBuffer<float> object3DBuffer("ObjectBuffer", GL_ARRAY_BUFFER, GL_STATIC_DRAW, GL_FLOAT);
        object3DBuffer.AddData("Positions", mesh.Positions(), 3); // 0
        object3DBuffer.AddData("Colors", mesh.Colors(), 4); // 1
        object3DBuffer.AddData("TextureCoord", mesh.TexCoords(), 2); //  2
        object3DBuffer.AddData("Normals", mesh.Normals(), 3); // 3

        // Send the CPU buffer on the GPU thanks to the bufferGPU class.
        objBuffGPU.SetVAO(&vao);
        objBuffGPU.SendToGPU(object3DBuffer);

        // Index buffer

        CPUBuffer<unsigned int> indexBuffCPU("IndexBuffer", GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW, GL_UNSIGNED_INT);
        indexBuffCPU.AddData("Index", mesh.Connectivity(), 0);

        indexBuffGPU.SetVAO(&vao);
        indexBuffGPU.SendToGPU(indexBuffCPU);

        // Instance buffer

        instanceBuffGPU.SetVAO(&vao);
        meshEntry.fUpdateInstanceBuffer = true;
    }

    fIsLoaded = true;
}

void Object::Display(const QMatrix4x4& model, const QMatrix4x4& view, const QMatrix4x4& proj, const QVector3D& cameraPos, LightManager& lightManager)
{
    // Check that the loaded method has been called
    if(!fIsLoaded)
    {
        std::stringstream ss;
        ss << "Load method was not called."<<std::endl;
        ss << "Object name: "<<this->GetName()<<std::endl;
        Utils::Exception(Utils::ExceptionType::Fatal, "Object::Display", ss.str() );
    }

    for(int i=0; i<GetNumOfMeshEntries(); i++)
    {
        //glDisable(GL_CULL_FACE);

        MeshEntry& meshEntry = GetMeshEntry(i);

        VAO& vao = meshEntry.fVao;
        GPUIndexBuffer& indexBuffGPU = meshEntry.fIndexBuffGPU;
        GPUInstanceBuffer& instanceBuffGPU = meshEntry.fInstanceBuffGPU;
        int& textureDiffuseId = meshEntry.fTextureDiffuseId;
        int& textureSpecularId = meshEntry.fTextureSpecularId;
        int& textureEmissionId = meshEntry.fTextureEmissionId;
        int& shaderId = GetMeshEntry(0).fShaderId;
        unsigned int& connectivitySize = meshEntry.fConnectivitySize;
        bool& updateInstanceBuffer = meshEntry.fUpdateInstanceBuffer;

        int numInstances (0);

        // Fill the GPU instance buffer, if needed
        if(updateInstanceBuffer && fpInstanceBuffer != nullptr)
        {
            // Fill the GPU instance buffer

            // Send the instance buffer to the GPU after processing it
            instanceBuffGPU.SendToGPU(*fpInstanceBuffer);

            // Number of instances to be rendered
            numInstances = fpInstanceBuffer->GetData(0).GetNumOfValues() / fpInstanceBuffer->GetData(0).GetSetSize();

            // Set the flag to false because the buffer is filled,
            // no need to fill it twice.
            updateInstanceBuffer = false;
        }

        // Shader activation

        if(shaderId==-1)
        {
            Utils::Exception(Utils::ExceptionType::Fatal, "Object::Display",
                             "fShaderId=-1 and, thus, was not defined for \""+fName+"\"");
        }

        // Retrieve the shader
        Shader* shader = fpAssetsManager->GetShader(shaderId);

        shader->Use();

        // Lock the VAO
        vao.Bind();

        // Send the uniform variables to the shader
        //shader->Send4fv("model", model);
        shader->Send4fv("view", view);
        shader->Send4fv("projection", proj);

        shader->Send3f("viewPos", cameraPos.x(), cameraPos.y(), cameraPos.z());

        // Light management

        lightManager.SendLightsToShader(shader);

        // Set material properties
        shader->Send1i("material_diffuse", 0); // Texture
        shader->Send1i("material_specular", 1); // Texture
        shader->Send1i("material_emission", 2); // Texture
        shader->Send1f("material.shininess", 16.0f); // Default 32

        // Lock the VBO
        // We bind the index buffer to allow indexing.
        indexBuffGPU.GetVbo()->Bind();

        // Bind texture if any

        Texture* textureDiffuse = nullptr;

        if(textureDiffuseId != -1)
        {
            textureDiffuse = fpAssetsManager->GetTexture(textureDiffuseId);
            textureDiffuse->Bind(GL_TEXTURE0);
        }

        Texture* textureSpecular = nullptr;

        if(textureSpecularId != -1)
        {
            textureSpecular = fpAssetsManager->GetTexture(textureSpecularId);
            textureSpecular->Bind(GL_TEXTURE1);
        }

        Texture* textureEmission = nullptr;

        if(textureEmissionId != -1)
        {
            textureEmission = fpAssetsManager->GetTexture(textureEmissionId);
            textureEmission->Bind(GL_TEXTURE2);
        }

        glDrawElementsInstanced(
                    GL_TRIANGLES,           // mode
                    connectivitySize,   // count
                    GL_UNSIGNED_INT,        // type
                    (void*)0,               // offset
                    numInstances            // num of instances
                    );

        // Unbind texture if a texture was binded before
        if(textureDiffuse != nullptr)
            textureDiffuse->Unbind();
        if(textureSpecular != nullptr)
            textureSpecular->Unbind();
        if(textureEmission != nullptr)
            textureEmission->Unbind();

        // Unlock the VBO
        indexBuffGPU.GetVbo()->Unbind();

        // Unlock the VAO
        vao.Unbind();

        // Deactivate shader
        shader->Unuse();

        //glEnable(GL_CULL_FACE);
    }

    fpInstanceBuffer = nullptr;
}



}}
