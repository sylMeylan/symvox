/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Shader.hh"

namespace SV{ namespace TriD {

Shader::Shader()
    : fVertexID(0),
      fFragmentID(0),
      fProgramID(0),
      fGeoID(0),
      fVertexSource(""),
      fFragmentSource(""),
      fGeoSource(""),
      fIsShaderLoaded(false)
{
    this->initializeOpenGLFunctions();
}

Shader::Shader(const std::string& vertexSource, const std::string& fragmentSource, const std::string& geoSource)
    : fVertexID(0),
      fFragmentID(0),
      fProgramID(0),
      fGeoID(0),
      fVertexSource(vertexSource),
      fFragmentSource(fragmentSource),
      fGeoSource(geoSource),
      fIsShaderLoaded(false)
{
    this->initializeOpenGLFunctions();
}

Shader::~Shader()
{
    // Delete the shader
    glDeleteShader(fVertexID);
    glDeleteShader(fFragmentID);
    glDeleteShader(fProgramID);
    glDeleteShader(fGeoID);
}


Shader::Shader(Shader const &shaderToBeCopied)
{
    // Copy the source files
    fVertexSource = shaderToBeCopied.fVertexSource;
    fFragmentSource = shaderToBeCopied.fFragmentSource;
    fGeoSource = shaderToBeCopied.fGeoSource;
    fVaryings = shaderToBeCopied.fVaryings;

    // Load the new shader only if the old one was loaded
    if(shaderToBeCopied.fIsShaderLoaded) Load();

    this->initializeOpenGLFunctions();
}

Shader& Shader::operator=(Shader const &shaderToBeCopied)
{
    // Copy the source files
    fVertexSource = shaderToBeCopied.fVertexSource;
    fFragmentSource = shaderToBeCopied.fFragmentSource;
    fGeoSource = shaderToBeCopied.fGeoSource;
    fVaryings = shaderToBeCopied.fVaryings;

    // Load the new shader only if the old one was loaded
    if(shaderToBeCopied.fIsShaderLoaded) Load();

    this->initializeOpenGLFunctions();

    // Return the this pointer
    return *this;
}

bool Shader::Load()
{
    // Check if there is a former shader with the same IDs
    // If yes then delete it
    //
    if(glIsShader(fVertexID) == GL_TRUE)
        glDeleteShader(fVertexID);

    if(glIsShader(fFragmentID) == GL_TRUE)
        glDeleteShader(fFragmentID);

    if(glIsShader(fGeoID) == GL_TRUE)
        glDeleteShader(fGeoID);

    if(glIsProgram(fProgramID) == GL_TRUE)
        glDeleteProgram(fProgramID);

    // Compile the shaders
    //
    if(!CompileShader(fVertexID, GL_VERTEX_SHADER, fVertexSource))
        return false;

    if(!CompileShader(fFragmentID, GL_FRAGMENT_SHADER, fFragmentSource))
        return false;

    if(fGeoSource != "")
        if(!CompileShader(fGeoID, GL_GEOMETRY_SHADER, fGeoSource))
            return false;

    // Shader program creation
    fProgramID = glCreateProgram();

    // Associate the shaders to the program
    //
    glAttachShader(fProgramID, fVertexID);
    glAttachShader(fProgramID, fFragmentID);

    if(fGeoSource != "")
        glAttachShader(fProgramID, fGeoID);

    //    glBindAttribLocation(fProgramID, 0, "in_Vertex");
    //    glBindAttribLocation(fProgramID, 1, "in_Color");
    //    glBindAttribLocation(fProgramID, 2, "in_TextureCoord");
    //    glBindAttribLocation(fProgramID, 3, "in_NormalCoord");
    //    glBindAttribLocation(fProgramID, 4, "in_Model");

    if(fAttribLocations.size() != 0)
    {
        // Lock the shader entries
        for(unsigned int j=0, je=fAttribLocations.size(); j<je; j++)
        {
            glBindAttribLocation(fProgramID, j, fAttribLocations.at(j).c_str() );
        }
    }

    if(fVaryings.size() != 0)
    {
        const GLchar* varyings[fVaryings.size()];
        for(unsigned int j=0, je=fVaryings.size(); j<je; j++)
        {
            varyings[j] = fVaryings.at(j).c_str();
        }

        glTransformFeedbackVaryings(fProgramID, fVaryings.size(), varyings, GL_INTERLEAVED_ATTRIBS);
    }

    // Link the program
    glLinkProgram(fProgramID);

    // Check the link
    GLint linkError (0);
    glGetProgramiv(fProgramID, GL_LINK_STATUS, &linkError);

    // If there was an error
    if(linkError != GL_TRUE)
    {
        // Get the error size
        GLint errorSize (0);
        glGetProgramiv(fProgramID, GL_INFO_LOG_LENGTH, &errorSize);

        // Memory allocation
        GLchar *error = new GLchar[errorSize+1];

        // Get the error
        glGetShaderInfoLog(fProgramID, errorSize, &errorSize, error);
        error[errorSize] = '\0';

        // Display the error
        std::stringstream ss;
        ss << "There was an error during shader compilation: \n"<<std::string(error);
        Utils::Exception(Utils::ExceptionType::Warning, "Shader::Load", ss.str() );

        // Free the memory
        delete[] error;
        glDeleteProgram(fProgramID);

        fIsShaderLoaded = true;

        return false;
    }
    // If there was no error then everythin is ok
    else
    {
        return true;
    }
}

bool Shader::CompileShader(unsigned int &shader, unsigned int type, const std::string& sourceFile)
{
    // Shader creation
    shader = glCreateShader(type);

    // Check the shader
    if(shader == 0)
    {
        std::stringstream ss;
        ss <<"Error, the shader type \""<<type<<"\" doesn't exist";
        Utils::Exception(Utils::ExceptionType::Warning, "Shader::CompileShader", ss.str() );

        return false;
    }

    QFile file(QString(sourceFile.c_str() ) );
    bool isOpen = file.open(QIODevice::ReadOnly);

    if(!isOpen)
    {
        std::stringstream ss;
        ss <<"Error, the file \""<<sourceFile<<"\" cannot be found";
        Utils::Exception(Utils::ExceptionType::Warning, "Shader::CompileShader", ss.str() );

        glDeleteShader(shader);

        return false;
    }

    std::string sourceCode;

    QTextStream in(&file);

    while(!in.atEnd())
    {
        std::string line = in.readLine().toStdString();
        sourceCode += line + "\n";
    }

    file.close();

    // Get the C char chain of the source code
    const GLchar* sourceCodeChain = sourceCode.c_str();

    // Send the source code to the shader
    glShaderSource(shader, 1, &sourceCodeChain, 0);

    // Shader compilation
    glCompileShader(shader);

    // Check the compilation
    GLint compilationError (0);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compilationError);

    // If there was an error
    if(compilationError != GL_TRUE)
    {
        // Get the error size
        GLint errorSize (0);
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &errorSize);

        // Memory allocation
        char *error = new char[errorSize+1];

        // Get the error
        error[0] = '\0';
        glGetShaderInfoLog(shader, errorSize, &errorSize, error);
        error[errorSize] = '\0';

        // Display the error
        std::stringstream ss;
        ss << sourceFile.c_str() << "\n";
        ss << "There was an error during the shader compilation: \n";
        ss <<std::string(error);
        Utils::Exception(Utils::ExceptionType::Warning, "Shader::CompileShader", ss.str() );

        // Free the memory
        delete[] error;
        glDeleteShader(shader);

        return false;
    }
    // If there was no error then everythin is ok
    else
    {
        return true;
    }
}

int Shader::GetUniformLocation(const std::string& locName)
{
    int location = glGetUniformLocation(fProgramID, locName.c_str());

    if(location == GL_INVALID_OPERATION)
    {
        std::string msg = "The uniform variable "+locName;
        msg += " cannot be localized in the shader: ";
        msg += this->GetVertSource();
        msg += " "+this->GetFragSource();
        Utils::Exception(Utils::ExceptionType::Fatal, "Shader::GetUniformLocation", msg);
    }

    return location;
}

void Shader::Send4fv(const std::string& name, const QMatrix4x4& mat)
{
    // Matrix localisation
    int localisation = GetUniformLocation(name);

    // Send the matrix values
    glUniformMatrix4fv(localisation, 1, GL_FALSE, &mat(0,0));
}

void Shader::Send3vf(const std::string& name, const QVector3D& vec)
{
    // Localisation
    int localisation = GetUniformLocation(name);

    float vect[3];
    vect[0] = vec.x();
    vect[1] = vec.y();
    vect[2] = vec.z();

    // Send
    glUniform3fv(localisation, 1, &vect[0] );
}

void Shader::Send3f(const std::string& name, float a, float b, float c)
{
    // Localisation
    int localisation = GetUniformLocation(name);
    glUniform3f(localisation, a, b, c);
}

void Shader::Send1f(const std::string& name, float a)
{
    // Localisation
    int localisation = GetUniformLocation(name);
    glUniform1f(localisation, a);
}

void Shader::Send1i(const std::string& name, int i)
{
    int localisation = GetUniformLocation(name);
    glUniform1i(localisation, i);
}

void Shader::AddAttribLocation(const std::string& location)
{
    fAttribLocations.push_back(location);
}

void Shader::ResetLocations()
{
    fAttribLocations.clear();
}

void Shader::AddVaryings(const std::string &v)
{
    fVaryings.push_back(v);
}

void Shader::ResetVaryings()
{
    fVaryings.clear();
}

void Shader::Use()
{
    glUseProgram(fProgramID);
}

void Shader::Unuse()
{
    glUseProgram(0);
}

}}
