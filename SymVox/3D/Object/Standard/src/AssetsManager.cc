/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/AssetsManager.hh"

namespace SV{ namespace TriD {

AssetsManager::AssetsManager()
{

}

AssetsManager::~AssetsManager()
{

}

int AssetsManager::AddTexture(Texture* texture, const std::string& name)
{
    return fTextures.Add(texture, name);
}

int AssetsManager::GetTextureId(const std::string& name)
{
    return fTextures.GetIndex(name);
}

int AssetsManager::AutoAddTexture(const std::string& fileName, bool isMirrored)
{
    int id = -1;

    if(!IsTextureInside(fileName))
    {
        Texture* tex = new Texture(fileName);
        id = this->AddTexture(tex, fileName);
        tex->SetIsLoaded(isMirrored);
        tex->Load();
    }
    else
    {
        id = this->GetTextureId(fileName);
    }

    return id;
}

int AssetsManager::AddShader(Shader* shader)
{
    std::string nameStrId = shader->GetVertSource() + " " + shader->GetFragSource();

    if(shader->GetGeoSource() != "")
        nameStrId += " " + shader->GetGeoSource();

    return fShaders.Add(shader, nameStrId);
}

bool AssetsManager::IsShaderInside(Shader* shader)
{
    std::stringstream ss;
    ss << shader->GetVertSource() << " " << shader->GetFragSource();

    if(shader->GetGeoSource() != "")
        ss << " " << shader->GetGeoSource();

    return IsShaderInside(ss.str() );
}

bool AssetsManager::IsShaderInside(const std::string& vert, const std::string& frag, const std::string &geom)
{
    std::stringstream ss;
    ss << vert << " " << frag;

    if(geom != "")
        ss << " " << geom;

    return IsShaderInside(ss.str() );
}

int AssetsManager::GetShaderId(const std::string& n)
{
    return fShaders.GetIndex(n);
}

int AssetsManager::GetShaderId(const std::string& vert, const std::string& frag)
{
    std::string name = vert + " " + frag;

    return GetShaderId(name);
}

int AssetsManager::GetShaderId(const std::string& vert, const std::string& frag, const std::string& geo)
{
    std::string name = vert + " " + frag;

    if(geo != "")
        name += " " + geo;

    return GetShaderId(name);
}

int AssetsManager::AutoAddShader(const std::string& vert,
                                 const std::string& frag,
                                 const std::string& geom,
                                 const std::vector<std::string>& locations)
{
    int shaderId = -1;

    if(!this->IsShaderInside(vert, frag, geom) )
    {
        Shader* shader = new Shader(vert, frag, geom);

        for(const std::string& location : locations)
        {
            shader->AddAttribLocation(location);
        }

        shader->Load();

        shaderId = this->AddShader(shader);
    }
    else
    {
        shaderId = this->GetShaderId(vert, frag);
    }

    return shaderId;
}

}}
