/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/RandomTexture.hh"

namespace SV{ namespace TriD {

RandomTexture::RandomTexture() : Texture()
{
    fSize = 0;
}

RandomTexture::~RandomTexture()
{

}

bool RandomTexture::Load(GLenum textureUnit)
{
    QVector3D* pRandomData = new QVector3D[fSize];

    for (unsigned int i = 0 ; i < fSize ; i++) {
        pRandomData[i].setX( static_cast<float>(std::rand() ) / static_cast<float>(RAND_MAX) );
        pRandomData[i].setY( static_cast<float>(std::rand() ) / static_cast<float>(RAND_MAX) );
        pRandomData[i].setZ( static_cast<float>(std::rand() ) / static_cast<float>(RAND_MAX) );
    }

    // Delete former texture if any
    if(glIsTexture(fId)==GL_TRUE) glDeleteTextures(1, &fId);

    // Id generation
    glGenTextures(1, &fId);

    glBindTexture(GL_TEXTURE_1D, fId);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, fSize, 0.0f, GL_RGB, GL_FLOAT, pRandomData);
    glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);

    delete[] pRandomData;

    return true;
}

void RandomTexture::Bind(GLenum textureUnit)
{
    glActiveTexture(textureUnit);
    glBindTexture(GL_TEXTURE_1D, fId);
}

void RandomTexture::Unbind()
{
    glBindTexture(GL_TEXTURE_1D, 0);
}

}}
