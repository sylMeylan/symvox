/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/BillboardObject.hh"

namespace SV{ namespace TriD {

BillboardObject::BillboardObject(const std::string& texturePath, AssetsManager *assetsManager)
    : VObject(assetsManager)
{
    fTexturePath = texturePath;

    fTextureId = assetsManager->AutoAddTexture(fTexturePath);

    AddMeshEntry();

    fShaderId = 0; // To avoid auto generation of shader
}

BillboardObject::~BillboardObject()
{

}

void BillboardObject::Load()
{
    initializeOpenGLFunctions();

    // **************************************
    // Texture and mesh
    // **************************************

    fpAssetsManager->GetTexture(fTextureId)->Load();

    SV::TriD::MeshEntry& meshEntry = *fMeshEntryVect.at(0);

    SV::TriD::VAO& vao = meshEntry.fVao;
    vao.Init();

    GetMeshEntry().fObjBuffGPU.SetVAO(&vao);

    /*
    std::vector<float> positions = CreatePositionBuffer();

    SV::TriD::CPUBuffer<float> positionsBuffCPU("CPU_billboard_positions");
    positionsBuffCPU.AddData("positions", positions, 3);
    meshEntry.fObjBuffGPU.SetVAO(&vao);
    meshEntry.fObjBuffGPU.SendToGPU(positionsBuffCPU);
    */

    // **************************************
    // Shader
    // **************************************

    SV::TriD::Shader* shader = new SV::TriD::Shader(":/SymVox/3D/Shaders/billboard_model.vert",
                                                      ":/SymVox/3D/Shaders/billboard_model.frag",
                                                      ":/SymVox/3D/Shaders/billboard_model.geom"
                                                    );

    //SV::TriD::Shader* shader = new SV::TriD::Shader("./billboard_model.vert",
    //                                                  "./billboard_model.frag",
    //                                                  "./billboard_model.geom");

    // No need to set this if we use layout in shaders
    //
    //shader->AddAttribLocation("in_Model");
    //shader->AddAttribLocation("Position");
    //shader->AddAttribLocation("Color");



    bool alreadyHasShader = fpAssetsManager->IsShaderInside(shader->GetVertSource(),
                                                            shader->GetFragSource(),
                                                            shader->GetGeoSource() );

    if(alreadyHasShader)
    {

        fShaderId = fpAssetsManager->GetShaderId(shader->GetVertSource(),
                                                 shader->GetFragSource(),
                                                 shader->GetGeoSource() );
    }
    else
    {
        shader->Load();
        fShaderId = fpAssetsManager->AddShader(shader);
    }

    if(alreadyHasShader)
        delete shader;


    // Create a dummy mesh entry to give it a shader ID.
    // This ID will be detected during render as a custom shader.
    // Thus, no automatic data transfer will occur.
    GetMeshEntry().fShaderId = fShaderId;
}

void BillboardObject::Display(const QMatrix4x4 &/*model*/,
                        const QMatrix4x4 &view,
                        const QMatrix4x4 &proj,
                        const QVector3D &cameraPos,
                        LightManager &/*lightManager*/
                        )
{
    SV::TriD::Shader* shader = fpAssetsManager->GetShader(fShaderId);
    SV::TriD::Texture* texture = fpAssetsManager->GetTexture(fTextureId);
    SV::TriD::MeshEntry& meshEntry = *fMeshEntryVect.at(0);
    SV::TriD::VAO& vao = meshEntry.fVao;
    SV::TriD::GPUObj3DBuffer& objBuffer = GetMeshEntry().fObjBuffGPU; //fGPUDataBuffer;

    int numOfBillboards = fpInstanceBuffer->GetData(0).GetNumOfValues()
            / fpInstanceBuffer->GetData(0).GetSetSize();

    objBuffer.SendToGPU(*fpInstanceBuffer);

    shader->Use();

    vao.Bind();

    texture->Bind(GL_TEXTURE0);

    shader->Send4fv("gP", proj);
    shader->Send4fv("gV", view);
    shader->Send3f("gCameraPos", cameraPos.x(), cameraPos.y(), cameraPos.z() );
    shader->Send1i("gColorMap", 0 );
    shader->Send1f("gBillboardSize", 1 );

    objBuffer.GetVbo()->Bind();

    glDrawArrays(GL_POINTS, 0, numOfBillboards);

    objBuffer.GetVbo()->Unbind();
    texture->Unbind();

    vao.Unbind();

    shader->Unuse();
}

}}
