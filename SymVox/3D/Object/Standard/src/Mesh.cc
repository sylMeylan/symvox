/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Mesh.hh"

namespace SV{ namespace TriD {

Vertex::Vertex(const QVector3D &position,
               const QVector3D &normal,
               const QColor &color,
               const QVector2D &text_coord):
    fPosition(position),
    fNormal(normal),
    fColor(color),
    fTextCoord(text_coord)
{

}

Vertex::Vertex(const Vertex& to_copy):
    fPosition(to_copy.Position()),
    fNormal(to_copy.Normal()),
    fColor(to_copy.Color()),
    fTextCoord(to_copy.TextCoord())
{

}

Vertex::~Vertex()
{

}

Mesh::Mesh():
    nbFaces(0),
    nbVertices(0)
{

}

Mesh::~Mesh()
{

}

void Mesh::RemoveData()
{
    fPosition.clear();
    fNormal.clear();
    fColor.clear();
    fTexCoord.clear();
    fConnectivity.clear();

    fPosition.shrink_to_fit();
    fNormal.shrink_to_fit();
    fColor.shrink_to_fit();
    fTexCoord.shrink_to_fit();
    fConnectivity.shrink_to_fit();
}

VertID Mesh::AddVertex(const QVector3D& position, const QVector3D& normal, const QColor& color, const QVector2D& text_coord)
{
    fPosition.push_back(position[0]);
    fPosition.push_back(position[1]);
    fPosition.push_back(position[2]);

    fNormal.push_back(normal[0]);
    fNormal.push_back(normal[1]);
    fNormal.push_back(normal[2]);

    fColor.push_back(color.redF());
    fColor.push_back(color.greenF());
    fColor.push_back(color.blueF());
    fColor.push_back(color.alphaF());

    fTexCoord.push_back(text_coord[0]);
    fTexCoord.push_back(text_coord[1]);

    return nbVertices++;
}

VertID Mesh::AddVertex2(const QVector3D& position, const QVector3D& normal, const QVector2D& text_coord)
{
   return AddVertex(position, normal, QColor(0.5,0.5,0.5), text_coord);
}

FaceID Mesh::AddFace(const VertID id0, const VertID id1, const VertID id2)
{
    fConnectivity.push_back(id0);
    fConnectivity.push_back(id1);
    fConnectivity.push_back(id2);

    return nbFaces++;
}

void Mesh::ComputeNormals()
{
    std::vector<QVector3D> normals = GetVerticesNormals();
    for(VertID i = 0; i < nbVertices; i++)
    {
        for(uint j = 0; j < 3; j++)
            fNormal[i*3+j] = normals[i][j];
    }
}

void Mesh::InvertNormals()
{
    for(VertID vid = 0; vid < nbVertices; vid++)
    {
        QVector3D normal = Normal(vid);
        normal *= -1;
        SetVertNormal(vid, normal);
    }
}

void Mesh::InvertFace(const FaceID fid)
{
    VertID temp = fConnectivity[fid*3 + 2];
    fConnectivity[fid*3 + 2] = fConnectivity[fid*3 + 1];
    fConnectivity[fid*3 + 1] = temp;
}

void Mesh::InvertFaces()
{
    for(FaceID fid = 0; fid < FacesCount(); fid++)
    {
        InvertFace(fid);
    }
}

std::vector<QVector3D> Mesh::GetFacesNormals() const
{
    std::vector<QVector3D> output;
    output.reserve(FacesCount());


    for(FaceID i = 0; i  < FacesCount(); i++)
    {
        if(GetFace(i).size() != 3)
        {
            std::cerr<<"***************** Fatal Error *****************"<<std::endl;
            std::cerr<<"Mesh::GetFacesNormals: Non triangular face (id = " << i << ")."<<std::endl;
            std::cerr<<"*********************************************"<<std::endl;
            //            exit(EXIT_FAILURE);
            output.push_back(QVector3D(0,0,0));
            continue;
        }

        QVector3D p0 = GetVertex(GetFace(i)[0]).Position();
        QVector3D p1 = GetVertex(GetFace(i)[1]).Position();
        QVector3D p2 = GetVertex(GetFace(i)[2]).Position();

        QVector3D normal = QVector3D::crossProduct(p1-p0, p2-p0);

        try
        {
            normal.normalize();
        }
        catch(std::exception e)
        {
            std::stringstream ss;
            ss << "Non triangular face (id = " << i << ")." << std::endl;

            Utils::Exception(Utils::ExceptionType::Warning, "Mesh::GetFacesNormals", ss.str() );

            normal = QVector3D(0,0,0);
        }

        output.push_back(normal);
    }

    return output;
}

std::vector<std::vector<FaceID> > Mesh::GetVerticesFaces() const
{
    std::vector<std::vector<FaceID> > output;
    output.reserve(nbVertices);

    for(VertID i = 0; i < nbVertices; i++)
    {
        std::vector<FaceID> temp;
        output.push_back(temp);
    }

    for(FaceID i = 0; i < FacesCount(); i++)
    {
        for(unsigned int j = 0; j < GetFace(i).size(); j++)
        {
            output[GetFace(i)[j]].push_back(i);
        }
    }

    return output;
}

std::vector<QVector3D> Mesh::GetVerticesNormals() const
{
    std::vector<QVector3D> normal_polygon = GetFacesNormals();
    std::vector<std::vector<FaceID> > vertices_faces = GetVerticesFaces();

    std::vector<QVector3D> output;
    output.reserve(nbVertices);

    for(VertID i = 0; i < nbVertices; i++)
    {
        QVector3D temp_normal(0,0,0);

        for(unsigned int j = 0; j < vertices_faces[i].size(); j++)
        {
            temp_normal += normal_polygon[vertices_faces[i][j]];
        }

        output.push_back(temp_normal.normalized());
    }

    return output;
}

void Mesh::FillColor(const QColor& color)
{
    for(VertID vid = 0; vid < nbVertices; vid++)
    {
        SetVertColor(vid, color);
    }
}

void Mesh::ColorsFromNormals()
{
    for(VertID vid = 0; vid < nbVertices; vid++)
    {
        QVector3D normal = Normal(vid);
        QColor color = QColor::fromRgb(int(normal[0] * normal[0] * 255.0),
                int(normal[1] * normal[1] * 255.0),
                int(normal[2] * normal[2] * 255.0));
        SetVertColor(vid, color);
    }
}

void Mesh::ColorsFromPositions()
{
    for(VertID vid = 0; vid < nbVertices; vid++)
    {
        QVector3D position = Position(vid);
        QColor color = QColor::fromRgb(int(position[0] * position[0] * 255.0),
                int(position[1] * position[1] * 255.0),
                int(position[2] * position[2] * 255.0));
        SetVertColor(vid, color);
    }
}

void Mesh::SetVertColor(const VertID vid, const QColor& color)
{
    fColor[vid*4+0] = color.redF();
    fColor[vid*4+1] = color.greenF();
    fColor[vid*4+2] = color.blueF();
    fColor[vid*4+3] = color.alphaF();
}

void Mesh::SetFaceColor(const FaceID id, const QColor& color)
{
    VertID vid;

    for(uint j=0; j < 3; j++)
    {
        vid = fConnectivity[id*3 + j];
        SetVertColor(vid, color);
    }
}

void Mesh::SetVertPosition(const VertID vid, const QVector3D& pos)
{
    for(uint j=0; j < 3; j++)
    {
        fPosition[3*vid+j] = pos[j];
    }
}

void Mesh::SetVertNormal  (const VertID vid, const QVector3D& nor)
{
    for(uint j=0; j < 2; j++)
    {
        fNormal[3*vid+j] = nor[j];
    }
}

void Mesh::SetVertTexCoord(const VertID vid, const QVector2D& txc)
{
    for(uint j=0; j < 2; j++)
    {
        fTexCoord[2*vid+j] = txc[j];
    }
}

const Vertex Mesh::GetVertex(const VertID id) const
{
    return Vertex(Position(id), Normal(id), Color(id), TexCoord(id));
}

const Face Mesh::GetFace(const FaceID id) const
{
    return Face(fConnectivity[id*3], fConnectivity[id*3+1], fConnectivity[id*3+2]);
}

void Mesh::DisplayVertices() const
{
    for(unsigned int i = 0; i < nbVertices; i++)
    {
        std::cout << i << "\t";
        for(unsigned int j = 0; j < 3; j++)
        {
            std::cout << GetVertex(i).Position()[j] << "\t";
        }
        std::cout << std::endl;
    }
}

void Mesh::DisplayConnectivity() const
{
    for(unsigned int i = 0; i < FacesCount(); i++)
    {
        std::cout << i << "\t";
        for(unsigned int j = 0; j < GetFace(i).size(); j++)
        {
            std::cout << GetFace(i)[j] << "\t";
        }
        std::cout << std::endl;
    }
}

BoundingBox Mesh::ComputeBB() const
{
    if (nbVertices == 0)
    {
        std::cerr<<"***************** Fatal Error *****************"<<std::endl;
        std::cerr<<"Mesh::ComputeBB: empty mesh."<<std::endl;
        std::cerr<<"*********************************************"<<std::endl;
        exit(EXIT_FAILURE);
    }

    BoundingBox output;
    output.fMin = GetVertex(0).Position();
    output.fMax = GetVertex(0).Position();

    for(VertID vid=0; vid < nbVertices; vid++)
    {
        output.fMin[0] = std::min(output.fMin[0], GetVertex(vid).Position()[0]);
        output.fMin[1] = std::min(output.fMin[1], GetVertex(vid).Position()[1]);
        output.fMin[2] = std::min(output.fMin[2], GetVertex(vid).Position()[2]);

        output.fMax[0] = std::max(output.fMax[0], GetVertex(vid).Position()[0]);
        output.fMax[1] = std::max(output.fMax[1], GetVertex(vid).Position()[1]);
        output.fMax[2] = std::max(output.fMax[2], GetVertex(vid).Position()[2]);
    }

    return output;
}

void Mesh::ApplyTransform(const QMatrix4x4 &mat)
{
    for(VertID vid=0; vid < nbVertices; vid++)
    {
        QVector3D pos = Position(vid);
        pos = (mat * QVector4D(pos, 1.0)).toVector3D();
        SetVertPosition(vid, pos);

        // Thought this normal tranform would be ok but not...

        QVector3D normal = Normal(vid);
//        normal = QVector3D(mat * QVector4D(normal, 1.0));

//        QVector3D zeroRef(0,0,0);
//        zeroRef = QVector3D(mat * QVector4D(zeroRef, 1.0));

//        normal = (normal - zeroRef);


        QMatrix4x4 normalMat = mat.inverted().transposed();

        normal = QVector3D(normalMat * QVector4D(normal, 0.0));


        SetVertNormal(vid, normal);
    }

    ComputeNormals();
}

void Mesh::Scale(const double coeff)
{
    for(VertID vid=0; vid < nbVertices; vid++)
    {
        QVector3D pos = Position(vid);
        pos *= coeff;
        SetVertPosition(vid, pos);
    }
}

void Mesh::Scale(const QVector3D& vect)
{
    for(VertID vid=0; vid < nbVertices; vid++)
    {
        QVector3D pos = Position(vid);

        pos[0] *= vect[0];
        pos[1] *= vect[1];
        pos[2] *= vect[2];

        SetVertPosition(vid, pos);
    }
}

void Mesh::Translate(const QVector3D& vect)
{
    for(VertID vid=0; vid < nbVertices; vid++)
    {
        QVector3D pos = Position(vid);

        pos += vect;

        SetVertPosition(vid, pos);
    }
}


void Mesh::AutoScale()
{
    BoundingBox bb = ComputeBB();

    Translate(-(bb.fMax + bb.fMin)*0.5);

    float maxWidth = std::max(bb.fMax[0]-bb.fMin[0], bb.fMax[1]-bb.fMin[1]);
    maxWidth = std::max(maxWidth, bb.fMax[2]-bb.fMin[2]);
    Scale(2.0 / maxWidth);
}


void Mesh::add_mesh(const Mesh& m)
{
    unsigned int nb_vert = this->nbVertices;

    fPosition.insert(fPosition.end(), m.Positions().begin(), m.Positions().end());
    fNormal.insert(fNormal.end(), m.Normals().begin(), m.Normals().end());
    fColor.insert(fColor.end(), m.Colors().begin(), m.Colors().end());
    fTexCoord.insert(fTexCoord.end(), m.TexCoords().begin(), m.TexCoords().end());

    this->nbVertices += m.nbVertices;

    for(unsigned int i = 0; i < m.nbFaces; i++)
    {
        Face f = m.GetFace(i);
        this->AddFace(f.ID0() + nb_vert, f.ID1() + nb_vert, f.ID2() + nb_vert);
    }
}

std::vector<std::string>& split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim = ' ')
{
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

}}
