/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Texture.hh"

namespace SV{ namespace TriD {

Texture::Texture() :
    fId(0),
    fImageFile(""),
    fWidth(0),
    fHeight(0),
    fFormat(GL_RGBA),
    fInternFormat(GL_RGBA),
    fEmptyTexture(false),
    fIsLoaded(false),
    fIsMirrored(false),
    fFilter(Filter::basic)
{
    this->initializeOpenGLFunctions();
}

Texture::Texture(std::string imageFile, bool mirrored) :
    fId(0), fImageFile(imageFile), fWidth(0), fHeight(0), fFormat(GL_RGBA),
    fInternFormat(GL_RGBA), fEmptyTexture(false), fIsLoaded(false), fIsMirrored(mirrored),
    fFilter(Filter::basic)
{
    this->initializeOpenGLFunctions();
}

Texture::Texture(int width, int height, GLenum format, GLenum internFormat, bool emptyTexture) :
    fId(0), fImageFile(""), fWidth(width), fHeight(height), fFormat(format),
    fInternFormat(internFormat), fEmptyTexture(emptyTexture), fIsLoaded(false), fIsMirrored(false),
    fFilter(Filter::basic)
{
    this->initializeOpenGLFunctions();
}

Texture::~Texture()
{
    // Delete the texture
    if(glIsTexture(fId)==GL_TRUE) glDeleteTextures(1, &fId);
}

Texture::Texture(const Texture& textureToBeCopied)
{
    fImageFile = textureToBeCopied.fImageFile;

    fWidth = textureToBeCopied.fWidth;
    fHeight = textureToBeCopied.fHeight;
    fFormat = textureToBeCopied.fFormat;
    fInternFormat = textureToBeCopied.fInternFormat;
    fEmptyTexture = textureToBeCopied.fEmptyTexture;
    fIsLoaded = textureToBeCopied.fIsLoaded;
    fIsMirrored = textureToBeCopied.fIsMirrored;
    fFilter = textureToBeCopied.fFilter;

    // We need to add glIsTexture(textureToBeCopied.fId) because we don't want that a non loaded copied texture
    // tries to load itself within the copy method
    if(fEmptyTexture && glIsTexture(textureToBeCopied.fId) == GL_TRUE)
        LoadEmptyTexture();
    else if(glIsTexture(textureToBeCopied.fId) == GL_TRUE)
        Load();

    this->initializeOpenGLFunctions();
}

Texture& Texture::operator=(const Texture& textureToBeCopied)
{
    fImageFile = textureToBeCopied.fImageFile;

    fWidth = textureToBeCopied.fWidth;
    fHeight = textureToBeCopied.fHeight;
    fFormat = textureToBeCopied.fFormat;
    fInternFormat = textureToBeCopied.fInternFormat;
    fEmptyTexture = textureToBeCopied.fEmptyTexture;
    fIsLoaded = textureToBeCopied.fIsLoaded;
    fIsMirrored = textureToBeCopied.fIsMirrored;
    fFilter = textureToBeCopied.fFilter;

    // We need to add glIsTexture(textureToBeCopied.fId) because we don't want that a non loaded copied texture
    // tries to load itself within the copy method
    if(fEmptyTexture && glIsTexture(textureToBeCopied.fId) == GL_TRUE)
        LoadEmptyTexture();
    else if(glIsTexture(textureToBeCopied.fId) == GL_TRUE)
        Load();

    this->initializeOpenGLFunctions();

    return *this;
}

bool Texture::Load(GLenum textureUnit)
{
    // Load the picture in a QImage
    QImage image(QString(fImageFile.c_str() ) );

    // Should convert the image and mirror it
    image = image.convertToFormat(QImage::Format_RGBA8888);

    if(image.isNull())
    {
        std::stringstream ss;
        ss <<  "Error loading the image "<<fImageFile.c_str();

        Utils::Exception(Utils::ExceptionType::Warning, "Texture::Load", ss.str() );

        return false;
    }

    // Inverse picture
    if(fIsMirrored)
        image = image.mirrored();

    fWidth = image.width();
    fHeight = image.height();

    // Delete former texture if any
    if(glIsTexture(fId)==GL_TRUE) glDeleteTextures(1, &fId);

    // Id generation
    glGenTextures(1, &fId);

    // Lock
    glActiveTexture(textureUnit);
    glBindTexture(GL_TEXTURE_2D, fId);

    // Copy the pixels
    glTexImage2D(GL_TEXTURE_2D, 0, fInternFormat,
                 fWidth, fHeight, 0,
                 fFormat, GL_UNSIGNED_BYTE, image.bits() );

    if(fFilter == Filter::basic)
    {
        // Basic filters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    else if(fFilter == Filter::mimap)
    {
        // MipMap
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else if(fFilter == Filter::trilinear)
    {
        // Nice trilinear filtering.
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "Texture::Load",
                         "The chosen filter is not registered here.");
    }

    // Unlock
    glBindTexture(GL_TEXTURE_2D, 0);

    fIsLoaded = true;

    return true;
}

void Texture::Bind(GLenum textureUnit)
{
    glActiveTexture(textureUnit);
    glBindTexture(GL_TEXTURE_2D, fId);
}

void Texture::Unbind()
{
    glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint Texture::GetId() const
{
    return fId;
}

bool Texture::SetImageFile(const std::string &imageFile)
{
    fImageFile = imageFile;

    return true;
}

void Texture::LoadEmptyTexture()
{
    // Check if there is a former texture, if yes then delete it
    if(glIsTexture((fId) == GL_TRUE)) glDeleteTextures(1, &fId);

    // Id generation
    glGenTextures(1, &fId);

    // Lock
    glBindTexture(GL_TEXTURE_2D, fId);

        // Define texture charateristics
        glTexImage2D(GL_TEXTURE_2D, 0, fInternFormat,
                     fWidth, fHeight, 0, fFormat, GL_UNSIGNED_BYTE, 0);

        // Apply filters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // Unlock
    glBindTexture(GL_TEXTURE_2D, 0);

    fIsLoaded = true;
}

}}
