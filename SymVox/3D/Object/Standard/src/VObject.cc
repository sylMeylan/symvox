/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/VObject.hh"

namespace SV{ namespace TriD {

VObject::VObject()
{
    fpAssetsManager = nullptr;
    fpInstanceBuffer = nullptr;
    fName = "noName";
    fAutoId = -1;
    fIsLoaded = false;
}

VObject::VObject(AssetsManager* assetsManager)
{
    fpAssetsManager = assetsManager;
    fpInstanceBuffer = nullptr;
    fName = "noName";
    fAutoId = -1;
    fIsLoaded = false;
}

VObject::~VObject()
{
    for(MeshEntry* p : fMeshEntryVect)
    {
        delete p;
    }

    fMeshEntryVect.clear();
}

void VObject::SetMesh(const Mesh& mesh, int index)
{
    fMeshEntryVect.at(index)->fMesh = mesh;
}

Mesh& VObject::GetMesh(int index)
{
    return fMeshEntryVect.at(index)->fMesh;
}

const Mesh& VObject::GetMesh(int index) const
{
    return fMeshEntryVect.at(index)->fMesh;
}

void VObject::SetShaderId(int id, int index)
{
    fMeshEntryVect.at(index)->fShaderId=id;
}

int VObject::GetShaderID(int index) const
{
    return static_cast<int>(fMeshEntryVect.at(index)->fShaderId);
}

void VObject::SetTextureDiffuseId(int id, int index)
{
    fMeshEntryVect.at(index)->fTextureDiffuseId = id;
}

int VObject::GetTextureDiffuseId(int index) const
{
    return static_cast<int>(fMeshEntryVect.at(index)->fTextureDiffuseId);
}

void VObject::SetTextureSpecularId(int id, int index)
{
    fMeshEntryVect.at(index)->fTextureSpecularId = id;
}

int VObject::GetTextureSpecularId(int index) const
{
    return static_cast<int>(fMeshEntryVect.at(index)->fTextureSpecularId);
}

void VObject::SetTextureNormalId(int id, int index)
{
    fMeshEntryVect.at(index)->fTextureNormalId = id;
}

int VObject::GetTextureNormalId(int index) const
{
    return static_cast<int>(fMeshEntryVect.at(index)->fTextureNormalId);
}

void VObject::SetTextureReflectionId(int id, int index)
{
    fMeshEntryVect.at(index)->fTextureReflectionId = id;
}

int VObject::GetTextureReflectionId(int index) const
{
    return static_cast<int>(fMeshEntryVect.at(index)->fTextureReflectionId);
}

void VObject::SetTextureEmissionId(int id, int index)
{
    fMeshEntryVect.at(index)->fTextureEmissionId = id;
}

int VObject::GetTextureEmissionId(int index) const
{
    return fMeshEntryVect.at(index)->fTextureEmissionId;
}

void VObject::SetUpdateInstanceBuffer(bool b)
{
    for(size_t i=0, ie=fMeshEntryVect.size(); i<ie; i++)
    {
        fMeshEntryVect.at(i)->fUpdateInstanceBuffer = b;
    }
}

bool VObject::GetIsUpdateInstanceBuffer(int index) const
{
    return fMeshEntryVect.at(index)->fUpdateInstanceBuffer;
}

MeshEntry& VObject::GetMeshEntry(int index)
{
    return *fMeshEntryVect.at(index);
}

const MeshEntry&VObject::GetMeshEntry(int index) const
{
    return *fMeshEntryVect.at(index);
}

int VObject::GetNumOfMeshEntries() const
{
    return static_cast<int>(fMeshEntryVect.size() );
}

void VObject::AddMeshEntry()
{
    fMeshEntryVect.push_back(new MeshEntry() );
}

void VObject::RemoveMeshEntry(int index)
{
    delete fMeshEntryVect.at(index);

    fMeshEntryVect.erase(fMeshEntryVect.begin()+index);
    fMeshEntryVect.shrink_to_fit();
}

void VObject::RemoveAllMeshEntries()
{
    for(auto& entry : fMeshEntryVect)
    {
        if(entry != nullptr)
            delete entry;
    }

    fMeshEntryVect.clear();
}

}}
