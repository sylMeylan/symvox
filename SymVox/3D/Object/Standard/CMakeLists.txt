#add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Standard")

add_sources(
    VObject.cc
    Object.cc
    BillboardObject.cc
    Mesh.cc
    AssetsManager.cc
    Shader.cc
    Texture.cc
    RandomTexture.cc
)

add_headers(
    VObject.hh
    Object.hh
    BillboardObject.hh
    Mesh.hh
    AssetsManager.hh
    Shader.hh
    Texture.hh
    RandomTexture.hh
)

add_uis()
