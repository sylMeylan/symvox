/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_OBJ_SPOTLIGHT_HH
#define TRID_OBJ_SPOTLIGHT_HH

#include "PointLight.hh"

namespace SV{ namespace TriD {

/*!
 * \brief The SpotLight class
 * A light beam with a source point, attenuation and direction.
 */
class SpotLight : public PointLight
{
public:
    SpotLight();
    ~SpotLight();

    void SetSpot(const QVector3D& direction, float cutOff, float outerCutOff);

    void SetDirection(const QVector3D& direction) {fDirection=direction;}
    void SetCutOff(float cutOff) {fCutOff=cutOff;}
    void SetOuterCutOff(float outerCutOff) {fOuterCutOff=outerCutOff;}

    const QVector3D& GetDirection() {return fDirection;}
    float GetCutOff() {return fCutOff;}
    float GetOuterCutOff() {return fOuterCutOff;}

    virtual void SendLightToShader(Shader* shader, const std::string& prefix="") override;

private:
    QVector3D fDirection;

    float fCutOff;
    float fOuterCutOff;
};

// θ, θ in degrees, ϕ (inner cutoff), ϕ in degrees, γ (outer cutoff), γ (outer cutoff), ϵ, I
//
// 0.87 	30 	0.91 	25 	0.82 	35 	0.91 - 0.82 = 0.09 	0.87 - 0.82 / 0.09 = 0.56
// 0.9 	26 	0.91 	25 	0.82 	35 	0.91 - 0.82 = 0.09 	0.9 - 0.82 / 0.09 = 0.89
// 0.97 	14 	0.91 	25 	0.82 	35 	0.91 - 0.82 = 0.09 	0.97 - 0.82 / 0.09 = 1.67
// 0.83 	34 	0.91 	25 	0.82 	35 	0.91 - 0.82 = 0.09 	0.83 - 0.82 / 0.09 = 0.11
// 0.64 	50 	0.91 	25 	0.82 	35 	0.91 - 0.82 = 0.09 	0.64 - 0.82 / 0.09 = -2.0
// 0.966 	15 	0.9978 	12.5 	0.953 	17.5 	0.966 - 0.953 = 0.0448 	0.966 - 0.953 / 0.0448 = 0.29

}}

#endif // TRID_OBJ_SPOTLIGHT_HH
