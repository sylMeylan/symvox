/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_OBJ_LIGHT_HH
#define TRID_OBJ_LIGHT_HH

#include <QVector3D>
#include "SymVox/3D/Object/Standard/include/Shader.hh"

namespace SV{ namespace TriD {

/*!
 * \brief The Light class
 * A class to put together some base characteristics of a light source
 */
class Light
{
public:
    Light();
    Light(const QVector3D& ambient, const QVector3D& diffuse, const QVector3D specular);
    virtual ~Light();

    void SetLight(const QVector3D& ambient, const QVector3D& diffuse, const QVector3D& specular);
    void SetAmbient(const QVector3D& ambient) {fAmbient=ambient;}
    void SetDiffuse(const QVector3D& diffuse) {fDiffuse=diffuse;}
    void SetSpecular(const QVector3D& specular) {fSpecular=specular;}

    const QVector3D& GetAmbient() {return fAmbient;}
    const QVector3D& GetDiffuse() {return fDiffuse;}
    const QVector3D& GetSpecular() {return fSpecular;}

    virtual void SendLightToShader(Shader* shader, const std::string& prefix="");

protected:
    QVector3D fAmbient;
    QVector3D fDiffuse;
    QVector3D fSpecular;
};

}}

#endif // TRID_OBJ_LIGHT_HH
