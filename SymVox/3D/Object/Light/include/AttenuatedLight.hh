/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_OBJ_ATTENUATEDLIGHT_HH
#define TRID_OBJ_ATTENUATEDLIGHT_HH

#include "Light.hh"

namespace SV{ namespace TriD {

/*!
 * \brief The AttenuatedLight class
 * Simple light but with attenuation over distance
 */
class AttenuatedLight : public Light
{
public:
    AttenuatedLight();
    virtual ~AttenuatedLight();

    void SetAttenuation(float constant, float linear, float quadratic);

    void SetConstant(float constant) {fConstant=constant;}
    void SetLinear(float linear) {fLinear=linear;}
    void SetQuadratic(float quadratic) {fQuadratic=quadratic;}

    float GetConstant() {return fConstant;}
    float GetLinear() {return fLinear;}
    float GetQuadratic() {return fQuadratic;}

    virtual void SendLightToShader(Shader* shader, const std::string& prefix="") override;

protected:
    float fConstant;
    float fLinear;
    float fQuadratic;
};

// Ogre3D wiki
// Distance 	Constant 	Linear 	Quadratic
// 7 	1.0 	0.7 	1.8
// 13 	1.0 	0.35 	0.44
// 20 	1.0 	0.22 	0.20
// 32 	1.0 	0.14 	0.07
// 50 	1.0 	0.09 	0.032
// 65 	1.0 	0.07 	0.017
// 100 	1.0 	0.045 	0.0075
// 160 	1.0 	0.027 	0.0028
// 200 	1.0 	0.022 	0.0019
// 325 	1.0 	0.014 	0.0007
// 600 	1.0 	0.007 	0.0002
// 3250 	1.0 	0.0014 	0.000007

}}

#endif // TRID_OBJ_ATTENUATED_LIGHT_HH
