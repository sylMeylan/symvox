/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/LightManager.hh"

namespace SV{ namespace TriD {

LightManager::LightManager()
{

}

LightManager::~LightManager()
{
    RemoveAllLights();
}

void LightManager::SendLightsToShader(Shader* shader)
{
    // **************
    // Dir lights
    // **************

    std::map<int, DirectionalLight*>& dirMap = fDirLights.GetObjMap();

    int numOfLights = dirMap.size();

    if(numOfLights>1)
    {
        std::stringstream msg;
        msg << "There should be only one direction light with this LightManager implementation.\nnumOfLights=";
        msg << numOfLights;
        Utils::Exception(Utils::ExceptionType::Fatal,
                         "LightManager::SendLightsToShader",
                         msg.str() );
    }

    // Loop on every directional light

    int index = 0;
    for(auto& l : dirMap)
    {
        DirectionalLight* light = l.second;

        light->SendLightToShader(shader, "dirLight.");

        index++;
    }

    // **************
    // Point lights
    // **************

    std::map<int, PointLight*>& pointMap = fPointLights.GetObjMap();

    numOfLights = pointMap.size();

    shader->Send1i("numPointLights", numOfLights);

    // Loop on every point light
    index = 0;
    for(auto& l : pointMap)
    {
        PointLight* light = l.second;

        std::stringstream ss;
        ss << "pointLights[" << index << "].";

        light->SendLightToShader(shader, ss.str() );

        index++;
    }

    // **************
    // Spot lights
    // **************

    std::map<int, SpotLight*>& spotMap = fSpotLights.GetObjMap();

    numOfLights = spotMap.size();

    shader->Send1i("numSpotLights", numOfLights);

    // Loop on every spot light
    index = 0;
    for(auto& l : spotMap)
    {
        SpotLight* light = l.second;

        std::stringstream ss;
        ss << "spotLights[" << index << "].";

        light->SendLightToShader(shader, ss.str() );

        index++;
    }
}

void LightManager::AddDirLight(const std::string& name, DirectionalLight* light)
{
    fDirLights.Add(light, name);
}

void LightManager::RemoveDirLight(const std::string& name)
{
    fDirLights.Remove(name);
}

void LightManager::AddPointLight(const std::string& name, PointLight* light)
{
    fPointLights.Add(light, name);
}

void LightManager::RemovePointLight(const std::string& name)
{
    fPointLights.Remove(name);
}

void LightManager::AddSpotLight(const std::string& name, SpotLight* light)
{
    fSpotLights.Add(light, name);
}

void LightManager::RemoveSpotLight(const std::string& name)
{
    fSpotLights.Remove(name);
}

void LightManager::RemoveAllLights()
{
    fDirLights.RemoveAll();
    fSpotLights.RemoveAll();
    fPointLights.RemoveAll();
}

}}
