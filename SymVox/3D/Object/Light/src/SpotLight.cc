/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/SpotLight.hh"
#include <cmath>

namespace SV{ namespace TriD {

SpotLight::SpotLight() : PointLight()
{
    fDirection = QVector3D(0.0f, 0.0f, 1.0f);

    fCutOff = std::cos(12.5f/180*M_PI);
    fOuterCutOff = std::cos(15.0f/180*M_PI);
}

SpotLight::~SpotLight()
{

}

void SpotLight::SetSpot(const QVector3D& direction, float cutOff, float outerCutOff)
{
    SetDirection(direction);
    SetCutOff(cutOff);
    SetOuterCutOff(outerCutOff);
}

void SpotLight::SendLightToShader(Shader* shader, const std::string& prefix)
{
    PointLight::SendLightToShader(shader, prefix);

    shader->Send3f(prefix+"direction", fDirection.x(), fDirection.y(), fDirection.z() );
    shader->Send1f(prefix+"cutOff", fCutOff);
    shader->Send1f(prefix+"outerCutOff", fOuterCutOff);
}

}}
