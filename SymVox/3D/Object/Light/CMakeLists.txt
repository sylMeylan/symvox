#add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/Standard")

add_sources(
    LightManager.cc
    Light.cc
    AttenuatedLight.cc
    DirectionalLight.cc
    PointLight.cc
    SpotLight.cc
)

add_headers(
    LightManager.hh
    Light.hh
    AttenuatedLight.hh
    DirectionalLight.hh
    PointLight.hh
    SpotLight.hh
)

add_uis()
