/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/CubeMapTexture.hh"

namespace SV{ namespace TriD {

CubeMapTexture::CubeMapTexture(const std::string& posXFileName,
                               const std::string& negXFileName,
                               const std::string& posYFileName,
                               const std::string& negYFileName,
                               const std::string& posZFileName,
                               const std::string& negZFileName)
{
    fFileNames[0] = posXFileName;
    fFileNames[1] = negXFileName;
    fFileNames[2] = posYFileName;
    fFileNames[3] = negYFileName;
    fFileNames[4] = posZFileName;
    fFileNames[5] = negZFileName;

    fTextureObj = 0;

    this->initializeOpenGLFunctions();
}

CubeMapTexture::~CubeMapTexture()
{
    if(fTextureObj != 0)
        glDeleteTextures(1, &fTextureObj);
}

bool CubeMapTexture::Load()
{
    glGenTextures(1, &fTextureObj);

    glBindTexture(GL_TEXTURE_CUBE_MAP, fTextureObj);

    // Loop on all the image files
    for(unsigned int i=0; i<6; i++)
    {
        // ***********
        // Load one image file
        // ***********

        const std::string& imageFile = fFileNames[i];

        // Load the picture in a QImage
        QImage image(QString(imageFile.c_str() ));

        // Should convert the image and mirror it
        image = image.convertToFormat(QImage::Format_RGBA8888);

        image = image.mirrored(true, false);

        if(image.isNull())
        {
            std::stringstream ss;
            ss <<  "Error loading the image "<<imageFile.c_str();

            Utils::Exception(Utils::ExceptionType::Warning, "Texture::Load", ss.str() );

            return false;
        }

        // ***********
        // Copy it into graphic memory
        // ***********

        // Copy the pixels
        glTexImage2D(fTypes[i], 0, GL_RGB,
                     image.width(), image.height(), 0,
                     GL_RGBA, GL_UNSIGNED_BYTE, image.bits());

        // Filters
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    }

    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return true;
}

void CubeMapTexture::Bind(GLenum textureUnit)
{
    glActiveTexture(textureUnit);
    glBindTexture(GL_TEXTURE_CUBE_MAP, fTextureObj);
}

void CubeMapTexture::Unbind()
{
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

}}
