/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/SkyBox.hh"

namespace SV{ namespace TriD {

SkyBox::SkyBox(GUI::Camera* camera,
               const std::string& posXFileName,
               const std::string& negXFileName,
               const std::string& posYFileName,
               const std::string& negYFileName,
               const std::string& posZFileName,
               const std::string& negZFileName)
    : Object(),
      fShader(":/SymVox/3D/Shaders/skybox.vert", ":/SymVox/3D/Shaders/skybox.frag"),
      fCubeMapTex(posXFileName, negXFileName, posYFileName, negYFileName,
                  posZFileName, negZFileName)
{
    // The cube should e big to avoid stuttering when moving far away
    // from the origin. Look for z-buffer non-linear precision for more information.
    SV::TriD::Cube cube(10000000);
    fpBox = cube.CreateObject(nullptr);

    fpCamera = camera;

    Load();
}

SkyBox::~SkyBox()
{
    if(fpBox != nullptr)
        delete fpBox;
}

void SkyBox::Load()
{
    // Check mesh is not null
    if(fpBox == nullptr)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "SkyBox::Load",
                         "fpBox pointer cannot be null");
    }

    this->initializeOpenGLFunctions();

    Mesh& mesh = fpBox->GetMesh();

    fConnectivitySize = mesh.Connectivity().size();

    CPUBuffer<float> obj3DBuffer("ObjectBuffer", GL_ARRAY_BUFFER, GL_STATIC_DRAW, GL_FLOAT);
    obj3DBuffer.AddData("Positions", mesh.Positions(), 3);
    obj3DBuffer.AddData("Colors", mesh.Colors(), 4); // 1
    obj3DBuffer.AddData("TextureCoord", mesh.TexCoords(), 2); //  2
    obj3DBuffer.AddData("Normals", mesh.Normals(), 3); // 3

    std::vector<float> f;
    f.push_back(1);
    obj3DBuffer.AddData("Model", f, 1); // 3

    fVao.Init();

    fObjBuffGPU.SetVAO(&fVao);
    fObjBuffGPU.SendToGPU(obj3DBuffer);

    CPUBuffer<unsigned int> indexBuffCPU("indexBuffer", GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW, GL_UNSIGNED_INT);
    indexBuffCPU.AddData("Index", mesh.Connectivity(), 0);

    fIndexBuffGPU.SetVAO(&fVao);
    fIndexBuffGPU.SendToGPU(indexBuffCPU);

    fCubeMapTex.Load();

    fShader.Load();
}

void SkyBox::Display(const QMatrix4x4& /*model*/, const QMatrix4x4& view, const QMatrix4x4& proj, const QVector3D& /*cameraPos*/, LightManager& /*lightManager*/)
{
    GLint OldCullFaceMode;
    glGetIntegerv(GL_CULL_FACE_MODE, &OldCullFaceMode);
    GLint OldDepthFuncMode;
    glGetIntegerv(GL_DEPTH_FUNC, &OldDepthFuncMode);

    glCullFace(GL_FRONT);
    glDepthFunc(GL_LEQUAL);

    fShader.Use();

    fVao.Bind();

    QMatrix4x4 mat;
    mat.setToIdentity();
    mat.translate(fpCamera->GetPosition() );

    fShader.Send4fv("cam_mat", mat);
    fShader.Send4fv("view", view);
    fShader.Send4fv("projection", proj);

    int localisation = glGetUniformLocation(fShader.GetProgramID(),"gCubeMaptexture");
    glUniform1i(localisation, GL_TEXTURE0);

    fIndexBuffGPU.GetVbo()->Bind();

    fCubeMapTex.Bind(GL_TEXTURE0);

    glDrawElements(GL_TRIANGLES,
                   fConnectivitySize,
                   GL_UNSIGNED_INT,
                   (void*) 0);

    fCubeMapTex.Unbind();

    fIndexBuffGPU.GetVbo()->Unbind();

    fVao.Unbind();

    fShader.Unuse();

    glCullFace(OldCullFaceMode);
    glDepthFunc(OldDepthFuncMode);
}

}}
