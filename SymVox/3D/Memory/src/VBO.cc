/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/VBO.hh"

namespace SV { namespace TriD {

VBO::VBO(GLenum type, GLenum draw) :
    fIsGenerated(false),
    fId(0),
    fType(type),
    fDraw(draw),
    fByteSize(0),
    fOffset(0)
{
    this->initializeOpenGLFunctions();
}

VBO::~VBO()
{
    if(fIsGenerated)
        Delete();
}

unsigned int VBO::Init()
{
    if(glIsBuffer(fId)==GL_TRUE)
        Delete();

    // Id generation
    glGenBuffers(1, &fId);

    fIsGenerated = true;

    return fId;
}

void VBO::Delete()
{
    if(glIsBuffer(fId)==GL_TRUE)
        glDeleteBuffers(1, &fId);

    fIsGenerated = false;

    fByteSize = 0;
    fOffset = 0;
}

void VBO::Bind()
{
    glBindBuffer(fType, fId);
}

void VBO::Unbind()
{
    glBindBuffer(fType, 0);
}

void VBO::Allocate(int byteSize)
{
    glBufferData(fType, byteSize, nullptr, fDraw);

    fByteSize = static_cast<unsigned int>(byteSize);
    fOffset = 0;
}

void VBO::Orphan()
{
    glBufferData(fType, fByteSize, nullptr, fDraw);

    //fByteSize = 0;
    fOffset = 0;
}

GLvoid* VBO::MapBufferRange(unsigned int offset, unsigned int byteSize, GLbitfield access)
{
    GLvoid* address = glMapBufferRange(fType, offset, byteSize, access);

    if(address==NULL)
    {
        std::cerr<<"******** Fatal Error *******"<<std::endl;
        std::cerr<<"VBO::MapBufferRange: glMapBufferRange failed."<<std::endl;
        std::cerr<<"offset: "<<offset<<" b"<<std::endl;
        std::cerr<<"byteSize: "<<byteSize<<" b"<<std::endl;
        std::cerr<<"glError: "<<this->glGetError()<<std::endl;
        std::cerr<<GL_OUT_OF_MEMORY<<" "<<GL_INVALID_OPERATION<<std::endl;
        std::cerr<<"**************************"<<std::endl;
        exit(EXIT_FAILURE);
    }

    return address;
}

bool VBO::Unmap()
{
    return glUnmapBuffer(fType);
}

}}
