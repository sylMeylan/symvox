/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/GPUCustomInstanceBuffer.hh"

namespace SV { namespace TriD {

GPUCustomInstanceBuffer::GPUCustomInstanceBuffer()
{

}

GPUCustomInstanceBuffer::~GPUCustomInstanceBuffer()
{

}

void GPUCustomInstanceBuffer::SendToGPU(CPUBuffer<float> &bufferCPU)
{
    // Retrieve CP buffer GL infos
    GLenum type = bufferCPU.GetBufferType();
    GLenum draw = bufferCPU.GetBufferDraw();
    GLenum dataType = bufferCPU.GetDataType();

    // VBO creation

    if(fpVbo==nullptr)
    {
        CreateVBO(type, draw);
        fpVbo->Init();
    }

    fpVbo->Bind();

    // Allocate the memory
    fpVbo->Allocate(bufferCPU.GetBufferMemSize() );

    // Write the data

    unsigned int posCount = 0;

    int numOfInstances = bufferCPU.GetData(0).GetNumOfValues()/bufferCPU.GetData(0).GetSetSize();

    // Loop on all the instances
    for(int n=0; n<numOfInstances; n++)
    {
        // Map the area of the main instance buffer in which we will write the instance data
        GLvoid* addressVBO = fpVbo->MapBufferRange(0,
                                                   static_cast<unsigned int>(bufferCPU.GetBufferMemSize() ),
                                                   GL_MAP_WRITE_BIT
                                                   );

        // Check if the mapping failed
        if(addressVBO==nullptr)
        {
            Utils::Exception(Utils::ExceptionType::Fatal, "GPUCustomDataBuffer::SendToGPU",
                             "MapBufferRange failed");
        }

        // Loop on all the data
        for(int i=0, ie=bufferCPU.GetNumOfData(); i<ie; i++)
        {
            std::vector<float>& data = bufferCPU.GetData(i).GetValues();

            int dataInstanceSetSize = bufferCPU.GetData(i).GetSetSize();

            // Loop on all the item to copy
            for(int t=0; t<dataInstanceSetSize; t++)
            {
                *( static_cast<float*>(addressVBO)+posCount ) = data[n*dataInstanceSetSize];
                posCount++;
            }
        }
    }

    for(int i=0; i<bufferCPU.GetNumOfData(); i++)
    {
        fpVbo->AddSubData(bufferCPU.GetData(i).GetValues() );
    }

    fpVbo->Unbind();

    // VBO mapping in VAO

    fpVao->Bind();
    fpVbo->Bind();

    unsigned int stride = 0;

    for(int i=0; i<bufferCPU.GetNumOfData(); i++)
    {
        Data<float>& data = bufferCPU.GetData(i);
        int memSetSize =  data.GetMemSetSize();

        stride += static_cast<unsigned int>(memSetSize);
    }

    unsigned int locationIndex = 0;
    unsigned int byteLocation = 0;

    for(int i=0; i<bufferCPU.GetNumOfData(); i++)
    {
        Data<float>& data = bufferCPU.GetData(i);

        // We need to store a matrix thus a set of 4 vec4 elements
        if(data.GetSetSize() == 16)
        {
            for(unsigned int l=0; l<4; l++)
            {
                unsigned int index = locationIndex+l;
                unsigned int matByteLocation = byteLocation + static_cast<unsigned int>(data.GetMemSetSize() )/4*l;

                fpVao->VertexAttribPointer(index, data.GetSetSize()/4, dataType, matByteLocation, stride);
                fpVao->EnableVertexAttribArray(index);
                fpVao->VertexAttribDivisor(index);
            }

            locationIndex += 4;
        }
        else
        {
            fpVao->VertexAttribPointer(locationIndex, data.GetSetSize(), dataType, byteLocation, stride);
            fpVao->EnableVertexAttribArray(locationIndex);
            fpVao->VertexAttribDivisor(locationIndex);

            locationIndex += 1;
        }
    }

    fpVbo->Unmap();

    fpVbo->Unbind();
    fpVao->Unbind();
}

}}
