/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/FrameBuffer.hh"

namespace SV { namespace TriD {

FrameBuffer::FrameBuffer()
{
    this->initializeOpenGLFunctions();

    fBindType = GL_FRAMEBUFFER;

    glGenFramebuffers(1, &fOpenGLId);
}

FrameBuffer::~FrameBuffer()
{
    glDeleteFramebuffers(1, &fOpenGLId);
}

void FrameBuffer::Bind(GLenum t)
{
    fBindType = t;

    glBindFramebuffer(fBindType, fOpenGLId);
}

void FrameBuffer::Unbind()
{
    glBindFramebuffer(fBindType, fOpenGLId);
}

void FrameBuffer::AttachTexture(unsigned int length, unsigned int height)
{
    fTexture.SetSizes(length, height);

    fTexture.LoadEmptyTexture();

    // Attach the texture to the framebuffer
    glFramebufferTexture2D(fBindType, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fTexture.GetId(), 0);
}

void FrameBuffer::AttachStencilAndDepthRenderBuffer(unsigned int length, unsigned int height)
{
    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, length, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    // Attach the renderbuffer to the framebuffer
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
}

GLenum FrameBuffer::CheckStatus()
{
    return glCheckFramebufferStatus(GL_FRAMEBUFFER);
}

void FrameBuffer::Initialize(unsigned int length, unsigned int height)
{
    Bind();

    AttachTexture(length, height);
    AttachStencilAndDepthRenderBuffer(length, height);

    if(GL_FRAMEBUFFER_COMPLETE != CheckStatus() )
    {
        std::stringstream ss;
        ss << "The frame buffer was not correctly bind";
        Utils::Exception(Utils::ExceptionType::Fatal, "GPUFrameBuffer::Create", ss.str() );
    }

    Unbind();
}

}}
