/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/GPUObj3DBuffer.hh"

namespace SV { namespace TriD {

GPUObj3DBuffer::GPUObj3DBuffer() : GPUBuffer()
{

}

GPUObj3DBuffer::~GPUObj3DBuffer()
{

}

void GPUObj3DBuffer::SendToGPU(CPUBuffer<float>& bufferCPU)
{
    // Retrieve CP buffer GL infos
    GLenum type = bufferCPU.GetBufferType();
    GLenum draw = bufferCPU.GetBufferDraw();
    GLenum dataType = bufferCPU.GetDataType();

    // VBO creation

    if(fpVbo==nullptr)
    {
        CreateVBO(type, draw);
        fpVbo->Init();
    }
/*    else {
        std::cerr<<"************* Fatal Error ***********"<<std::endl;
        std::cerr<<"GPUObj3DBuffer::SendToGPU: fpVbo should not exist here"<<std::endl;
        std::cerr<<"************************"<<std::endl;
        exit(EXIT_FAILURE);
    } */

    fpVbo->Bind();

    fpVbo->Allocate(bufferCPU.GetBufferMemSize() );

    for(int i=0, ie=bufferCPU.GetNumOfData(); i<ie; i++)
    {
        fpVbo->AddSubData(bufferCPU.GetData(i).GetValues() );
    }

    fpVbo->Unbind();

    // VBO mapping in VAO

    fpVao->Bind();
    fpVbo->Bind();

    unsigned int locationIndex = 0;
    unsigned int byteLocation = 0;

    for(int i=0; i<bufferCPU.GetNumOfData(); i++)
    {
        Data<float>& data = bufferCPU.GetData(i);

        fpVao->VertexAttribPointer(locationIndex, data.GetSetSize(), dataType, byteLocation);
        fpVao->EnableVertexAttribArray(locationIndex);

        locationIndex++;

        byteLocation += static_cast<unsigned int>(data.GetMemSize() );
    }

    fpVbo->Unbind();
    fpVao->Unbind();
}

}}


