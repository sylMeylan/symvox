/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/VAO.hh"

namespace SV { namespace TriD {

VAO::VAO() :
    fIsGenerated(false),
    fId(0)
{
    this->initializeOpenGLFunctions();
}

VAO::~VAO()
{
    Delete();
}

void VAO::Init()
{
    if(glIsVertexArray(fId)==GL_TRUE)
        Delete();

    glGenVertexArrays(1, &fId);
}

void VAO::Delete()
{
    if(glIsVertexArray(fId)==GL_TRUE)
        glDeleteVertexArrays(1, &fId);
}

void VAO::Bind()
{
    glBindVertexArray(fId);
}

void VAO::Unbind()
{
    glBindVertexArray(0);
}

void VAO::VertexAttribPointer(unsigned int index, int numOfVal, GLenum type, unsigned int byteLocation, unsigned int stride, bool normalized)
{
    glVertexAttribPointer(index, numOfVal, type, normalized, stride, ((char*)NULL + (byteLocation)) );
}

void VAO::EnableVertexAttribArray(unsigned int index)
{
    glEnableVertexAttribArray(index);
}

void VAO::VertexAttribDivisor(unsigned int index, unsigned int divisor)
{
    glVertexAttribDivisor(index, divisor);
}

bool VAO::IsExisting()
{
    return (glIsVertexArray(fId)==GL_TRUE);
}


}}
