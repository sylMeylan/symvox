/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/GPUInstanceBuffer.hh"

namespace SV { namespace TriD {

GPUInstanceBuffer::GPUInstanceBuffer() : GPUBuffer(),
    fInstanceBufferByteSize(4000000), // 4 Mb
    fMultiple(6), // number of subBuffer into the buffer
    fCursorOffset(0),
    fIsFirstBufferCreated(false)
{

}

GPUInstanceBuffer::~GPUInstanceBuffer()
{

}

void GPUInstanceBuffer::SendToGPU(CPUBuffer<float>& bufferCPU)
{
    bool isColorInstancing = (bufferCPU.GetData(1).GetNumOfValues() != 0);

    // ****************************
    // VBO data update
    // ****************************

    if(fpVbo==nullptr)
    {
       CreateVBO(bufferCPU.GetBufferType(), bufferCPU.GetBufferDraw() );
       fpVbo->Init();
    }

    fpVbo->Bind();

    int numOfInstances = bufferCPU.GetData(0).GetNumOfValues()/bufferCPU.GetData(0).GetSetSize();

    unsigned int subBufferByteSize = bufferCPU.GetBufferMemSize();

    // Round the buffer size to a number of two
    subBufferByteSize = subBufferByteSize/64 * 64 + 64;

    // Initialisation time

    if(!fIsFirstBufferCreated)
    {
        // If the default instance buffer size is too small then extend it
        if(fInstanceBufferByteSize < subBufferByteSize * fMultiple)
        {
            // Extension
            fInstanceBufferByteSize = subBufferByteSize * fMultiple;

            std::stringstream ss;
            ss<<"Instance buffers are really big and bufferTotSize has been set to "<<fInstanceBufferByteSize/1000000<<" Mb."<<std::endl;
            ss<<"SubBuffers expected within the main buffer: "<<fMultiple<<std::endl;
            ss<<"Instance number: "<<numOfInstances<<std::endl;
            ss<<"***************************";
            Utils::Exception(Utils::ExceptionType::Warning, "GPUInstanceBuffer::SendToGPU", ss.str() );
        }

        // Allocate the memory on the GPU
        fpVbo->Allocate(fInstanceBufferByteSize);

        // The offset is 0
        fCursorOffset = 0;

        // Now, the initialisation is done
        fIsFirstBufferCreated = true;
    }

    // Runtime

    // If the initial buffer is too small then we need to increase its size
    if(fInstanceBufferByteSize < subBufferByteSize * fMultiple)
    {
        // Orphan the previously allocated buffer
        fpVbo->Orphan();

        // Set the new size
        fInstanceBufferByteSize = subBufferByteSize * fMultiple;

        // Allocate the GPU memory needed to handle the new size
        fpVbo->Allocate(fInstanceBufferByteSize);

        // Set the cursor offset to zero since we start a new buffer
        fCursorOffset = 0;

        // Display informations in the terminal
        std::stringstream ss;
        ss<<"Instance buffers are really big and bufferTotSize has been set to "<<fInstanceBufferByteSize/1000000<<" Mb."<<std::endl;
        ss<<"SubBuffers expected within the main buffer: "<<fMultiple<<std::endl;
        ss<<"Instance number: "<<numOfInstances;
        Utils::Exception(Utils::ExceptionType::Warning, "GPUInstanceBuffer::SendToGPU", ss.str() );
    }

    // Check if the cursor is at the end of the buffer
    if(fCursorOffset + subBufferByteSize >= fInstanceBufferByteSize)
    {
        // If we are at the end of the main instance buffer then we want to orphan it in order to start a new one
        fpVbo->Orphan();

        // Allocate the memory for the new buffer
        //fpVbo->Allocate(fInstanceBufferByteSize);

        fCursorOffset = 0;
    }

    // Map the area of the main instance buffer in which we will write the instance data (ie the sub buffer)
    // GL_MAP_UNSYNCHRONIZED_BIT means the GPU will not block the CPU for this command to complete
    // In our implementation, this is an advantage and not an issue because the Draw command is always issued on a previously written subBuffer
    GLvoid* addressVBO = fpVbo->MapBufferRange(fCursorOffset, subBufferByteSize, GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);

    // Check if the mapping failed
    if(addressVBO==NULL)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "GPUInstanceBuffer::SendToGPU",
                         "MapBufferRange failed");
    }

    // Write the data into the VBO

    unsigned int posCount = 0;

    std::vector<float>& modelsMatricesVect = bufferCPU.GetData(0).GetValues();
    std::vector<float>& colorsVect = bufferCPU.GetData(1).GetValues();
    std::vector<float>& radiusVect = bufferCPU.GetData(2).GetValues();

    // Loop on all the instances
    for(int n=0; n<numOfInstances; n++)
    {
        // Copy the model matrix data
        for(int m=0; m<16; m++)
        {
            *(  (float*)addressVBO+posCount) = modelsMatricesVect[n*16 + m];
            posCount++;
        }

        if(isColorInstancing)
        {
            // Copy the radius data
            *( (float*)addressVBO+posCount) = radiusVect[n*1];
            posCount++;

            // Copy the color data
            for(unsigned int m=0; m<4; m++)
            {
                *( (float*)addressVBO+posCount) = colorsVect[n*4 + m];
                posCount++;
            }
        }
    }

    // Unmap the buffer
    bool isUnmapped = fpVbo->Unmap();

    if(!isUnmapped)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "GPUInstanceBuffer::SendToGPU",
                         "UnmapBuffer failed");
    }

    fpVbo->Unbind();

    // ***********************************
    // VAO
    // ***********************************

    // Bind the VAO and VBO
    fpVao->Bind();
    fpVbo->Bind();

    Data<float>& modelMatsData = bufferCPU.GetData(0);
    Data<float>& colorData = bufferCPU.GetData(1);
    Data<float>& radiusData = bufferCPU.GetData(2);

    GLenum dataType = bufferCPU.GetDataType();

    // Stride represents the memory byte size needed when the GPU will switch from one instance to another.
    // At this time, the GPU needs to go the next instance data and it will use the stride value to do so.
    unsigned int stride (0);

    if(isColorInstancing)
        stride = modelMatsData.GetMemSetSize() // model matrix
            + radiusData.GetMemSetSize() // radius
            + colorData.GetMemSetSize(); // color
    else
        stride = modelMatsData.GetMemSetSize(); // model matrix

    // Attrib pointer 0 is mesh
    // 1 is color
    // 2 is texture
    // 3 is normal

    // Model matrix

    // A 4x4 matrix is equivalent to 4 vectors within the buffer
    for(unsigned int i=0; i<4; i++)
    {
        fpVao->VertexAttribPointer(4+i, modelMatsData.GetSetSize()/4, dataType, fCursorOffset + modelMatsData.GetMemSetSize()/4*i, stride);
        fpVao->EnableVertexAttribArray(4+i);
        fpVao->VertexAttribDivisor(4+i);
    }

    if(isColorInstancing)
    {
        // Radius

        fpVao->VertexAttribPointer(8, radiusData.GetSetSize(), dataType, fCursorOffset+modelMatsData.GetMemSetSize(), stride);
        fpVao->EnableVertexAttribArray(8);
        fpVao->VertexAttribDivisor(8);

        // Color

        fpVao->VertexAttribPointer(9, colorData.GetSetSize(), dataType, fCursorOffset+modelMatsData.GetMemSetSize() + radiusData.GetMemSetSize(), stride);
        fpVao->EnableVertexAttribArray(9);
        fpVao->VertexAttribDivisor(9);
    }

    fpVbo->Unbind();
    fpVao->Unbind();

    // Cursor incrementation
    fCursorOffset += subBufferByteSize;
}

}}
