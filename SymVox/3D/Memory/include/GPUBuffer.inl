#ifndef OGL_BUFFERGPU_INL
#define OGL_BUFFERGPU_INL

#include "GPUBuffer.hh"

namespace SV { namespace TriD {

template<typename T>
void GPUBuffer::SendToGPUAsSubData(CPUBuffer<T>& bufferCPU, VBO* vbo)
{
    // Retrieve CP buffer GL infos
    GLenum type = bufferCPU.GetBufferType();
    GLenum draw = bufferCPU.GetBufferDraw();
    GLenum dataType = bufferCPU.GetDataType();

    // VBO creation and filling

    if(vbo==nullptr)
    {
        vbo = CreateVBO(type, draw);
        vbo->Init();
    }

    vbo->Bind();

    vbo->Allocate(bufferCPU.GetBufferMemSize() );

    for(int i=0; i<bufferCPU.GetNumOfData(); i++)
    {
        vbo->AddSubData(bufferCPU.GetData(i).GetValues() );
    }

    vbo->Unbind();

    // VBO mapping in VAO

    fpVao->Bind();
    vbo->Bind();

    unsigned int byteLocation = 0;

    for(int i=0; i<bufferCPU.GetNumOfData(); i++)
    {
        Data<T>& data = bufferCPU.GetData(i);

        fpVao->VertexAttribPointer(i, data.GetSetSize(), dataType, byteLocation);
        fpVao->EnableVertexAttribArray(i);

        byteLocation += data.GetMemSize();
    }

    vbo->Unbind();
    fpVao->Unbind();
}

}}

#endif // OGL_BUFFERGPU_INL

