#ifndef OGL_VBO_INL
#define OGL_VBO_INL

#include "VBO.hh"

namespace SV { namespace TriD {

template<typename T>
void VBO::AddData(std::vector<T>& data)
{
    unsigned int byteSize = sizeof(T) * data.size();

    glBufferData(fType, byteSize, &data[0], fDraw);

    fByteSize = byteSize;
}

template<typename T>
void VBO::AddSubData(std::vector<T>& data)
{
    unsigned int byteSize = sizeof(T) * data.size();

    glBufferSubData(fType, fOffset, byteSize, &data[0]);

    fOffset += byteSize;
}

template void VBO::AddData(std::vector<float>& data);
template void VBO::AddSubData(std::vector<float>& data);

template void VBO::AddData(std::vector<int>& data);
template void VBO::AddSubData(std::vector<int>& data);

template void VBO::AddData(std::vector<unsigned int>& data);
template void VBO::AddSubData(std::vector<unsigned int>& data);

}}

#endif // OGL_VBO_INL
