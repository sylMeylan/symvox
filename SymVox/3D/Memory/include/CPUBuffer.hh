/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_MEM_BUFFER_HH
#define TRID_MEM_BUFFER_HH

#include <QOpenGLContext>
#include <vector>
#include <unordered_map>
#include <sstream>
#include "SymVox/Utils/include/Exception.hh"

namespace SV { namespace TriD {

template<typename T>
class Data
{
public:
    Data(int setSize) :
        fSetSize(setSize)
    {}

    ~Data()
    {}

    Data(const std::vector<T>& vect, int setSize)
    {
        fValues.insert(fValues.begin(), vect.begin(), vect.end() );
        fSetSize = setSize;
    }

    void AddValue(T val)
    {
        fValues.push_back(val);
    }

    void Reset()
    {
        fValues.clear();
        fValues.shrink_to_fit();
    }

    void ResetWithoutShrink()
    {
        fValues.clear();
    }

    void SetValue(int index, T val)
    {
        fValues[index] = val;
    }

    int GetMemSize() const {return sizeof(T) * fValues.size();}
    int GetNumOfValues() const {return fValues.size();}

    float GetValue(int index) const {return fValues.at(index);}

    const std::vector<T>& GetValues() const {return fValues;}
    std::vector<T>& GetValues() {return fValues;}

    T* GetStartValue() {return &fValues.at(0);}

    int GetSetSize() const {return fSetSize;}

    int GetMemSetSize() const {return sizeof(T) * fSetSize;}

private:
        int fSetSize;
        std::vector<T> fValues;
};

template<typename T>
class CPUBuffer
{
public:
    CPUBuffer(const std::string& name="noName", GLenum type=GL_ARRAY_BUFFER, GLenum draw=GL_STATIC_DRAW, GLenum dataType=GL_FLOAT);
    ~CPUBuffer();

    void ResetBuffer();
    void ResetAllData();

    void AddData(const std::string& id, int setSize);
    void AddData(const std::string& id, const Data<T>& data);
    void AddData(const std::string& id, const std::vector<T>& vect, int setSize);
    void AddValToData(const std::string& id, T val);
    void ResetData(const std::string& id);

    const Data<T>& GetData(const std::string& id) const {return fDataVect.at(fDataIndexMap.at(id) );;}
    Data<T>& GetData(const std::string& id) {return fDataVect.at(fDataIndexMap.at(id) );}

    const Data<T>& GetData(int index) const;
    Data<T>& GetData(int index);

    const std::string& GetDataName(int index) const;

    int GetNumOfData() const {return fDataVect.size();}

    int GetBufferMemSize() const;

    int GetVboID() const {return fVboId;}
    void SetVboID(int id) {fVboId=id;}

    GLenum GetBufferType() const {return fBufferType;}
    void SetBufferType(GLenum t) {fBufferType=t;}

    GLenum GetBufferDraw() const {return fBufferDraw;}
    void SetBufferDraw(GLenum d) {fBufferDraw=d;}

    GLenum GetDataType() const {return fDataType;}
    void SetDataType(GLenum dt) {fDataType=dt;}

    void SetName(const std::string& name) const {fName=name;}
    const std::string& GetName() const {return fName;}

    void SetIsInitialised(bool b) {fIsInitialized=b;}
    bool GetIsInitialised() const {return fIsInitialized;}

private:
    std::vector<Data<T> > fDataVect;

    // string, index
    std::map<std::string, int> fDataIndexMap;

    std::string fName;

    int fVboId;
    GLenum fBufferType;
    GLenum fBufferDraw;
    GLenum fDataType;
    bool fIsInitialized;
};

}}

#include "CPUBuffer.inl"

#endif // TRID_MEM_BUFFER_HH
