/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_MEM_BUFFERGPU_HH
#define TRID_MEM_BUFFERGPU_HH

#include "VBO.hh"
#include "VAO.hh"
#include "CPUBuffer.hh"
#include "SymVox/Utils/include/Exception.hh"
#include "SymVox/Utils/include/OpenGL.hh"

namespace SV { namespace TriD {

class GPUBuffer : public Utils::OpenGL
{
public:
    GPUBuffer(VAO* vao=nullptr);

    virtual ~GPUBuffer();

    VBO* CreateVBO(GLenum type, GLenum draw);

    template<typename T>
    void SendToGPUAsSubData(CPUBuffer<T>& bufferCPU, VBO* vbo=nullptr);

    virtual void SendToGPU(CPUBuffer<float>& bufferCPU)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "GPUBuffer:SendToGPU",
                         "GPUBuffer:SendToGPU(CPUBuffer<float>... should not be called");
    }

    virtual void SendToGPU(CPUBuffer<int>& bufferCPU)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "GPUBuffer:SendToGPU",
                         "GPUBuffer:SendToGPU(CPUBuffer<int>... should not be called");
    }

    virtual void SendToGPU(CPUBuffer<unsigned int>& bufferCPU)
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "GPUBuffer:SendToGPU",
                         "GPUBuffer:SendToGPU(CPUBuffer<unsigned int>... should not be called");
    }

    void SetVAO(VAO* vao) {fpVao=vao;}

    VAO* GetVAO() {return fpVao;}
    const VAO* GetVAO() const {return fpVao;}

    VBO* GetVbo() {return fpVbo;}
    const VBO* GetVbo() const {return fpVbo;}

    void SetVbo(VBO* vbo) {fpVbo=vbo;}

protected:
    VAO* fpVao;

    VBO* fpVbo;
};

}}

#include "GPUBuffer.inl"

#endif // TRID_MEM_BUFFERGPU_HH
