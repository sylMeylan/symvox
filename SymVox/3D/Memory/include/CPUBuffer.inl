#ifndef OGL_BUFFERCPU_INL
#define OGL_BUFFERCPU_INL

#include "CPUBuffer.hh"

namespace SV { namespace TriD {

template<typename T>
CPUBuffer<T>::CPUBuffer(const std::string& name, GLenum type, GLenum draw, GLenum dataType)
{
    fName = name;
    fBufferType = type;
    fBufferDraw = draw;
    fDataType = dataType;
    fVboId = -1;
    fIsInitialized = false;
}

template<typename T>
CPUBuffer<T>::~CPUBuffer()
{
}

template<typename T>
void CPUBuffer<T>::ResetBuffer()
{
    fDataIndexMap.clear();
    fDataVect.clear();
}

template<typename T>
void CPUBuffer<T>::ResetAllData()
{
    // Loop on all the data
    for(auto& it : fDataVect)
    {
        it.ResetWithoutShrink();
    }
}

template<typename T>
void CPUBuffer<T>::AddData(const std::string& id, int setSize)
{
    fDataVect.push_back(Data<T>(setSize) );

    fDataIndexMap[id] = fDataVect.size()-1;
}

template<typename T>
void CPUBuffer<T>::AddData(const std::string& id, const Data<T>& data)
{
    fDataVect.push_back(data);

    fDataIndexMap[id] = fDataVect.size()-1;
}

template<typename T>
void CPUBuffer<T>::AddData(const std::string& id, const std::vector<T>& vect, int setSize)
{  
    fDataVect.push_back(Data<T>(vect, setSize) );

    fDataIndexMap[id] = fDataVect.size()-1;
}

template<typename T>
void CPUBuffer<T>::AddValToData(const std::string& id, T val)
{
    GetData(id).AddValue(val);
}

template<typename T>
void CPUBuffer<T>::ResetData(const std::string& id)
{
    GetData(id).Reset();
}

template<typename T>
const Data<T>& CPUBuffer<T>::GetData(int index) const
{
    return fDataVect.at(index);
}

template<typename T>
Data<T>& CPUBuffer<T>::GetData(int index)
{
    return fDataVect.at(index);
}

template<typename T>
const std::string& CPUBuffer<T>::GetDataName(int index) const
{
    int i (-1);
    int count (0);

    while(i!=index && count<fDataVect.size() )
    {
        auto it = fDataIndexMap.begin();
        std::advance(it, count);

        i = it->second;

        count++;
    }

    count--;

    if(count>=fDataVect.size() || count < 0)
    {
        std::stringstream ss;
        ss << index <<" does not exist"<<std::endl;
        ss <<"fDataVect size "<< fDataVect.size() << std::endl;
        ss <<"i "<< i << std::endl;
        ss <<"count "<< count << std::endl;
        Utils::Exception(Utils::ExceptionType::Fatal, "CPUBuffer<T>::GetDataName", ss.str() );
    }

    auto it = fDataIndexMap.begin();
    std::advance(it, count);

    return it->first;
}

template<typename T>
int CPUBuffer<T>::GetBufferMemSize() const
{
    int memSize = 0;

    for(auto& it : fDataVect)
    {
        memSize += it.GetMemSize();
    }

    return memSize;
}


}}

#endif // OGL_BUFFER_INL
