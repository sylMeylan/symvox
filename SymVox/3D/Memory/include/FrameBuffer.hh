/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef TRID_MEM_GPUFRAMEBUFFER_HH
#define TRID_MEM_GPUFRAMEBUFFER_HH

#include "SymVox/Utils/include/OpenGL.hh"
#include <sstream>

#include "SymVox/Utils/include/Exception.hh"
#include "SymVox/3D/Object/Standard/include/Texture.hh"

namespace SV { namespace TriD {

/*!
 * \brief The FrameBuffer class
 * Not tested yet
 */
class FrameBuffer : public Utils::OpenGL
{
public:
    FrameBuffer();
    virtual ~FrameBuffer();

    void Initialize(unsigned int length, unsigned int height);

    // GL_FRAMEBUFFER: most of the time.
    // GL_READ_FRAMEBUFFER: all read operations like glReadPixels.
    // GL_DRAW_FRAMEBUFFER: for rendering, clearing and other write operations.
    void Bind(GLenum t = GL_FRAMEBUFFER);
    void Unbind();

    SV::TriD::Texture& GetTexture() {return fTexture;}

    unsigned int GetOpenGLId() const {return fOpenGLId;}

private:
    unsigned int fOpenGLId;
    GLenum fBindType;
    SV::TriD::Texture fTexture;

    GLenum CheckStatus();
    void AttachTexture(unsigned int length, unsigned int height);
    void AttachStencilAndDepthRenderBuffer(unsigned int length, unsigned int height);
};

}}

#endif // TRID_MEM_GPUFRAMEBUFFER_HH
