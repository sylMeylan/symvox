/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef GUI_VCAMANIMATION_HH
#define GUI_VCAMANIMATION_HH

#include <QTime>
#include <QElapsedTimer>

#include "Camera.hh"
#include "SymVox/Run/Scene/Graph/include/VSceneNode.hh"

namespace SV { namespace GUI {

class CamMove
{
public:
    CamMove()
    {
        fIsInit=false;
    }

    CamMove(const QVector3D& p1, const QVector3D& p2, double time)
    {
        Init(p1, p2, time);
    }

    ~CamMove() {}

    QVector3D ComputeMove(double currentTime)
    {
        return (fP2-fP1)/fT * currentTime;
    }

    QVector3D ComputePos(double currentTime)
    {
        return ComputeMove(currentTime)+fP1;
    }

    void Init(const QVector3D& start, const QVector3D& end, double time)
    {
        fP1 = start;
        fP2 = end;
        fT = time;
        fIsInit = true;
    }

    bool GetIsInit() {return fIsInit;}

private:
    QVector3D fP1, fP2;
    double fT;
    bool fIsInit;

    void SetStart(const QVector3D& s) {fP1=s;}
    void SetEnd(const QVector3D& e) {fP2=e;}
    void SetTime(double t) {fT=t;}
};

class VCamAnimation
{
public:
    VCamAnimation(GUI::Camera* camera, Run::VSceneNode* world);
    virtual ~VCamAnimation();

    void Start();
    void Stop();
    void Pause(bool b=true);

    bool GetIsRunning() const {return fIsRunning;}

    void TryRun();
    virtual void Run()=0;

    virtual void Reset()=0;

    double ElapsedSecs();
    int ElapsedMSecs();

protected:
    GUI::Camera* fpCamera;
    Run::VSceneNode* fpWorld;

private:
    QTime fTime;
    QTime fPauseTime;
    int fPauseDuration;

    bool fIsRunning;
    bool fIsPause;
};

}}

#endif // GUI_VCAMANIMATION_HH
