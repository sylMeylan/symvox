/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef GUI_CAMERA_HH
#define GUI_CAMERA_HH

#include <QVector3D>
#include <QMatrix4x4>

#include <cmath>

#include "SymVox/GUI/Input/include/Input.hh"

namespace SV { namespace GUI {

class Camera
{
public:
    Camera();
    Camera(QVector3D position, QVector3D target, QVector3D verticalAxis,
              float sensitivity, float speed);
    ~Camera();

    Camera& operator=(Camera const &toBeCopied);

    void LookAt(QMatrix4x4 &modelView);

    void MoveForward(const double dist);
    void MoveUp(const double dist);
    void MoveLeft(const double dist);

    float GetSentivity() const;
    float GetSpeed() const;
    const QVector3D& GetPosition() const {return fPosition;}
    const QVector3D& GetOrientation() const {return fOrientation;}

    void SetPosition(QVector3D position);
    void SetViewDirection(const QVector3D& dir);
    void SetSensitivity(float sensitivity);
    void SetSpeed(float speed);

    void Orientate(int xRel, int yRel);

    void SetOrientation(const QVector3D& ori);
    QVector3D GetOrientation2() const;

private:

    // Orientation
    float fPhi;
    float fTheta;
    QVector3D fOrientation;

    // Movement
    QVector3D fVerticalAxis;
    QVector3D fLateralVector;

    // Look at
    QVector3D fPosition;
    QVector3D fTargetPoint;

    // Custom
    float fSensitivity;
    float fSpeed;

    // For the copy operator
    bool fIsInitialized;

    void SetTargetPoint(QVector3D targetPoint);
};

}}

#endif // GUI_CAMERA_HH
