/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Camera.hh"
#include "iostream"
namespace SV { namespace GUI {

Camera::Camera()
    : fPhi(0.0),
      fTheta(0.0),
      fOrientation(),
      fVerticalAxis(0,0,1),
      fLateralVector(),
      fPosition(),
      fTargetPoint(),
      fSensitivity(0.0),
      fSpeed(0.0),
      fIsInitialized(false)
{

}

Camera::Camera(QVector3D position, QVector3D target, QVector3D verticalAxis,
                     float sensitivity, float speed)
    : fPhi(0.0),
      fTheta(0.0),
      fOrientation(0,0,0),
      fVerticalAxis(verticalAxis),
      fLateralVector(),
      fPosition(position),
      fTargetPoint(target),
      fSensitivity(sensitivity),
      fSpeed(speed),
      fIsInitialized(false)
{
    // Calculate first angles
    SetTargetPoint(target);

    fOrientation = (target - position).normalized();

    // Calculate lateral vector
    fLateralVector = QVector3D::crossProduct(fVerticalAxis, fOrientation);
    fLateralVector.normalize();

    fIsInitialized = true;
}

Camera::~Camera()
{

}

Camera& Camera::operator=(Camera const &toBeCopied)
{
    fVerticalAxis = toBeCopied.fVerticalAxis;
    fPosition = toBeCopied.fPosition;
    fTargetPoint = toBeCopied.fTargetPoint;
    fSensitivity = toBeCopied.fSensitivity;
    fSpeed = toBeCopied.fSpeed;
    fIsInitialized = toBeCopied.fIsInitialized;

    // Calculate first angles
    SetTargetPoint(fTargetPoint);

    // Calculate lateral vector
    fLateralVector = QVector3D::crossProduct(fVerticalAxis, fOrientation);
    fLateralVector.normalize();

    fIsInitialized = true;

    return *this;
}

void Camera::Orientate(int xRel, int yRel)
{
    fPhi += -yRel * fSensitivity;
    fTheta += -xRel * fSensitivity; // -xRel at the beginning...

    // Phi angle limit
    if(fPhi>89.0) fPhi=89.0;
    else if(fPhi<-89.0) fPhi=-89.0;

    // Theta angle limit
    if(fTheta>360) fTheta = 0.0;
    else if(fTheta<0) fTheta = 360;

    // From deg to rad
    float phiRad  = fPhi*M_PI/180;
    float thetaRad = fTheta*M_PI/180; // The "-" is here to correct a bug... Probably in the equations.

    // Spherical coordinates

    // if vertical axis is X
    if(fVerticalAxis.x()==1.0)
    {
        fOrientation.setX(std::sin(phiRad));
        fOrientation.setY(std::cos(phiRad)*std::cos(thetaRad));
        fOrientation.setZ(std::cos(phiRad)*std::sin(thetaRad));
    }
    // if vertical axis is Y
    else if(fVerticalAxis.y()==1.0)
    {
        fOrientation.setX(std::cos(phiRad)*std::sin(thetaRad));
        fOrientation.setY(std::sin(phiRad));
        fOrientation.setZ(std::cos(phiRad)*std::cos(thetaRad));
    }
    // if vertical axis is Z
    else if(fVerticalAxis.z()==1.0)
    {
        fOrientation.setX(std::cos(phiRad)*std::cos(thetaRad));
        fOrientation.setY(std::cos(phiRad)*std::sin(thetaRad));
        fOrientation.setZ(std::sin(phiRad));
    }

    // Lateral vector calculation
    fLateralVector = QVector3D::crossProduct(fVerticalAxis, fOrientation);
    fLateralVector.normalize();

    // Calculate target point
    fTargetPoint = fPosition + fOrientation;
}

void Camera::MoveForward(const double dist)
{
    fPosition = fPosition + fOrientation * dist;
    fTargetPoint = fPosition + fOrientation;

//    fPosition = fPosition + ForwardVector() * dist;
//    fTargetPoint = fPosition + fOrientation;
}

void Camera::MoveUp(const double dist)
{
    fPosition = fPosition + fVerticalAxis * dist;
    fTargetPoint = fPosition + fOrientation;

//    fPosition = fPosition + UpVector() * dist;
//    fTargetPoint = fPosition + fOrientation;
}

void Camera::MoveLeft(const double dist)
{
    fPosition = fPosition + fLateralVector * dist;
    fTargetPoint = fPosition + fOrientation;

//    fPosition = fPosition + LeftVector() * dist;
//    fTargetPoint = fPosition + fOrientation;
}

void Camera::LookAt(QMatrix4x4 &modelView)
{
    // Refresh the view within the matrix
    modelView.lookAt(fPosition, fTargetPoint, fVerticalAxis);
}

float Camera::GetSentivity() const
{
    return fSensitivity;
}

float Camera::GetSpeed() const
{
    return fSpeed;
}

void Camera::SetTargetPoint(QVector3D targetPoint)
{
    fTargetPoint = targetPoint;

    // Calculate orientation vector
    fOrientation = fTargetPoint - fPosition;
    fOrientation.normalize();

    if(fVerticalAxis.x()==1.0)
    {
        fPhi = std::asin(fOrientation.x());
        fTheta = std::acos(fOrientation.y() / std::cos(fPhi));
        if(fOrientation.y()<0) fTheta *= -1;
    }
    else if(fVerticalAxis.y()==1.0)
    {
        fPhi = std::asin(fOrientation.y());
        fTheta = std::acos(fOrientation.z() / std::cos(fPhi));
        if(fOrientation.z()<0) fTheta *= -1;
    }
    else
    {
        fPhi = std::asin(fOrientation.x());
        fTheta = std::acos(fOrientation.z() / std::cos(fPhi));
        if(fOrientation.x()<0) fTheta *= -1;
    }

    // From rad to deg
    fPhi = fPhi*180.0/M_PI;
    fTheta = fTheta*180.0/M_PI;

    // Avoid a "jump" when moving the camera for the first time
    if(fTheta>360)
    {
        fTheta = fTheta-360;
    }
    else if(fTheta<0.0f)
    {
        fTheta = fTheta+360;
    }
}

void Camera::SetPosition(QVector3D position)
{
    // Position update
    fPosition = position;

    // Update target point
    fTargetPoint = fPosition+fOrientation;
}

void Camera::SetViewDirection(const QVector3D& dir)
{
    SetTargetPoint(fPosition+dir);

    // Lateral vector calculation
    fLateralVector = QVector3D::crossProduct(fVerticalAxis, fOrientation);
    fLateralVector.normalize();
}

void Camera::SetSensitivity(float sensitivity)
{
    fSensitivity = sensitivity;
}

void Camera::SetSpeed(float speed)
{
    fSpeed = speed;
}

void Camera::SetOrientation(const QVector3D& ori)
{
//    QQuaternion quat = QQuaternion::fromAxisAndAngle(1,0,0,ori[0])
//                     * QQuaternion::fromAxisAndAngle(0,1,0,ori[1])
//                     * QQuaternion::fromAxisAndAngle(0,0,1,ori[2]);
}

QVector3D Camera::GetOrientation2() const
{
    return fOrientation;
}

}}
