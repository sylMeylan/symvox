/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/CamAnimSequence.hh"

namespace SV { namespace GUI {

CamAnimSequence::CamAnimSequence()
{
    fIsOn=false;
    fIndex=0;
    fpCurrentAnim = nullptr;
}

CamAnimSequence::~CamAnimSequence()
{
    RemoveAllAnims();
}

void CamAnimSequence::Start()
{
    fIsOn = true;

    fpCurrentAnim = fAnims.at(fIndex);
    fpCurrentAnim->Start();
}

void CamAnimSequence::Stop()
{
    fIsOn = false;
}

void CamAnimSequence::Reset()
{
    for(VCamAnimation* anim : fAnims)
    {
        if(anim != nullptr)
            anim->Reset();
    }

    fIndex = 0;
}

void CamAnimSequence::Run()
{
    if(fIsOn)
    {
        if(fpCurrentAnim->GetIsRunning() )
            fpCurrentAnim->Run();
        else if(fIndex < GetNumOfAnim()-1 )
            NextAnim();
        else
            Stop();
    }
}

void CamAnimSequence::AddAnim(VCamAnimation *anim)
{
    fAnims.push_back(anim);
}

void CamAnimSequence::RemoveAnim(int index)
{
    delete fAnims.at(index);
    fAnims.erase(fAnims.begin()+index);
}

void CamAnimSequence::RemoveAllAnims()
{
    for(VCamAnimation* anim : fAnims)
    {
        if(anim != nullptr)
            delete anim;
    }
}

VCamAnimation *CamAnimSequence::GetAnim(int index)
{
    return fAnims.at(index);
}

unsigned int CamAnimSequence::GetNumOfAnim()
{
    return fAnims.size();
}

void CamAnimSequence::NextAnim()
{
    fIndex++;
    fpCurrentAnim = fAnims.at(fIndex);
    fpCurrentAnim->Start();
}

}}
