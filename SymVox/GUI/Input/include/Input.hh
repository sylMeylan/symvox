/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef GUI_INPUT_HH
#define GUI_INPUT_HH

#include <map>
#include <vector>

namespace SV { namespace GUI {

struct MouseClick
{
    MouseClick(int buttonId, int x, int y)
    {
        fButtonId = buttonId;
        fX = x;
        fY = y;
    }

    int fButtonId;
    int fX;
    int fY;
};

struct MouseClickValues
{
    MouseClickValues()
    {
        fBool = false;
        fX = 0;
        fY = 0;
    }

    MouseClickValues(bool b, int x, int y)
    {
        fBool = b;
        fX = x;
        fY = y;
    }

    bool fBool;
    int fX;
    int fY;
};

class Input
{
public:
    Input();
    ~Input();

    void UpdateEvenements(std::vector<int> &keysPressed,
                          std::vector<int> &keysReleased,
                          std::vector<MouseClick> &mouseKeysPressed,
                          std::vector<MouseClick> &mouseKeysReleased,
                          int mouseX,
                          int mouseY,
                          int mouseXRel,
                          int mouseYRel);

    bool End() const;
    bool MoveMouse() const;

    bool GetKey(const int keyIndex);
    MouseClickValues GetMouseKey(const int keyIndex);
    int GetX() const;
    int GetY() const;
    int GetDeltaX() const;
    int GetDeltaY() const;

private:
   std::map<int, bool> fKeys;
   std::map<int, MouseClickValues> fMouseKeys;

   int fX;
   int fY;
   int fXRel;
   int fYRel;

   bool fEnd;
};

}}

#endif // GUI_INPUT_HH
