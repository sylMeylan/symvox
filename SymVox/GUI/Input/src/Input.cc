/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/Input.hh"

namespace SV { namespace GUI {

Input::Input()
    : fX(0), fY(0), fXRel(0), fYRel(0), fEnd(false)
{

}

Input::~Input()
{

}

void Input::UpdateEvenements(std::vector<int> &keysPressed,
                                std::vector<int> &keysReleased,
                                std::vector<MouseClick> &mouseKeysPressed,
                                std::vector<MouseClick> &mouseKeysReleased,
                                int mouseX,
                                int mouseY,
                                int mouseXRel,
                                int mouseYRel)
{
    // mouse coordinates
    fX = mouseX;
    fY = mouseY;
    fXRel = mouseXRel;
    fYRel = mouseYRel;

    // Push case
    for(size_t i=0, ie=keysPressed.size(); i<ie; ++i)
    {
        fKeys[keysPressed[i]] = true;
    }

    // Release case
    for(size_t i=0, ie=keysReleased.size(); i<ie; ++i)
    {
        fKeys[keysReleased[i]] = false;
    }

    // Mouse click case
    for(size_t i=0, ie=mouseKeysPressed.size(); i<ie; ++i)
    {
        fMouseKeys[mouseKeysPressed[i].fButtonId] = MouseClickValues(true, mouseKeysPressed[i].fX, mouseKeysPressed[i].fY);
    }

    // Mouse release click case
    for(size_t i=0, ie=mouseKeysReleased.size(); i<ie; ++i)
    {
        fMouseKeys[mouseKeysReleased[i].fButtonId] = MouseClickValues(false, mouseKeysReleased[i].fX, mouseKeysReleased[i].fY);
    }
}

bool Input::End() const
{
    return fEnd;
}

bool Input::MoveMouse() const
{
    if(fXRel==0 && fYRel==0) return false;
    else return true;
}

bool Input::GetKey(const int keyIndex)
{
    return fKeys[keyIndex];
}

MouseClickValues Input::GetMouseKey(const int keyIndex)
{
    return fMouseKeys[keyIndex];
}

int Input::GetX() const
{
    return fX;
}

int Input::GetY() const
{
    return fY;
}

int Input::GetDeltaX() const
{
    return fXRel;
}

int Input::GetDeltaY() const
{
    return fYRel;
}

}}
