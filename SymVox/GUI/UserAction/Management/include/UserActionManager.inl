#ifndef GUI_USERACTION_USERACTIONMANAGER_INL
#define GUI_USERACTION_USERACTIONMANAGER_INL

#include "UserActionManager.hh"

namespace SV { namespace GUI {

template<typename T>
UserActionManager<T>::UserActionManager()
{
    fpSelected = nullptr;
    fSelectedIndex = 0;
}

template<typename T>
UserActionManager<T>::~UserActionManager()
{
    for(T* action : fActions)
    {
        delete action;
    }

    fActions.clear();

    fpSelected = nullptr;
}

template<typename T>
void UserActionManager<T>::Register(T* action)
{
    fActions.push_back(action);
}

template<typename T>
void UserActionManager<T>::Remove(unsigned int index)
{
    fActions.erase(fActions.begin()+index);
}

template<typename T>
void UserActionManager<T>::Select(unsigned int index)
{
    fpSelected = Get(index);
    fSelectedIndex = index;
}

template<typename T>
T* UserActionManager<T>::Get(unsigned int index)
{
    return fActions.at(index);
}

template<typename T>
T* UserActionManager<T>::GetSelected()
{
    return fpSelected;
}

template<typename T>
const T* UserActionManager<T>::GetSelected() const
{
    return fpSelected;
}

}}

#endif // USERACTIONMANAGER_INL

