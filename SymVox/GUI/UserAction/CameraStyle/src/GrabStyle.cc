/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/GrabStyle.hh"

namespace SV { namespace GUI {

GrabStyle::GrabStyle(const std::string& name, ViewOpenGL* oglWindow, Camera* camera) : VMoveStyle(name, oglWindow, camera),
    fIsCameraOn(false),
    fLockKey(false)
{
}

GrabStyle::~GrabStyle()
{
}

void GrabStyle::MoveCamera(GUI::Input& input, Camera& camera)
{
    // ************************************************
    // Choose if we are in or out of the camera mode
    // ************************************************

    if(input.GetKey(Qt::Key_F) )
        fLockKey = true;
    else
    {
        // If lockKey is true and F is released it means we just released the F key.
        // Therefore, we should enter or quit the camera mode
        if(fLockKey)
        {
            fIsCameraOn = !fIsCameraOn;

            if(fIsCameraOn)
            {

                fpOglWindow->setKeyboardGrabEnabled(true);
                fpOglWindow->setMouseGrabEnabled(true);

                QCursor cursor = fpOglWindow->cursor();
                cursor.setShape(Qt::BlankCursor);
                cursor.setPos(0,0,0);
                fpOglWindow->setCursor(cursor);
            }
            else
            {
                fpOglWindow->setCursor(Qt::ArrowCursor);
                fpOglWindow->setKeyboardGrabEnabled(false);
                fpOglWindow->setMouseGrabEnabled(false);
            }

            fLockKey = false;
        }
    }

    // ************************************************
    // Camera movements
    // ************************************************

    if(fIsCameraOn)
    {
        float speed = camera.GetSpeed();

        bool isOrientationChanged (false), isPosChanged (false);

        // Orientation management
        if(input.MoveMouse() )
        {
            bool movedCursor = KeepMouseInWidget(input);

            if(movedCursor)
            {
                int x = fpOglWindow->cursor().pos().x();
                int y = fpOglWindow->cursor().pos().y();
                fpOglWindow->AdjustMouseMove(x, y, x, y );
            }

            camera.Orientate(input.GetDeltaX(), input.GetDeltaY() );
            isOrientationChanged = true;
        }

        // Move forward
        if(input.GetKey(Qt::Key_Forward) || input.GetKey(Qt::Key_Z))
        {
            camera.MoveForward(speed);
            isPosChanged = true;
        }

        // Move backward
        if(input.GetKey(Qt::Key_BackForward) || input.GetKey(Qt::Key_S))
        {
            camera.MoveForward(-speed);
            isPosChanged = true;
        }

        // Move up
        if(input.GetKey(Qt::Key_Shift) )
        {
            camera.MoveUp(speed);
            isPosChanged = true;
        }

        // Move down
        if(input.GetKey(Qt::Key_Control) )
        {
            camera.MoveUp(-speed);
            isPosChanged = true;
        }

        // Move left
        if(input.GetKey(Qt::Key_Left) || input.GetKey(Qt::Key_Q))
        {
            camera.MoveLeft(speed);
            isPosChanged = true;
        }

        // Move right
        if(input.GetKey(Qt::Key_Right) || input.GetKey(Qt::Key_D))
        {
            camera.MoveLeft(-speed);
            isPosChanged = true;
        }
    }
}

void GrabStyle::SetDefaultCursor()
{
    QCursor cursor = fpOglWindow->cursor();
    cursor.setShape(Qt::CursorShape::ArrowCursor);
    fpOglWindow->setCursor(cursor);
}

bool GrabStyle::KeepMouseInWidget(GUI::Input& input)
{
    bool moveCursor (false);

    // Get the screen X and Y limits
    int limX (fpOglWindow->width()-1);
    int limY (fpOglWindow->height()-1);

    QCursor cursor = fpOglWindow->cursor();

    QPoint p = fpOglWindow->mapFromGlobal(cursor.pos() );

    int mouseRelX = p.x();
    int mouseRelY = p.y();

    if(mouseRelX >= limX
            || mouseRelX <= 0
            || mouseRelY >= limY
            || mouseRelY <= 0)
    {
        moveCursor = true;

        cursor.setPos(p);

        // Deal with the X limit case
        //
        if(mouseRelX >= limX)
        {
            cursor.setPos(1, mouseRelY);
            mouseRelX=1;
        }
        else if(mouseRelX <= 0)
        {
            cursor.setPos(limX-1, mouseRelY);
            mouseRelX=limX-1;
        }

        // Deal with the Y limit case
        //
        if(mouseRelY >= limY)
        {
            cursor.setPos(mouseRelX, 1);
            mouseRelY=1;
        }
        else if(mouseRelY <= 0)
        {
            cursor.setPos(mouseRelX, limY-1);
            mouseRelY=limY-1;
        }

        p = fpOglWindow->mapToGlobal(cursor.pos() );
        cursor.setPos(p);

        fpOglWindow->setCursor(cursor);
    }

    return moveCursor;
}

}}

