/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/MouseStyle.hh"

namespace SV { namespace GUI {

MouseStyle::MouseStyle(const std::string& name, ViewOpenGL* oglWindow, Camera* camera) : VMoveStyle(name, oglWindow, camera),
    fIsClikedLeft(false),
    fIsClikedMiddle(false),
    fIsClikedRight(false)
{
}

MouseStyle::~MouseStyle()
{
}

void MouseStyle::MoveCamera(GUI::Input& input, Camera& camera)
{
    if(input.GetMouseKey(Qt::LeftButton).fBool)
        fIsClikedLeft = true;
    else
        fIsClikedLeft = false;

    if(input.GetMouseKey(Qt::RightButton).fBool)
        fIsClikedRight = true;
    else
        fIsClikedRight = false;

    if(input.GetMouseKey(Qt::MiddleButton).fBool)
        fIsClikedMiddle = true;
    else
        fIsClikedMiddle = false;

    if(fIsClikedLeft || fIsClikedMiddle || fIsClikedRight)
    {
        if(fpOglWindow->cursor().shape() != Qt::CursorShape::ClosedHandCursor)
        {
            QCursor cursor = fpOglWindow->cursor();
            cursor.setShape(Qt::CursorShape::ClosedHandCursor);
            fpOglWindow->setCursor(cursor);
        }

        float speed = camera.GetSpeed();
        float sensitivity = camera.GetSentivity();

        bool pos_changed = false, ori_changed = false;

        if(fIsClikedLeft)
        {
            camera.Orientate(-input.GetDeltaX() * 0.2 * sensitivity, -input.GetDeltaY() * 0.2 * sensitivity);
            ori_changed = true;
        }
        else if(fIsClikedRight)
        {
            camera.MoveLeft(input.GetDeltaX() * 0.01 * speed);
            camera.MoveUp(input.GetDeltaY() * 0.01 * speed);

            pos_changed = true;
        }
        else if(fIsClikedMiddle)
        {
            camera.MoveForward(input.GetDeltaY() * 0.01 * speed);

            pos_changed = true;
        }
    }
    else
    {
        if(fpOglWindow->cursor().shape() != Qt::CursorShape::OpenHandCursor)
        {
            QCursor cursor = fpOglWindow->cursor();
            cursor.setShape(Qt::CursorShape::OpenHandCursor);
            fpOglWindow->setCursor(cursor);
        }
    }
}

void MouseStyle::SetDefaultCursor()
{
    QCursor cursor = fpOglWindow->cursor();
    cursor.setShape(Qt::CursorShape::ArrowCursor);
    fpOglWindow->setCursor(cursor);
}

}}
