/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef VMOVESTYLE_HH
#define VMOVESTYLE_HH

#include "SymVox/GUI/Input/include/Input.hh"
#include "SymVox/GUI/Camera/include/Camera.hh"
#include "SymVox/GUI/Windows/include/ViewOpenGL.hh"

#include "SymVox/GUI/UserAction/Management/include/VUserAction.hh"

namespace SV { namespace GUI {

/*!
 * \brief The VMoveStyle class
 * This class is a user action class dedicated to the implementation of camera movements.
 * It is the base class for all style classes and, as such, it is a bridge between the UserAction and CameraStyle (or MoveStyle) concepts.
 *
 * # MoveStyle concept
 *
 * Camera movements are implemented in "style" classes with the following requirements:
 * - A "style" is a camera bahavior that is itself a camera response to a user input.
 * - Each camera style has a MoveCamera method in charge of setting up the camera response to the user input.
 * - The user input is given to the MoveCamera method through an Input class passed as argument.
 * - One "style" concept (or one camera behaviour) per class.
 * - The SetDefaultCursor() method is used to control the shape of the mouse pointer.
 *
 * # UserAction concept
 *
 * In short, the program tries at each frame to perform all registered user actions and starts by asking to the user action class if the conditions required to its use are met.
 * This is done through the call of the Test method that should return a boolean value. If this value is true then the program will call the DoIt method of the UserAction class.
 */
class VMoveStyle : public VUserAction
{
public:
    /*!
     * \brief VMoveStyle
     * \param name
     * \param oglWindow
     * \param camera
     */
    VMoveStyle(const std::string& name, ViewOpenGL* oglWindow, Camera* camera);

    /*!
     * \brief ~VMoveStyle
     */
    virtual ~VMoveStyle();

    /*!
     * \brief SetDefaultCursor
     */
    virtual void SetDefaultCursor() = 0;

    /*!
     * \brief GetName
     * \return
     */
    const std::string& GetName() const  {return fName;}

    /*!
     * \brief SetCamera
     * \param camera
     */
    void SetCamera(Camera* camera) {fpCamera=camera;}
    /*!
     * \brief GetCamera
     * \return
     */
    const Camera* GetCamera() const {return fpCamera;}
    /*!
     * \brief GetCamera
     * \return
     */
    Camera* GetCamera() {return fpCamera;}

    /*!
     * \brief Test
     * \param input
     * \return
     */
    bool Test(Input& input) override final;

protected:
    std::string fName; ///< Name of the MoveStyle
    ViewOpenGL* fpOglWindow; ///< Pointer to the Qt widget that contains the opengl area
    Camera* fpCamera; ///< Pointer to the camera object to be used by the style

    /*!
     * \brief MoveCamera
     * \param input
     * \param camera
     */
    virtual void MoveCamera(GUI::Input& input, Camera& camera) = 0;

    /*!
     * \brief DoIt
     * \param input
     * \return
     */
    bool DoIt(GUI::Input& input) override final;
};

}}

#endif // VMOVESTYLE_HH
