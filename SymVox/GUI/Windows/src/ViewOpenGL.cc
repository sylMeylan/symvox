/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/ViewOpenGL.hh"

namespace SV { namespace GUI {

ViewOpenGL::ViewOpenGL(QWindow* parent) : QWindow(parent), Utils::OpenGL()
{
    fIsOpenGLContextInitialized = false;

    fNearPlane = 1.f;
    fFarPlane = 100000000.f;
    fVerticalAngle = 70.f;
    fEndMainLoop = false;

    fProjection.setToIdentity();
    fModelView.setToIdentity();
    fSavedMVBeforeLookAt.setToIdentity();
    fViewMatrix.setToIdentity();

    fBackgroundColor = QColor(0,0,0,255);
}

ViewOpenGL::~ViewOpenGL()
{

}

void ViewOpenGL::InitialiseContext()
{
    // Initialize the OpenGL context with Qt

    if(!fIsOpenGLContextInitialized)
    {
        QSurfaceFormat format;
        // OpenGL major and minor version
        format.setVersion(4,0);
        // OpenGL profile
        format.setProfile(QSurfaceFormat::CoreProfile);
        //format.setSwapBehavior(QSurfaceFormat::DoubleBuffer); // Auto set
        format.setDepthBufferSize(24);
        format.setSamples(16);

        // Set the surface to be used for rendering
        this->setSurfaceType(OpenGLSurface);
        this->setFormat(format);
        this->create();

        // Create the OpenGL context
        fContext = new QOpenGLContext(this);
        fContext->setFormat(format);
        fContext->create();

        // Make the openGL context "current"
        // Without this, function initialization fails
        fContext->makeCurrent(this);

        // OpenGL function initialisation
        // Qt wraps the openGL functions to allow cross-platform portability
        this->initializeOpenGLFunctions();

        // To avoid messing up faces
        glEnable(GL_DEPTH_TEST);

        glFrontFace(GL_CCW);
        glCullFace(GL_BACK);
        // Do not calculate info about hidden faces (faces with normal not oriented toward the camera)
        glEnable(GL_CULL_FACE);

        // Enable the alpha chanel for transparency
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glEnable( GL_BLEND );

        glEnable(GL_PROGRAM_POINT_SIZE);

        // Init is done, we set the variable to aknowledge that
        fIsOpenGLContextInitialized = true;
    }
    else
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "ViewOpenGL::InitialiseContext",
                         "Context has already been initialized");
    }
}

void ViewOpenGL::InitialiseView()
{
    fProjection.setToIdentity();

    // Projection and modelView matrix
    fProjection.perspective(fVerticalAngle, this->width()/this->width(), fNearPlane, fFarPlane);

    fModelView.setToIdentity();
}

void ViewOpenGL::UpdateInput()
{
    // Set the key variables for the openGL mode
    fInput.UpdateEvenements(fKeysPressed, fKeysReleased,
                            fMousePressed, fMouseReleased,
                            fMouseX, fMouseY,
                            fMouseDeltaX, fMouseDeltaY);

    // Reset the variable containers for next iteration
    this->ResetKeyboardAndMouse();
}

void ViewOpenGL::Clean()
{
    glClearColor(fBackgroundColor.redF(), fBackgroundColor.greenF(), fBackgroundColor.blueF(), fBackgroundColor.alphaF() );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void ViewOpenGL::Resize()
{
    glViewport(0, 0, this->width(), this->width() );
}

void ViewOpenGL::SaveViewMat()
{
    fSavedMVBeforeLookAt = fModelView;
}

void ViewOpenGL::LoadSavedViewMat()
{
    fModelView = fSavedMVBeforeLookAt;
}

void ViewOpenGL::Refresh()
{
    if(this->isExposed())
    {
        fContext->makeCurrent(this);
        fContext->swapBuffers(this);
    }
}

void ViewOpenGL::MakeContextCurrent()
{
    fContext->makeCurrent(this);
}

void ViewOpenGL::AdjustMouseMove(int x, int y, int oldX, int oldY)
{
    fMouseX = x;
    fMouseY = y;
    fOldMouseX = oldX;
    fOldMouseY = oldY;
}

void ViewOpenGL::Render()
{
    // Clean the screen
    glClearColor(fBackgroundColor.redF(), fBackgroundColor.greenF(), fBackgroundColor.blueF(), fBackgroundColor.alphaF() );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Adapt the openGL view to the current View size
    glViewport(0, 0, this->width(), this->width() );

    // Save the modelView matrix
    fSavedMVBeforeLookAt = fModelView;

    // Camera placement
    //fpCamera->LookAt(fModelView);

    // Save modelView matrix for object placements
    fViewMatrix = fModelView;

    // To display what is defined within the currently activated model
    //
    // Check if we have a defined model manager
//    if(fpModelManager)
//    {
//        VModelInterface* currentModelInterface = fpModelManager->GetCurrentModel();

//        // Check if we have an activated current model
//        // and if yes then display it
//        if(currentModelInterface)
//            currentModelInterface->Display(fProjection, fModelView, fViewMatrix, fLightPosition);
//    }

    // Reset the matrix
    fModelView = fSavedMVBeforeLookAt;

    // Refresh the window if it is exposed
    if(this->isExposed())
    {
        fContext->makeCurrent(this);
        fContext->swapBuffers(this);
    }
}

void ViewOpenGL::keyPressEvent(QKeyEvent *ev)
{
        if(!ev->isAutoRepeat())
            fKeysPressed.push_back(ev->key());

        QWindow::keyPressEvent(ev);
}

void ViewOpenGL::keyReleaseEvent(QKeyEvent *ev)
{
        if(!ev->isAutoRepeat())
            fKeysReleased.push_back(ev->key());

        QWindow::keyReleaseEvent(ev);
}

void ViewOpenGL::mousePressEvent(QMouseEvent *ev)
{
        fMousePressed.push_back(MouseClick(ev->button(), ev->pos().x(), ev->pos().y() ) );

        QWindow::mousePressEvent(ev);
}

void ViewOpenGL::mouseReleaseEvent(QMouseEvent *ev)
{
        fMouseReleased.push_back(MouseClick(ev->button(), ev->pos().x(), ev->pos().y() ));

        QWindow::mouseReleaseEvent(ev);
}

void ViewOpenGL::mouseMoveEvent(QMouseEvent *ev)
{
        // Retrieve the mouse position
        //
        fMouseX = ev->globalPos().x();
        fMouseY = ev->globalPos().y();

        // Calculate the delta variables to be passed to the Camera
        //
        fMouseDeltaX = fMouseX - fOldMouseX;
        fMouseDeltaY = fMouseY - fOldMouseY;
}

void ViewOpenGL::ResetKeyboardAndMouse()
{
        // Clean the keyboard and mouse button vectors
        //
        fKeysPressed.clear();
        fKeysReleased.clear();
        fMousePressed.clear();
        fMouseReleased.clear();

        // If the mouse is static with regards to the last update then
        // set the delta variables to 0
        //
        if(fMouseX==fOldMouseX
                && fMouseY==fOldMouseY)
        {
            fMouseDeltaX = 0;
            fMouseDeltaY = 0;
        }

        // Update the old position variables
        //
        fOldMouseX = fMouseX;
        fOldMouseY = fMouseY;
}

}}
