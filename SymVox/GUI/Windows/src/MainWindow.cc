/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#include "../include/MainWindow.hh"
#include "ui_MainWindow.h"

namespace SV { namespace GUI {

MainWindow::MainWindow(QWidget *parent) : VMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowState(Qt::WindowMaximized);
}

MainWindow::~MainWindow()
{
    delete ui;

    if(fpViewOpenGL != nullptr)
        delete fpViewOpenGL;
}

void MainWindow::Initialise()
{
    if(!fIsInit)
    {
        if(fpViewOpenGL == nullptr)
            fpViewOpenGL = new ViewOpenGL();

        // Add a grid layout to the widget placed in the mainWindow
        QGridLayout* gridLayout = new QGridLayout();
        ui->view_widget->setLayout(gridLayout);
        // No margins to get full screen display
        gridLayout->setContentsMargins(0,0,0,0);

        // Create the window contained widget linked to the openGL window
        QWidget* openGLContainer = QWidget::createWindowContainer(fpViewOpenGL);
        openGLContainer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        // Add the window container widget to the grid layout
        gridLayout->addWidget(openGLContainer);

        fIsInit = true;
    }
    else
    {
        Utils::Exception(Utils::ExceptionType::Fatal, "MainWindow::Initialise",
                         "Already initialised");
    }
}

}}
