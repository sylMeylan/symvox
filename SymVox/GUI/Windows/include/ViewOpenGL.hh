/* ----------------------------------------------------------------------------------
Copyright (c) 2019, SymAlgo Technologies
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by SymAlgo Technologies.
4. Neither the name of SymAlgo Technologies nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY SymAlgo Technologies ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL SymAlgo Technologies BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
----------------------------------------------------------------------------------- */

#ifndef GUI_VIEWOPENGL_HH
#define GUI_VIEWOPENGL_HH

#include <QWindow>
#include <QMatrix4x4>
#include <iostream>
#include <QKeyEvent>
#include <QMouseEvent>

#include "SymVox/Utils/include/OpenGL.hh"
#include "SymVox/Utils/include/Exception.hh"
#include "SymVox/GUI/Input/include/Input.hh"

namespace SV { namespace GUI {

/*!
 * \brief The ViewOpenGL class
 * Deal with the OpenGL initialisation and the runtime render calls
 */
class ViewOpenGL : public QWindow,  public Utils::OpenGL
{
public:
    ViewOpenGL(QWindow* parent = nullptr);
    ~ViewOpenGL() override;

    void InitialiseContext();
    void InitialiseView();
    void UpdateInput();
    void Render();

    void SetEndMainLoop(bool b) {fEndMainLoop=b;}
    bool GetEndMainLoop() {return fEndMainLoop;}

    void Clean();
    void Resize();
    void SaveViewMat();
    void LoadSavedViewMat();
    void Refresh();
    void MakeContextCurrent();

    QMatrix4x4& GetProjection() {return fProjection;}
    QMatrix4x4& GetView() {return fModelView;}

    GUI::Input& GetInput() {return fInput;}
    const GUI::Input& GetInput() const {return fInput;}

    void SetNearPlane(float p) {fNearPlane=p;}
    float GetNearPlane() const {return fNearPlane;}

    void SetFarPlane(float p) {fFarPlane=p;}
    float GetFarPlane() const {return fFarPlane;}

    void SetBackgroundColor(const QColor& color) {fBackgroundColor=color;}

    void AdjustMouseMove(int x, int y, int oldX, int oldY);

private:

    QOpenGLContext* fContext;

    bool fEndMainLoop;
    bool fIsOpenGLContextInitialized;

    float fNearPlane;
    float fFarPlane;
    float fVerticalAngle;

    // View matrix related data
    QMatrix4x4 fProjection;
    QMatrix4x4 fModelView;
    QMatrix4x4 fSavedMVBeforeLookAt;
    QMatrix4x4 fViewMatrix;

    QColor fBackgroundColor;

    // Input management

    GUI::Input fInput; ///< Class used to centralize all the input user actions (mouse and keyboard)

    std::vector<int> fKeysPressed; ///< List all the pressed keys
    std::vector<int> fKeysReleased; ///< List all the released keys
    std::vector<MouseClick> fMousePressed; ///< List the pressed mouse buttons
    std::vector<MouseClick> fMouseReleased; ///< List the released mouse buttons

    int fMouseX; ///< X coordinate of the mouse cursor
    int fMouseY; ///< Y coordinate of the mouse cursor
    int fMouseDeltaX; ///< Projection on the X axis of the vector "previous_position"/"current_position"
    int fMouseDeltaY; ///< Projection on the Y axis of the vector "previous_position"/"current_position"
    int fOldMouseX; ///< X coordinate of the previous mouse cursor position
    int fOldMouseY; ///< Y coordinate of the previous mouse cursor position

    void keyPressEvent(QKeyEvent *ev) override final; ///< Take into account keyboard press events
    void keyReleaseEvent(QKeyEvent *ev) override final; ///< Take into account keyboard release events
    void mousePressEvent(QMouseEvent *ev) override final; ///< Take into account mouse press events
    void mouseReleaseEvent(QMouseEvent *ev) override final; ///< Take into account mouse release events
    void mouseMoveEvent(QMouseEvent *ev) override final; ///< Take into account mouse movements
    void ResetKeyboardAndMouse(); ///< Reset the variables that register keyboard and mouse input
};

}}

#endif // GUI_VIEWOPENGL_HH
