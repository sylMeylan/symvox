#include "../include/Utils.hh"

namespace SV { namespace Utils {

QVector3D TanToSphereSurfaceVect(QVector3D normal)
{
    // Normalize just in case it was not done before
    normal.normalize();

    // Perturbation
    QVector3D p = normal;

    int count = 0;

    // Select a perturbate vector but be sure it is not colinear
    while( QVector3D::crossProduct(p, normal).length() < 0.01 && count<1000)
    {
        double x = ( double(std::rand() ) / RAND_MAX ) * 2;
        double y = ( double(std::rand() ) / RAND_MAX ) * 2;
        double z = ( double(std::rand() ) / RAND_MAX ) * 2;

        p = normal + QVector3D(x,y,z);
    }

    if(count >= 999)
    {
        std::cerr<<"********* Fatal Error ***********"<<std::endl;
        std::cerr<<"TanToSphereSurfaceVect: count to high, very long while loop..."<<std::endl;
        std::cerr<<"*******************************"<<std::endl;
    }

    float scalProd = QVector3D::dotProduct(normal, p);

    QVector3D tanVect = p - scalProd * normal;

    return tanVect.normalized();
}

QVector3D RandomPosOnSphereSurface(float radius)
{
    QVector3D dir;

    double testVal = 2;

    while(testVal >= 1)
    {
        double x1 = ( double(std::rand() ) / RAND_MAX ) * 2 - 1;
        double x2 = ( double(std::rand() ) / RAND_MAX ) * 2 - 1;

        testVal = std::pow(x1, 2) + std::pow(x2, 2);

        double x = 2*x1*std::sqrt(1 - x1*x1 - x2*x2 );
        double y = 2*x2*std::sqrt(1 - x1*x1 - x2*x2);
        double z = 1 - 2 * (x1*x1 + x2*x2);

        dir.setX(x);
        dir.setY(y);
        dir.setZ(z);

        dir *= radius;
    }

    return dir;
}

QVector3D ExtractPosFromMatrix(const QMatrix4x4& model)
{
    return QVector3D(model(0,3), model(1,3), model(2,3) );
}

void ClearLayout(QLayout* layout)
{
    // Loop on all the elements within the layout
    for(int i=0, ie=layout->count(); i<ie; ++i)
    {
        // Get the element of the layout one by one
        QLayoutItem *child = layout->takeAt(0);

        // If element is a layout
        if(child->layout())
        {
            // Recursion here
            ClearLayout(child->layout());
        }
        // If the element is a widget
        else if(child->widget())
        {
            delete child->widget();
        }

        // Remove the element from the layout
        layout->removeItem(child);

        // Delete the element
        delete child;
    }
}

std::string dumpMatrix(const QMatrix4x4 &mat)
{
    std::ostringstream oss;

    for(int line=0; line<4; ++line)
    {
        for(int column=0; column<4; ++column)
        {
            oss<<mat(line, column)<<" ";
        }
       oss<<std::endl;
    }

    return oss.str();
}

}}
