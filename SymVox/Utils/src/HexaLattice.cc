#include "../include/HexaLattice.hh"

namespace SV { namespace Utils {

HexaLattice::HexaLattice(Run::VSceneNode* refNode, float hexaSize, const QVector3D& xAxis, const QVector3D& yAxis, const QVector3D& zAxis)
{
    fpReferenceNode = refNode;

    fHexagoneSize = hexaSize;

    fHexaAxisMat.setColumn(0, QVector4D(xAxis, 0));
    fHexaAxisMat.setColumn(1, QVector4D(yAxis, 0));
    fHexaAxisMat.setColumn(2, QVector4D(zAxis, 0));
    fHexaAxisMat.setColumn(3, QVector4D(0,0,0, 1));
}

HexaLattice::~HexaLattice()
{
}

bool HexaLattice::AddHexa(int x, int y, int z)
{
    if(IsHexaPosFree(x, y, z) )
    {
        fHexaPlData[x][y][z] = true;

        return true;
    }

    else return false;
}

bool HexaLattice::AddHexa(const QVector3D& pos)
{
    QVector3D posInLatticeFrame = ConvertFromOrthoToHexa(pos);

    // Get the hexagone indexes
    int x, y, z;
    GetIndexes(posInLatticeFrame, x, y, z);

    return AddHexa(x,y,z);
}

bool HexaLattice::IsHexaPosFree(int x, int y, int z)
{
    bool output (false);

    // Check if there is another voxel at the potential futur position
    if((fHexaPlData)[x][y][z] == false)
    {
        // There is none

        output = true;
    }

    return output;
}

bool HexaLattice::IsHexaPosFree(const QVector3D& pos)
{
    // Convert the pos vector (ortho coordinates) to the hexa lattice coord system.
    QVector3D posL = ConvertFromOrthoToHexa(pos);

    // Get the hexagone indexes
    int x, y, z;
    GetIndexes(posL, x, y, z);

    // Test the indexes
    return IsHexaPosFree(x,y,z);
}

bool HexaLattice::RemoveHexa(int x, int y, int z)
{
    if(!IsHexaPosFree(x, y, z) )
    {
        fHexaPlData[x][y][z] = false;

        // Position is free now
        return true;
    }

    // There is no need to free anything
    else
        return false;
}

bool HexaLattice::RemoveHexa(const QVector3D& pos)
{
    // Convert the pos vector (ortho coordinates) to the hexa lattice coord system.
    QVector3D posL = ConvertFromOrthoToHexa(pos);

    // Get the hexagone indexes
    int x, y, z;
    GetIndexes(posL, x, y, z);

    // Test the indexes
    return RemoveHexa(x,y,z);
}

void HexaLattice::GetIndexes(const QVector3D& posInLatticeFrame, int& x, int &y, int& z)
{
    // Get the hexagone indexes
    x = std::round(posInLatticeFrame.x()/fHexagoneSize);
    y = std::round(posInLatticeFrame.y()/fHexagoneSize);
    z = std::round(posInLatticeFrame.z()/fHexagoneSize);
}

QVector3D HexaLattice::ConvertFromOrthoToHexa(const QVector3D& pos)
{
    // Convert the pos vector (ortho coordinates) to the hexa lattice coord system.
    QVector4D posL4D = fHexaAxisMat.inverted() * QVector4D(pos, 1);

    // Remove the 4th dim
    QVector3D posL(posL4D.x(), posL4D.y(), posL4D.z() );

    return posL;
}

QVector3D HexaLattice::ConvertFromHexaToOrtho(const QVector3D& pos)
{
    // Convert the pos vector (ortho coordinates) to the hexa lattice coord system.
    QVector4D posL4D = fHexaAxisMat * QVector4D(pos, 1);

    // Remove the 4th dim
    QVector3D posL(posL4D.x(), posL4D.y(), posL4D.z() );

    return posL;
}

}}
