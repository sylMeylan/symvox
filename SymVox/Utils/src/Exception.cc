#include "../include/Exception.hh"

namespace SV { namespace Utils {

Exception::Exception(ExceptionType type, const std::string& location, const std::string& msg)
{
    if(type == ExceptionType::Warning)
    {
        std::cerr<<"******* Warning *******"<<std::endl;
        std::cerr<<location<<": "<<msg<<std::endl;
        std::cerr<<"***********************"<<std::endl;
    }

    else if (type == ExceptionType::Fatal)
    {
        std::cerr<<"******* Fatal Error *******"<<std::endl;
        std::cerr<<location<<": "<<msg<<std::endl;
        std::cerr<<"**************************"<<std::endl;
        std::terminate();
    }

    else
    {
        std::cerr<<"******* Unknown Error *******"<<std::endl;
        std::cerr<<location<<": "<<msg<<std::endl;
        std::cerr<<"**************************"<<std::endl;
        std::terminate();
    }
}

Exception::~Exception()
{

}



}}
