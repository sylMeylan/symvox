#include "../include/CubicLattice.hh"

namespace SV { namespace Utils {

CubicLattice::CubicLattice(Run::VSceneNode* refNode)
{
    fpReferenceNode = refNode;
}

CubicLattice::~CubicLattice()
{

}

bool CubicLattice::AddCube(int x, int y, int z)
{
    if(IsCubePosFree(x,y,z) )
    {
        fVoxelPlData[x][y][z] = true;

        return true;
    }

    else return false;
}

bool CubicLattice::AddCube(float x, float y, float z)
{
    return AddCube(QVector3D(x,y,z) );
}

bool CubicLattice::AddCube(const QVector3D& pos)
{
    int x,y,z;
    GetIndexes(pos, x,y,z);

    return AddCube(x,y,z);
}

bool CubicLattice::IsCubePosFree(int x, int y, int z)
{
    bool output (false);

    // Check if there is another voxel at the potential futur position
    if(fVoxelPlData[x][y][z] == false)
    {
        // There is none

        output = true;
    }

    return output;
}

bool CubicLattice::IsCubePosFree(float x, float y, float z)
{
    return IsCubePosFree(QVector3D(x,y,z) );
}

bool CubicLattice::IsCubePosFree(const QVector3D& pos)
{
    int x,y,z;
    GetIndexes(pos, x,y,z);

    return IsCubePosFree(x,y,z);
}

bool CubicLattice::RemoveCube(int x, int y, int z)
{
    if(!IsCubePosFree(x, y, z) )
    {
        fVoxelPlData[x][y][z] = false;

        // Position is free now
        return true;
    }

    // There is no need to free anything
    else return false;
}

bool CubicLattice::RemoveCube(const QVector3D& pos)
{
    int x,y,z;
    GetIndexes(pos, x,y,z);

    return RemoveCube(x, y, z);
}

bool CubicLattice::RemoveCube(float x, float y, float z)
{
    return RemoveCube(QVector3D(x,y,z) );
}

void CubicLattice::GetIndexes(const QVector3D& posInLatticeFrame, int& x, int &y, int& z)
{
    // Get the hexagone indexes
    x = std::round(posInLatticeFrame.x()/fCubeSize);
    y = std::round(posInLatticeFrame.y()/fCubeSize);
    z = std::round(posInLatticeFrame.z()/fCubeSize);
}

}}
