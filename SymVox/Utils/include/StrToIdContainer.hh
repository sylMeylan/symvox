#ifndef UTILS_ASSETSCONTAINER_HH
#define UTILS_ASSETSCONTAINER_HH

#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <functional>

#include "SymVox/Utils/include/Exception.hh"

namespace SV { namespace Utils {

template <typename T>
/*!
 * \brief The StrToIdContainer class
 * Implements two synchronized containers : [int, obj] et [string, int].
 * Those containers give to the user the ability to identify an object both with a "string" and an "int".
 */
class StrToIdContainer
{
public:
    StrToIdContainer(); ///< Constructor
    ~StrToIdContainer(); ///< Destructor

    /// Add an object into the container and identify it with a string.
    int Add(T obj, const std::string& name);

    // Remove
    void Remove(int index); ///< Remove the object corresponding to the given index from the container.
    void Remove(const std::string& name); ///< Remove the object corresponding to the given string from the container.
    void RemoveAll(); ///< Remove every object from the container.

    // Total number
    int GetNumOfObj() const {return fObjMap.size();} ///< Get the number of objects in the container.

    // Index getters
    T Get(int index);
    const T Get(int index) const;

    // Name getters
    T Get(const std::string& name);
    const T Get(const std::string& name) const;

    // Is inside
    bool IsInside(const std::string& name);
    bool IsInside(int index);

    const std::string& GetName(int index);

    int GetIndex(const std::string& name);

    void ApplyOnEveryObj(std::function<void(T)> fct);
    void ApplyOnEveryObj(std::function<void(T, int id)> fct);

    // Direct map access, use with caution
    std::map<int, T>& GetObjMap() {return fObjMap;}

    const std::map<std::string, int>& GetIndexMap()const {return fIndexMap;}

private:
    int fFreeId;

    std::map<int, T> fObjMap;
    std::map<std::string, int> fIndexMap;

    int GetFreeId();
};

}}

#include "StrToIdContainer.inl"

#endif // UTILS_ASSETSCONTAINER_HH
