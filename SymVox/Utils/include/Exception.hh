#ifndef UTILS_EXCEPTION_HH
#define UTILS_EXCEPTION_HH

#include <string>
#include <iostream>

namespace SV { namespace Utils {

enum class ExceptionType {Fatal, Warning};

class Exception
{
public:
    Exception(ExceptionType type, const std::string& location, const std::string& msg);
    ~Exception();

};

}}

#endif // UTILS_EXCEPTION_HH
