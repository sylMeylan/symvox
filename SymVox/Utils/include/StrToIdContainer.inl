#include "StrToIdContainer.hh"

namespace SV { namespace Utils {

template <class T>
StrToIdContainer<T>::StrToIdContainer()
{
    fFreeId = 0;
}

template <class T>
StrToIdContainer<T>::~StrToIdContainer()
{
    RemoveAll();
}

template <class T>
int StrToIdContainer<T>::Add(T obj, const std::string& name)
{
    // If the object already exist there is a problem somewhere as we only want to have unique objects.
    if(fIndexMap.find(name) != fIndexMap.end() )
    {
        std::stringstream ss;
        ss << "The object "<<name<<" already exist in the container.";

        Utils::Exception(Utils::ExceptionType::Warning, "StrToIdContainer::Add", ss.str() );

        return GetIndex(name);
    }

    // Get the id to be attributed to the object
    int id = GetFreeId();

    // Add the id to the map
    fIndexMap[name] = id;

    // Add the object address
    fObjMap[id] = obj;

    return id;
}

template <class T>
void StrToIdContainer<T>::Remove(int index)
{
    // Retrive the name of the object
    const std::string& name = GetName(index);

    // Get the map iterator corresponding to the name
    auto it = fIndexMap.find(name);

    // Remove the entry
    fIndexMap.erase(it);

    // Remove the object and its address
    delete fObjMap.at(index);
    fObjMap.erase(index);
}

template <class T>
void StrToIdContainer<T>::Remove(const std::string& name)
{
    // Get the corresponding index
    int index = GetIndex(name);

    // Get the map iterator corresponding to the name
    auto it = fIndexMap.find(name);

    // Remove the entry
    fIndexMap.erase(it);

    // Remove the object and its address
    delete fObjMap.at(index);
    fObjMap.erase(index);
}

template <class T>
void StrToIdContainer<T>::RemoveAll()
{
    // Remove all the objects behind the pointers within the vector
    for(auto& p : fObjMap)
    {
        delete p.second;
    }

    fObjMap.clear();

    fIndexMap.clear();

    fFreeId = 0;
}

template <class T>
T StrToIdContainer<T>::Get(int index)
{
    return fObjMap.at(index);
}

template <class T>
const T StrToIdContainer<T>::Get(int index) const
{
    return fObjMap.at(index);
}

template <class T>
T StrToIdContainer<T>::Get(const std::string& name)
{
    return fObjMap.at(fIndexMap.at(name) );
}

template <class T>
const T StrToIdContainer<T>::Get(const std::string& name) const
{
    return fObjMap.at(fIndexMap.at(name) );
}

template <class T>
bool StrToIdContainer<T>::IsInside(const std::string& name)
{
    if(fIndexMap.find(name) != fIndexMap.end() )
        return true;
    else
        return false;
}

template <class T>
bool StrToIdContainer<T>::IsInside(int index)
{
    if(fObjMap.find(index) != fObjMap.end() )
        return true;
    else
        return false;
}

template <class T>
void StrToIdContainer<T>::ApplyOnEveryObj(std::function<void(T)> fct)
{
    for(auto& i : fObjMap)
    {
        fct(i.second);
    }
}

template <class T>
void StrToIdContainer<T>::ApplyOnEveryObj(std::function<void(T, int id)> fct)
{
    for(auto& i : fObjMap)
    {
        fct(i.second, i.first);
    }
}

template <class T>
const std::string& StrToIdContainer<T>::GetName(int index)
{
    auto it = fIndexMap.begin();
    auto ite = fIndexMap.end();

    int i = -1;

    bool success = false;

    while(it != ite && success == false)
    {
        if(it->second == index)
            success = true;

        if(!success)
            it++;
    }

    return it->first;
}

template <class T>
int StrToIdContainer<T>::GetIndex(const std::string& name)
{
    return fIndexMap.at(name);
}

template <class T>
int StrToIdContainer<T>::GetFreeId()
{
    int freeId = fFreeId;

    fFreeId++;

    return freeId;
}

}}
