#ifndef UTILS_SINGLETON_HH
#define UTILS_SINGLETON_HH

namespace SV { namespace Utils {

/*!
 * \brief The Singleton templated class
 * This class implements a Singleton pattern using template.
 * Singletons used in the project should inherit from this class and be friend with it.
 * The implementation comes from:
 * http://come-david.developpez.com/tutoriels/dps/?page=Singleton
 */
template <class T>
class Singleton
{
public:
    static T* Instance();

    static void DeleteInstance();

protected:
    static T* fS;

    virtual ~Singleton(){}

private:
    T& operator= (const T&) = delete;
};

}}

#include "Singleton.inl"

#endif // UTILS_SINGLETON_HH
