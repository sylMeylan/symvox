#ifndef CUBICLATTICE_HH
#define CUBICLATTICE_HH

#include <map>
#include <QVector3D>
#include <QMatrix4x4>

#include "SymVox/Run/Scene/Graph/include/VSceneNode.hh"

namespace SV { namespace Utils {

class CubicLattice
{
public:
    CubicLattice(Run::VSceneNode* refNode);
    ~CubicLattice();

    void SetCubeSize(float size) {fCubeSize=size;}
    float GetCubeSize() const {return fCubeSize;}

    bool AddCube(int x, int y, int z);
    bool AddCube(float x, float y, float z);
    bool AddCube(const QVector3D& pos);

    bool IsCubePosFree(int x, int y, int z);
    bool IsCubePosFree(float x, float y, float z);
    bool IsCubePosFree(const QVector3D& pos);

    bool RemoveCube(int x, int y, int z);
    bool RemoveCube(float x, float y, float z);
    bool RemoveCube(const QVector3D& pos); 

    const Run::VSceneNode* GetReferenceNode() const {return fpReferenceNode;}
    Run::VSceneNode* GetReferenceNode() {return fpReferenceNode;}
    void SetReferenceNode(Run::VSceneNode* refNode) {fpReferenceNode=refNode;}

protected:

    float fCubeSize;
    std::map<int, std::map<int, std::map<int, bool> > > fVoxelPlData;
    Run::VSceneNode* fpReferenceNode;

    void GetIndexes(const QVector3D& posInLatticeFrame, int& x, int& y, int& z);
};

}}

#endif // CUBICLATTICE_HH
