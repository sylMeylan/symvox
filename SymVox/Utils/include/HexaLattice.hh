#ifndef HEXALATTICE_HH
#define HEXALATTICE_HH

#include <map>
#include "SymVox/Run/Scene/Graph/include/SceneNode.hh"

namespace SV { namespace Utils {

class HexaLattice
{
public:
    HexaLattice(Run::VSceneNode* refNode, float hexaSize, const QVector3D& xAxis, const QVector3D& yAxis, const QVector3D& zAxis);
    ~HexaLattice();

    bool AddHexa(int x, int y, int z);
    bool AddHexa(const QVector3D& pos);

    bool IsHexaPosFree(int x, int y, int z);
    bool IsHexaPosFree(const QVector3D& pos);

    bool RemoveHexa(int x, int y, int z);
    bool RemoveHexa(const QVector3D& pos);

    QVector3D ConvertFromOrthoToHexa(const QVector3D& pos);
    QVector3D ConvertFromHexaToOrtho(const QVector3D& pos);

    const Run::VSceneNode* GetReferenceNode() const {return fpReferenceNode;}
    Run::VSceneNode* GetReferenceNode() {return fpReferenceNode;}

protected:
    float fHexagoneSize;
    std::map<int, std::map<int, std::map<int, bool> > > fHexaPlData;
    Run::VSceneNode* fpReferenceNode;
    QMatrix4x4 fHexaAxisMat;

    void GetIndexes(const QVector3D& posInLatticeFrame, int& x, int& y, int& z);
};

}}

#endif // HEXALATTICE_HH
