#ifndef UTILS_HH
#define UTILS_HH

#include <iostream>
#include <sstream>
#include <QMatrix4x4>
#include <QVector3D>
#include <QLayout>
#include <QWidget>
#include <cmath>

namespace SV { namespace Utils {

    QVector3D TanToSphereSurfaceVect(QVector3D normal);

    QVector3D RandomPosOnSphereSurface(float radius);

    QVector3D ExtractPosFromMatrix(const QMatrix4x4& model);

    std::string dumpMatrix(const QMatrix4x4& mat);

    void ClearLayout(QLayout* layout);
}}

#endif // UTILS_HH
