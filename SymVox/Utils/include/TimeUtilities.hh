#ifndef UTILS_TIMEUTILITIES
#define UTILS_TIMEUTILITIES

#include "Singleton.hh"

namespace SV { namespace Utils {

class TimeUtilities : public Singleton<TimeUtilities>
{
public:
    friend TimeUtilities* Singleton<TimeUtilities>::Instance();
    friend void Singleton<TimeUtilities>::DeleteInstance();

    void SetDeltaTime(unsigned int delta) {fDeltaTime = delta;}
    unsigned int GetDeltaTime() {return fDeltaTime;}

private:
    TimeUtilities(const TimeUtilities&) {}
    TimeUtilities& operator=(const TimeUtilities&) {}

    TimeUtilities() {fDeltaTime=0;}
    ~TimeUtilities() {}

    unsigned int fDeltaTime;
};

}}


#endif // UTILS_TIMEUTILITIES

