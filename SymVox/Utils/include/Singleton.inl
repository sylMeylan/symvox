#include "SymVox/Utils/include/Singleton.hh"

namespace SV { namespace Utils {

template <class T>
T* Singleton<T>::fS = 0;

template <class T>
T* Singleton<T>::Instance()
{
    if(fS==0)
    {
        fS = new T();
    }

    return fS;
}

template <class T>
void Singleton<T>::DeleteInstance()
{
    if(fS) delete fS;
    fS = 0;
}

}}
